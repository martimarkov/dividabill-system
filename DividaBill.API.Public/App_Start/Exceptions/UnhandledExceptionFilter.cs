using System;
using System.Net.Http;
using System.Web;
using System.Web.Http.Filters;
using DividaBill.Models.Errors;
using DividaBill.Services.Emails.Exceptions;
using Elmah;

namespace DividaBill.API.Exceptions
{
    public class UnhandledExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            ErrorLog.GetDefault(HttpContext.Current).Log(new Error(context.Exception));

            try
            {
                var httpMethod = context.Request.Method;
                var exceptionInfo = new ExceptionInfo(context.Exception, typeof (WebApiApplication).Assembly)
                {
                    Form = httpMethod == HttpMethod.Get ? context.Request.RequestUri.Query : context.Request.Content.ReadAsStringAsync().Result,
                    HttpMethod = httpMethod.Method,
                    Url = context.Request.RequestUri.AbsolutePath,
                    UserAgent = context.Request.Headers.UserAgent.ToString(),
                    UserHostAddress = context.Request.Headers.Host
                };
                ExceptionEmail.SendEmail(UnityConfig.GetConfiguredContainer(), exceptionInfo);
            }
            catch (Exception exception)
            {
                ExceptionEmail.SendEmail(UnityConfig.GetConfiguredContainer(), new ExceptionInfo(exception, typeof (WebApiApplication).Assembly));
            }

            base.OnException(context);
        }
    }
}