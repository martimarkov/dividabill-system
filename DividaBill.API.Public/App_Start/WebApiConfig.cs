﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using DividaBill.API.Exceptions;

namespace DividaBill.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            var json = config.Formatters.JsonFormatter;
            json.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.None;
            config.Formatters.Remove(config.Formatters.XmlFormatter);

            config.MapWebParentRoutes(
                "api/{parent}/{controller}/{action}/{id}",
                "api/{parent}/{controller}/{id}");

            // Default Web API routes
            //
            //config.MapHttpAttributeRoutes();
            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "v1/{controller}/{action}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);
            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi2",
            //    routeTemplate: "v1/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);


            //Capture Exceptions using ELMAH
            config.Filters.Add(new UnhandledExceptionFilter());
        }
    }
}
