﻿using DividaBill.Services.Interfaces;
using DividaBill.ViewModels;
using System.Web.Http;
using System.Web.Http.Description;

namespace DividaBill.API.Controllers.Public
{
    [RouteParent("Public")]
    public class HouseController : ApiController
    {
        private readonly IHouseService houseService;
        public HouseController(IHouseService houseService)
        {
            this.houseService = houseService;
        }

        // GET api/House/[id:HouseSearchViewModel]
        [ResponseType(typeof(HouseDetailsViewModel))]

        public IHttpActionResult GetHouse(int id)
        {
            if (ModelState.IsValid)
            {
                var house = houseService.GetHouseDetails(id);
                if (house == null)
                {
                    return NotFound();
                }
                return Ok(house);
            }
            return BadRequest(ModelState);

        }

    }
}