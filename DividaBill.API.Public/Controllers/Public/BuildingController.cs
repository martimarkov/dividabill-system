﻿using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using DividaBill.Models;
using DividaBill.Services.Factories;
using DividaBill.Services.Factories.Impl;
using DividaBill.Services.Interfaces;
using DividaBill.ViewModels;

namespace DividaBill.API.Controllers.Public
{
    [RouteParent("Public")]
    public class BuildingController : ApiController
    {
        private readonly IMappingFactory<Building, BuildingViewModel> _buildingToBuildingViewModelMappingFactory;
        private readonly IPremiseService _premiseService;

        public BuildingController(IPremiseService premiseService, IMappingEngine mappingEngine)
        {
            _premiseService = premiseService;
            _buildingToBuildingViewModelMappingFactory = new AutoMapperFactory<Building, BuildingViewModel>(mappingEngine);
        }

        [Route("{id:double}")]
        [ResponseType(typeof (BuildingViewModel))]
        public IHttpActionResult Get(double id)
        {
            var building = _premiseService.GetBuildingsForPremise(id);
            var buildingViewModel = _buildingToBuildingViewModelMappingFactory.Map(building);
            return Ok(buildingViewModel);
        }
    }
}