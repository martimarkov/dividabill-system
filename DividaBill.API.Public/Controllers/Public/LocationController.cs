﻿using System;
using System.Web.Http;
using System.Web.Http.Description;
using DividaBill.Services.Factories;
using DividaBill.Services.ThirdPartyServices.Interfaces;
using DividaBill.Services.ThirdPartyServices.Models.PostCodeAnyWhere;
using DividaBill.ViewModels.API;

namespace DividaBill.API.Controllers.Public
{
    [RouteParent("Public")]
    public class LocationController : ApiController
    {
        private readonly IDbIpService _dbIpService;
        private readonly IPostCodeAnyWhereService _postCodeAnyWhereService;
        private readonly IThirdPartyModelToApiOutputViewModelMappingFactory<GeoLocation, GeoLocationApiOutputViewModel> _thirdPartyModelToApiOutputViewModelMappingFactory;

        public LocationController(IPostCodeAnyWhereService postCodeAnyWhereService, IDbIpService dbIpService, IThirdPartyModelToApiOutputViewModelMappingFactory<GeoLocation, GeoLocationApiOutputViewModel> thirdPartyModelToApiOutputViewModelMappingFactory)
        {
            _dbIpService = dbIpService;
            _thirdPartyModelToApiOutputViewModelMappingFactory = thirdPartyModelToApiOutputViewModelMappingFactory;
            _postCodeAnyWhereService = postCodeAnyWhereService;
        }

        [HttpGet]
        [ResponseType(typeof (GeoLocationApiOutputViewModel))]
        public IHttpActionResult GetLocation()
        {
            var ip = GetIp();
            var addressInfo = _dbIpService.GetIpAddressInfo(ip);
            if (addressInfo != null)
            {
                var locationByCoords = _postCodeAnyWhereService.GetLocationByCoords(addressInfo.latitude, addressInfo.longitude);
                var viewModel = _thirdPartyModelToApiOutputViewModelMappingFactory.Map(locationByCoords);
                return Ok(viewModel);
            }

            return NotFound();
        }

        [HttpGet]
        [ResponseType(typeof (IpOutputViewModel))]
        public IHttpActionResult GetMyIp()
        {
            var ipOutputViewModel = new IpOutputViewModel
            {
                Ip = GetIp()
            };
            return Ok(ipOutputViewModel);
        }

        private string GetIp()
        {
            var ip = "local";
            if (!Request.IsLocal())
                ip = Request.GetClientIpAddress();

            return ip;
        }
    }
}