﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using DividaBill.Models;
using DividaBill.Services.Factories;
using DividaBill.Services.Factories.Impl;
using DividaBill.Services.Interfaces;
using DividaBill.ViewModels;

namespace DividaBill.API.Controllers.Public
{
    [RouteParent("Public")]
    public class AddressController : ApiController
    {
        private readonly IPremiseService _premiseService;
        private readonly IMappingFactory<Premise, PremiseViewModel> _premiseToPremiseViewModelMappingFactory;

        public AddressController(IPremiseService premiseService, IMappingEngine mappingEngine)
        {
            _premiseService = premiseService;
            _premiseToPremiseViewModelMappingFactory = new AutoMapperFactory<Premise, PremiseViewModel>(mappingEngine);
        }

        [HttpGet, ResponseType(typeof (List<PremiseViewModel>))]
        public IHttpActionResult Get(string id)
        {
            if (ModelState.IsValid)
            {
                id = id.Replace(" ", string.Empty).ToUpper();

                var premises = _premiseService.GetPremisesForPostcode(id).AsEnumerable().Select(x => _premiseToPremiseViewModelMappingFactory.Map(x)).ToList();
                return Ok(premises);
            }
            return BadRequest(ModelState);
        }
    }
}