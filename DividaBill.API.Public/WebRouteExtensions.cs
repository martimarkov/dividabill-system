﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using DividaBill;
using DividaBill.Library;

namespace DividaBill.API
{
    static class WebRouteExtensions
    {
        /// <summary>
        /// Maps classes of type <see cref="ApiController"/> that have a <see cref="RouteParentAttribute"/> attribute
        /// to the routes specified in the <paramref name="templates"/> variable. 
        /// </summary>
        /// <param name="config"></param>
        /// <param name="templates"></param>
        public static void MapWebParentRoutes(this HttpConfiguration config, 
            params string[] templates)
        {
            var controllers = Assembly.GetCallingAssembly()
                .GetTypes()             //all types
                .Select(ty => new       //that have RouteParentAttribute
                {
                    Type = ty,
                    Route = ((RouteParentAttribute)ty.GetCustomAttribute(typeof(RouteParentAttribute)))?.Path,
                })
                .Where(d => !string.IsNullOrEmpty(d.Route));

            foreach(var c in controllers)
            {
                //trim "Controller" from the end of the type name
                var controllerName = c.Type.Name;
                if (controllerName.EndsWith("Controller"))
                    controllerName = controllerName.Remove(controllerName.LastIndexOf("Controller"));

                for(int i = 0; i < templates.Length; i++)
                {
                    //get the parented route
                    var route = templates[i]
                        .Replace("{parent}", c.Route);

                    //add the routes
                    config.Routes.MapHttpRoute(
                        name: "Api.{0}.{1}.{2}".F(c.Route, controllerName, i),
                        routeTemplate: route,
                        defaults: new { id = RouteParameter.Optional, controller = controllerName }
                    );
                }
            }
        }
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class RouteParentAttribute : Attribute
    {
        public readonly string Path;

        public RouteParentAttribute(string path)
        {
            this.Path = path;
        }
    }
}
