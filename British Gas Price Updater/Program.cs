﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DividaBill_Library.Models;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using PostcodeLibrary;


namespace PostcodeUpdater
{
    // To learn more about Microsoft Azure WebJobs, please see http://go.microsoft.com/fwlink/?LinkID=401557
    class Program
    {
        static List<PostCode> notAdded = new List<PostCode>();
        static void Main()
        {
            

            string startupPath = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName, "Dividabill", "Postcodes", "postcodes.csv");

            Console.WriteLine("Location of postcodes.csv (Empty for default): ");
            string newStartupPath = Console.ReadLine();
            if (newStartupPath != "")
            {
                startupPath = newStartupPath;
            }

            Console.WriteLine("Start from: ");
            int limit = Convert.ToInt32(Console.ReadLine());
            
            using (TextReader reader = File.OpenText(startupPath))
            {
                using (var csv = new CsvReader(reader))
                {
                    csv.Configuration.RegisterClassMap<PostCodeCSVMapper>();
                    int counter = 0;
                    int total = 1783119;
                    while (csv.Read())
                    {
                        PostCode postcodeObject = csv.GetRecord<PostCode>();
                        if (postcodeObject.Terminated == "")
                        {
                            Console.WriteLine("Adding {0} ", postcodeObject.Postcode);
                            counter++;
                            if (counter <= limit)
                                continue;
                            Console.Write("Progress: " + String.Format("[{0:0000000}]", counter) + ((counter / total) * 100) + "% ");
                            PostCode item = csv.GetRecord<PostCode>();
                            var stopwatch = Stopwatch.StartNew();
                            try
                            {
                                var client = new HttpClient();
                                //client.Timeout = TimeSpan.FromMinutes(5);
                                var r = client.PostAsJsonAsync<PostCode>("https://www.dividabill.co.uk/odata/PostCodes", postcodeObject).Result;
                                Console.WriteLine(r.Content.ReadAsStringAsync().Result);

                                if ((r.StatusCode != HttpStatusCode.Created) && (r.StatusCode != HttpStatusCode.Conflict))
                                {
                                    Console.WriteLine("Not added. Code: " + r.StatusCode);
                                    notAdded.Add(postcodeObject);
                                }
                                else
                                {
                                    Console.WriteLine("Added {0}. Code: " + r.StatusCode, stopwatch.Elapsed.TotalSeconds);
                                }

                            }
                            catch (Exception exception)
                            {
                                Console.WriteLine("Not added. Reason: Exception");
                                notAdded.Add(postcodeObject);
                                Console.WriteLine(exception);
                            }
                            //SendHttpRequest("https://www.dividabill.co.uk/odata/PostCodes", csv.GetRecord<PostCode>(), stopwatch)
                            //    .ContinueWith(s => Console.WriteLine(s.Result));
                        }
                    }


                }
            }

            using (Stream stream = File.Open("notAdded.csv", FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read))
            using (TextWriter writer = new StreamWriter(stream))
            {
                using (var csv = new CsvWriter(writer))
                {
                    csv.WriteRecords(notAdded);
                }
            }

            Console.ReadKey();

        }

        public static async Task<String> SendHttpRequest(string url, PostCode data, Stopwatch stopwatch)
        {
            Console.WriteLine("Adding {0} ", data.Postcode);
            var client = new HttpClient();
            //client.Timeout = TimeSpan.FromMinutes(5);
            var r = await client.PostAsJsonAsync<PostCode>(url, data);
            //Console.WriteLine(await r.Content.ReadAsStringAsync());

            if ((r.StatusCode != HttpStatusCode.Created) && (r.StatusCode != HttpStatusCode.Conflict))
            {
                notAdded.Add(data);
            }
            return string.Format("Added {0} in {1} sec", data.Postcode, stopwatch.Elapsed.TotalSeconds).ToString();
        }
    }
}
