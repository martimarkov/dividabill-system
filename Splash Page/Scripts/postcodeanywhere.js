﻿function CapturePlus_Interactive_Find_v2_10Begin(Key, SearchTerm, LastId, SearchFor, Country, LanguagePreference, MaxSuggestions, MaxResults) {
    var script = document.createElement("script"),
        head = document.getElementsByTagName("head")[0],
        url = "http://services.postcodeanywhere.co.uk/CapturePlus/Interactive/Find/v2.10/json3.ws?";
    // Build the query string
    url += "&Key=" + encodeURIComponent(Key);
    url += "&SearchTerm=" + encodeURIComponent(SearchTerm);
    url += "&LastId=" + encodeURIComponent(LastId);
    url += "&SearchFor=" + encodeURIComponent(SearchFor);
    url += "&Country=" + encodeURIComponent(Country);
    url += "&LanguagePreference=" + encodeURIComponent(LanguagePreference);
    url += "&MaxSuggestions=" + encodeURIComponent(MaxSuggestions);
    url += "&MaxResults=" + encodeURIComponent(MaxResults);
    url += "&callback=CapturePlus_Interactive_Find_v2_10End";

    script.src = url;

    // Make the request
    script.onload = script.onreadystatechange = function () {
        if (!this.readyState || this.readyState === "loaded" || this.readyState === "complete") {
            script.onload = script.onreadystatechange = null;
            if (head && script.parentNode)
                head.removeChild(script);
        }
    }

    head.insertBefore(script, head.firstChild);
};

//onclick="Javascript: CapturePlus_Interactive_Find_v2_10Begin('GW89-NW67-ZH94-PG48', document.getElementById('PostCode').value, '','Everything','GBR', 'EN', 7, 100); return false;"

function CapturePlus_Interactive_Find_v2_10End(response) {
    // Test for an error
    if (response.Items.length == 1 && typeof (response.Items[0].Error) != "undefined") {
        // Show the error message
        alert(response.Items[0].Description);
    }
    else {
        // Check if there were any items found
        if (response.Items.length == 0)
            alert("Sorry, there were no results");
        else {
            // PUT YOUR CODE HERE
            //FYI: The output is an array of key value pairs (e.g. response.Items[0].Id), the keys being:
            //Id
            //Text
            //Highlight
            //Cursor
            //Description
            //Next
            var postcodes = [];

            for (var i = 0; i <= response.Items.length - 1; i++) {
                postcodes.push({ value: response.Items[i].Text, data: response.Items[i].Id });            
            }

            $('#quote-details').autocomplete({
                lookup: postcodes,
                onSelect: function (suggestion) {
                    alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
            }
            });

            $(".autocomplete-suggestions").show();
        }
    }
}

function PostcodeAnywhere_Interactive_Find_v1_10Begin2(Key, SearchTerm, PreferredLanguage, Filter, UserName) {
    var script = document.createElement("script"),
    head = document.getElementsByTagName("head")[0],
    url = "https://services.postcodeanywhere.co.uk/PostcodeAnywhere/Interactive/Find/v1.10/json2.ws?";

    // Build the query string
    url += "&Key=" + encodeURIComponent(Key);
    url += "&SearchTerm=" + encodeURIComponent(SearchTerm);
    url += "&PreferredLanguage=" + encodeURIComponent(PreferredLanguage);
    url += "&Filter=" + encodeURIComponent(Filter);
    url += "&UserName=" + encodeURIComponent(UserName);
    url += "&CallbackFunction=PostcodeAnywhere_Interactive_Find_v1_10End2";

    script.src = url;

    // Make the request
    script.onload = script.onreadystatechange = function () {
        if (!this.readyState || this.readyState === "loaded" || this.readyState === "complete") {
            script.onload = script.onreadystatechange = null;
            if (head && script.parentNode)
                head.removeChild(script);
        }
    }

    head.insertBefore(script, head.firstChild);
}

function PostcodeAnywhere_Interactive_Find_v1_10End2(response) {
    // Test for an error
    if (response.length == 1 && typeof (response[0].Error) != "undefined") {
        // Show the error message
        alert(response[0].Description);
    }
    else {
        // Check if there were any items found
        if (response.length == 0)
            alert("Sorry, there were no results");
        else {

            $('#buildingOT').parent().show();
            //document.getElementById("buildingOT").style.display = '';
            document.getElementById("buildingOT").innerHTML = ""
            for (var i = 0; i <= response.length - 1; i++) {
                var opt = document.createElement("option");

                document.getElementById("buildingOT").options.add(opt);
                opt.text = response[i].StreetAddress + ", " + response[i].Place;
                opt.value = response[i].Id + 1;
            }
          
            $('.selectpicker').selectpicker('refresh');

        }
    }
}


function PostcodeAnywhere_Interactive_RetrieveById_v1_30Begin2(Key, Id, PreferredLanguage, UserName) {
    var script = document.createElement("script"),
    head = document.getElementsByTagName("head")[0],
    url = "https://services.postcodeanywhere.co.uk/PostcodeAnywhere/Interactive/RetrieveById/v1.30/json2.ws?";

    // Build the query string
    url += "&Key=" + encodeURIComponent(Key);
    url += "&Id=" + encodeURIComponent(Id);
    url += "&PreferredLanguage=" + encodeURIComponent(PreferredLanguage);
    url += "&UserName=" + encodeURIComponent(UserName);
    url += "&CallbackFunction=PostcodeAnywhere_Interactive_RetrieveById_v1_30End2";

    script.src = url;

    // Make the request
    script.onload = script.onreadystatechange = function () {
        if (!this.readyState || this.readyState === "loaded" || this.readyState === "complete") {
            script.onload = script.onreadystatechange = null;
            if (head && script.parentNode)
                head.removeChild(script);
        }
    }

    head.insertBefore(script, head.firstChild);
}

function PostcodeAnywhere_Interactive_RetrieveById_v1_30End2(response) {
    // Test for an error
    if (response.length == 1 && typeof (response[0].Error) != "undefined") {
        // Show the error message
        alert(response[0].Description);
    }
    else {
        // Check if there were any items found
        if (response.length == 0)
            alert("Sorry, there were no results");
        else {

            document.getElementById("AddressOT1").value = response[0].Line1;
            document.getElementById("AddressOT2").value = response[0].Line2;
            //document.getElementById("AddressOT3").value = response[0].Line3;
            document.getElementById("CityOT").value = response[0].PostTown;
            document.getElementById("CountyOT").value = response[0].County;
            document.getElementById("PostCodeOT").value = response[0].Postcode;

        }
    }
}




//Bank check

function BankAccountValidation_Interactive_Validate_v2_00(Key, AccountNumber, SortCode, valid) {
    $.ajax({
        url: "http://services.postcodeanywhere.co.uk/BankAccountValidation/Interactive/Validate/v2.00/json3.ws?callback=?",
        dataType: 'json',
        async: false,
        data: {
            Key: Key,
            AccountNumber: AccountNumber,
            SortCode: SortCode
        },
        success: function (data) {

            // Test for an error
            if (data.Items.length == 1 && typeof (data.Items[0].Error) != "undefined") {
                // Show the error message
                alert(data.Items[0].Description);
            }
            else {
                // Check if there were any items found
                if (data.Items.length == 0)
                    alert("Sorry, there were no results");
                else {
                    valid = data.Items[0].IsCorrect;
                    console.log("From code: " + valid);
                    return valid;
                }
            }
        }
    });
}
