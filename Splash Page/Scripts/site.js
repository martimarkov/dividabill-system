/* ///////////////////
COOKIES
//////////////////// */

/* Set cookies */
function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
	var expires = "expires=" + d.toGMTString();
	document.cookie = cname + "=" + cvalue + "; " + expires;
}

/* Get cookies */
function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i].trim();
		if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
	}
	return "";
}

// cookie popup hide
function hideCookie() {
	document.getElementById('cookieLaw').style.display = 'none';
	setCookie('law', 'closed', '365');
}

//Calculates how much the sidebar to be scrolled
$(window).scroll(function () {

	// distance from top of footer to top of document
	footertotop = ($('.footer').position().top);
	// distance user has scrolled from top, adjusted to take in height of sidebar (570 pixels inc. padding)
	scrolltop = $(document).scrollTop() + 570;
	// difference between the two
	difference = scrolltop - footertotop;

	// if user has scrolled further than footer,
	// pull sidebar up using a negative margin

	if (scrolltop > footertotop) {
		$('.sidebars').css('margin-top', 0 - difference);
	}
	else {
		$('.sidebars').css('margin-top', 0);
	}
});

/*(function (w, d, s, l, i) {
	w[l] = w[l] || []; w[l].push({
		'gtm.start':
		new Date().getTime(), event: 'gtm.js'
	}); var f = d.getElementsByTagName(s)[0],
	j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
	'//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
})(window, document, 'script', 'dataLayer', 'GTM-5T86QP');


(function (i, s, o, g, r, a, m) {
	i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
		(i[r].q = i[r].q || []).push(arguments)
	}, i[r].l = 1 * new Date(); a = s.createElement(o),
	m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

ga('create', 'UA-49161645-1', 'auto');
ga('send', 'pageview');*/


$("#get_geo").click(
function getLocation() {

	if (navigator.geolocation) {
		$("#marker")
			.removeClass("fa-map-marker")
			.addClass("fa-spin fa-spinner");
		navigator.geolocation.getCurrentPosition(getPosition);
	} else {

		$("#quote_postcode").val("Geolocation is not supported by this browser.");
	}
});

function getPosition(position) {
	var coords = position.coords.latitude + "," + position.coords.longitude;

	$.getJSON(window.env.getPublicApiUrl('Location?id=' + coords)).done(
		function (result) {
			console.log(result);
			// Check if there were any items found

			if (result.length == 0) {
				alert("Sorry, there were no results");
			}
			else {
				$("#quote_postcode").val(result);
			}
			$("#marker")
				.removeClass("fa-spin fa-spinner")
				.addClass("fa-map-marker");
		}
	)
};