﻿using DividaBill.Models;
using DividaBill.Models.Sheets;
using DividaBill.Services;
using DividaBill.Services.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DividaBill.ScriptRunner
{
    class printallpeople : Script
    {

        public printallpeople()
        {
            //RequiresWriteAccess = true;
        }

        protected override void doIt()
        {
            var hs = unitOfWork.ActiveHouses
                .ToArray();

            var max_tenants = unitOfWork.ActiveHouses
                .Max(h => h.Housemates);

            var lines = new List<string>();

            //header
            var csvHeader = "House Id,City,Postcode,StartDate,EndDate,Registered Tenants,Total Tenants," 
                + Enumerable.Range(1, max_tenants)
                    .Select(i => "Tenant Name {0},Tenant Phone {0},Tenant Email {0}".F(i))
                    .Aggregate((a, b) => a + "," + b); ;
            lines.Add(csvHeader);

            //rows
            Console.CursorLeft = 0; Console.Write("{0}/{1}".F(0, hs.Length));
            foreach (var i in Enumerable.Range(0, hs.Length))
            {
                var h = hs[i];
                var houseData = new[]
                {
                    h.ID.ToString(),
                    h.Address.City.ToString(),
                    h.Address.Postcode.ToString(),
                    h.StartDate.Date.ToShortDateString(),
                    h.EndDate.Date.ToShortDateString(),
                    h.Tenants.Count.ToString(),
                    h.Housemates.ToString(),
                };
                var tenantData = Enumerable.Range(0, h.Tenants.Count)
                    .SelectMany(j => new[]
                    {
                        h.Tenants[j]?.FullName,
                        h.Tenants[j]?.PhoneNumber,
                        h.Tenants[j]?.Email,
                    });

                var fullRowData = houseData.Concat(tenantData)
                    .Select(v => '"' + v + '"')
                    .Aggregate((a, b) => a + ',' + b);

                lines.Add(fullRowData);

                Console.CursorLeft = 0; Console.Write("{0}/{1}".F(i, hs.Length));
            }

            System.IO.File.WriteAllLines("report.csv", lines);
        }
    }
}
