﻿using DividaBill.Models;
using DividaBill.Models.Sheets;
using DividaBill.Services;
using DividaBill.Services.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DividaBill.ScriptRunner
{
    class someshittyscriptlmao : Script
    {

        public someshittyscriptlmao()
        {
            //RequiresWriteAccess = true;
        }

        protected override void doIt()
        {
            var hs = unitOfWork.ActiveHouses
                .Where(h => h.Address.Postcode.StartsWith("BN")
                    || h.Address.Postcode.StartsWith("PO")
                    || h.Address.Postcode.StartsWith("SO")
                    || h.Address.Postcode.StartsWith("TN")
                    || h.Address.Postcode.StartsWith("CT"))
                .Where(h => h.Contracts.Any(c => c.ServiceNew_ID == ServiceType.Water.Id))
                .ToArray();

            var max_tenants = unitOfWork.ActiveHouses
                .Max(h => h.Housemates);

            var lines = new List<string>();
            var cAll = "Address,StartDate,EndDate,# Tenants," + Enumerable.Range(1, max_tenants)
                .Select(i => "Tenant Name {0},Tenant Phone {0},Tenant Email {0}".F(i))
                .Aggregate((a, b) => a + "," + b); ;

            lines.Add(cAll);

            foreach (var h in hs)
            {
                var c = h.Contracts
                    .Where(cc => cc.ServiceNew_ID == ServiceType.Water.Id)
                    .ArgMax(cc => cc.RequestedStartDate.Date);

                //skip if contract not confirmed
                if (!c.StartDate.HasValue)
                {
                    Console.WriteLine("House #{0} has a water contract that was to start on {1} but still hasn't!".F(h.ID, c.RequestedStartDate.Date.ToShortDateString()));
                    continue;
                }

                var vals = new List<string>
                {
                    h.Address.ToString(),
                    c.StartDate.Value.Date.ToShortDateString(),
                    c.EndDate.Date.ToShortDateString(),
                    h.Housemates.ToString(),
                };

                foreach(var i in Enumerable.Range(0, max_tenants))
                {
                    var u = h.Tenants.Count > i ? h.Tenants[i] : null;
                    vals.Add(u?.FullName);
                    vals.Add(u?.PhoneNumber);
                    vals.Add(u?.Email);
                }

                lines.Add(vals
                    .Select(v => "\"" + v + "\"")
                    .Aggregate((a, b) => a + "," + b));
            }

            System.IO.File.WriteAllLines("duda.csv", lines);
            System.Diagnostics.Process.Start("duda.csv");
        }
    }
}
