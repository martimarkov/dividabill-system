﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DividaBill.Library;
using DividaBill.Models;
using DividaBill.Security.Utilities;
using DividaBill.Services.Factories.Impl;
using DividaBill.Services.Impl;

namespace DividaBill.ScriptRunner
{
    internal class OriginSparkPrinter : Script
    {
        private Dictionary<string, Func<IEnumerable<HouseModel>, DateTime, DateTime, string>> _dict;

        protected override void doIt()
        {
            var providerService = new ProviderService(unitOfWork);
            var contractService = new ContractService(unitOfWork, providerService);
            var tenantEncryptionService = new TenantEncryptionService(new AesEncryptionService(), new AddressEncryptionService(new AesEncryptionService()));
            var couponService = new CouponService(unitOfWork);
            var houseDetailsViewModelToHouseModelMappingFactory = new HouseDetailsViewModelToHouseModelMappingFactory(couponService);
            var tenancyService = new TenancyService(unitOfWork);
            var service = new HouseService(unitOfWork, contractService, tenantEncryptionService, houseDetailsViewModelToHouseModelMappingFactory, tenancyService);

            _dict = new Dictionary<string, Func<IEnumerable<HouseModel>, DateTime, DateTime, string>>
            {
                {"Origin", service.GenerateOriginTemplate},
                {"Spark", service.GenerateSparkTemplate}
            };


            var pickedEntry = PickListItem(_dict.ToArray(), "service", kv => kv.Key);
            var from = PickDateTime("Pick a start date (inclusive): ");
            var to = PickDateTime("Pick an end date (exclusive): ");

            var allHouses = unitOfWork.ActiveHouses.Get()
                .OrderBy(h => h.StartDate)
                .ToArray();

            var duda = pickedEntry.Value(allHouses, from, to);

            Console.WriteLine("Wrote all {0} guys to the `{1}.csv` file. ", pickedEntry.Key, pickedEntry.Key);
            File.WriteAllText("{0}.csv".F(pickedEntry.Key), duda);
        }
    }
}