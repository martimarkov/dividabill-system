﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DividaBill.Library;
using DividaBill.Models;
using DividaBill.Security.Utilities;
using DividaBill.Services.Factories.Impl;
using DividaBill.Services.Impl;

namespace DividaBill.ScriptRunner.scripts
{
    internal class SkyWaterTvPrinter : Script
    {
        private readonly Dictionary<ServiceType, Func<IEnumerable<Contract>, string>> _dict;
        private readonly HouseService _serv;

        public SkyWaterTvPrinter()
        {
            var tenantEncryptionService = new TenantEncryptionService(new AesEncryptionService(), new AddressEncryptionService(new AesEncryptionService()));
            var providerService = new ProviderService(unitOfWork);
            var couponService = new CouponService(unitOfWork);
            var houseDetailsViewModelToHouseModelMappingFactory = new HouseDetailsViewModelToHouseModelMappingFactory(couponService);
            var tenancyService = new TenancyService(unitOfWork);
            _serv = new HouseService(unitOfWork, new ContractService(unitOfWork, providerService), tenantEncryptionService, houseDetailsViewModelToHouseModelMappingFactory, tenancyService);

            _dict = new Dictionary<ServiceType, Func<IEnumerable<Contract>, string>>
            {
                {ServiceType.Water, hs => _serv.GenerateWaterTemplate(hs)},
                {ServiceType.SkyTV, hs => _serv.GenerateSkyInstallationOnboardingForm(hs)},
                {ServiceType.TVLicense, hs => _serv.GenerateTvLicensingOnboardingForm(hs)}
            };
        }

        protected override void doIt()
        {
            var pickedEntry = PickListItem(_dict.ToArray(), "service", kv => kv.Key.Name);

            var from = PickDateTime("Pick a start date (inclusive): ");
            var to = PickDateTime("Pick an end date (exclusive): ");

            var allContracts = unitOfWork.ActiveHouses.Get()
                .ToArray()
                .Select(h => h.GetContractStartingBetween(pickedEntry.Key, from, to))
                .Where(c => c != null)
                .OrderBy(c => c.RequestedStartDate)
                .ToArray();

            var csvContent = pickedEntry.Value(allContracts);
            var fileName = "{0}.csv".F(pickedEntry.Key);

            File.WriteAllText(fileName, csvContent);
            Console.WriteLine("Wrote all {0} guys to the `{1}` file. ", pickedEntry.Key, fileName);
        }
    }
}