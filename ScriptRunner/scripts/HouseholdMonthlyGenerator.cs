﻿using Dividabill.DAL;
using Dividabill.Models;
using Dividabill.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class HouseholdMonthlyGenerator : Script
    {
        public override void DoIt()
        {
            Console.Write("Start month: ");
            var month = Console.ReadLine();
            Console.Write("Start year: ");
            var year = Console.ReadLine();
            Console.Write("End month: ");
            var endMonth = Console.ReadLine();
            Console.Write("End year: ");
            var endYear = Console.ReadLine();
            Console.Write("Payment day: ");
            var paymentDay = Console.ReadLine();
            Console.Write("Payment month: ");
            var paymentMonth = Console.ReadLine();
            Console.Write("Payment year: ");
            var paymentYear = Console.ReadLine();

            DateTime StartDate = new DateTime(int.Parse(year), int.Parse(month), 1);
            DateTime EndDate = new DateTime(int.Parse(endYear), int.Parse(endMonth), 1);
            DateTime PaymentDate = new DateTime(int.Parse(paymentYear), int.Parse(paymentMonth), int.Parse(paymentDay));
            var _houseService = new HouseService(unitOfWork);
            var _billService = new BillService(unitOfWork);
            List<HouseModel> dbActiveHouses = _houseService.GetActiveHouses().Where(p => p.SetUpDate <= DateTime.Today && p.StartDate > new DateTime(2015, 7, 1) && p.StartDate <= DateTime.Today && p.EndDate> DateTime.Today && p.ID <= 1273).Include("Tenants").ToList();
            foreach (HouseModel house in dbActiveHouses)
            {
                _billService.GenerateMonthlyBill(StartDate.Date, EndDate.Date, house.ID, PaymentDate, false, false);
            }


            //TODO: Get only the currently generated bills
            var bills = _billService.GetHouseholdBillsAsQ().Where(b => b.CreatedDate.Date == DateTime.Today);
            var duda = bills.Aggregate("", (a,b) => a + "," + b);

            File.WriteAllText("bills.csv", duda);
        }
    }
}
