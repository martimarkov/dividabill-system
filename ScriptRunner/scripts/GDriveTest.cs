﻿using System;
using System.Linq;
using DividaBill.Services.Sheets;

namespace DividaBill.ScriptRunner
{
    class GDriveTest : Script
    {
        GDriveService gDriveService;

        public GDriveTest()
        {
            RequiresWriteAccess = true;
        }

        protected override void doIt()
        {
            gDriveService = new GDriveService(unitOfWork);

            var houses = unitOfWork.ActiveHouses.Get()
                .Where(h => h.StartDate.Year < 2020 && h.Tenants.Any())
                .ToArray();

            var sparkFile = unitOfWork.GDriveFileRepository.Get().Single(f => f.FileName == "Spark");

            while (true)
            {
                var commitToDb = false;

                int action;
                do
                {
                    Console.WriteLine("Select action: ");
                    Console.WriteLine("[0] Push");
                    Console.WriteLine("[1] Pull");
                    Console.WriteLine("[2] Sync (Push + Pull)");
                    Console.Write(">");

                    action = Console.ReadKey().KeyChar - '0';
                }
                while (action < 0 || action > 2);
                Console.WriteLine();

                switch (action)
                {
                    case 0:
                        gDriveService.Push(sparkFile, commitToDb, houses);
                        break;
                    case 1:
                        gDriveService.Pull(sparkFile, commitToDb);
                        break;
                    default:
                        gDriveService.Sync(sparkFile, commitToDb, houses);
                        break;
                }

            }
        }
    }
}
