﻿using DividaBill.Models;
using DividaBill.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DividaBill;
using System.IO;
using DividaBill.DAL.Interfaces;
using CsvHelper;
using System.Diagnostics;
using DividaBill.Library;
using DividaBill.Services.Impl;

namespace DividaBill.ScriptRunner
{
    class UpdateStartDateFromCSV : Script
    {
        public static Dictionary<ServiceType, Func<HouseModel, bool>> houseServiceGetter = new Dictionary<ServiceType, Func<HouseModel, bool>>
        {
            { ServiceType.Broadband, (m) => m.Broadband },
            { ServiceType.Electricity, (m) => m.Electricity },
            { ServiceType.Gas, (m) => m.Gas },
            { ServiceType.LandlinePhone, (m) => m.LandlinePhone },
            { ServiceType.LineRental, (m) => m.LandlinePhone || m.Broadband },
            { ServiceType.NetflixTV, (m) => m.NetflixTV },
            { ServiceType.SkyTV, (m) => m.SkyTV },
            { ServiceType.TVLicense, (m) => m.TVLicense },
            { ServiceType.Water, (m) => m.Water },
        };

        public static Dictionary<ServiceType, Func<HouseModel, int>> contractPackageGetter = new Dictionary<ServiceType, Func<HouseModel, int>>
        {
            { ServiceType.Broadband,        (m) => (int)m.BroadbandType },
            { ServiceType.Electricity,      (m) => 0 },
            { ServiceType.Gas,              (m) => 0 },
            { ServiceType.LandlinePhone,    (m) => (int)m.LandlineType },
            { ServiceType.LineRental,       (m) => 0 },
            { ServiceType.NetflixTV,        (m) => 0 },
            { ServiceType.SkyTV,            (m) => 0 },
            { ServiceType.TVLicense,        (m) => 0 },
            { ServiceType.Water,            (m) => 0 },
        };

        static Dictionary<ServiceType, Action<IUnitOfWork, int>> contractDeleteGuy = new Dictionary<ServiceType, Action<IUnitOfWork, int>>
        {
            { ServiceType.Broadband, (u, c) => u.BroadbandContractRepository.Delete(c) },
            { ServiceType.Electricity, (u, c) => u.ElectricityContractRepository.Delete(c) },
            { ServiceType.Gas, (u, c) => u.GasContractRepository.Delete(c) },
            { ServiceType.LandlinePhone, (u, c) => u.LandlinePhoneContractRepository.Delete(c) },
            { ServiceType.LineRental, (u, c) => u.LineRentalContractRepository.Delete(c) },
            { ServiceType.NetflixTV, (u, c) => u.NetflixTVContractRepository.Delete(c) },
            { ServiceType.SkyTV, (u, c) => u.SkyTVContractRepository.Delete(c) },
            { ServiceType.TVLicense, (u, c) => u.TVLicenseContractRepository.Delete(c) },
            { ServiceType.Water, (u, c) => u.WaterContractRepository.Delete(c) },
        };

        ContractService contractService;

        public UpdateStartDateFromCSV()
        {
            RequiresWriteAccess = true;
        }

        protected override void doIt()
        {
            //read the house/service/start stuff
            var lines = File.ReadAllLines("updateServices.csv")
                .Skip(1);

            //parse it
            var preReqs = lines
                .Select(ln => ln.Split(','))
                .Select(ln => new
                {
                    HouseId = int.Parse(ln[0]),
                    ContractType = int.Parse(ln[1]),
                    ActualStartDate = DateTimeOffset.Parse(ln[2]),
                });

            //do some sanity checks
            Debug.Assert(preReqs.All(d => d.ContractType == 1 || d.ContractType == 2));

            foreach(var req in preReqs)
            {

                var h = unitOfWork.ActiveHouses
                    .Single(hh => hh.ID == req.HouseId);
                var c = h.Contracts
                    .SingleOrDefault(cc => cc.ServiceNew_ID == req.ContractType);

                if(c == null)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("House #{0} has no {1} contract, you idiot!".F(h.ID, ((ServiceType)req.ContractType).Name));
                    Console.ForegroundColor = ConsoleColor.Gray;
                    continue;
                }

                Debug.Assert(c.RequestedStartDate <= req.ActualStartDate);

                Console.Write("Making {0} contract in house #{1} start on {2}.. ".F(((ServiceType)req.ContractType).Name, req.HouseId, req.ActualStartDate));
                c.SetAsActive(req.ActualStartDate);

                Console.WriteLine("done!");
            }

            unitOfWork.Save();
            Console.WriteLine("And saved!");
        }
        

        ContractRequest requestFromHouse(int houseId, ServiceType ty, int raw)
        {

            var h = unitOfWork.ActiveHouses.Get().Where(x => x.ID == houseId).First();


            //was this service checked?
            if (!houseServiceGetter[ty](h) || !h.Tenants.Any())
                return null;

            var user = h.Tenants.First();
            var provider = contractService.GetDefaultProvider(user.House, ty);
            var conLength = (h.EndDate.GetMonthsPassed() - h.StartDate.GetMonthsPassed());

            if (conLength < 9 && h.ID > 150)
                Console.WriteLine("House {0} has a duration of {1}!", h.ID, conLength);

            return new ContractRequest
            {
                Duration = (ContractLength)conLength,
                Provider = provider,
                TriggeringTenant = user,
                StartDate = h.StartDate,
                Service = ty,
                PackageRaw = raw,
            };
        }
    }
}
