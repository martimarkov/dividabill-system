﻿using DividaBill.Models;
using DividaBill.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DividaBill;
using System.IO;
using DividaBill.DAL.Interfaces;
using DividaBill.Library;
using DividaBill.Services.Impl;

namespace DividaBill.ScriptRunner
{
    class CreateMissingContracts : Script
    {
        public static Dictionary<ServiceType, Func<HouseModel, bool>> houseServiceGetter = new Dictionary<ServiceType, Func<HouseModel, bool>>
        {
            { ServiceType.Broadband, (m) => m.Broadband },
            { ServiceType.Electricity, (m) => m.Electricity },
            { ServiceType.Gas, (m) => m.Gas },
            { ServiceType.LandlinePhone, (m) => m.LandlinePhone },
            { ServiceType.LineRental, (m) => m.LandlinePhone || m.Broadband },
            { ServiceType.NetflixTV, (m) => m.NetflixTV },
            { ServiceType.SkyTV, (m) => m.SkyTV },
            { ServiceType.TVLicense, (m) => m.TVLicense },
            { ServiceType.Water, (m) => m.Water },
        };

        public static Dictionary<ServiceType, Func<HouseModel, int>> contractPackageGetter = new Dictionary<ServiceType, Func<HouseModel, int>>
        {
            { ServiceType.Broadband,        (m) => (int)m.BroadbandType },
            { ServiceType.Electricity,      (m) => 0 },
            { ServiceType.Gas,              (m) => 0 },
            { ServiceType.LandlinePhone,    (m) => (int)m.LandlineType },
            { ServiceType.LineRental,       (m) => 0 },
            { ServiceType.NetflixTV,        (m) => 0 },
            { ServiceType.SkyTV,            (m) => 0 },
            { ServiceType.TVLicense,        (m) => 0 },
            { ServiceType.Water,            (m) => 0 },
        };

        static Dictionary<ServiceType, Action<IUnitOfWork, int>> contractDeleteGuy = new Dictionary<ServiceType, Action<IUnitOfWork, int>>
        {
            { ServiceType.Broadband, (u, c) => u.BroadbandContractRepository.Delete(c) },
            { ServiceType.Electricity, (u, c) => u.ElectricityContractRepository.Delete(c) },
            { ServiceType.Gas, (u, c) => u.GasContractRepository.Delete(c) },
            { ServiceType.LandlinePhone, (u, c) => u.LandlinePhoneContractRepository.Delete(c) },
            { ServiceType.LineRental, (u, c) => u.LineRentalContractRepository.Delete(c) },
            { ServiceType.NetflixTV, (u, c) => u.NetflixTVContractRepository.Delete(c) },
            { ServiceType.SkyTV, (u, c) => u.SkyTVContractRepository.Delete(c) },
            { ServiceType.TVLicense, (u, c) => u.TVLicenseContractRepository.Delete(c) },
            { ServiceType.Water, (u, c) => u.WaterContractRepository.Delete(c) },
        };

        ContractService contractService;
        int n_created, n_deleted;

        public CreateMissingContracts()
        {
            RequiresWriteAccess = true;
        }

        protected override void doIt()
        {
            n_created = 0;
            n_deleted = 0;

            var providerService = new ProviderService(unitOfWork);
            contractService = new ContractService(unitOfWork, providerService);
            var houses = unitOfWork.ActiveHouses.Get().ToArray();
            var allServices = ServiceType.All.Where(ty => ty != ServiceType.NetflixTV);

            foreach (var h in houses)
            {
                foreach(var ty in allServices)
                {
                    //check for existing contracts
                    var existingContract = unitOfWork.ContractRepository.Get()
                        .FirstOrDefault(c =>
                            c.House_ID == h.ID &&
                            (c.ServiceNew_ID == ty.Id || (int)c.Service == ty.Id));
                    var hasTheContract = (existingContract != null);

                    var req = requestFromHouse(h, ty);
                    var wantsTheContract = (req != null);

                    if (hasTheContract == wantsTheContract)
                    {
                        Console.Write(' ');
                        continue;
                    }

                    if(hasTheContract)
                    {
                        //has one, but does not want it
                        contractDeleteGuy[ty](unitOfWork, existingContract.ID);
                        unitOfWork.Save();

                        Console.Write('-');
                        n_deleted++;
                    }
                    else
                    {
                        //no contract, wants one
                        var newContract = contractService.Create(req);
                        unitOfWork.Save();

                        Console.Write('+');
                        n_created++;
                    }

                }
                Console.WriteLine();
            }
            Console.WriteLine("{0} contracts created!", n_created);
            Console.WriteLine("{0} contracts deleted!", n_deleted);

            Console.WriteLine("And saved!");
        }
        

        ContractRequest requestFromHouse(HouseModel h, ServiceType ty)
        {
            //was this service checked?
            if (!houseServiceGetter[ty](h) || !h.Tenants.Any())
                return null;

            var user = h.Tenants.First();
            var provider = contractService.GetDefaultProvider(user.House, ty);
            var conLength = (h.EndDate.GetMonthsPassed() - h.StartDate.GetMonthsPassed());

            if (conLength < 9 && h.ID > 150)
                Console.WriteLine("House {0} has a duration of {1}!", h.ID, conLength);

            return new ContractRequest
            {
                Duration = (ContractLength)conLength,
                Provider = provider,
                TriggeringTenant = user,
                RequestedStartDate = h.StartDate,
                Service = ty,
                PackageRaw = contractPackageGetter[ty](h),
            };
        }
    }
}
