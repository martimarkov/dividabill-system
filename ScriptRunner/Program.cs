﻿using DividaBill.ScriptRunner.scripts;
using DividaBill.Models;
using DividaBill.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DividaBill;
using DividaBill.Library;

namespace DividaBill.ScriptRunner
{
    class Program
    {
        static Script[] ScriptMenu = new Script[]
        {
            new GDriveTest(),

            new FixExistingContracts(),
            new CreateMissingContracts(),

            new SkyWaterTvPrinter(),
            new OriginSparkPrinter(),

            //new GenerateNewBills(),
            //new SendBillEmails(),
            //new SendBillsToGoCardless(),
            //new CancelPendingPaymentsGocardlessService(),
            //new RefreshPaymentStatusService(),

            new CreateMissingContractsFromCSV(),
            new UpdateStartDateFromCSV(),
            //new RemoveAllPaymentsService(),
        };

        static string[] dbList = new[]
        {
            "local",
            "remote-dev",
            "remote-beta",
            "remote-live",
        };



        static void Main(string[] args)
        {
            //setup log output
            var dtNow = DateTime.Now;
            var logName = "script-log-{0}-{1}-{2}.txt".F(dtNow.Year, dtNow.Month, dtNow.Day);
            var logWriter = File.AppendText(logName);
            logWriter.WriteLine(Environment.NewLine + "[{0}] ScriptRunner started!", dtNow.ToString());

            using (var newConsoleOut = new MultiTextWriter(Console.Out, logWriter))
            {
                Console.SetOut(newConsoleOut);

                do
                {
                    var currentDb = Script.PickListItem(dbList, "database", db => db);
                    var currentScript = Script.PickListItem(ScriptMenu, "script", s => s.GetType().Name);

                    currentScript.DoIt(currentDb);

                    
                }
                while (Console.ReadLine().ToLower() == "y");
            }
        }
    }
}
