﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.OData.Builder;
using System.Web.OData.Extensions;
using DividaBill.Models;

namespace DividaBill
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            config.Routes.MapHttpRoute(
                name: "ActionApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            var json = config.Formatters.JsonFormatter;
            json.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects;
            config.Formatters.Remove(config.Formatters.XmlFormatter);


            //ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
            //var postcodes = builder.EntitySet<PostCode>("PostCodes");
            //builder.EntitySet<PostcodeEstimatedPrice>("Prices");
            //builder.EntitySet<PostcodeEstimatedPrice>("PostcodeEstimatedPrices");

            ////var postcodesType = builder.EntityType<PostCode>();



            //FunctionConfiguration myFirstFunction = postcodes.EntityType.Collection.Function("LastPostcode");
            //myFirstFunction.ReturnsCollectionFromEntitySet<PostCode>("PostCodes");

            ////builder.Namespace = typeof(PostCode).Namespace;
            ////postcodes.EntityType.Collection.Function("LastPostcode").Returns<PostCode>();

            //config.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
        }
    }
}
