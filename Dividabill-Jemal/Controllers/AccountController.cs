﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Owin;
using DividaBill.Models;
using DividaBill.Models.ViewModels;
using System.Net.Mail;
using DividaBill.Helpers;
using SendGrid;
using System.Net;
using DividaBill.Services;
using AutoMapper;
using Postal;
using System.Web.Routing;
using DividaBill.DAL;
using Quartz;
using DividaBill.ScheduledTasks;
using Quartz.Impl;
using DividaBill.ViewModels;
using System.Data.Entity.Validation;
using DividaBill;
using DividaBill.Services.Interfaces;

namespace DividaBill.Controllers
{
    //[RequireHttps]
    [Authorize]
    public class AccountController : Controller
    {
        private ApplicationUserManager _userManager;
        private readonly IHouseService houseService;
        private readonly Services.Interfaces.IEmailService emailService;

        public AccountController(IHouseService houseService, Services.Interfaces.IEmailService emailService)
        {
            this.houseService = houseService;
            this.emailService = emailService;
        }


        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login()
        {
            return RedirectToAction("Login", "Home", null);
        }

        //
        // GET: /Account/SignUp
        [AllowAnonymous]
        public ActionResult SignUp()
        {
            var wizard = new SignUpViewModel();
            wizard.StartTime = DateTime.Now;
            wizard.DoB = new DateTime(1993, 4, 5);
            wizard.StartTime = DateTime.Today.AddDays(15);
            wizard.Gas = false;
            wizard.Electricity = false;
            wizard.Water = false;
            wizard.NetflixTV = false;
            wizard.Broadband = false;
            wizard.LandlinePhone = false;
            wizard.SkyTV = false;
            wizard.TVLicense = false;
            wizard.TV = false;
            wizard.NewsletterSubscription = true;
            return View(wizard);
        }


        //
        // GET: /Account/GetQuote
        [AllowAnonymous]
        public ActionResult GetQuote()
        {
            return RedirectToActionPermanent("Signup");
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(SignUpViewModel wizard)
        {
            throw new NotImplementedException();


            // If we got this far, something failed, redisplay form
            //return View("SignUp", wizard);
        }
        //
        // GET: /Account/ConfirmEmail
        public ActionResult Confirmation()
        {
            return View();
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }

            IdentityResult result = await UserManager.ConfirmEmailAsync(userId, code);
            if (result.Succeeded)
            {
                return View("ConfirmEmail");
            }
            else
            {
                AddErrors(result);
                return View();
            }
        }

        [AllowAnonymous]
        public ActionResult Bill(int id)
        {
            UnitOfWork unitOfWork = new UnitOfWork();
            HouseholdBill bill = unitOfWork.HouseholdBillRepository.GetByID(id);
            var emailService = new DividaBill.Services.EmailService(unitOfWork);
            dynamic email = emailService.GenerateBillEmail(bill);
            return new EmailViewResult(email);
        }
        [AllowAnonymous]
        public ActionResult WelcomeEmail()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login");
            }

            Services.EmailService emailService = new Services.EmailService(null);
            dynamic email = emailService.GenerateSignUpConfirmationEmail(User.Identity.GetUserId());
            return new EmailViewResult(email);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private async Task SignInAsync(User user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, await user.GenerateUserIdentityAsync(UserManager));
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties() { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }

        private string validBankDetails(SignUpViewModel valUser)
        {
            string key = "GW89-NW67-ZH94-PG48";

            //Build the url
            var url = "http://services.postcodeanywhere.co.uk/BankAccountValidation/Interactive/Validate/v2.00/dataset.ws?";
            url += "&Key=" + System.Web.HttpUtility.UrlEncode(key);
            url += "&AccountNumber=" + System.Web.HttpUtility.UrlEncode(Convert.ToString(valUser.AccountNumber));
            url += "&SortCode=" + System.Web.HttpUtility.UrlEncode(Convert.ToString(valUser.AccountSortCode.Replace("-","")));

            //Create the dataset
            var dataSet = new System.Data.DataSet();
            dataSet.ReadXml(url);

            //Check for an error
            if (dataSet.Tables.Count == 1 && dataSet.Tables[0].Columns.Count == 4 && dataSet.Tables[0].Columns[0].ColumnName == "Error")
                return "false";

            String answer = dataSet.Tables[0].Rows[0]["IsCorrect"].ToString();

            if (valUser.AccountNumber == "00000000" && valUser.AccountSortCode == "000000")
            {
                answer = "True";
            }

            return answer;

            //if (dataSet.Tables[0].Rows[0]["IsCorrect"] == "True")
            //{
            //    return "true";
            //}
            //else
            //{
            //    return "false";
            //}


        }
        #endregion
    }
}