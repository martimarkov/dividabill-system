﻿function getBuildings(buildingsContainer, postcode) {  

    if (!$("#quick-find").hasClass("fa-spinner")) {
        $("#quick-find").addClass("fa-spinner fa-spin");
        $.getJSON('https://microsoft-apiapp517eb760afa2461f847e629af8311b4c.azurewebsites.net/api/public/Address/' + postcode).done(
            function (result) {
                console.log(result);
                // Check if there were any items found
                $("#quick-find").removeClass("fa-spinner fa-spin");

                if (result.length == 0)
                    alert("Sorry, there were no results");
                else {
                    $(buildingsContainer).empty();
                    $(buildingsContainer).parent().parent().hide();

                    $(buildingsContainer)
                            .append($("<option></option>")
                            .text("Please Select building"));

                    $.each(result, function (key, value) {
                        $(buildingsContainer)
                            .append($("<option></option>")
                            .attr("value", value.PAId)
                            .text(value.StreetAddress + ", " + value.Place));
                    });
                    $(buildingsContainer).selectpicker('refresh');
                    $(buildingsContainer).parent().parent().show();
                }
            })
        .fail(function () {
            $("#quick-find").removeClass("fa-spinner fa-spin");
            $('#postcodeError').modal('toggle');
        });
    }
}

function selectBuilding(premiseId) {
    $.getJSON('https://microsoft-apiapp517eb760afa2461f847e629af8311b4c.azurewebsites.net/api/public/Building/' + premiseId).done(
		function (result) {
		    console.log(result);
		    // Check if there were any items found
		    if (result.length == 0)
		        alert("Sorry, there were no results");
		    else {
		        $("#filled-signup-info").removeClass("hidden");
		        $('#PostCode').val(result.PostCode).prop('readonly', true);
		        $('#CountyTT').val(result.County).prop('readonly', true);
		        $('#CityTT').val(result.PostTown).prop('readonly', true);
		        $('#AddressTT1').val(result.Line1).prop('readonly', true);
		        $('#AddressTT2').val(result.Line2).prop('readonly', true);
		        $('#AddressTT3').val(result.Line3).prop('readonly', true);
		        $('#AddressTT4').val(result.Line4).prop('readonly', true);
		        $('#Udprn').val(result.Udprn);

		        $.getJSON('https://microsoft-apiapp517eb760afa2461f847e629af8311b4c.azurewebsites.net/api/public/House/' + $("#Udprn").val())
                    .done(function (h) { parseHouse(h); })
                    .fail(function () { parseHouse(null); });
		    }
		});
}

function parseHouse(house) {
    //Bind to fields
    $('.quote-checker').unbind();
    $('#choose-internet').unbind();
    $('#choose-telephone').unbind();
    $('#choose-television').unbind();


    $('.quote-checker').click(MarkChecked);
    $('#choose-internet .radio').change(MarkChecked);
    $('#choose-telephone .radio').change(MarkChecked);
    $('#choose-television :checkbox').change(MarkChecked);


    // Check if there were any items found
    if (house == null || house.length == 0) {

        $('.ischecked').change(function () {
            if ($('#form1').validate().checkForm()) {
                $("#signup-services").removeClass("hidden");
            }
        });

        $('#Housemates').prop('disabled', false).selectedIndex = 0;
        $('#Period').prop('disabled', false).selectedIndex = 0;
        $('.selectpicker').selectpicker('refresh');
        $('#StartTime').datepicker();
        $('#Coupon').val("").prop('readonly', false);

        //Unbind the click event trackers so that they dont stackup.


    } else {
        $('.ischecked').unbind();
        $('#myModal').modal();

        $('#Housemates').val(house.Housemates).prop('disabled', true);
        console.log(house.Period)
        if (house.Period > 12) {
            $('#Period').prop('disabled', true).selectedIndex = 0;
            $('#Ongoing').prop("checked", true).prop('disabled', true);
        } else {
            $('#Period').val(house.Period).prop('disabled', true);
            $('#Ongoing').prop("checked", false).prop('disabled', true);
        }
        //$('#Period').val(9).prop('disabled', true);
        $('#Coupon').val(house.Coupon).prop('readonly', true);
        $('.selectpicker').selectpicker('refresh');
        $('#StartTime').datepicker('remove');



        if ($('#form1').validate().checkForm()) {
            $("#signup-services").removeClass("hidden");
        }

        if (house.Electricity && !$('#electricity').hasClass('electricity-active')) {
            $('#electricity').trigger("click", [{ checkPrice: 'false' }]);
        }

        if (house.Gas && !$('#gas').hasClass('gas-active')) {
            $('#gas').trigger("click", [{ checkPrice: 'false' }]);
        }

        if (house.Water && !$('#water').hasClass('water-active')) {
            $('#water').trigger("click", [{ checkPrice: 'false' }]);
        }

        if (house.BroadbandType == 1) {
            console.log(house.BroadbandType);
            $('#internet').trigger("click", [{ checkPrice: 'false' }]);
            $('#BroadbandTypeADSL20').prop('checked', true);
        } else if (house.BroadbandType == 2) {
            $('#internet').trigger("click", [{ checkPrice: 'false' }]);
            $('#BroadbandTypeFibre40').prop('checked', true);
        } else if ($('#internet').hasClass('internet-active')) {
            $('#internet').trigger("click", [{ checkPrice: 'false' }]);
        }

        if ((house.SkyTV || house.NetflixTV || house.TVLicense)
            && !$('#television').hasClass('television-active')) {
            $('#television').trigger("click", [{ checkPrice: 'false' }]);
        }
        $('#SkyTV').prop('checked', house.SkyTV);

        $('#NetflixTV').prop('checked', house.NetflixTV);

        $('#TVLicense').prop('checked', house.TVLicense);

        if (house.LandlinePhoneType == 1) {
            console.log(house.LandlinePhoneType);
            $('#telephone').trigger("click", [{ checkPrice: 'false' }]);
            $('#LandlineTypeBasic').prop('checked', true);
        } else if (house.LandlineType == 2) {
            $('#telephone').trigger("click", [{ checkPrice: 'false' }]);
            $('#LandlineTypeMedium').prop('checked', true);
        } else if ($('#telephone').hasClass('telephone-active')) {
            $('#telephone').trigger("click", [{ checkPrice: 'false' }]);
        }

        //Remove bindings
        $('.quote-checker').unbind("click");
        $('#choose-internet').unbind();
        $('#choose-telephone').unbind();
        $('#choose-television').unbind();
        $('#choose-internet .radio').unbind();
        $('#choose-telephone .radio').unbind();
        $('#choose-television :checkbox').unbind();
        checkCoupon();


        $(".quote-checker").click(function () {
            $('#servicesInformation').modal('toggle');
        })
    }
}

$("#get_geo").click(
function getLocation() {

    if (navigator.geolocation) {
        $("#marker")
            .removeClass("fa-map-marker")
            .addClass("fa-spin fa-spinner");
        navigator.geolocation.getCurrentPosition(getPosition);
    } else {

        $("#quote_postcode").val("Geolocation is not supported by this browser.");
    }
});

function getPosition(position) {
    var coords = position.coords.latitude + "," + position.coords.longitude;

    $.getJSON('https://microsoft-apiapp517eb760afa2461f847e629af8311b4c.azurewebsites.net/api/public/Location?id=' + coords).done(
		function (result) {
		    console.log(result);
		    // Check if there were any items found

		    if (result.length == 0) {
		        alert("Sorry, there were no results");
		    }
		    else {
		        $("#quote_postcode").val(result);
		    }
		    $("#marker")
                .removeClass("fa-spin fa-spinner")
                .addClass("fa-map-marker");
		}
    )
};