﻿(function (App) {
    'use strict';

    var myController = {

        getTheApp: _.once(function () {
            return new App.View.MainWindow();
        }),
        initApp: function () {
            try {
                App.Window.show(this.getTheApp());
            } catch (e) {
                console.error('Couldn\'t start app: ', e, e.stack);
            }
        },
        showIndex: function () {
            this.initApp();
            App.vent.trigger('show:Index');
            $(".drop_cart").hide(); $(".signup-nav").show();
        },
        showAboutus: function () {
            this.initApp();
            App.vent.trigger('show:aboutUs');
            $(".drop_cart").hide(); $(".signup-nav").show();
        },
        showWhyItWorks: function () {
            this.initApp();
            App.vent.trigger("show:whyItWorks");
            $(".drop_cart").hide(); $(".signup-nav").show();
        },
        showContactUs: function () {
            this.initApp();
            App.vent.trigger("show:contactUs");
            $(".drop_cart").hide(); $(".signup-nav").show();
        },
        showServices: function () {
            this.initApp();
            App.vent.trigger("show:Services");
            $(".drop_cart").hide(); $(".signup-nav").show();
        },
        showSignUp: function () {
            this.initApp();
            App.vent.trigger("show:SignUp");
        },
        GetQuote: function (query) {
            console.log("String: " + query);
            this.initApp();
            App.vent.trigger("show:GetQuote", query);


        }
    };

    var MyRouter = new Marionette.AppRouter({
        controller: myController,
        appRoutes: {
            "": "showIndex",
            "Home/AboutUs": "showAboutus",
            "Home/WhyItWorks": "showWhyItWorks",
            "Home/ContactUs": "showContactUs",
            "Home/Services": "showServices",
            "Account/SignUp": "showSignUp",
            "Account/SignUp/:query": "GetQuote"
        },
    });

    App.Router = MyRouter;

})(window.App);


