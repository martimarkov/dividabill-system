﻿(function (App) {
    'use strict';

    var ConfirmSignup = Marionette.LayoutView.extend({
        template: "#confirmation-page-tpl",
        initialize: function () {
            console.log("Init Confirmation Page")
        },
        onShow: function () {
            var totalbill = (this.model.get("TotalBill") * this.model.get("Housemates")).toFixed(2);
            var personbill = (this.model.get("TotalBill")).toFixed(2);
            $("#house-cost").val("£" + totalbill);
            $("#cost-person").val("£" + personbill);
            $("#active-dat").val(this.model.get("StartTime"));
        }
    });

    App.View.ConfirmSignup = ConfirmSignup;

})(window.App);
