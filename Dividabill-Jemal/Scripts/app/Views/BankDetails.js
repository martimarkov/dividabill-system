﻿(function (App) {
    'use strict';

    var BankDetails = Marionette.LayoutView.extend({
        template: "#bankdetails-tpl",

        initialize: function () {
            console.log("Init Bank Details");
        },
        events: {
            'change #AccountName': 'changeAccName',
            'change #AccountNumber': 'changeAccNumber',
            'change #AccountSortCode': 'changeAccSortCode',
            'change #AccountStandalone': "changeAccountHolder",
            'change #NewsletterSubscription': 'changeNewsletter',
            'click .bank-checkbox': 'changeCheckboxIcon'
        },
        onShow: function () {
            this.accessFromAnotherStep();
        },
        changeAccName: function (ev) {
            var value = $(ev.target).val();
            this.model.set({ 'AccountName': value });
            $("#AccountNameConfirmation").text(value);
        },
        changeAccNumber: function (ev) {
            var value = $(ev.target).val();
            this.model.set({ 'AccountNumber': value });
            $("#AccountNumberConfirmation").text(value);
        },
        changeAccSortCode: function (ev) {
            var value = $(ev.target).val();
            this.model.set({ 'AccountSortCode': value });
            $("#AccountSortCodeConfirmation").text(value);
        },
        accessFromAnotherStep: function () {
            $("#AccountName").val(this.model.get("AccountName"));
            $("#AccountNumber").val(this.model.get("AccountNumber"));
            $("#AccountSortCode").val(this.model.get("AccountSortCode"));
        },
        changeCheckboxIcon: function (ev) {
            $(ev.target).toggleClass("fa-circle-thin fa-check-circle");
            console.log(ev.target);
            var checkBox = $('#' + $(ev.target).data('checkbox-id'));
            checkBox.prop("checked", !checkBox.prop("checked"));
            console.log(checkBox.prop("checked"));

        }
    });



    App.View.BankDetails = BankDetails;

})(window.App);