﻿(function (App) {
    'use strict';

    var FooterView = Marionette.ItemView.extend({
      template: "#footer-tpl",
      initialize: function() {
        console.log("Init  Footer")
      }
    });


    App.View.FooterView = FooterView;

})(window.App);