﻿(function (App) {
    'use strict';

    var Services = Marionette.ItemView.extend({
        template: "#services-tpl",
        initialize: function () {
            console.log("Init Services");

        },
        events: {
            'click .small-service': 'showContent'
        },
        onShow: function () {
            $('.services-sidebar').affix({
                offset: {
                    top: 300,
                }
            });
            this.scrollToFooter();
        },
        scrollToFooter: function () {
            $(window).scroll(function () {
                footertotop = ($('#footer').position().top);
                scrolltop = $(document).scrollTop() + 400;

               // console.log("Footer top: " + footertotop);
              //  console.log("Scroll top: " + scrolltop);
                difference = scrolltop - footertotop;
                if (scrolltop > footertotop) {

                    $('.services-sidebar').css('margin-top', 0 - difference);
                }

                else {
                    $('.services-sidebar').css('margin-top', 0);
                }
            });
        },
        showContent: function (e) {
            var service = $(e.target).closest(".small-service");
            var target = service.data("target");
            console.log(target);
            if (!$(service).hasClass("active")) {
                $(".small-service").removeClass("active");
                $(".service-section").slideUp("slow");
                $(".service-title").removeClass("active");

                $(service).parent().find(".service-title").addClass("active");
                console.log($(service).parent().find(".service-title"));
                $(service).addClass("active");
                $("." + target + "-block").slideDown("slow");
            }
        }
    });
    App.View.Services = Services;

})(window.App);