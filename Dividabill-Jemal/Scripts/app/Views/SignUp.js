﻿(function (App) {
    'use strict';
    var step_one = 0,step_two = 0,step_three = 0;
    var SignUp = Marionette.LayoutView.extend({
        template: "#signup-tpl",
        regions: {
            Container: "#form1"
        },
        initialize: function () {
            console.log("Init Sign Up");
            var that = this;
            this.model = new App.Model.SignupDetails();

        },
        events: {
            'click #continue_step2_btn': 'showServicesDetails',
            'click #previous-step1': 'backToStepOne',
            'click #continue_step3_btn': 'showPersonalDetails',
            'click #continue_step4_btn': 'showBankDetails',
            'click #previous-step2': 'backToStepTwo',
            'click #previous-step3': 'backToStepThree',
            'click #confirm': 'ConfirmSignup',
            'click #joinedHouse_btn': 'showBankDetails',
            'click #changePersonalDetails': 'backToStepThree',
            'click #approveRegistration' : 'approveRegistration'

        },
        showServicesDetails: function () {
            if ($("#form1").valid()) {
                this.Container.empty();
                this.Container.show(new App.View.ServicesDetails({ model: this.model }));
                $("#continue_step2").hide();
                $("#step2-btns").show();
                $("body").stop().animate({ scrollTop: 0 }, '500', 'swing');
                //Change progress bar
                if ($(".bar").css("width")[0] == 0) {
                    $(".step-one").removeClass("step-active");
                    $(".step-one").addClass("step-done");
                    $(".step-two").addClass("step-active");
                    $(".bar").css({ "width": "35%"});

                    //Cart Link
                    $(".signup-nav").hide();
                    $(".drop_cart").show();
                }              
            }           
        },
        showPersonalDetails: function () {
            if ($("#form1 .service_check").valid()) {
                this.Container.empty();
                this.Container.show(new App.View.PersonalDetails({model: this.model}));
                $("#step3-btns").show();
                $("#step2-btns").hide();
                $("body").stop().animate({ scrollTop: 0 }, '500', 'swing');
                //Change progress bar
                if ($(".bar").css("width")[0] == 1) {
                    $(".step-two").removeClass("step-active");
                    $(".step-two").addClass("step-done");
                    $(".step-three").addClass("step-active");
                    $(".bar").css({ "width": "70%" });
                }
            }             
        },
        showBankDetails: function () {
            if ($("#form1").valid()) {
                this.Container.empty();
                this.Container.show(new App.View.BankDetails({ model: this.model }));
                $("#step3-btns").hide();
                $("#continue_step_joinedHouse").hide();
                $("#step4-btns").show();
                $("body").stop().animate({ scrollTop: 0 }, '500', 'swing');
                //Change progress bar
                if ($(".bar").css("width")[0] == 2) {
                    $(".step-three").removeClass("step-active");
                    $(".step-three").addClass("step-done");
                    $(".step-four").addClass("step-active");
                    $(".bar").css({ "width": 100 + "%" });
                }
            }
        },
        ConfirmSignup: function () {
            if ($("#form1").valid()) {
                $("#confirmationModal").modal("toggle");
                $(".step-four").removeClass("step-active");
                $(".step-four").addClass("step-done");
            }
        },
        backToStepOne: function () {
            var house_details = new App.View.HouseDetails({ model: this.model });
            this.Container.empty();
            this.Container.show(house_details);

            $("#continue_step2").show();
            $("#step2-btns").hide();
        },
        backToStepTwo: function () {
            var service_details = new App.View.ServicesDetails({ model: this.model });
            this.Container.empty();
            this.Container.show(service_details);

            $(".total_prices_services").show();
            $("#step3-btns").hide();
            $("#step2-btns").show();
        },
        backToStepThree: function () {
            var personal_details = new App.View.PersonalDetails({ model: this.model });
            this.Container.empty();
            this.Container.show(personal_details);

            if (this.model.get("Changed") == true) {
                $("#continue_step_joinedHouse").show();
            }
            else
            {
                $("#step3-btns").show();
            }
            $("#step4-btns").hide();
            
        },
        approveRegistration: function() {
            this.Container.empty();
            this.Container.show(new App.View.ConfirmSignup({model: this.model}));
            $("#step4-btns").hide();
            $("body").stop().animate({ scrollTop: 0 }, '500', 'swing');
        },
        onShow: function () {
            this.formValidation();

            var that = this;
            step_one = $('.step-one').offset().left;
            step_two = $('.step-two').offset().left;
            step_three = $('.step-three').offset().left;

            this.house_details = new App.View.HouseDetails({ model: this.model });
            this.service_details = new App.View.ServicesDetails({ model: this.model });
            this.Container.show(this.house_details);

            //If the house is signed up
            this.model.on('change:Changed', function () {

                //Cart Link
                $(".signup-nav").hide();
                $(".drop_cart").show();

                that.Container.empty();
                that.service_details.checkServiceFromModel();
                that.Container.show(new App.View.PersonalDetails({ model: that.model }));

                $(".step-one").addClass("step-done");
                $(".step-one").removeClass("step-active");
                $(".step-two").addClass("step-done");
                $(".step-three").addClass("step-active");
                $(".bar").css({ "width": "70%" });
                $("#continue_step2").hide();
                $("#continue_step_joinedHouse").show();
                $("#step3-btns").hide();
                $('#existingHouseModal').modal('toggle');


            });

        },
        getQueryString: function (queryString) {
            var string = queryString.split("=");
            if (string[0] == "postcode") {
                var query = string[1].replace(/\+/g, "");
                this.model.set({ "Postcode": query });
            }
            if (string[0] == "coupon") {
                this.model.set({"Coupon" : string[1]})
            }
            //console.log(string[1]);
        },
        formValidation: function () {
            $("#form1").validate({
                ignore: ':not(select:hidden, input:visible, .form-control, textarea:visible, input[type=checkbox]:hidden, input[type=radio]:hidden, #DoB)',
                rules: {
                    BroadbandType: {
                        required: true
                    },
                    LandlinePhoneType: {
                        required: true
                    },
                    ConfirmPassword: {
                        equalTo: "#Password"
                    },
                    Password: {
                        minlength: 6
                    },
                    Email: {
                        email: true
                    },
                    ConfirmEmail: {
                        email: true,
                        equalTo: "#Email"
                    },
                    SkyTV: {
                        tvitems: true
                    },
                    TVLicense: {
                        tvitems: true
                    },
                    Electricity: {
                        services: true
                    },
                    Water: {
                        services: true
                    },
                    AccountSortCode: {
                        minlength: 6,
                        maxlength: 6
                    },
                    AccountNumber: {
                        minlength: 8,
                        maxlength: 8
                    },
                    Phone: {
                        minlength: 10,
                        maxlength: 11
                    }

                },
                groups: {
                    TVitemsGroup: "SkyTV TVLicense",
                    ServicesGroup: "Electricity Water"
                },
                messages: {
                    BroadbandType: {
                        required: "Please select Broadband Type!"
                    },
                    LandlinePhoneType: {
                        required: "Please select landline phone type!"
                    },
                    TVitemsGroup: {
                        tvitems: "Please select at least one TV item!"
                    },
                    AccountNumber: {
                        minlength: "Please enter a valid account number!",
                        maxlength: "Please enter a valid account number!"
                    },
                    AccountSortCode: {
                        minlength: "Please enter a valid sort code!",
                        maxlength: "Please enter a valid sort code!"
                    },
                    Phone: {
                        minlength: "Please enter a valid phone number!",
                        maxlength: "Please enter a valid phone number!"
                    }

                },
                success: function (label, element) {
                    label.remove();
                },
                errorPlacement: function (error, element) {
                    if ($(element).is('select')) {
                        element.next().after(error); // special placement for select elements
                    }
                    else if ($(element).attr("name") == "Password") {
                        element.next().after(error);
                    }
                    else if ($(element).is(".chck:checkbox")) {
                        error.appendTo(".tvitems-error");
                   } 
                    else if ($(element).is(".service_check:checkbox")) {
                        error.appendTo(".services-error");
                    }
                    else {
                        error.insertAfter(element);  // default placement for everything else
                    }
                },
                highlight: function (element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                }
            });
            jQuery.validator.addMethod("tvitems", function (value, elem, param) {
                if ($(".chck:checkbox:checked").length > 0) {
                    return true;
                    $(".alert-danger").hide();
                } else {
                    return false;
                }
            }, "Please select at least one TV item!");
            jQuery.validator.addMethod("services", function (value, elem, param) {
                if ($(".service_check:checkbox:checked").length > 0) {
                    return true;
                } else {
                    return false;
                }
            }, "Please select at least one Service!");
        }
    });
    App.View.SignUp = SignUp;
})(window.App);