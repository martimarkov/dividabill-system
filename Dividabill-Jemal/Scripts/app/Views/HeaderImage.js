﻿(function (App) {
    'use strict';

    var HeaderImage = Marionette.ItemView.extend({
        template: "#header-img-tpl",
        initialize: function () {
            console.log("Header Image")
        },
    });

    App.View.HeaderImage = HeaderImage;

})(window.App);
