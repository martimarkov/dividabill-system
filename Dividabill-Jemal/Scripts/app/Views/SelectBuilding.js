﻿(function (App) {
    'use strict';

    var SelectBuilding = Marionette.ItemView.extend({
        template: "#building-tpl",
        initialize: function () {
            console.log("Select Building")
        },
        onShow: function () {
            console.log("Building show");
        }
    });

    App.View.SelectBuilding = SelectBuilding;

})(window.App);
