﻿(function (App) {
    
    var Pricing = Backbone.Model.extend({
        urlRoot: 'https://microsoft-apiapp517eb760afa2461f847e629af8311b4c.azurewebsites.net/api/public/Price/'
    });

    App.Model.Pricing = Pricing;
})(window.App);