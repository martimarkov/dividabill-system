﻿(function (App) {
    var SignupDetails = Backbone.Model.extend({
        defaults: {
            Postcode: "",
            Udprn: "",
            StartTime: "",
            Period: 0,
            Ongoing: false,
            Housemates: 0,
            Coupon: "",
            Electricity: false,
            Gas: false,
            Water: false,
            Broadband: false,
            BroadbandType: 0,
            SkyTV: false,
            TVLicense: false,
            LandlinePhone: false,
            LandlinePhoneType: 0,
            FirstName: "",
            LastName: "",
            Email: "",
            ConfirmEmail: "",
            Password: "",
            ConfirmPassword: "",
            DateOfBirth: "",
            Phone: "",
            AccountName:"",
            AccountNumber: "",
            AccountSortCode: "",

        }
    });

    App.Model.SignupDetails = SignupDetails;
})(window.App);