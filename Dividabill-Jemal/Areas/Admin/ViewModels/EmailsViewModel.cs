﻿using S22.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dividabill.Areas.Admin.ViewModels
{
    public class EmailsViewModel
    {
        public int ID { get; set; }
        public string To { get; set; }
        public string Message { get; set; }
        public string Subject { get; set;}
        public bool HasAttachement
        {
            get
            {
                return (Attachments.Count() == 0) ? false : true;
            }
        }
        public DateTime Sent { get; set; }
        public SerializableAttachmentCollection Attachments { get; set; }

        public SerializableMailAddress From { get; set; }
    }
}