﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dividabill.Areas.Admin.ViewModels
{
    public class UserMinimalViewModel
    {
        public String ID { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String FullName { get { return FirstName + " " + LastName; } }
    }
}