﻿using Dividabill.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dividabill.Areas.Admin.ViewModels
{
    public class BillEditViewModel
    {
        public Bill Bill { get; set; }
        public IEnumerable<SelectListItem> WarrantySelectListItems { get; set; }

    }
}