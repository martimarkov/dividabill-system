﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Dividabill.Models;
using System.IO;
using System.Xml.Serialization;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Text;
using Dividabill.Areas.Admin.ViewModels;
using Dividabill.DAL;
using System.Reflection;
using Dividabill.Services;
using ExcelLibrary.SpreadSheet;
using Postal;
using System.Net.Mail;

namespace Dividabill.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class HousesController : AdminController
    {
        // GET: Admin/Houses
        public ActionResult Index()
        {
            return View(unitOfWork.ActiveHouses.GetHouses());
        }
        // GET Admin/Houses/New
        public ActionResult New()
        {
            return View(unitOfWork.ActiveHouses.GetTodayHouses());
        }

        // GET Admin/Houses/Active
        public ActionResult Active()
        {
            return View(unitOfWork.ActiveHouses.GetHouses(h => h.Housemates == h.Tenants.Count()));
        }

        // GET Admin/Houses/Problem
        public ActionResult Problem(int id = 100)
        {
            DateTime offset = DateTime.Now.AddDays(-7 * id);
            List<HouseModel> problemHouses = unitOfWork.ActiveHouses.GetProblemHouses(offset).ToList();
            List<ProblemHousesViewModel> viewProblemHouses = new List<ProblemHousesViewModel>();
            foreach (HouseModel house in problemHouses)
            {
                viewProblemHouses.Add(
                    new ProblemHousesViewModel
                    {
                        HouseAddress = house.Address,
                        RegisteredTenants = house.Tenants.Count,
                        TotalTenants = house.Housemates,
                        RegisteredDate = house.RegisteredDate,
                        ID = house.ID
                    });
            }
            return View(viewProblemHouses);
        }

        // GET Admin/Houses/Merge
        public ActionResult Merge()
        {
            List<HouseModel> problemHouses = unitOfWork.ActiveHouses.GetHouses(p => p.Housemates != p.Tenants.Count).ToList();
            List<ProblemHousesViewModel> viewProblemHouses = new List<ProblemHousesViewModel>();
            foreach (HouseModel house in problemHouses)
            {
                viewProblemHouses.Add(
                    new ProblemHousesViewModel
                    {
                        HouseAddress = house.Address,
                        RegisteredTenants = house.Tenants.Count,
                        TotalTenants = house.Housemates,
                        RegisteredDate = house.RegisteredDate,
                        ID = house.ID
                    });
            }
            return View(viewProblemHouses);
        }



        public ActionResult Archived(int id = 100)
        {
            DateTime offset = DateTime.Now.AddDays(-7 * id);
            List<HouseModel> problemHouses = unitOfWork.ArchivedHouses.GetArchivedHouses().ToList();
            List<ProblemHousesViewModel> viewProblemHouses = new List<ProblemHousesViewModel>();
            foreach (HouseModel house in problemHouses)
            {
                viewProblemHouses.Add(
                    new ProblemHousesViewModel
                    {
                        HouseAddress = house.Address,
                        RegisteredTenants = house.Tenants.Count,
                        TotalTenants = house.Housemates,
                        RegisteredDate = house.RegisteredDate,
                        ID = house.ID
                    });
            }
            return View(viewProblemHouses);
        }


        public ActionResult Export(string FileType = "Excel", string Type = "Houses", int days = 100)
        {
            var houses = new System.Data.DataTable("Houses");

            houses.Columns.Add("Housemates", typeof(int));
            houses.Columns.Add("Address", typeof(String));
            houses.Columns.Add("Electricity", typeof(bool));
            houses.Columns.Add("Gas", typeof(bool));
            houses.Columns.Add("Water", typeof(bool));
            houses.Columns.Add("Media", typeof(bool));
            houses.Columns.Add("StartDate", typeof(DateTime));
            houses.Columns.Add("EndDate", typeof(DateTime));
            houses.Columns.Add("RegisteredDate", typeof(DateTime));
            houses.Columns.Add("CompletedDate", typeof(DateTime));
            houses.Columns.Add("Coupon", typeof(string));

            List<HouseModel> dbHouses;
            String Filename;
            switch (Type)
            {
                case "ProblemHouses":
                    Filename = "ProblemHouses";
                    DateTime offset = DateTime.Now.AddDays(-7 * days);
                    dbHouses = unitOfWork.ActiveHouses.GetProblemHouses(offset).ToList();
                    break;
                case "ArchivedHouses":
                    Filename = "ArchivedHouses";
                    dbHouses = unitOfWork.ArchivedHouses.GetHouses().ToList();
                    break;
                case "Active":
                    Filename = "ActiveHouses";
                    dbHouses = unitOfWork.ActiveHouses.GetHouses(h => h.Tenants.Count() == h.Housemates).ToList();
                    break;
                default:
                    Filename = "Houses";
                    dbHouses = unitOfWork.ActiveHouses.GetHouses().ToList();
                    break;
            }

            foreach (HouseModel house in dbHouses)
            {
                houses.Rows.Add(house.Housemates, house.Address, house.Electricity, house.Gas, house.Water, house.Media,
                    house.StartDate, house.EndDate, house.RegisteredDate, house.SetUpDate, (house.Coupon != null) ? house.Coupon.Code : "");
            }


            var grid = new GridView();
            grid.DataSource = houses;
            grid.DataBind();

            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=" + Filename + ".xls");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            grid.RenderControl(htw);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
            return RedirectToAction("Index");
        }

        public ActionResult GenerateEstimatedBills()
        {
            DateTime splitdate = new DateTime();
            List<HouseModel> dbActiveHouses = unitOfWork.ActiveHouses.GetHouses(p => p.SetUpDate <= DateTime.Today && p.Housemates == p.Tenants.Count(), null, "Tenants").ToList();
            BillService _service = new BillService(unitOfWork);
            foreach (HouseModel house in dbActiveHouses)
            {
                _service.GenerateMonthlyBill(new DateTime(2014, 12, 3).Date, new DateTime(2014, 12, 31).Date, house.ID, new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(1).Month, 1));
            }

            //List<HouseModel> dbActiveHouses2 = unitOfWork.ActiveHouses.GetHouses(p => p.SetUpDate > splitdate && p.SetUpDate <= new DateTime(2014, 10, 23) && p.Housemates == p.Tenants.Count(), null, "Tenants").ToList();
            //foreach (HouseModel house in dbActiveHouses2)
            //{
            //    //_service.GenerateEstimatedBill(house.SetUpDate.Value.Date, new DateTime(2014, 11, 30).Date, house.ID, new DateTime(DateTime.Now.Year, DateTime.Now.Month, 15));
            //}
            return Index();
        }

        public ActionResult GenerateBGForm(int id)
        {
            HouseService _service = new HouseService(unitOfWork);
            Workbook workbook = _service.GenerateBritishGasRegistrationForm(id);

            //Write-out the workbook
            MemoryStream m = new MemoryStream();
            workbook.SaveToStream(m);
            m.Position = 0;

            return File(m, "application/vnd.ms-excel", "BritishGas-" + id + ".xls");
        }

        public ActionResult SendBGEmail(int id)
        {
            HouseService _service = new HouseService(unitOfWork);
            _service.SendBritishGasRegistrationForm(id);

            return RedirectToAction("Details", new { id = id });
        }



        public ActionResult ExportECPayForm()
        {
            List<HouseModel> dbActiveHouses = unitOfWork.ActiveHouses.GetHouses(h => h.Housemates == h.Tenants.Count()).ToList();
            List<HouseModel> addedHouses = new List<HouseModel>();
            List<EazyCollectPayment> eazycollect = new List<EazyCollectPayment>();
            foreach (HouseModel house in dbActiveHouses)
            {
                foreach (User client in house.Tenants)
                {
                    if (client.DDRef == null || (!client.DDRef.StartsWith("DBILL")))
                    {
                        addedHouses.Add(house);
                        break;
                    }
                }
            }
            foreach (HouseModel house in dbActiveHouses)
            {

                //_billService.GenerateEstimatedBill(new DateTime(2014))
                List<Bill> bills = unitOfWork.BillRepository.Get(b => b.HouseModelID == house.ID && b.Type == BillType.Estimated && b.Status == BillStatus.Pending && b.StartDate > DateTime.Today).ToList();
                foreach (Bill bill in bills)
                {
                    foreach (User client in house.Tenants)
                    {

                        if (client.DDRef != null && (client.DDRef.StartsWith("DBILL")))
                        {

                            eazycollect.Add(
                               new EazyCollectPayment
                               {
                                   Due = bill.PaymentDate.Date.ToShortDateString(),
                                   DirectDebitRef = client.DDRef,
                                   Amount = Convert.ToString(Math.Truncate(bill.TotalBill / house.Tenants.Count() * 100) / 100),
                                   Comment = bill.StartDate.ToShortDateString() + " - " + bill.EndDate.ToShortDateString()
                               }
                               );

                            Services.EmailService emailService = new Services.EmailService(unitOfWork);
                            dynamic email = emailService.GenerateBillEmail(bill, client);
                            email.Send();

                        }
                        else
                        {

                            String ExtraMessage = "As this is your first month, the bank was unable to set up your details in time for a direct debit. From next month this will be resolved. You can choose to pay now through BACs transfer and if you don't pay it will be automatically transferred to a next month combined with your normal monthly bill. Our bank details are Account number: 60496154 Sort code: 20-79-29";
                            Services.EmailService emailService = new Services.EmailService(unitOfWork);
                            dynamic email = emailService.GenerateBillEmail(bill, client, ExtraMessage);
                            emailService.SendBillEmail(bill, client, ExtraMessage);
                            //email.Send();
                        }
                    }

                }

            }

            string csv = GetCSV(eazycollect);

            //string csv = String.Join(",", eazycollect.Select(x => x.ToString()).ToArray());

            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=Clients.csv");
            //Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter sw = new StringWriter();
            Response.Output.Write(csv);
            Response.Flush();
            Response.End();

            return RedirectToAction("Index");
        }

        // GET: Admin/Houses/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HouseModel houseModel = unitOfWork.ActiveHouses.GetByID(id) ?? unitOfWork.ArchivedHouses.GetByID(id);
            if (houseModel == null)
            {
                return HttpNotFound();
            }
            return View(houseModel);
        }

        // GET: Admin/Houses/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Houses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Housemates,Address,Electricity,Gas,Water,Media,Note,StartDate,EndDate,RegisteredDate")] HouseModel houseModel)
        {
            if (ModelState.IsValid)
            {
                unitOfWork.ActiveHouses.Insert(houseModel);
                unitOfWork.Save();
                return RedirectToAction("Index");
            }

            return View(houseModel);
        }

        // GET: Admin/Houses/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HouseModel houseModel = unitOfWork.ActiveHouses.GetByID(id);
            if (houseModel == null)
            {
                return HttpNotFound();
            }
            return View(houseModel);
        }

        // POST: Admin/Houses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Housemates,Address,Electricity,Gas,Water,Media,Note,StartDate,EndDate,RegisteredDate,SetUpDate,Housemates")] HouseModel houseModel)
        {
            if (ModelState.IsValid)
            {
                unitOfWork.ActiveHouses.Update(houseModel);
                unitOfWork.Save();
                return RedirectToAction("Index");
            }
            return View(houseModel);
        }

        // POST: Admin/Houses/Archive/5
        [HttpPost]
        public ActionResult Archive(int id)
        {
            unitOfWork.ActiveHouses.Archive(id);
            unitOfWork.Save();

            return Json("success");
        }

        // POST: Admin/Houses/Unarchive/5
        [HttpPost]
        public ActionResult Unarchive(int id)
        {
            unitOfWork.ArchivedHouses.Unarchive(id);
            unitOfWork.Save();

            return Json("success");
        }

        // POST: Admin/Houses/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult RemoveArchived(int id)
        {
            unitOfWork.ArchivedHouses.Delete(id);
            unitOfWork.Save();

            return Json("success");
        }

        // POST Admin/NewHouses
        [HttpPost]
        public JsonResult RemoveTenantFromHouse(string tenantID, int houseID)
        {
            User tenant = unitOfWork.UserRepository.GetByID(tenantID);
            try
            {
                unitOfWork.ActiveHouses.RemoveTenant(tenant, houseID);
                unitOfWork.Save();
                return Json(true);
            }
            catch
            {
                return Json(false);
            }
        }


        // POST Admin/NewHouses
        [HttpPost]
        public JsonResult MergeHouses(int[] houses)
        {
            HouseService _service = new HouseService(unitOfWork);
            try
            {
                _service.MergeHouses(houses);

                return Json(true);
            }
            catch (Exception e)
            {
                return Json(e.ToString());
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }


        private static string GetCSV(List<EazyCollectPayment> list)
        {
            StringBuilder csvData = null;
            try
            {
                csvData = new StringBuilder();

                //Get the properties for type T for the headers
                PropertyInfo[] propInfos = typeof(EazyCollectPayment).GetProperties();
                for (int i = 0; i <= propInfos.Length - 1; i++)
                {
                    csvData.Append(propInfos[i].Name);
                    csvData.Append(",");
                }

                csvData.AppendLine();

                //Loop through the collection, then the properties and add the values
                for (int i = 0; i <= list.Count - 1; i++)
                {
                    EazyCollectPayment item = list[i];
                    for (int j = 0; j <= propInfos.Length - 1; j++)
                    {
                        object csvProperty = item.GetType().GetProperty(propInfos[j].Name).GetValue(item, null);
                        if (csvProperty != null)
                        {
                            string value = csvProperty.ToString();
                            //Check if the value contans a comma and place it in quotes if so
                            if (value.Contains(","))
                            {
                                value = string.Concat("\"", value, "\"");
                            }
                            //Replace any \r or \n special characters from a new line with a space
                            if (value.Contains("\r"))
                            {
                                value = value.Replace("\r", " ");
                            }
                            if (value.Contains("\n"))
                            {
                                value = value.Replace("\n", " ");
                            }
                            csvData.Append(value);
                        }
                        csvData.Append(",");
                    }

                    csvData.AppendLine();
                }
            }
            catch (Exception exGeneral)
            {
                //Exception
            }
            return csvData.ToString();
        }


    }


    public class EazyCollectPayment
    {
        public String DirectDebitRef { get; set; }
        public String Amount { get; set; }
        public String Due { get; set; }
        public String Comment { get; set; }
    }
}
