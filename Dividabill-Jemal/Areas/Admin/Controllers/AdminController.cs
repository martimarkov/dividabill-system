﻿using Dividabill.Areas.Admin.ViewModels;
using Dividabill.DAL;
using Dividabill.Helpers;
using Dividabill.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Dividabill.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public abstract class AdminController : Controller
    {
        protected UnitOfWork unitOfWork = new UnitOfWork();

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var manager = new UserManager<User>(new UserStore<User>(new ApplicationDbContext()));
            var currentUser = manager.FindById(User.Identity.GetUserId());
            var user = new UserMinimalViewModel
            {
                FirstName = currentUser.FirstName,
                LastName = currentUser.LastName
            };

            var notifications = new List<INotificationViewModel>();

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                UrlHelper u = new UrlHelper(this.ControllerContext.RequestContext);
                foreach (HouseModel house in db.Houses.Where(h => h.RegisteredDate >= currentUser.LastActivityDate).OrderByDescending(h => h.RegisteredDate))
                {
                    notifications.Add(new NotificationViewModel
                    {
                        Type = "label-success",
                        Title = "New house registered",
                        Created = Functions.TimeAgo(house.RegisteredDate),
                        Link = u.Action("Details", "Houses", new { @id = house.ID }),
                        Action = "fa-plus"

                    });
                }

                //foreach (HouseModel house in db.Users.Where(h => h. >= currentUser.LastActivityDate))
                //{
                //    notifications.Add(new NotificationViewModel
                //    {
                //        Type = NotificationType.Success,
                //        Title = "New client registered to a house",
                //        Created = house.RegisteredDate,
                //        Link = u.Action("Details", "Houses", new { Area = "Admin", @id = house.ID }),
                //        Action = "fa-add"

                //    });
                //}

            }
            filterContext.Controller.ViewBag.AdminLeftMenuViewModel = new AdminLeftMenuViewModel
            {
                User = user,
                Notifications = notifications
            };

            base.OnActionExecuting(filterContext);
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {

            base.OnActionExecuted(filterContext);
        }
    }
}