﻿using DividaBill.DAL;
using DividaBill.Models;
using Postal;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DividaBill.ScheduledTasks
{
    public class SendAccountsTransferredJob : IJob
    {
        private UnitOfWork _unitofwork = new UnitOfWork();

        public void Execute(IJobExecutionContext context)
        {
            JobDataMap dataMap = context.JobDetail.JobDataMap;

            int houseId = dataMap.GetInt("houseId");
            HouseModel house = _unitofwork.ActiveHouses.GetByID(houseId);

            dynamic accountsTransferredEmail = new Email("AccountsTransferred");
            accountsTransferredEmail.Subject = "Your bills have been switched to DividaBill";
            foreach (User tenant in house.Tenants)
            {
                accountsTransferredEmail.To = tenant.Email;
                accountsTransferredEmail.Send();
            }
        }
    }
}