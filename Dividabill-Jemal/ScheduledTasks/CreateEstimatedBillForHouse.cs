﻿using DividaBill.DAL;
using DividaBill.Models;
using Postal;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DividaBill.ScheduledTasks
{
    public class CreateEstimatedBillForHouse : IJob
    {
        private UnitOfWork _unitofwork = new UnitOfWork();

        public void Execute(IJobExecutionContext context)
        {
            JobDataMap dataMap = context.JobDetail.JobDataMap;

            int houseId = dataMap.GetInt("houseId");
            HouseModel house = _unitofwork.ActiveHouses.GetByID(houseId);


            //decimal paid = house.GetHousePaid(DateTime.Now);


            //First bill by estimate

            //Adjust estimates by real bill

            //



            //Send Email
            dynamic accountsTransferredEmail = new Email("EstimatedBill");
            accountsTransferredEmail.Subject = "Your estimated bill is ready";
            foreach (User tenant in house.Tenants)
            {
                accountsTransferredEmail.To = tenant.Email;
                accountsTransferredEmail.Send();
            }
        }
    }
}