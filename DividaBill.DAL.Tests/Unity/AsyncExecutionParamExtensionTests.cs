﻿using System;
using DividaBill.DAL.Interfaces;
using DividaBill.DAL.Unity;
using Moq.AutoMock;
using NUnit.Framework;

namespace DividaBill.DAL.Tests.Unity
{
    [TestFixture]
    public class AsyncExecutionParamExtensionTests
    {
        [SetUp]
        public void SetUp()
        {
            _mocker = new AutoMocker();
        }

        private AutoMocker _mocker;

        [Test]
        public void GivenAsyncUnitOfWork_WhenAssignThreadSafeUnitOfWorkInvoked_ThenAsyncUnitOfWorkReturned()
        {
            var unitOfWorkAsync = _mocker.GetMock<IUnitOfWork>();
            var asyncExecutionParam = new AsyncExecutionParam(unitOfWorkAsync.Object);

            var unitOfWorkFromIoc = _mocker.GetMock<IUnitOfWork>();
            var result = asyncExecutionParam.AssignThreadSafeUnitOfWork(unitOfWorkFromIoc.Object);

            Assert.AreSame(unitOfWorkAsync.Object, result.UnitOfWork);
        }

        [Test]
        public void GivenBothNullUnitOfWorks_WhenAssignThreadSafeUnitOfWorkInvoked_ThenExceptionInvoked()
        {
            Assert.Throws<Exception>(() => { ((AsyncExecutionParam) null).AssignThreadSafeUnitOfWork(null); });
        }

        [Test]
        public void GivenNullAsyncUnitOfWork_WhenAssignThreadSafeUnitOfWorkInvoked_ThenIocUnitOfWorkReturned()
        {
            var unitOfWorkFromIoc = _mocker.GetMock<IUnitOfWork>();
            var result = ((AsyncExecutionParam) null).AssignThreadSafeUnitOfWork(unitOfWorkFromIoc.Object);

            Assert.AreSame(unitOfWorkFromIoc.Object, result.UnitOfWork);
        }

        [Test]
        public void GivenNullIocUnitOfWork_WhenAssignThreadSafeUnitOfWorkInvoked_ThenAsyncUnitOfWorkReturned()
        {
            var unitOfWorkAsync = _mocker.GetMock<IUnitOfWork>();
            var asyncExecutionParam = new AsyncExecutionParam(unitOfWorkAsync.Object);

            var result = asyncExecutionParam.AssignThreadSafeUnitOfWork(null);

            Assert.AreSame(unitOfWorkAsync.Object, result.UnitOfWork);
        }
    }
}