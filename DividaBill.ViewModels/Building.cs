﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DividaBill.ViewModels
{
    public class BuildingViewModel
    {
        public double Udprn { get; set; }
        public String Company { get; set; }
        public String Department { get; set; }
        public String Line1 { get; set; }
        public String Line2 { get; set; }
        public String Line3 { get; set; }
        public String Line4 { get; set; }
        public String Line5 { get; set; }
        public String PostTown { get; set; }
        public String County { get; set; }
        public String PostCode { get; set; }
        public int Mailsort { get; set; }
        public String Barcode { get; set; }
        public String Type { get; set; }
        public String DeliveryPointSuffix { get; set; }
        public String SubBuilding { get; set; }
        public String BuildingName { get; set; }
        public String BuildingNumber { get; set; }
        public String PrimaryStreet { get; set; }
        public String SecondaryStreet { get; set; }
        public String DoubleDependentLocality { get; set; }
        public String DependentLocality { get; set; }
        public String PoBox { get; set; }
        public String PrimaryStreetName { get; set; }
        public String PrimaryStreetType { get; set; }
        public String SecondaryStreetName { get; set; }
        public String SecondaryStreetType { get; set; }
        public String CountryName { get; set; }
        public String CountryISO2 { get; set; }
        public String CountryISO3 { get; set; }

    }
}