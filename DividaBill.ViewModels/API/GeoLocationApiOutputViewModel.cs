﻿using DividaBill.ViewModels.Abstractions;

namespace DividaBill.ViewModels.API
{
    public class GeoLocationApiOutputViewModel : IApiOutputViewModel
    {
        public string PostCode { get; set; }
    }
}