﻿using System.Collections.Generic;
using DividaBill.ViewModels.Abstractions;

namespace DividaBill.ViewModels.API
{
    public class ForceRegenerateGcMandateApiOutputViewElement
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }

    public class ForceRegenerateGcMandateApiOutputViewModel : IApiOutputViewModel
    {
        public List<ForceRegenerateGcMandateApiOutputViewElement> Successful { get; set; }
        public List<ForceRegenerateGcMandateApiOutputViewElement> Failed { get; set; }
    }
}