﻿using DividaBill.ViewModels.Abstractions;

namespace DividaBill.ViewModels.API
{
    public class UpdateAllMandatesApiOutputViewModel : IApiOutputViewModel
    {
        public int Successful { get; set; }
        public int Failed { get; set; }
    }
}