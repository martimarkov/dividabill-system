using System;
using System.ComponentModel.DataAnnotations;
using DividaBill.ViewModels.Abstractions;

namespace DividaBill.ViewModels
{
    public class ContractRemoveViewModel : IViewModel
    {
        [Required]
        public int ID { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required]
        [Display(Name = "Requested Start Date")]
        public DateTimeOffset RequestedStartDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required]
        [Display(Name = "End Date")]
        public DateTimeOffset EndDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Actual Start Date")]
        public DateTimeOffset? StartDate { get; set; }
    }

}