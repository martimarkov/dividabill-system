using System;
using System.ComponentModel.DataAnnotations;
using DividaBill.Models;
using DividaBill.ViewModels.Abstractions;

namespace DividaBill.ViewModels
{
    public class TenancyViewModel : IViewModel
    {
        [Required]
        public int TenancyId { get; set; }

        [Required]
        public string UserId { get; set; }

        public User User { get ; set; }

        public HouseModel HouseModel { get; set; }

        public int HouseId { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Joining Date")]
        public DateTime? JoiningDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Leaving Date")]
        public DateTime? LeavingDate { get; set; }

        [DataType(DataType.MultilineText)]
        public string Notes { get; set; }

        public bool IsActive => (JoiningDate == null || JoiningDate < DateTime.UtcNow) &&
                                (LeavingDate == null || LeavingDate > DateTime.UtcNow);
    }
    }
