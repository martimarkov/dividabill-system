﻿using System;
using System.ComponentModel.DataAnnotations;
using DividaBill.Models;

namespace DividaBill.ViewModels
{
    public class ContractViewModel
    {
        [Required]
        public int ID { get; set; }

        [Required]
        [Display(Name = "House ID")]
        public int House_ID { get; set; }

        [Required]
        [Display(Name = "Provider")]
        public int? Provider_ID { get; set; }

        [Display(Name = "Service")]
        public int Service { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required]
        [Display(Name = "Requested Start Date")]
        public DateTimeOffset RequestedStartDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Actual Start Date")]
        public DateTimeOffset? StartDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required]
        [Display(Name = "End Date")]
        public DateTimeOffset EndDate { get; set; }

        [Required]
        [Display(Name = "Length")]
        public ContractLength Length { get; set; }

        [Display(Name = "Package")]
        public int PackageRaw { get; set; } = 0;

        public User TriggeringTenant { get; set; }

        [Display(Name = "Requested Tenant")]
        public string RequestedTenantId { get; set; }

        [Display(Name = "Submission status")]
        public ProviderSubmissionStatus SubmissionStatus { get; set; }
    }
}