﻿using DividaBill.Library;
using DividaBill.Models;
using DividaBill.Models.Common;
using DividaBill.ViewModels.Abstractions;

namespace DividaBill.ViewModels
{
    public class HouseDetailsViewModel : IViewModel
    {
        public int Housemates { get; set; }
        public SimpleAddress Address { get; set; }
        public bool Electricity { get; set; }
        public bool Gas { get; set; }
        public bool Water { get; set; }
        public bool Media { get; set; }


        public bool Broadband { get; set; }

        public bool SkyTV { get; set; }

        public bool NetflixTV { get; set; }

        public bool LandlinePhone { get; set; }

        public bool TVLicense { get; set; }

        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Coupon { get; set; }
        public BroadbandType BroadbandType { get; set; }
        public LandlineType LandlinePhoneType { get; set; }
        public int Period { get; set; }

        public static HouseDetailsViewModel Create(HouseModel existingHouse)
        {
            return new HouseDetailsViewModel
            {
                Address = existingHouse.Address,
                Coupon = existingHouse.Coupon == null ? "" : existingHouse.Coupon.Code,
                Electricity = existingHouse.WantsElectricity,
                Water = existingHouse.WantsWater,
                Gas = existingHouse.WantsGas,
                NetflixTV = existingHouse.WantsNetflixTV,
                SkyTV = existingHouse.WantsSkyTV,
                TVLicense = existingHouse.WantsTVLicense,
                LandlinePhone = existingHouse.WantsLandlinePhone,
                Broadband = existingHouse.WantsBroadband,
                EndDate = string.Format("{0:dd/MM/yyyy}", existingHouse.EndDate),
                Period = Functions.MonthsBetweenTwoDates(existingHouse.StartDate, existingHouse.EndDate),
                Housemates = existingHouse.Housemates,
                StartDate = string.Format("{0:dd/MM/yyyy}", existingHouse.StartDate),
                BroadbandType = existingHouse.GetRequestedContract<BroadbandContract>()?.Speed ?? BroadbandType.None,
                LandlinePhoneType = existingHouse.GetRequestedContract<LandlinePhoneContract>()?.Package ?? LandlineType.None
            };
        }
    }
}