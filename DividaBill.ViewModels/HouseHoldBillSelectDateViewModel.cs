﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace DividaBill.ViewModels
{
    public class HouseHoldBillSelectDateViewModel
    {
        public IEnumerable<SelectListItem> Months
        {
            get
            {
                return DateTimeFormatInfo
                       .InvariantInfo
                       .MonthNames
                       .Select((monthName, index) => new SelectListItem
                       {
                           Value = (index + 1).ToString(),
                           Text = monthName
                       });
            }
        }

        [Display(Name = "Bill Month")]
        public int Month { get; set; }

        public IEnumerable<SelectListItem> Years { get; set; }

        [Display(Name = "Year Month")]
        public int Year { get; set; }
    }
}