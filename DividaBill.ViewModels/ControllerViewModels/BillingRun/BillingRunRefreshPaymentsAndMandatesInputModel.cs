﻿using GoCardless_API;

namespace DividaBill.ViewModels.ControllerViewModels.BillingRun
{
    public class BillingRunRefreshPaymentsAndMandatesInputModel
    {
        public GoCardless.Environments Environment { get; set; }
    }
}