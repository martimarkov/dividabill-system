﻿using System.Collections.Generic;
using DividaBill.Models.Business;

namespace DividaBill.ViewModels.ControllerViewModels.Dashboard
{
    public class DashboardIndexViewModel
    {
        public List<MonthlyBillingReport> MonthlyBillingReports { get; set; }
        public int RegisteredHousesToday { get; set; }
        public int ActiveHouses { get; set; }
        public int TotalHouses { get; set; }
        public int YesterdayHouses { get; set; }
        public int LastWeekHouses { get; set; }
    }
}