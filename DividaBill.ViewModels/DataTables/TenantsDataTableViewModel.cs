using System;
using DividaBill.ViewModels.Abstractions;
using Mvc.JQuery.DataTables;

namespace DividaBill.ViewModels.DataTables
{
    public class TenantsDataTableViewModel : IViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        [DataTables(DisplayName = "Date of Birth")]
        public DateTime DOB { get; set; }
        public int? HouseId { get; set; }
        public string Id { get; set; }
        public string AccountNumber { get; set; }
        public string AccountSortCode { get; set; }
        public DateTime SignupDate { get; set; }
        [DataTables(DisplayName = "Options", Searchable = false, Sortable = false)]
        public string Options { get; set; }
    }
}