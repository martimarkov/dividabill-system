﻿using System;
using DividaBill.ViewModels.Abstractions;
using Mvc.JQuery.DataTables;

namespace DividaBill.ViewModels.DataTables
{
    public class TenantsViewModel : IViewModel
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        [DataTables(DisplayName = "Date of Birth")]
        public DateTime DOB { get; set; }

        public bool Locked { get; set; }

        [DataTables(DisplayName = "Options", Searchable = false, Sortable = false)]
        public string Options { get; set; }
    }
}