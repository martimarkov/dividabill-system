﻿using DividaBill.ViewModels.Abstractions;
using Mvc.JQuery.DataTables;

namespace DividaBill.ViewModels.DataTables
{
    public class HousesToBeBilledViewModel : IViewModel
    {
        public int ID { get; set; }

        public string Address { get; set; }

        public int Housemates { get; set; }

        [DataTablesFilter(DataTablesFilterType.NumberRange)]
        public int RegisteredTenants { get; set; }

        [DataTables(DisplayName = "Already added")]
        public bool IsAlreadyInQueue { get; set; }

        [DataTables(DisplayName = "Options", Searchable = false, Sortable = false)]
        public string Options { get; set; }
    }
}