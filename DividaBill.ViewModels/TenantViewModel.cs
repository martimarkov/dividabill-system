﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DividaBill.Models.Common;
using DividaBill.ViewModels.Abstractions;

namespace DividaBill.ViewModels
{
    public class TenantViewModel : IViewModel
    {
        //Personal tab
        [Required]
        public string Id { get; set; }

        [Required]
        [Display(Name = "Title")]
        public string RefTitle { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        [DataType(DataType.EmailAddress)]
        [StringLength(256)]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Contact Number")]
        [Phone]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        public string DDRef { get; set; } //uneditable field

        [Required]
        [Display(Name = "Date of Birth")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DOB { get; set; }

        [Required]
        public bool NewsletterSubscription { get; set; }

        //Address tab
        public SimpleAddress HomeAddress { get; set; } //uneditable field

        //Bank tab
        [Required]
        [Display(Name = "Account Name")]
        public string AccountHolder { get; set; }

        [Required]
        [Display(Name = "Account Number")]
        [StringLength(8, ErrorMessage = "Invalid account number code.")]
        public string AccountNumber { get; set; }

        [Required]
        [Display(Name = "Account Sort Code")]
        [StringLength(6, ErrorMessage = "Invalid sort code.")]
        public string AccountSortCode { get; set; }

        [Required]
        public bool AccountSoleHolder { get; set; }

        //Note tab
        [DataType(DataType.MultilineText)]
        public string Note { get; set; }

        //House tab
        [Required]
        [Display(Name = "House")]
        public int House_ID { get; set; }

        public string FullName => $"{RefTitle} {FirstName} {LastName}";
        public DateTime SignupDate { get; set; }
        public string GoCardlessCustomerID { get; set; }
        public bool LockoutEnabled { get; set; }
    }
}