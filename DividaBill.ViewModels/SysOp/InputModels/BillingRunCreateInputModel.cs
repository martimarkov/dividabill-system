﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DividaBill.ViewModels.Abstractions;

namespace DividaBill.ViewModels.SysOp.InputModels
{
    public class BillingRunCreateInputModel : IViewModel
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Period { get; set; }

        [Required]
        [Display(Name = "SendGrid template ID")]
        public string EmailTemplateId { get; set; }

        [Required]
        [Display(Name = "Email subject")]
        public string EmailSubject { get; set; }

        [Display(Name = "Send from email Display Name")]
        public string SendFromEmailDisplayName { get; set; }

        [Required]
        [Display(Name = "Send from email")]
        [EmailAddress]
        [DataType(DataType.EmailAddress)]
        public string SendFromEmail { get; set; }
    }
}