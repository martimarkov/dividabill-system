﻿using System;
using System.Linq;
using DividaBill.Library;
using DividaBill.Models.Sheets;

namespace DividaBill.ViewModels.GDrive
{
    public class GDriveFileViewModel
    {
        public string Name { get; private set; }

        public string OurLastChangeDate { get; private set; }

        public string SupplierLastChangeDate { get; private set; }

        public string OurLastChangesInfo { get; private set; }

        public string SupplierLastChangesInfo { get; private set; }

        public GDriveFileViewModel(File f)
        {
            Name = f.FileName;

            // Changes by DividaBill
            var ourLastRev = f.Revisions
                .Where(r => r.WasDoneByUs)
                .ArgMax(r => r.CreationDate);

            if (ourLastRev != null)
            {
                OurLastChangeDate = ourLastRev.CreationDate.ToString();

                var nOurAdds = ourLastRev.ChangedRows.Count(ch => ch.ChangeType == RowChangeEvent.Added);
                var nOurEdits = ourLastRev.ChangedRows.Count(ch => ch.ChangeType == RowChangeEvent.Edited);
                var nOurDeletes = ourLastRev.ChangedRows.Count(ch => ch.ChangeType == RowChangeEvent.Removed);
                OurLastChangesInfo = "{0} additions, {1} edits, {2} deletes".F(nOurAdds, nOurEdits, nOurDeletes);
            }
            else
            {
                OurLastChangesInfo = "N/A";
                OurLastChangeDate = "N/A";
            }

            // Changes by Supplier
            var supplierLastRev = f.Revisions
                .Where(r => r.WasDoneByUs == false)
                .ArgMax(r => r.CreationDate);

            if(supplierLastRev != null)
            {
                SupplierLastChangeDate = supplierLastRev.CreationDate.ToString() ?? "N/A";

                var nSupplierAdds = supplierLastRev.ChangedRows.Count(ch => ch.ChangeType == RowChangeEvent.Added);
                var nSupplierEdits = supplierLastRev.ChangedRows.Count(ch => ch.ChangeType == RowChangeEvent.Edited);
                var nSupplierDeletes = supplierLastRev.ChangedRows.Count(ch => ch.ChangeType == RowChangeEvent.Removed);
                SupplierLastChangesInfo = "{0} additions, {1} edits, {2} deletes".F(nSupplierAdds, nSupplierEdits, nSupplierDeletes);
            }
            else
            {
                SupplierLastChangesInfo = "N/A";
                SupplierLastChangeDate = "N/A";
            }
        }
    }
}