﻿using System.Web.Mvc;
using DividaBill.Exceptions;

namespace DividaBill
{
    public static class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new CustomExceptionAttribute());

#if !DEBUG
            filters.Add(new RequireHttpsAttribute());
#endif
        }
    }
}