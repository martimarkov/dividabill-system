﻿using System.ComponentModel.DataAnnotations;

namespace DividaBill.Models.ViewModels
{
    public class WorkWithUsViewModel
    {
        [Required]
        [Display(Name = "Your Name (required)")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Your Email (required)")]
        [EmailAddress]
        public string Email { get; set; }

        [Display(Name = "Subject")]
        public string Subject { get; set; }

        [Display(Name = "Your Message")]
        public string MessageText { get; set; }
    }
}