﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DividaBill.Models.ViewModels
{
    public class InviteHousematesViewModel
    {
        [Display(Name="Insert your house mates emails below searated by a space:")]
        public String emails { get; set; }
    }

    public class ReferViewModel
    {
        [Display(Name = "Insert your friend emails below searated by a space:")]
        public String emails { get; set; }
    }

    public class EmailInvitationViewModel
    {
        public ReferViewModel referViewModel { get; set; }
        public InviteHousematesViewModel housematesViewModel { get; set; }
    }
}