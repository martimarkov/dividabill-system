﻿using System;
using System.Net.Mime;
using System.Web.Mvc;
using System.Web.UI;
using DividaBill.DAL;
using DividaBill.DAL.DbContext;
using DividaBill.Models;
using DividaBill.Models.ViewModels;

namespace DividaBill.Controllers
{
    //[RequireHttps]
    public class HomeController : Controller
    {
        [OutputCache(Location = OutputCacheLocation.Any, Duration = 36000)]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        public ActionResult BillingSupport()
        {
            return View();
        }

        public ActionResult Services()
        {
            return View();
        }

        public ActionResult Privacy()
        {
            return View();
        }

        public ActionResult AboutUs()
        {
            return View();
        }

        public ActionResult ContactUs()
        {
            return View();
        }

        public ActionResult Partners()
        {
            return RedirectToActionPermanent("Business");
        }

        public ActionResult Business()
        {
            return View();
        }

        public ActionResult WhyItWorks()
        {
            return View();
        }

        public ActionResult Pricing()
        {
            return RedirectToActionPermanent("Services");
        }

        public ActionResult Questions()
        {
            return View();
        }

        public ActionResult TermsOfUse()
        {
            return View();
        }

        public ActionResult Customers()
        {
            return View();
        }

        public ActionResult WhosItFor()
        {
            return RedirectToActionPermanent("Customers");
        }

        public ActionResult Blog()
        {
            return View();
        }

        public ActionResult Error()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult WorkWithUs(WorkWithUsViewModel msg)
        {
            using (var db = new ApplicationDbContext())
            {
                var newMsg = new Message {Email = msg.Email, MessageText = msg.MessageText, Name = msg.Name, Subject = msg.Subject, Created = DateTime.Now};
                db.Messages.Add(newMsg);
                db.SaveChanges();
            }
            return View("Success");
        }

        public ActionResult Terms()
        {
            //Response.AppendHeader("Content-Disposition", "inline; filename=something.pdf");
            //ActionResultreturn File("something.pdf", "application/pdf",);

            return File("~/Content/Documents/TermsAndConditionsV2.pdf", System.Net.Mime.MediaTypeNames.Application.Pdf);
        }

        public ActionResult DirectDebit()
        {
            //Response.AppendHeader("Content-Disposition", "inline; filename=something.pdf");
            //ActionResultreturn File("something.pdf", "application/pdf",);

            return File("~/Content/Documents/direct-debit-guarantee.png", "image/png");
        }

        public ActionResult About()
        {
            return RedirectToActionPermanent("AboutUs");
        }

        public ActionResult FAQ()
        {
            return RedirectToActionPermanent("Questions");
        }
    }
}