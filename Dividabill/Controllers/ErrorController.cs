﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DividaBill.Library;

namespace DividaBill.Controllers
{
    public class ErrorController : Controller
    {
        [ActionName("404")]
        public ActionResult err404()
        {
            var url = Request.Params["aspxerrorpath"];
            if (!string.IsNullOrEmpty(url))
            {
                ViewBag.Message = "The web page '{0}' does not seem to exist!".F(url);
                return View("ErrorBase");
            }
            else
                return RedirectToAction("Index", "Home");

        }
    }
}