﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using DividaBill.Models;
using DividaBill.Services.Auth;
using DividaBill.Services.Emails;
using DividaBill.Services.Interfaces;
using DividaBill.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;

//using Postal;

namespace DividaBill.Controllers
{
    //[RequireHttps]
    [Authorize]
    public class AccountController : Controller
    {
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";
        private readonly IEmailService _emailService;
        private readonly ApplicationUserManager _userManager;
        private readonly IBankAccountRegistrationService _bankAccountRegistrationService;


        public AccountController(IEmailService emailService,
            ApplicationUserManager userManager, IBankAccountRegistrationService bankAccountRegistrationService)
        {
            _bankAccountRegistrationService = bankAccountRegistrationService;
            _emailService = emailService;
            _userManager = userManager;
        }

        private IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication;


        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login()
        {
            return RedirectToAction("Login", "Home", null);
        }

        //
        // GET: /Account/SignUp
        [AllowAnonymous]
        public ActionResult SignUp()
        {
            var wizard = new SignUpViewModel();
            wizard.StartTime = DateTime.Now;
            wizard.DoB = new DateTime(1993, 4, 5);
            wizard.StartTime = DateTime.Today.AddDays(15);
            wizard.Gas = false;
            wizard.Electricity = false;
            wizard.Water = false;
            wizard.NetflixTV = false;
            wizard.Broadband = false;
            wizard.LandlinePhone = false;
            wizard.SkyTV = false;
            wizard.TVLicense = false;
            wizard.TV = false;
            return View(wizard);
        }


        //
        // GET: /Account/GetQuote
        [AllowAnonymous]
        public ActionResult GetQuote()
        {
            return RedirectToActionPermanent("Signup");
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(SignUpViewModel wizard)
        {
            // validate base model
            if (!ModelState.IsValid)
                return View("SignUp", wizard); // return bad request

            //register the account
            var registerResult = await _bankAccountRegistrationService.RegisterAccount(_userManager, wizard);

            if (registerResult.Errors.Any())
            {
                wizard.ValidationErrors = new Dictionary<string, string>();
                foreach (var uiError in registerResult.Errors)
                {
                    if (!wizard.ValidationErrors.ContainsKey(uiError.Key))
                    {
                        ModelState.AddModelError(uiError.Key, uiError.ErrorMessage);
                        wizard.ValidationErrors.Add(uiError.Key, uiError.ErrorMessage);
                    }
                }
                return View("SignUp", wizard); // return bad request
            }

            if (registerResult.User == null)
                return View("SignUp", wizard);
            await SignInAsync(registerResult.User, false); // return 400 status ????

            //make an email-task
            await Task.Run(async () =>
            {
                var confirmCode = await _userManager.GenerateEmailConfirmationTokenAsync(registerResult.User.Id);

                // send an e-mail confirmation e-mail
                var callbackUrl = Url.Action("ConfirmEmail", "Account",
                    new {userId = registerResult.User.Id, code = confirmCode}, Request.Url?.Scheme);
                await _emailService.SendSignUpConfirmationEmail(registerResult.User, callbackUrl);

                //send a user registered e-mail
                await _emailService.SendUserRegisteredEmail(registerResult.User, !registerResult.HouseExisted);
            });

            return RedirectToAction("Confirmation", "Account");
        }


        //
        // GET: /Account/ConfirmEmail
        public ActionResult Confirmation()
        {
            return View();
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }

            var result = await _userManager.ConfirmEmailAsync(userId, code);
            if (result.Succeeded)
            {
                return View("ConfirmEmail");
            }
            AddErrors(result);
            return View();
        }

        private async Task SignInAsync(User user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);

            var claimsId = await user.GenerateUserIdentityAsync(_userManager);
            AuthenticationManager.SignIn(new AuthenticationProperties {IsPersistent = isPersistent}, claimsId);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
                ModelState.AddModelError("", error);
        }

        private class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; }
            public string RedirectUri { get; }
            public string UserId { get; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties {RedirectUri = RedirectUri};
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
    }
}