﻿using System.Data.Entity.Infrastructure;
using DividaBill.DAL.DbContext;
using DividaBill.DAL.Interfaces;

namespace DividaBill.DAL
{
    // TODO: make non-public so exposed only thru the interfaces. 
    public class UnitOfWork : UnitOfWorkBase, ImbaUnitOfWork
    {
        public UnitOfWork(string connectionString, bool readOnly = false)
        {
            context = new ApplicationDbContext(connectionString, readOnly);
            ((IObjectContextAdapter) context).ObjectContext.CommandTimeout = 900;
            referalDB = new ReferalDbContext();
            //this.storedProcsContext = new Entities();

            Initialize();
        }

        public UnitOfWork()
        {
            context = new ApplicationDbContext();
            ((IObjectContextAdapter) context).ObjectContext.CommandTimeout = 900;
            referalDB = new ReferalDbContext();
            //this.storedProcsContext = new Entities();
            Initialize();
        }

        public UnitOfWork(IApplicationContext appContext, IReferalContext refContext, IStoredProcsContext storedProcsContext)
        {
            //this.storedProcsContext = storedProcsContext;
            context = appContext;
            referalDB = refContext;
            //this.warehouseContext = warehouseContext;

            Initialize();
        }
    }
}