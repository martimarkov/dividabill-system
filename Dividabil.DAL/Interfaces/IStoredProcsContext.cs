﻿using System;
using System.Data.Entity.Core.Objects;
using DividaBill.DAL.StoredProcs;

namespace DividaBill.DAL.Interfaces
{
    public interface IStoredProcsContext
    {
        ObjectResult<sp_Monthly_Billing_Report_Result> sp_Monthly_Billing_Report(Nullable<System.DateTime> startDate, Nullable<System.DateTime> endDate);
        ObjectResult<sp_goCardlessExport_Result> sp_goCardlessExport(Nullable<int> billingRunId);
    }
}