﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DividaBill.DAL.Interfaces
{
    interface IArchivedHousesRepository<TEntity, DBContext> : IHousesRepository<TEntity, DBContext>
        where TEntity : class
        where DBContext : IApplicationContext
    {

    }
}