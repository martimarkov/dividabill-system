﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DividaBill.DAL.Interfaces;

namespace DividaBill.DAL
{
    interface IActiveHousesRepository<TEntity, DBContext> : IHousesRepository<TEntity, DBContext>
        where TEntity : class
        where DBContext : IApplicationContext
    {
        void Archive(int houseId);
    }
}
