﻿using DividaBill.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using DividaBill.DAL.Interfaces;

namespace DividaBill.DAL
{
    interface IHousesRepository<TEntity, DBContext> : IGenericRepository<TEntity, User, DBContext>
        where TEntity : class
        where DBContext : IDividaBillWebContext
    {
        IEnumerable<TEntity> GetHouses();
        IQueryable<TEntity> GetHouses(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, string includeProperties = "");
        TEntity GetHouseByUser(User user);
    }
}