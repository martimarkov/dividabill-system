﻿using DividaBill.Models;
using System;
using System.Collections.Generic;
namespace DividaBill.DAL
{
    public interface IUserRepository
    {
        void Archive(int userId);

        void AddToHouse(User tenant, HouseModel house);
    }
}
