﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using DividaBill.DAL.StoredProcs;
using DividaBill.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using TrackerEnabledDbContext.Common.Models;

namespace DividaBill.DAL.Interfaces
{
    public interface IApplicationContext : IHousesStorage
    {
        DbSet<Message> Messages { get; }
        DbSet<Premise> Premises { get; }
        DbSet<Building> Buildings { get; }
        DbSet<MediaRequest> MediaRequests { get; }
        DbSet<HouseEstimatedPrice> HouseEstimatedPrices { get; }
        DbSet<PostcodeEstimatedPrice> PostcodeEstimatedPrices { get; }
        DbSet<PostCode> PostCodes { get; }

        DbSet<AuditLog> AuditLog { get; }

        DbContextTransaction BeginTransaction();


        IQueryable<AuditLog> GetLogs(string tableName);

        IQueryable<AuditLog> GetLogs<T>();

        List<sp_Monthly_Billing_Report_Result> sp_Monthly_Billing_Report(DateTime? startDate, DateTime? endDate);
        List<sp_goCardlessExport_Result> sp_goCardlessExport(int billingRunId);

        void SetBulkInsertOptimisationOn();
    }

    public interface IReferalContext : IDbContext<ReferalUser>
    {
    }

    public interface IClientContext : IDbContext<User>
    {
    }

    public interface IHousesStorage : IDividaBillWebContext
    {
        DbSet<HouseModel> Houses { get; set; }
        DbSet<Coupon> Coupons { get; set; }
    }

    public interface IDividaBillWebContext : IDbContext<User>
    {
    }

    public interface IDbContext<UserType>
        where UserType : IdentityUser
    {
        IDbSet<UserType> Users { get; set; }
        DbSet<T> Set<T>() where T : class;

        int SaveChanges();
        Task<int> SaveChangesAsync();
        void Dispose();
        DbEntityEntry Entry(object entity);
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;
    }
}