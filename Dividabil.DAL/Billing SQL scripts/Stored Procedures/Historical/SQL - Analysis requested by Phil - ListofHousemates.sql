/*House id

start date of the house

list of all the tenants living in the house

the start date each tenant signed up to the house

also if it exists the exit date any tenant left the house

also if it exists the cancellation date of the house if they are no longer using our services
*/


SELECT House_ID
, h.housemates as NbofHousemates
, RefTitle as Title
, FirstName
, LastName
, SignUpDate
, h.StartDate AS 'HouseStartDate'
, h.EndDate AS 'HouseEndDate'
, h.RegisteredDate AS 'HouseRegisteredDate'
FROM ASPNETUsers au
inner join housemodels h on au.House_ID = h.ID
WHERE au.house_id != 1270
ORDER BY au.House_ID 