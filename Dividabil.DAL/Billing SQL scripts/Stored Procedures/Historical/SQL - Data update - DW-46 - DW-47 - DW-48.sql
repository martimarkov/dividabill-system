BEGIN TRAN


--- DW-46 --- Coontract update start date 
SELECT DISTINCT House_IDINTO #HouseIDListFROM (SELECT 1280 AS House_IDUNION
SELECT 1280UNION
SELECT 1188UNION
SELECT 1188UNION
SELECT 1188UNION
SELECT 1523UNION
SELECT 1523UNION
SELECT 1527UNION
SELECT 1527UNION
SELECT 1555UNION
SELECT 1555UNION
SELECT 1558UNION
SELECT 1558UNION
SELECT 1573UNION
SELECT 1573UNION
SELECT 1588UNION
SELECT 1591UNION
SELECT 1591UNION
SELECT 1592UNION
SELECT 1592UNION
SELECT 1626UNION
SELECT 1626UNION
SELECT 1637UNION
SELECT 1637UNION
SELECT 1643UNION
SELECT 1643UNION
SELECT 1669UNION
SELECT 1669UNION
SELECT 1687UNION
SELECT 1687UNION
SELECT 1688UNION
SELECT 1688UNION
SELECT 1692UNION
SELECT 1692UNION
SELECT 1695UNION
SELECT 1695UNION
SELECT 1696UNION
SELECT 1696UNION
SELECT 1700UNION
SELECT 1700UNION
SELECT 1706UNION
SELECT 1706UNION
SELECT 1710UNION
SELECT 1710UNION
SELECT 1716UNION
SELECT 1716UNION
SELECT 1716UNION
SELECT 1717UNION
SELECT 1717UNION
SELECT 1724UNION
SELECT 1724UNION
SELECT 1823UNION
SELECT 1823UNION
SELECT 1837UNION
SELECT 1837UNION
SELECT 1840UNION
SELECT 1840UNION
SELECT 1857UNION
SELECT 1857UNION
SELECT 1737UNION
SELECT 1737UNION
SELECT 1755UNION
SELECT 1755UNION
SELECT 1773UNION
SELECT 1773UNION
SELECT 1787UNION
SELECT 1787UNION
SELECT 1809UNION
SELECT 1811UNION
SELECT 1811UNION
SELECT 1809) AS T-- SELECT COUNT(*) FROM #HouseIDListSELECT 'DW-46 - contract start date - before update', c.House_ID	, c.ServiceNew_ID	, st.Name as ServiceType	, c.StartDate	, c.* FROM Contracts c 	INNER JOIN ServiceTypes st ON c.ServiceNew_ID = st.Id	INNER JOIN #HouseIDList AS t ON t.House_ID = c.House_IDORDER BY c.House_ID--Id	Name
--1	Electricity
--2	Gas
--3	Water
--4	Broadband
--5	NetflixTV
--6	SkyTV
--7	TVLicense
--8	LandlinePhone
--9	LineRental-- UPDATE StartDateUPDATE Contracts SET StartDate = '2015-09-30' WHERE ServiceNew_ID IN (9,8,4) AND House_ID = 1188UPDATE Contracts SET StartDate = '2015-10-07' WHERE ServiceNew_ID IN (9,4) AND House_ID = 1280UPDATE Contracts SET StartDate = '2015-09-28' WHERE ServiceNew_ID IN (9,4) AND House_ID = 1523UPDATE Contracts SET StartDate = '2015-10-30' WHERE ServiceNew_ID IN (9,4) AND House_ID = 1527UPDATE Contracts SET StartDate = '2015-10-29' WHERE ServiceNew_ID IN (9,4) AND House_ID = 1555UPDATE Contracts SET StartDate = '2015-09-29' WHERE ServiceNew_ID IN (9,4) AND House_ID = 1558UPDATE Contracts SET StartDate = '2015-09-22' WHERE ServiceNew_ID IN (9,4) AND House_ID = 1573UPDATE Contracts SET StartDate = '2015-11-20' WHERE ServiceNew_ID = 4 AND House_ID = 1588UPDATE Contracts SET StartDate = '2015-10-02' WHERE ServiceNew_ID IN (9,4) AND House_ID = 1591UPDATE Contracts SET StartDate = '2015-09-24' WHERE ServiceNew_ID IN (9,4) AND House_ID = 1592UPDATE Contracts SET StartDate = '2015-10-19' WHERE ServiceNew_ID IN (9,4) AND House_ID = 1626UPDATE Contracts SET StartDate = '2015-09-22' WHERE ServiceNew_ID IN (9,4) AND House_ID = 1637UPDATE Contracts SET StartDate = '2015-10-08' WHERE ServiceNew_ID IN (9,4) AND House_ID = 1643UPDATE Contracts SET StartDate = '2015-10-01' WHERE ServiceNew_ID IN (9,4) AND House_ID = 1669UPDATE Contracts SET StartDate = '2015-10-01' WHERE ServiceNew_ID IN (9,4) AND House_ID = 1687UPDATE Contracts SET StartDate = '2015-09-30' WHERE ServiceNew_ID IN (9,4) AND House_ID = 1688UPDATE Contracts SET StartDate = '2015-10-01' WHERE ServiceNew_ID IN (9,4) AND House_ID = 1692UPDATE Contracts SET StartDate = '2015-10-01' WHERE ServiceNew_ID IN (9,4) AND House_ID = 1695UPDATE Contracts SET StartDate = '2015-09-29' WHERE ServiceNew_ID IN (9,4) AND House_ID = 1696UPDATE Contracts SET StartDate = '2015-10-06' WHERE ServiceNew_ID IN (9,4) AND House_ID = 1700UPDATE Contracts SET StartDate = '2015-10-07' WHERE ServiceNew_ID IN (9,4) AND House_ID = 1706UPDATE Contracts SET StartDate = '2015-10-07' WHERE ServiceNew_ID IN (9,4) AND House_ID = 1710UPDATE Contracts SET StartDate = '2015-10-05' WHERE ServiceNew_ID IN (9,8,4) AND House_ID = 1716UPDATE Contracts SET StartDate = '2015-09-30' WHERE ServiceNew_ID IN (9,4) AND House_ID = 1724UPDATE Contracts SET StartDate = '2015-10-09' WHERE ServiceNew_ID IN (9,4) AND House_ID = 1737UPDATE Contracts SET StartDate = '2015-10-02' WHERE ServiceNew_ID IN (9,4) AND House_ID = 1755UPDATE Contracts SET StartDate = '2015-10-09' WHERE ServiceNew_ID IN (9,4) AND House_ID = 1773UPDATE Contracts SET StartDate = '2015-10-14' WHERE ServiceNew_ID IN (9,4) AND House_ID = 1787UPDATE Contracts SET StartDate = '2015-10-23' WHERE ServiceNew_ID IN (9,4) AND House_ID = 1809UPDATE Contracts SET StartDate = '2015-10-14' WHERE ServiceNew_ID IN (9,4) AND House_ID = 1811UPDATE Contracts SET StartDate = '2015-11-05' WHERE ServiceNew_ID IN (9,4) AND House_ID = 1823UPDATE Contracts SET StartDate = '2015-10-19' WHERE ServiceNew_ID IN (9,4) AND House_ID = 1837UPDATE Contracts SET StartDate = '2015-10-20' WHERE ServiceNew_ID IN (9,4) AND House_ID = 1840UPDATE Contracts SET StartDate = '2015-11-06' WHERE ServiceNew_ID IN (9,4) AND House_ID = 1857SELECT 'DW-46 - contract start date - after update', st.Name as 'ServiceType'	, c.ServiceNew_ID	, c.*FROM Contracts c 	INNER JOIN ServiceTypes st ON C.ServiceNew_ID = st.Id	INNER JOIN #HouseIDList t ON t.House_ID = c.House_IDORDER BY C.IDDROP TABLE #HouseIDList

--- DW-46 --- Create new contracts for existing houses
---- Providers ---
--ID	Name
--1	GB Energy
--2	BT Local Business
--3	British Gas
--4	Origin Broadband
--5	Netflix
--6	Water Company Place Holder
--7	TV License Company Limited
--8	Sky TV
--9	GB Energy
--10	Spark Energy
--11	Origin Broadband
--12	BT Local Business
--13	TV Licensing Company Ltd.
--14	Water company placeholder
--15	Sky Installer Company


--- Service Types ---
--Id	Name	Abbreviation
--1	Electricity	E
--2	Gas	G
--3	Water	W
--4	Broadband	BB
--5	NetflixTV	N
--6	SkyTV	Sky
--7	TVLicense	TVL
--8	LandlinePhone	Ph
--9	LineRental	LR




--- New Service Contracts ---
INSERT INTO Contracts (House_ID,Service,StartDate,EndDate,Length,RequestedBy_ID,Provider_ID,SubmissionStatus,ServiceNew_ID,PackageRaw,RequestedStartDate)
SELECT 1790, 0, '2015-10-20', '2016-10-20', 12, (SELECT TOP 1 RequestedBy_ID FROM Contracts WHERE House_ID = 1790), 11, 0, 4, 0, '2015-10-20'
UNION ALL
SELECT 1790, 0, '2015-10-20', '2016-10-20', 12, (SELECT TOP 1 RequestedBy_ID FROM Contracts WHERE House_ID = 1790), 11, 0, 9, 0, '2015-10-20'
UNION ALL
SELECT 1788, 0, '2015-10-20', '2016-07-20', 9, (SELECT TOP 1 RequestedBy_ID FROM Contracts WHERE House_ID = 1788), 11, 0, 4, 0, '2015-10-20'
UNION ALL
SELECT 1788, 0, '2015-10-20', '2016-07-20', 9, (SELECT TOP 1 RequestedBy_ID FROM Contracts WHERE House_ID = 1788), 11, 0, 9, 0, '2015-10-20'
UNION ALL
SELECT 1223, 0, '2015-10-13', '2016-07-13', 9, (SELECT TOP 1 RequestedBy_ID FROM Contracts WHERE House_ID = 1233), 11, 0, 9, 0, '2015-10-13'
UNION ALL
SELECT 1786, 0, '2015-10-13', '2016-07-13', 9, (SELECT TOP 1 RequestedBy_ID FROM Contracts WHERE House_ID = 1786), 11, 0, 4, 0, '2015-10-13'
UNION ALL
SELECT 1786, 0, '2015-10-13', '2016-07-13', 9, (SELECT TOP 1 RequestedBy_ID FROM Contracts WHERE House_ID = 1786), 11, 0, 9, 0, '2015-10-13'
UNION ALL
SELECT 1752, 0, '2015-10-08', '2016-10-08', 9, (SELECT TOP 1 RequestedBy_ID FROM Contracts WHERE House_ID = 1752), 11, 0, 4, 0, '2015-10-08'
UNION ALL
SELECT 1752, 0, '2015-10-08', '2016-10-08', 9, (SELECT TOP 1 RequestedBy_ID FROM Contracts WHERE House_ID = 1752), 11, 0, 9, 0, '2015-10-08'
UNION ALL
SELECT 1492, 0, '2015-10-07', '2016-09-07', 12, (SELECT TOP 1 RequestedBy_ID FROM Contracts WHERE House_ID = 1492), 11, 0, 9, 0, '2015-10-07'
UNION ALL
SELECT 1728, 0, '2015-10-01', '2016-06-01', 9, (SELECT TOP 1 RequestedBy_ID FROM Contracts WHERE House_ID = 1728), 11, 0, 9, 0, '2015-10-01'
UNION ALL
SELECT 1728, 0, '2015-10-01', '2016-06-01', 9, (SELECT TOP 1 RequestedBy_ID FROM Contracts WHERE House_ID = 1728), 11, 0, 4, 0, '2015-10-01'



SELECT 'New Line Rental Contracts', * FROM Contracts WHERE ServiceNew_ID = 9 AND House_ID IN (1492, 1790, 1788, 1223, 1786, 1752, 1728) ORDER BY House_ID

SELECT 'New Broadband Contracts', * FROM Contracts WHERE ServiceNew_ID = 4 AND House_ID IN (1790, 1788, 1786, 1752, 1728) ORDER BY House_ID


INSERT INTO LineRentalContracts
SELECT TOP 1 ID FROM Contracts WHERE ServiceNew_ID = 9 AND House_ID = 1790
UNION ALL
SELECT TOP 1 ID FROM Contracts WHERE ServiceNew_ID = 9 AND House_ID = 1788
UNION ALL 
SELECT TOP 1 ID FROM Contracts WHERE ServiceNew_ID = 9 AND House_ID = 1223
UNION ALL 
SELECT TOP 1 ID FROM Contracts WHERE ServiceNew_ID = 9 AND House_ID = 1786
UNION ALL 
SELECT TOP 1 ID FROM Contracts WHERE ServiceNew_ID = 9 AND House_ID = 1752
UNION ALL 
SELECT TOP 1 ID FROM Contracts WHERE ServiceNew_ID = 9 AND House_ID = 1492
UNION ALL 
SELECT TOP 1 ID FROM Contracts WHERE ServiceNew_ID = 9 AND House_ID = 1728


INSERT INTO BroadbandContracts
SELECT TOP 1 ID FROM Contracts WHERE ServiceNew_ID = 4 AND House_ID = 1790
UNION ALL
SELECT TOP 1 ID FROM Contracts WHERE ServiceNew_ID = 4 AND House_ID = 1788
UNION ALL
SELECT TOP 1 ID FROM Contracts WHERE ServiceNew_ID = 4 AND House_ID = 1786
UNION ALL
SELECT TOP 1 ID FROM Contracts WHERE ServiceNew_ID = 4 AND House_ID = 1752
UNION ALL
SELECT TOP 1 ID FROM Contracts WHERE ServiceNew_ID = 4 AND House_ID = 1728



--- DW-47 --- Spark contract start date update
--- Electricity ---
SELECT DISTINCT House_ID
INTO #ElectricityHouseIDsList
FROM 
(SELECT 1330 AS House_IDUNION
SELECT 1351UNION
SELECT 1414UNION
SELECT 1441UNION
SELECT 1476UNION
SELECT 1549UNION
SELECT 1607UNION
SELECT 1639UNION
SELECT 1675) as t

SELECT 'DW-47 - Before update - Electricity', 
	c.House_ID
	, c.ServiceNew_ID
	, c.StartDate
	, st.Name as 'Service Type'
FROM Contracts c
	INNER JOIN ServiceTypes st ON c.ServiceNew_ID = st.Id
	INNER JOIN #ElectricityHouseIDsList t ON t.House_ID = c.House_ID AND c.ServiceNew_ID = 1
ORDER BY C.House_ID

UPDATE Contracts SET StartDate = '2015-10-21' WHERE House_ID = 1330 AND ServiceNew_ID = 1
UPDATE Contracts SET StartDate = '2015-11-02' WHERE House_ID = 1351 AND ServiceNew_ID = 1
UPDATE Contracts SET StartDate = '2015-10-21' WHERE House_ID = 1414 AND ServiceNew_ID = 1
UPDATE Contracts SET StartDate = '2015-10-21' WHERE House_ID = 1441 AND ServiceNew_ID = 1
UPDATE Contracts SET StartDate = '2015-10-21' WHERE House_ID = 1476 AND ServiceNew_ID = 1
UPDATE Contracts SET StartDate = '2015-10-21' WHERE House_ID = 1549 AND ServiceNew_ID = 1
UPDATE Contracts SET StartDate = '2015-10-21' WHERE House_ID = 1607 AND ServiceNew_ID = 1
UPDATE Contracts SET StartDate = '2015-10-21' WHERE House_ID = 1639 AND ServiceNew_ID = 1
UPDATE Contracts SET StartDate = '2015-10-21' WHERE House_ID = 1675 AND ServiceNew_ID = 1

SELECT 'DW-47 - After update - Electricity',
	c.House_ID
	, c.ServiceNew_ID
	, c.StartDate
	, st.Name as 'Service Type'
FROM Contracts c
	INNER JOIN ServiceTypes st ON c.ServiceNew_ID = st.Id
	INNER JOIN #ElectricityHouseIDsList t ON t.House_ID = c.House_ID AND c.ServiceNew_ID = 1
ORDER BY C.House_ID


--- Gas ---
SELECT DISTINCT House_ID
INTO #GasHouseIDsList
FROM 
(SELECT 1345 AS House_ID	UNION
	SELECT 1351	UNION
	SELECT 1401	UNION
	SELECT 1467	UNION
	SELECT 1540	UNION
	SELECT 1574	UNION
	SELECT 1607	UNION
	SELECT 1639) as t

SELECT  'DW-47 - Before update - Gas',
	c.House_ID
	, c.ServiceNew_ID
	, c.StartDate
	, st.Name as 'Service Type'
FROM Contracts c
	INNER JOIN ServiceTypes st ON c.ServiceNew_ID = st.Id
	INNER JOIN #GasHouseIDsList t ON t.House_ID = c.House_ID AND c.ServiceNew_ID = 2
ORDER BY C.House_ID


UPDATE Contracts SET StartDate = '2015-11-04' WHERE House_ID = 1345 AND ServiceNew_ID = 2
UPDATE Contracts SET StartDate = '2015-11-02' WHERE House_ID = 1351 AND ServiceNew_ID = 2
UPDATE Contracts SET StartDate = '2015-11-04' WHERE House_ID = 1401 AND ServiceNew_ID = 2
UPDATE Contracts SET StartDate = '2015-11-04' WHERE House_ID = 1467 AND ServiceNew_ID = 2
UPDATE Contracts SET StartDate = '2015-11-04' WHERE House_ID = 1540 AND ServiceNew_ID = 2
UPDATE Contracts SET StartDate = '2015-11-04' WHERE House_ID = 1574 AND ServiceNew_ID = 2
UPDATE Contracts SET StartDate = '2015-11-04' WHERE House_ID = 1607 AND ServiceNew_ID = 2
UPDATE Contracts SET StartDate = '2015-11-04' WHERE House_ID = 1639 AND ServiceNew_ID = 2


SELECT  'DW-47 - After update - Gas',
	c.House_ID
	, c.ServiceNew_ID
	, c.StartDate
	, st.Name as 'Service Type'
FROM Contracts c
	INNER JOIN ServiceTypes st ON c.ServiceNew_ID = st.Id
	INNER JOIN #GasHouseIDsList t ON t.House_ID = c.House_ID AND c.ServiceNew_ID = 2
ORDER BY C.House_ID



DROP TABLE #GasHouseIDsList, #ElectricityHouseIDsList


--- DW-48 - Part 1 ----
--- Housemates Details ---

SELECT 'DW-48 - Housemates Emails Details - before update', EMail, * FROM AspNetUsers WHERE ID IN ('6d6a9de0-d886-4c03-b658-05230ecc846a','8f24065d-8bb4-456a-9e96-90a0ff3a6897','39e06173-3423-4564-abed-e2d1f8214c61')


UPDATE AspNetUsers SET Email = 'bradley-c@outlook.com' WHERE ID = '6d6a9de0-d886-4c03-b658-05230ecc846a'
UPDATE AspNetUsers SET Email = 'tobias.woodhouse@gmail.com' WHERE ID = '8f24065d-8bb4-456a-9e96-90a0ff3a6897'
UPDATE AspNetUsers SET Email = 'benedetta.tantalo@hotmail.it' WHERE ID = '39e06173-3423-4564-abed-e2d1f8214c61'


SELECT 'DW-48 - Housemates Emails Details - after update', EMail, * FROM AspNetUsers WHERE ID IN ('6d6a9de0-d886-4c03-b658-05230ecc846a','8f24065d-8bb4-456a-9e96-90a0ff3a6897','39e06173-3423-4564-abed-e2d1f8214c61')



--- Moving Housemates ---
SELECT ID
INTO #1270
FROM
(
SELECT '909e2650-f682-4752-8762-09d921d1b48e' AS IDUNION
SELECT '094a629f-aac4-46eb-a27a-d805152264ad'UNION
SELECT '3d15c8a6-11e2-4f9a-a76c-13ac0f5c959b'UNION
SELECT '41576521-6174-4ddc-ba1b-341efd83ea09'UNION
SELECT 'de98688d-5803-4251-9273-18c79eacbbb9'UNION
SELECT '88bd1dc5-db61-40d9-bf68-ad89e4b8b288'UNION
SELECT 'f6197dab-f045-4452-97f2-4762f39e3458'UNION
SELECT '810a3fa6-d3ec-4daa-86bc-d590bcd483e0'UNION
SELECT '9e811ca2-56a1-489a-827e-3806224c2b98'UNION
SELECT 'b1569996-5cb0-4101-9654-a64fab9e2ab3'UNION
SELECT '21366e77-9365-4825-b9b8-4d22e2417c37'UNION
SELECT '59d2088b-d044-4ae1-ad0a-bdf60ccd5ebd'UNION
SELECT '705ef0d6-0e3d-4473-a518-9404916ee501'
) AS T 
SELECT 'DW-48 - Move to 1270 - before update', House_ID, * FROM ASPNetUsers u INNER JOIN #1270 ON U.ID = #1270.ID

UPDATE u SET u.House_Id = 1270 FROM ASPNetUsers u INNER JOIN #1270 on #1270.ID = u.ID

SELECT 'DW-48 - Move to 1270 - after update', House_ID, * FROM ASPNetUsers u INNER JOIN #1270 ON U.ID = #1270.ID


SELECT ID 
INTO #1290
FROM 
(SELECT '383b0e95-59b5-4617-a377-03915f0655fd' AS IDUNION
SELECT '91f0eae9-dbe7-4188-ac8a-7f1a4bc94915'
) AS T


SELECT 'DW-48 - Move to 1290 - before update', House_ID, * FROM ASPNetUsers u INNER JOIN #1290 ON U.ID = #1290.ID

UPDATE u SET u.House_Id = 1290 FROM ASPNetUsers u INNER JOIN #1290 on #1290.ID = u.ID

SELECT 'DW-48 - Move to 1290 - after update', House_ID, * FROM ASPNetUsers u INNER JOIN #1290 ON U.ID = #1290.ID

DROP TABLE #1290, #1270


--- Bank Details ---
SELECT ID
INTO #BankDetailsIDsList
FROM 
(SELECT '35ed9624-ee96-47fa-b5cb-15bfec113196' as IDUNION
SELECT '0d941c85-0fd2-49ed-a484-a8fb82daf5c5'UNION
SELECT 'f2fa78ca-e61b-47d8-973f-debd3439e589'UNION
SELECT 'bc63d661-53d8-475d-86ff-539e25aa84d0'UNION
SELECT '11076572-4096-44a2-953f-f4f7a317c756'UNION
SELECT '69155ec0-0fdb-42da-93f7-fb44fbefb8ee'
) AS T


SELECT 'DW-48 - Before - bankDetail', * FROM ASPnetusers WHERE ID IN (SELECT ID FROM #BankDetailsIDsList) ORDER BY ID

UPDATE ASPNETUsers SET AccountNumber = '05585189', AccountSortCode = '070436' WHERE ID = '35ed9624-ee96-47fa-b5cb-15bfec113196'
UPDATE ASPNETUsers SET AccountNumber = '86150162', AccountSortCode = '090128' WHERE ID = '0d941c85-0fd2-49ed-a484-a8fb82daf5c5'
UPDATE ASPNETUsers SET AccountNumber = '90776238', AccountSortCode = '202267' WHERE ID = 'f2fa78ca-e61b-47d8-973f-debd3439e589'
UPDATE ASPNETUsers SET AccountNumber = '73838269', AccountSortCode = '200262' WHERE ID = 'bc63d661-53d8-475d-86ff-539e25aa84d0'
UPDATE ASPNETUsers SET AccountNumber = '00579670', AccountSortCode = '110369' WHERE ID = '11076572-4096-44a2-953f-f4f7a317c756'
UPDATE ASPNETUsers SET AccountNumber = '00276648', AccountSortCode = '309481' WHERE ID = '69155ec0-0fdb-42da-93f7-fb44fbefb8ee'

SELECT 'DW-48 - After - bankDetail', * FROM ASPnetusers WHERE ID IN (SELECT ID FROM #BankDetailsIDsList) ORDER BY ID


--- House Details ---
SELECT 'DW-48 - before - house details update', * FROM HouseModels WHERE ID = 1868
UPDATE HouseModels SET Address_Line1 = '6b Wilkinson Street' WHERE ID = 1868
SELECT 'DW-48 - after - house details update', * FROM HouseModels WHERE ID = 1868



--- New Service Contracts ---
INSERT INTO Contracts (House_ID,Service,StartDate,EndDate,Length,RequestedBy_ID,Provider_ID,SubmissionStatus,ServiceNew_ID,PackageRaw,RequestedStartDate)
--SELECT 1223, 0, '2015-10-13', '2016-07-13', 12, (SELECT TOP 1 RequestedBy_ID FROM Contracts WHERE House_ID = 1223), 11, 0, 9, 0, '2015-07-13'
--UNION ALL
--SELECT 1492, 0, '2015-10-07', '2016-07-01', 10, (SELECT TOP 1 RequestedBy_ID FROM Contracts WHERE House_ID = 1492), 11, 0, 9, 0, '2015-09-01'
--UNION ALL
SELECT 1577, 0, '2015-09-18', '2016-06-18', 9, (SELECT TOP 1 RequestedBy_ID FROM Contracts WHERE House_ID = 1577), 11, 0, 9, 0, '2015-09-08'

SELECT 'DW-48 - new contracts', * FROM Contracts WHERE ServiceNew_ID = 9 AND House_ID IN ( 1223, 1492, 1577)

INSERT INTO LineRentalContracts
--SELECT TOP 1 ID FROM Contracts WHERE ServiceNew_ID = 9 AND House_ID = 1223
--UNION ALL
--SELECT TOP 1 ID FROM Contracts WHERE ServiceNew_ID = 9 AND House_ID = 1492
--UNION ALL 
SELECT TOP 1 ID FROM Contracts WHERE ServiceNew_ID = 9 AND House_ID = 1577



--- DW-48 Update Services Start Date --- 

UPDATE Contracts SET StartDate = '2015-10-20', EndDate = '2016-08-20' WHERE House_ID = 1413 AND ServiceNew_ID = 7

UPDATE Contracts SET StartDate = '2015-11-05', EndDate = '2016-07-12' WHERE House_ID = 1788 AND ServiceNew_ID = 4
UPDATE Contracts SET StartDate = '2015-11-05', EndDate = '2016-07-12' WHERE House_ID = 1788 AND ServiceNew_ID = 9
UPDATE Contracts SET StartDate = '2015-10-12', EndDate = '2016-07-12' WHERE House_ID = 1788 AND ServiceNew_ID = 3

UPDATE Contracts SET EndDate = '2015-11-27' WHERE House_ID = 1185 AND ServiceNew_ID = 2
UPDATE Contracts SET EndDate = '2015-11-27' WHERE House_ID = 1185 AND ServiceNew_ID = 1

UPDATE Contracts SET EndDate = '2015-10-02' WHERE House_ID = 1519 AND ServiceNew_ID = 7

UPDATE Contracts SET StartDate = '2015-09-16', EndDate  = '2016-08-16' WHERE House_ID = 1623 AND ServiceNew_ID = 3

UPDATE Contracts SET StartDate = '2015-09-29', EndDate  = '2016-09-29' WHERE House_ID = 1558 AND ServiceNew_ID = 4
UPDATE Contracts SET StartDate = '2015-09-29', EndDate  = '2016-09-29' WHERE House_ID = 1558 AND ServiceNew_ID = 9

UPDATE Contracts SET EndDate  = '2015-11-13' WHERE House_ID = 1638 AND ServiceNew_ID = 3

UPDATE Contracts SET StartDate = '2015-09-18', EndDate  = '2016-07-18' WHERE House_ID = 1701 AND ServiceNew_ID = 3
UPDATE Contracts SET StartDate = '2015-10-05', EndDate  = '2016-07-18' WHERE House_ID = 1701 AND ServiceNew_ID = 2
UPDATE Contracts SET StartDate = '2015-10-05', EndDate  = '2016-07-18' WHERE House_ID = 1701 AND ServiceNew_ID = 1

UPDATE Contracts SET EndDate  = '2015-10-21' WHERE House_ID = 1723 AND ServiceNew_ID = 1
UPDATE Contracts SET EndDate  = '2015-10-21' WHERE House_ID = 1723 AND ServiceNew_ID = 3

UPDATE Contracts SET EndDate  = '2016-08-09' WHERE House_ID = 1600 AND ServiceNew_ID = 9
UPDATE Contracts SET StartDate = '2015-10-05', EndDate = '2016-08-09' WHERE House_ID = 1600 AND ServiceNew_ID = 2

UPDATE Contracts SET EndDate = '2015-10-22' WHERE House_ID = 1731 AND ServiceNew_ID = 3

UPDATE Contracts SET EndDate = '2015-10-14' WHERE House_ID = 1513 AND ServiceNew_ID = 3
UPDATE Contracts SET EndDate = '2015-10-14' WHERE House_ID = 1513 AND ServiceNew_ID = 9
UPDATE Contracts SET EndDate = '2015-10-14' WHERE House_ID = 1513 AND ServiceNew_ID = 4

UPDATE Contracts SET StartDate = '2015-10-02', EndDate = '2016-10-02' WHERE House_ID = 1626 AND ServiceNew_ID = 3
UPDATE Contracts SET StartDate = '2015-10-19', EndDate = '2016-10-19' WHERE House_ID = 1626 AND ServiceNew_ID = 4
UPDATE Contracts SET StartDate = '2015-10-19', EndDate = '2016-10-19' WHERE House_ID = 1626 AND ServiceNew_ID = 9

UPDATE Contracts SET EndDate = '2015-10-16' WHERE House_ID = 1676 AND ServiceNew_ID = 3

UPDATE Contracts SET StartDate='2015-12-04', EndDate = '2016-06-30' WHERE House_ID = 1868 AND ServiceNew_ID = 3
UPDATE Contracts SET EndDate = '2016-06-30' WHERE House_ID = 1868 AND ServiceNew_ID = 1
UPDATE Contracts SET EndDate = '2016-06-30' WHERE House_ID = 1868 AND ServiceNew_ID = 2

UPDATE Contracts SET StartDate = '2015-10-05', EndDate = '2016-06-01' WHERE House_ID = 142 AND ServiceNew_ID = 1
UPDATE Contracts SET StartDate = '2015-10-05', EndDate = '2016-06-01' WHERE House_ID = 142 AND ServiceNew_ID = 2

UPDATE Contracts SET EndDate = '2015-10-31' WHERE House_ID = 1447 AND ServiceNew_ID = 1
UPDATE Contracts SET EndDate = '2015-10-31' WHERE House_ID = 1447 AND ServiceNew_ID = 3
UPDATE Contracts SET EndDate = '2015-10-31' WHERE House_ID = 1447 AND ServiceNew_ID = 2

UPDATE Contracts SET  StartDate = '2015-10-30', EndDate = '2016-08-30' WHERE House_ID = 1527 AND ServiceNew_ID = 4
UPDATE Contracts SET  StartDate = '2015-10-30', EndDate = '2016-08-30' WHERE House_ID = 1527 AND ServiceNew_ID = 9
UPDATE Contracts SET  EndDate = '2015-11-12' WHERE House_ID = 1527 AND ServiceNew_ID = 2
UPDATE Contracts SET  EndDate = '2015-11-12' WHERE House_ID = 1527 AND ServiceNew_ID = 1

UPDATE Contracts SET  EndDate = '2015-10-13' WHERE House_ID = 1244 AND ServiceNew_ID = 3

UPDATE Contracts SET  EndDate = '2016-09-12' WHERE House_ID = 1874 AND ServiceNew_ID = 4

UPDATE Contracts SET  EndDate = '2015-12-29' WHERE House_ID = 1218 AND ServiceNew_ID = 3
UPDATE Contracts SET  EndDate = '2015-12-29' WHERE House_ID = 1218 AND ServiceNew_ID = 9
UPDATE Contracts SET  EndDate = '2015-12-29' WHERE House_ID = 1218 AND ServiceNew_ID = 4

UPDATE Contracts SET  EndDate = '2016-07-31' WHERE House_ID = 1499 AND ServiceNew_ID = 1
UPDATE Contracts SET  EndDate = '2016-07-31' WHERE House_ID = 1499 AND ServiceNew_ID = 2
UPDATE Contracts SET  EndDate = '2016-07-31' WHERE House_ID = 1499 AND ServiceNew_ID = 3
UPDATE Contracts SET  EndDate = '2016-07-31' WHERE House_ID = 1499 AND ServiceNew_ID = 7
UPDATE Contracts SET  EndDate = '2016-07-31' WHERE House_ID = 1499 AND ServiceNew_ID = 9
UPDATE Contracts SET  EndDate = '2016-07-31' WHERE House_ID = 1499 AND ServiceNew_ID = 4

UPDATE Contracts SET  EndDate = '2015-11-12' WHERE House_ID = 1631 AND ServiceNew_ID = 3

UPDATE Contracts SET  EndDate = '2015-12-13' WHERE House_ID = 1615 AND ServiceNew_ID = 7

UPDATE Contracts SET  EndDate = '2015-11-30' WHERE House_ID = 127 AND ServiceNew_ID = 8

UPDATE Contracts SET  EndDate = '2015-12-17' WHERE House_ID = 1691 AND ServiceNew_ID = 1
UPDATE Contracts SET  EndDate = '2015-12-17' WHERE House_ID = 1691 AND ServiceNew_ID = 2

UPDATE Contracts SET  EndDate = '2015-12-06' WHERE House_ID = 1569 AND ServiceNew_ID = 1

UPDATE Contracts SET  EndDate = '2015-11-13' WHERE House_ID = 1397 AND ServiceNew_ID = 3

UPDATE Contracts SET  EndDate = '2015-12-09' WHERE House_ID = 1346 AND ServiceNew_ID = 3

UPDATE Contracts SET  EndDate = '2015-11-13' WHERE House_ID = 1221 AND ServiceNew_ID = 3

UPDATE Contracts SET  EndDate = '2015-12-26' WHERE House_ID = 1467 AND ServiceNew_ID = 8

UPDATE Contracts SET  EndDate = '2015-12-01' WHERE House_ID = 1517 AND ServiceNew_ID = 7

UPDATE Contracts SET  EndDate = '2015-11-03' WHERE House_ID = 1534 AND ServiceNew_ID = 3

UPDATE Contracts SET  EndDate = '2015-11-01' WHERE House_ID = 1510 AND ServiceNew_ID = 2
UPDATE Contracts SET  EndDate = '2015-11-01' WHERE House_ID = 1510 AND ServiceNew_ID = 1

UPDATE Contracts SET  EndDate = '2015-11-03' WHERE House_ID = 1542 AND ServiceNew_ID = 2
UPDATE Contracts SET  EndDate = '2015-11-03' WHERE House_ID = 1542 AND ServiceNew_ID = 1


ROLLBACK