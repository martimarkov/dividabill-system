SELECT
h.ID as HouseID
, ISNULL(h.Address_Line1, 'n/a') AS Address_Line1
, ISNULL(h.Address_Line2, 'n/a') AS Address_Line2
, ISNULL(h.Address_Line3, 'n/a') AS Address_Line3
, ISNULL(h.Address_Line4, 'n/a') AS Address_Line4
, h.Address_Postcode
, h.Address_City
, ISNULL(h.Address_County, 'n/a') AS Address_County
, h.StartDate
, h.EndDate
, h.RegisteredDate
, u.ID as 'UserID'
, u.FirstName 'Tenant First Name'
, u.LastName 'Tenant Last Name'
, u.SignupDate
, u.Email
, u.PhoneNumber
, u.AccountNumber
, u.AccountSortCode
FROM HouseModels h
INNER JOIN AspNetUsers u ON h.ID = u.House_ID
ORDER BY h.ID, u.LastName


--exec sp_help HouseModels