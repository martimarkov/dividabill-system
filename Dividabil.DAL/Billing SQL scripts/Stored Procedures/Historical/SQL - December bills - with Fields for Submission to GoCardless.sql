SELECT * 
INTO #Decemberbills_HouseList
FROM
(
SELECT 0 as id
UNION
SELECT 0
UNION
SELECT 0
) AS T




SELECT hb.House_ID
, hb.Month
, hb.Year
, u.MandateID
, bp.User_Id
, u.FirstName
, u.LastName
, hb.CreatedDate
, 'December 2015 - ' + st.Name as 'Service'
, bp.Amount
FROM HouseholdBills hb 
INNER JOIN #Decemberbills_HouseList t on hb.House_ID = t.ID
INNER JOIN BillBases bb ON hb.Id = bb.HouseholdBill_Id and hb.[Month] = 12 and hb.[Year] = 2015
INNER JOIN Contracts c ON bb.Contract_Id = c.ID
INNER JOIN ServiceTypes st ON c.ServiceNew_ID = st.Id 
INNER JOIN BillPayments bp ON bb.ID = bp.Bill_Id
LEFT JOIN AspNetUsers u ON bp.User_Id = u.id
ORDER BY hb.House_ID
, st.Name
, u.Lastname
, u.FirstName


DROP TABLE #Decemberbills_HouseList

