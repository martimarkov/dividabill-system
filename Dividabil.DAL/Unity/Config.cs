﻿using DividaBill.DAL.DbContext;
using DividaBill.DAL.Interfaces;
using DividaBill.DAL.StoredProcs;
using Microsoft.Practices.Unity;

namespace DividaBill.DAL.Unity
{
    /// <summary>
    /// Registers all dependencies needed to work with the DAL objects. 
    /// </summary>
    public static class Config
    {
        public static void RegisterDAL(this IUnityContainer container)
        {
            container.RegisterType<IUnitOfWork, UnitOfWork>(new HierarchicalLifetimeManager());

            container.RegisterType<IUserRepository, UserRepository>();
            container.RegisterType<IApplicationContext, ApplicationDbContext>(new HierarchicalLifetimeManager());
            container.RegisterType<IReferalContext, ReferalDbContext>(new HierarchicalLifetimeManager());
            //container.RegisterType<IWarehouseContext, WarehouseDbContext>(new HierarchicalLifetimeManager());
            container.RegisterType<IStoredProcsContext, Entities>(new HierarchicalLifetimeManager());
        }
    }
}
