using System;
using DividaBill.DAL.Interfaces;

namespace DividaBill.DAL.Unity
{
    public class AsyncExecutionParam
    {
        public AsyncExecutionParam(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }

        public IUnitOfWork UnitOfWork { get; }
    }

    public static class AsyncExecutionParamExtension
    {
        public static AsyncExecutionParam AssignThreadSafeUnitOfWork(this AsyncExecutionParam asyncExecutionParam, IUnitOfWork unitOfWork)
        {
            if (asyncExecutionParam == null)
            {
                if (unitOfWork == null)
                    throw new Exception("Unable to identify UnitOfWork");
                return new AsyncExecutionParam(unitOfWork);
            }
            return asyncExecutionParam;
        }
    }
}