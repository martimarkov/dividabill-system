﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using DividaBill.DAL.Interfaces;
using DividaBill.Models;

namespace DividaBill.DAL
{
    public abstract class HouseRepository<DBContext> : GenericRepository<HouseModel, User, DBContext>, IHousesRepository<HouseModel, DBContext>
        where DBContext : IDividaBillWebContext, IHousesStorage
    {
        public HouseRepository(DBContext context)
            : base(context)
        {
        }

        public IEnumerable<HouseModel> GetHouses()
        {
            return Get();
        }

        public IQueryable<HouseModel> GetHouses(Expression<Func<HouseModel, bool>> filter = null, Func<IQueryable<HouseModel>, IOrderedQueryable<HouseModel>> orderBy = null, string includeProperties = "")
        {
            var now = DateTime.Now;
            return Get(filter, orderBy, includeProperties).Where(h => h.IsActive(now));
        }

        public HouseModel GetHouseByUser(User user)
        {
            return context.Houses.Find(user.House_ID);
        }

        public IEnumerable<HouseModel> GetProblemHouses(DateTime offset)
        {
            return GetHouses(p => p.RegisteredDate < DateTime.Now && p.RegisteredDate > offset && p.Housemates != p.Tenants.Count);
        }

        public void ActivateHouse(HouseModel house)
        {
            //house.SetUpDate = DateTime.Now;
            Update(house);
        }

        public void RemoveTenant(User tenant, int houseID)
        {
            context.Users.Attach(tenant);
            context.Houses.Find(houseID).Tenants.Remove(tenant);
        }

        public void AddTenant(User tenant, HouseModel house)
        {
            context.Users.Attach(tenant);
            context.Houses.Attach(house);
            house.Tenants.Add(tenant);
        }
    }
}