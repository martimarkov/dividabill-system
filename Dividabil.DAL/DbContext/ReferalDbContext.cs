using DividaBill.DAL.Interfaces;
using DividaBill.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DividaBill.DAL.DbContext
{
    public class ReferalDbContext : IdentityDbContext<ReferalUser>, IReferalContext
    {
        public ReferalDbContext()
            : base("ReferalConnection", false)
        {
        }

        public static ReferalDbContext Create()
        {
            return new ReferalDbContext();
        }
    }
}