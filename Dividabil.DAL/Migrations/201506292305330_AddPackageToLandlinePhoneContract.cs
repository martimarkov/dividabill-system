namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPackageToLandlinePhoneContract : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LandlinePhoneContracts", "Package", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.LandlinePhoneContracts", "Package");
        }
    }
}
