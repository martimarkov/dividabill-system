namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBuildingsModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Buildings",
                c => new
                    {
                        Udprn = c.Double(nullable: false),
                        Company = c.String(),
                        Department = c.String(),
                        Line1 = c.String(),
                        Line2 = c.String(),
                        Line3 = c.String(),
                        Line4 = c.String(),
                        Line5 = c.String(),
                        PostTown = c.String(),
                        County = c.String(),
                        PostCode = c.String(),
                        Mailsort = c.Int(nullable: false),
                        Barcode = c.String(),
                        Type = c.String(),
                        DeliveryPointSuffix = c.String(),
                        SubBuilding = c.String(),
                        BuildingName = c.String(),
                        BuildingNumber = c.String(),
                        PrimaryStreet = c.String(),
                        SecondaryStreet = c.String(),
                        DoubleDependentLocality = c.String(),
                        DependentLocality = c.String(),
                        PoBox = c.String(),
                        PrimaryStreetName = c.String(),
                        PrimaryStreetType = c.String(),
                        SecondaryStreetName = c.String(),
                        SecondaryStreetType = c.String(),
                        CountryName = c.String(),
                        CountryISO2 = c.String(),
                        CountryISO3 = c.String(),
                    })
                .PrimaryKey(t => t.Udprn);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Buildings");
        }
    }
}
