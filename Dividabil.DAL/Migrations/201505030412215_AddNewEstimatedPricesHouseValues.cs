namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNewEstimatedPricesHouseValues : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HouseEstimatedPrices", "BroadbandADSL", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.HouseEstimatedPrices", "BroadbandFible38", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.HouseEstimatedPrices", "BroadbandFible76", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.HouseEstimatedPrices", "LandlinePhoneBasic", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.HouseEstimatedPrices", "LandlinePhoneMedium", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.HouseEstimatedPrices", "LineRental", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.HouseEstimatedPrices", "SkyTV", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.HouseEstimatedPrices", "NetflixTV", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.HouseEstimatedPrices", "TVLicense", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.HouseEstimatedPrices", "BroadbandADSLMargin", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.HouseEstimatedPrices", "BroadbandADSLRouter", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.HouseEstimatedPrices", "BroadbandFible38Router", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.HouseEstimatedPrices", "BroadbandFible38Margin", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.HouseEstimatedPrices", "LineRentalMargin", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.HouseEstimatedPrices", "LineRentalInstallation", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.HouseEstimatedPrices", "SkyTVMargin", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.HouseEstimatedPrices", "SkyTVSTB", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.HouseEstimatedPrices", "SkyTVSatellite", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.HouseEstimatedPrices", "SkyTVInstallation", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.HouseEstimatedPrices", "LandlinePhoneBasicMargin", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.HouseEstimatedPrices", "LandlinePhoneMediumMargin", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.HouseEstimatedPrices", "BroadbandADSLRouterMargin", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.HouseEstimatedPrices", "BroadbandFibre40RouterMargin", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.HouseEstimatedPrices", "SkyTVInstallationMargin", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.HouseEstimatedPrices", "SkyTVSTBMargin", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.HouseEstimatedPrices", "SkyTVSatelliteMargin", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.HouseEstimatedPrices", "SkyTVSatelliteMargin");
            DropColumn("dbo.HouseEstimatedPrices", "SkyTVSTBMargin");
            DropColumn("dbo.HouseEstimatedPrices", "SkyTVInstallationMargin");
            DropColumn("dbo.HouseEstimatedPrices", "BroadbandFibre40RouterMargin");
            DropColumn("dbo.HouseEstimatedPrices", "BroadbandADSLRouterMargin");
            DropColumn("dbo.HouseEstimatedPrices", "LandlinePhoneMediumMargin");
            DropColumn("dbo.HouseEstimatedPrices", "LandlinePhoneBasicMargin");
            DropColumn("dbo.HouseEstimatedPrices", "SkyTVInstallation");
            DropColumn("dbo.HouseEstimatedPrices", "SkyTVSatellite");
            DropColumn("dbo.HouseEstimatedPrices", "SkyTVSTB");
            DropColumn("dbo.HouseEstimatedPrices", "SkyTVMargin");
            DropColumn("dbo.HouseEstimatedPrices", "LineRentalInstallation");
            DropColumn("dbo.HouseEstimatedPrices", "LineRentalMargin");
            DropColumn("dbo.HouseEstimatedPrices", "BroadbandFible38Margin");
            DropColumn("dbo.HouseEstimatedPrices", "BroadbandFible38Router");
            DropColumn("dbo.HouseEstimatedPrices", "BroadbandADSLRouter");
            DropColumn("dbo.HouseEstimatedPrices", "BroadbandADSLMargin");
            DropColumn("dbo.HouseEstimatedPrices", "TVLicense");
            DropColumn("dbo.HouseEstimatedPrices", "NetflixTV");
            DropColumn("dbo.HouseEstimatedPrices", "SkyTV");
            DropColumn("dbo.HouseEstimatedPrices", "LineRental");
            DropColumn("dbo.HouseEstimatedPrices", "LandlinePhoneMedium");
            DropColumn("dbo.HouseEstimatedPrices", "LandlinePhoneBasic");
            DropColumn("dbo.HouseEstimatedPrices", "BroadbandFible76");
            DropColumn("dbo.HouseEstimatedPrices", "BroadbandFible38");
            DropColumn("dbo.HouseEstimatedPrices", "BroadbandADSL");
        }
    }
}
