If there are some changes in Entity models and necessary to create migration script, then use this:
add-migration "Migration_Script_Name" -configurationtypename configuration -verbose

Update database to latest configuration from migration scripts:
update-database -configurationtypename configuration -verbose

Update database to exact configuration from migration scripts:
update-database -TargetMigration: "Migration_Script_Name" -configurationtypename configuration -verbose
