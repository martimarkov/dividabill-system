namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UsePostcodeAsPrimaryIndex : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.HouseEstimatedPrices", "HouseID", "dbo.HouseModels");
            DropForeignKey("dbo.PostcodeEstimatedPrices", "PostCode_ID", "dbo.PostCodes");
            DropIndex("dbo.PostcodeEstimatedPrices", new[] { "PostCode_ID" });
            DropIndex("dbo.HouseEstimatedPrices", new[] { "HouseID" });
            DropIndex("dbo.PostCodes", new[] { "Postcode" });
            DropPrimaryKey("dbo.HouseEstimatedPrices");
            DropPrimaryKey("dbo.PostcodeEstimatedPrices");
            DropPrimaryKey("dbo.PostCodes");
            AlterColumn("dbo.PostcodeEstimatedPrices", "PostCode_ID", c => c.String(nullable: false, maxLength: 15));
            AlterColumn("dbo.PostCodes", "Postcode", c => c.String(nullable: false, maxLength: 15));
            AlterColumn("dbo.HouseEstimatedPrices", "HouseID", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.HouseEstimatedPrices", "HouseID");
            AddPrimaryKey("dbo.PostcodeEstimatedPrices", "PostCode_ID");
            AddPrimaryKey("dbo.PostCodes", "Postcode");
            CreateIndex("dbo.PostcodeEstimatedPrices", "PostCode_ID", unique: true);
            CreateIndex("dbo.PostCodes", "Postcode", unique: true);
            AddForeignKey("dbo.HouseEstimatedPrices", "HouseID", "dbo.HouseModels", "ID");
            AddForeignKey("dbo.PostcodeEstimatedPrices", "PostCode_ID", "dbo.PostCodes", "Postcode");
            DropColumn("dbo.HouseEstimatedPrices", "ID");
            DropColumn("dbo.PostcodeEstimatedPrices", "ID");
            DropColumn("dbo.PostCodes", "ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PostCodes", "ID", c => c.Guid(nullable: false, identity: true));
            AddColumn("dbo.PostcodeEstimatedPrices", "ID", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.HouseEstimatedPrices", "ID", c => c.Int(nullable: false, identity: true));
            DropForeignKey("dbo.PostcodeEstimatedPrices", "PostCode_ID", "dbo.PostCodes");
            DropForeignKey("dbo.HouseEstimatedPrices", "HouseID", "dbo.HouseModels");
            DropIndex("dbo.PostCodes", new[] { "Postcode" });
            DropIndex("dbo.PostcodeEstimatedPrices", new[] { "PostCode_ID" });
            DropPrimaryKey("dbo.PostCodes");
            DropPrimaryKey("dbo.PostcodeEstimatedPrices");
            DropPrimaryKey("dbo.HouseEstimatedPrices");
            AlterColumn("dbo.PostCodes", "Postcode", c => c.String(maxLength: 15));
            AlterColumn("dbo.PostcodeEstimatedPrices", "PostCode_ID", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.PostCodes", "ID");
            AddPrimaryKey("dbo.PostcodeEstimatedPrices", "ID");
            AddPrimaryKey("dbo.HouseEstimatedPrices", "ID");
            CreateIndex("dbo.PostCodes", "Postcode", unique: true);
            CreateIndex("dbo.PostcodeEstimatedPrices", "PostCode_ID", unique: true);
            AddForeignKey("dbo.PostcodeEstimatedPrices", "PostCode_ID", "dbo.PostCodes", "ID", cascadeDelete: true);
            AddForeignKey("dbo.HouseEstimatedPrices", "HouseID", "dbo.HouseModels", "ID", cascadeDelete: true);
        }
    }
}
