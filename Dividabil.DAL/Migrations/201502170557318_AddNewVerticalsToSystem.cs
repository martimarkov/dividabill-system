namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNewVerticalsToSystem : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HouseModels", "Broadband", c => c.Boolean(nullable: false, defaultValue: false));
            AddColumn("dbo.HouseModels", "LandlinePhone", c => c.Boolean(nullable: false, defaultValue: false));
            AddColumn("dbo.HouseModels", "SkyTV", c => c.Boolean(nullable: false, defaultValue: false));
            AddColumn("dbo.HouseModels", "TVLicense", c => c.Boolean(nullable: false, defaultValue: false));
            AddColumn("dbo.HouseModels", "NetflixTV", c => c.Boolean(nullable: false, defaultValue: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.HouseModels", "NetflixTV");
            DropColumn("dbo.HouseModels", "TVLicense");
            DropColumn("dbo.HouseModels", "SkyTV");
            DropColumn("dbo.HouseModels", "LandlinePhone");
            DropColumn("dbo.HouseModels", "Broadband");
        }
    }
}
