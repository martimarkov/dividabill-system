namespace DividaBill.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSupplierFiles : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Suppliers.Files",
                c => new
                    {
                        FileName = c.String(nullable: false, maxLength: 128),
                        KeyColumnId = c.Int(nullable: false),
                        Provider_ID = c.Int(),
                    })
                .PrimaryKey(t => t.FileName)
                .ForeignKey("dbo.Providers", t => t.Provider_ID)
                .Index(t => t.Provider_ID);
            
            CreateTable(
                "Suppliers.FileRevisions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CreationDate = c.DateTime(nullable: false),
                        WasDoneByUs = c.Boolean(nullable: false),
                        FileData = c.Binary(),
                        ChangedFile_FileName = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Suppliers.Files", t => t.ChangedFile_FileName)
                .Index(t => t.ChangedFile_FileName);
            
            CreateTable(
                "Suppliers.FileRowChanges",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RowId = c.Int(nullable: false),
                        ChangeType = c.Byte(nullable: false),
                        Revision_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Suppliers.FileRevisions", t => t.Revision_Id)
                .Index(t => t.Revision_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Suppliers.FileRowChanges", "Revision_Id", "Suppliers.FileRevisions");
            DropForeignKey("Suppliers.FileRevisions", "ChangedFile_FileName", "Suppliers.Files");
            DropForeignKey("Suppliers.Files", "Provider_ID", "dbo.Providers");
            DropIndex("Suppliers.FileRowChanges", new[] { "Revision_Id" });
            DropIndex("Suppliers.FileRevisions", new[] { "ChangedFile_FileName" });
            DropIndex("Suppliers.Files", new[] { "Provider_ID" });
            DropTable("Suppliers.FileRowChanges");
            DropTable("Suppliers.FileRevisions");
            DropTable("Suppliers.Files");
        }
    }
}
