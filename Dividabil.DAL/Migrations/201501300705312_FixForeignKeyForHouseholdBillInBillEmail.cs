namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixForeignKeyForHouseholdBillInBillEmail : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.BillEmailMessages", name: "HouseholdBill_ID1", newName: "HouseholdBill_ID");
        }
        
        public override void Down()
        {
            RenameColumn(table: "dbo.BillEmailMessages", name: "HouseholdBill_ID", newName: "HouseholdBill_ID1");
        }
    }
}
