namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPercentageToBills : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UsersDeliveryBillsPercentages",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        BillId = c.Int(nullable: false),
                        Percentage = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.BillId })
                .ForeignKey("dbo.DeliveryBills", t => t.BillId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId)
                .Index(t => t.BillId);
            
            CreateTable(
                "dbo.UsersElectricityBillsPercentages",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        BillId = c.Int(nullable: false),
                        Percentage = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.BillId })
                .ForeignKey("dbo.ElectricityBills", t => t.BillId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId)
                .Index(t => t.BillId);
            
            CreateTable(
                "dbo.UsersGasBillsPercentages",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        BillId = c.Int(nullable: false),
                        Percentage = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.BillId })
                .ForeignKey("dbo.GasBills", t => t.BillId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId)
                .Index(t => t.BillId);
            
            CreateTable(
                "dbo.UsersWaterBillsPercentages",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        BillId = c.Int(nullable: false),
                        Percentage = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.BillId })
                .ForeignKey("dbo.WaterBills", t => t.BillId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId)
                .Index(t => t.BillId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UsersWaterBillsPercentages", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UsersWaterBillsPercentages", "BillId", "dbo.WaterBills");
            DropForeignKey("dbo.UsersGasBillsPercentages", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UsersGasBillsPercentages", "BillId", "dbo.GasBills");
            DropForeignKey("dbo.UsersElectricityBillsPercentages", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UsersElectricityBillsPercentages", "BillId", "dbo.ElectricityBills");
            DropForeignKey("dbo.UsersDeliveryBillsPercentages", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UsersDeliveryBillsPercentages", "BillId", "dbo.DeliveryBills");
            DropIndex("dbo.UsersWaterBillsPercentages", new[] { "BillId" });
            DropIndex("dbo.UsersWaterBillsPercentages", new[] { "UserId" });
            DropIndex("dbo.UsersGasBillsPercentages", new[] { "BillId" });
            DropIndex("dbo.UsersGasBillsPercentages", new[] { "UserId" });
            DropIndex("dbo.UsersElectricityBillsPercentages", new[] { "BillId" });
            DropIndex("dbo.UsersElectricityBillsPercentages", new[] { "UserId" });
            DropIndex("dbo.UsersDeliveryBillsPercentages", new[] { "BillId" });
            DropIndex("dbo.UsersDeliveryBillsPercentages", new[] { "UserId" });
            DropTable("dbo.UsersWaterBillsPercentages");
            DropTable("dbo.UsersGasBillsPercentages");
            DropTable("dbo.UsersElectricityBillsPercentages");
            DropTable("dbo.UsersDeliveryBillsPercentages");
        }
    }
}
