namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEmailMessageTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.HouseEmailMessages",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        House_ID = c.Int(nullable: false),
                        Sent = c.DateTime(nullable: false),
                        Message = c.Binary(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.HouseModels", t => t.House_ID, cascadeDelete: true)
                .Index(t => t.House_ID);
            
            CreateTable(
                "dbo.UserEmailMessages",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        User_ID = c.String(maxLength: 128),
                        Sent = c.DateTime(nullable: false),
                        Message = c.Binary(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.User_ID)
                .Index(t => t.User_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserEmailMessages", "User_ID", "dbo.AspNetUsers");
            DropForeignKey("dbo.HouseEmailMessages", "House_ID", "dbo.HouseModels");
            DropIndex("dbo.UserEmailMessages", new[] { "User_ID" });
            DropIndex("dbo.HouseEmailMessages", new[] { "House_ID" });
            DropTable("dbo.UserEmailMessages");
            DropTable("dbo.HouseEmailMessages");
        }
    }
}
