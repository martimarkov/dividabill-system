namespace DividaBill.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MakeServiceTypeNewRequired : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Contracts", "ServiceNew_ID", "dbo.ServiceTypes");
            DropIndex("dbo.Contracts", new[] { "ServiceNew_ID" });
            AlterColumn("dbo.Contracts", "ServiceNew_ID", c => c.Int(nullable: false));
            CreateIndex("dbo.Contracts", "ServiceNew_ID");
            AddForeignKey("dbo.Contracts", "ServiceNew_ID", "dbo.ServiceTypes", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Contracts", "ServiceNew_ID", "dbo.ServiceTypes");
            DropIndex("dbo.Contracts", new[] { "ServiceNew_ID" });
            AlterColumn("dbo.Contracts", "ServiceNew_ID", c => c.Int());
            CreateIndex("dbo.Contracts", "ServiceNew_ID");
            AddForeignKey("dbo.Contracts", "ServiceNew_ID", "dbo.ServiceTypes", "Id");
        }
    }
}
