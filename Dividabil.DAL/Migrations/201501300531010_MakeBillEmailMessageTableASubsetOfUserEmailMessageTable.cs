namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MakeBillEmailMessageTableASubsetOfUserEmailMessageTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BillEmailMessages", "ID", "dbo.HouseEmailMessages");
            AddColumn("dbo.BillEmailMessages", "HouseholdBill_ID", c => c.Int());
            CreateIndex("dbo.BillEmailMessages", "HouseholdBill_ID");
            AddForeignKey("dbo.BillEmailMessages", "ID", "dbo.UserEmailMessages", "ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserEmailMessages", "HouseholdBill_ID1", c => c.Int());
            AddColumn("dbo.UserEmailMessages", "HouseholdBill_ID", c => c.Int(nullable: false));
            DropForeignKey("dbo.BillEmailMessages", "ID", "dbo.UserEmailMessages");
            DropIndex("dbo.BillEmailMessages", new[] { "HouseholdBill_ID" });
            DropColumn("dbo.BillEmailMessages", "HouseholdBill_ID1");
            RenameColumn(table: "dbo.BillEmailMessages", name: "HouseholdBill_ID", newName: "HouseholdBill_ID1");
            CreateIndex("dbo.UserEmailMessages", "HouseholdBill_ID1");
            AddForeignKey("dbo.BillEmailMessages", "ID", "dbo.HouseEmailMessages", "ID");
        }
    }
}
