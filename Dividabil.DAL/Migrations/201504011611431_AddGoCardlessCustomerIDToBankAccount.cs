namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddGoCardlessCustomerIDToBankAccount : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BankAccounts", "GoCardlessCustomerID", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.BankAccounts", "GoCardlessCustomerID");
        }
    }
}
