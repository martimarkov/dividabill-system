// <auto-generated />
namespace DividaBill.DAL.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.2-31219")]
    public sealed partial class AddHouse_IDToBillEmailMessage : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddHouse_IDToBillEmailMessage));
        
        string IMigrationMetadata.Id
        {
            get { return "201502040518415_AddHouse_IDToBillEmailMessage"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
