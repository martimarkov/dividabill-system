namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovePostCodesTable : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.PostCodes", new[] { "Postcode" });
            DropIndex("dbo.PostCodes", new[] { "ClusteredID" });
            DropTable("dbo.PostCodes");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.PostCodes",
                c => new
                    {
                        Postcode = c.String(nullable: false, maxLength: 15),
                        ClusteredID = c.Int(nullable: false, identity: true),
                        Latitude = c.String(),
                        Longitude = c.String(),
                        Easting = c.String(),
                        Northing = c.String(),
                        GridRef = c.String(),
                        County = c.String(),
                        District = c.String(),
                        Ward = c.String(),
                        DistrictCode = c.String(),
                        WardCode = c.String(),
                        Country = c.String(),
                        CountyCode = c.String(),
                        Constituency = c.String(),
                        Introduced = c.String(),
                        Terminated = c.String(),
                        Parish = c.String(),
                        NationalPark = c.String(),
                        Population = c.String(),
                        Households = c.String(),
                        Built_up_area = c.String(),
                        Built_up_sub_division = c.String(),
                        Lower_layer_super_output_area = c.String(),
                        Added = c.DateTimeOffset(precision: 7),
                    })
                .PrimaryKey(t => t.Postcode);
            
            CreateIndex("dbo.PostCodes", "ClusteredID", clustered: true);
            CreateIndex("dbo.PostCodes", "Postcode", unique: true);
        }
    }
}
