namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangePostCode2ToPostCodes : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.PostCode2", newName: "PostCodes");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.PostCodes", newName: "PostCode2");
        }
    }
}
