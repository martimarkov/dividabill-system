namespace DividaBill.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateTrackerContext : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AuditLogDetails", "AuditLogId", "dbo.AuditLogs");
            DropIndex("dbo.AuditLogDetails", new[] { "AuditLogId" });
            DropPrimaryKey("dbo.AuditLogs");
            DropPrimaryKey("dbo.AuditLogDetails");
            AddColumn("dbo.AuditLogs", "TypeFullName", c => c.String(nullable: false, maxLength: 256));
            AddColumn("dbo.AuditLogDetails", "PropertyName", c => c.String(nullable: false, maxLength: 256));
            AlterColumn("dbo.AuditLogs", "AuditLogId", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.AuditLogDetails", "Id", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.AuditLogDetails", "AuditLogId", c => c.Long(nullable: false));
            AddPrimaryKey("dbo.AuditLogs", "AuditLogId");
            AddPrimaryKey("dbo.AuditLogDetails", "Id");
            CreateIndex("dbo.AuditLogDetails", "AuditLogId");
            AddForeignKey("dbo.AuditLogDetails", "AuditLogId", "dbo.AuditLogs", "AuditLogId", cascadeDelete: true);
            DropColumn("dbo.AuditLogs", "TableName");
            DropColumn("dbo.AuditLogDetails", "ColumnName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AuditLogDetails", "ColumnName", c => c.String(nullable: false, maxLength: 256));
            AddColumn("dbo.AuditLogs", "TableName", c => c.String(nullable: false, maxLength: 256));
            DropForeignKey("dbo.AuditLogDetails", "AuditLogId", "dbo.AuditLogs");
            DropIndex("dbo.AuditLogDetails", new[] { "AuditLogId" });
            DropPrimaryKey("dbo.AuditLogDetails");
            DropPrimaryKey("dbo.AuditLogs");
            AlterColumn("dbo.AuditLogDetails", "AuditLogId", c => c.Int(nullable: false));
            AlterColumn("dbo.AuditLogDetails", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.AuditLogs", "AuditLogId", c => c.Int(nullable: false, identity: true));
            DropColumn("dbo.AuditLogDetails", "PropertyName");
            DropColumn("dbo.AuditLogs", "TypeFullName");
            AddPrimaryKey("dbo.AuditLogDetails", "Id");
            AddPrimaryKey("dbo.AuditLogs", "AuditLogId");
            CreateIndex("dbo.AuditLogDetails", "AuditLogId");
            AddForeignKey("dbo.AuditLogDetails", "AuditLogId", "dbo.AuditLogs", "AuditLogId", cascadeDelete: true);
        }
    }
}
