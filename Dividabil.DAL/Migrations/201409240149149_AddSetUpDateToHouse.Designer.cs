// <auto-generated />
namespace DividaBill.DAL.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.1-30610")]
    public sealed partial class AddSetUpDateToHouse : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddSetUpDateToHouse));
        
        string IMigrationMetadata.Id
        {
            get { return "201409240149149_AddSetUpDateToHouse"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return Resources.GetString("Source"); }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
