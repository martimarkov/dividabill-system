namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBroadbandTypeToHouseModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HouseModels", "BroadbandType", c => c.Int(nullable: false, defaultValue: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.HouseModels", "BroadbandType");
        }
    }
}
