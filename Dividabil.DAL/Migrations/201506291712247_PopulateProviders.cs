namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateProviders : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO dbo.Providers (Name, PhoneNumber, ContactPerson) VALUES ('GB Energy', '01772 865740','Phil Darwick')");
            Sql("INSERT INTO dbo.Providers (Name, PhoneNumber, ContactPerson) VALUES ('BT Local Business', '01189777583','Harminder Chadda')");
            Sql("INSERT INTO dbo.Providers (Name, PhoneNumber, ContactPerson) VALUES ('British Gas', '','')");
            Sql("INSERT INTO dbo.Providers (Name, PhoneNumber, ContactPerson) VALUES ('Origin Broadband', '','')");
            Sql("INSERT INTO dbo.Providers (Name, PhoneNumber, ContactPerson) VALUES ('Netflix', '','')");
            Sql("INSERT INTO dbo.Providers (Name, PhoneNumber, ContactPerson) VALUES ('Water Company Place Holder', '','')");
            Sql("INSERT INTO dbo.Providers (Name, PhoneNumber, ContactPerson) VALUES ('TV License Company Limited', '','')");
            Sql("INSERT INTO dbo.Providers (Name, PhoneNumber, ContactPerson) VALUES ('Sky TV', '08442411611','Daniel Lacey')"); 
        }
        
        public override void Down()
        {
        }
    }
}
