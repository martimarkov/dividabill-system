namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenameFibre38ToFibre40 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HouseEstimatedPrices", "BroadbandFibre40RouterMargin", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.HouseEstimatedPrices", "BroadbandFibre38RouterMargin");
        }
        
        public override void Down()
        {
            AddColumn("dbo.HouseEstimatedPrices", "BroadbandFibre38RouterMargin", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.HouseEstimatedPrices", "BroadbandFibre40RouterMargin");
        }
    }
}
