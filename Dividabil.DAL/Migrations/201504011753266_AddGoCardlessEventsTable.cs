namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddGoCardlessEventsTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GoCardlessEvents",
                c => new
                    {
                        id = c.String(nullable: false, maxLength: 128),
                        created_at = c.String(),
                        eventMessage = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.GoCardlessEvents");
        }
    }
}
