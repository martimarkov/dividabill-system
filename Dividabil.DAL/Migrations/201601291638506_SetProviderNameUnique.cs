namespace DividaBill.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SetProviderNameUnique : DbMigration
    {
        public override void Up()
        {
            Sql("alter table providers alter column name nvarchar(100);");

            Sql(@"BEGIN TRAN
declare @duplicated_providers table (
name nvarchar(max),
cnt int
);

insert into @duplicated_providers
select name,count(*) cnt from providers
    group by name
    having count(*)=2;

update PLeft
set 
    PLeft.PhoneNumber = PLeft.PhoneNumber+'; '+PLeft.PhoneNumber,
    PLeft.ContactPerson = PLeft.ContactPerson+'; '+PLeft.ContactPerson,
    PLeft.AverageInstallationDelay = case when PLeft.AverageInstallationDelay>PRight.AverageInstallationDelay then PLeft.AverageInstallationDelay else PRight.AverageInstallationDelay end
from @duplicated_providers dp
join providers as PLeft on dp.name=PLeft.name
join providers as PRight on dp.name=PRight.name and PRight.id>PLeft.id
;

update c
set c.provider_id=PLeft.id
from 
@duplicated_providers dp
join providers as PLeft on dp.name=PLeft.name
join providers as PRight on dp.name=PRight.name and PRight.id>PLeft.id
join contracts c on c.provider_id=PRight.id
;

update stp
set stp.provider_id=PLeft.id
from 
@duplicated_providers dp
join providers as PLeft on dp.name=PLeft.name
join providers as PRight on dp.name=PRight.name and PRight.id>PLeft.id
join ServiceTypeProviders stp on stp.provider_id=PRight.id
;

delete from providers
where id in (
select PRight.ID from 
@duplicated_providers dp
join providers as PLeft on dp.name=PLeft.name
join providers as PRight on dp.name=PRight.name and PRight.id>PLeft.id)
;

commit tran;");

            CreateIndex("dbo.Providers", "Name", unique: true, name: "IX_ProviderName");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Providers", "IX_ProviderName");
        }
    }
}
