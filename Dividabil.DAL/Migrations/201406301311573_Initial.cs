namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Bills",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PowerBill = c.Decimal(nullable: false, precision: 18, scale: 2),
                        WaterBill = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Month = c.DateTime(nullable: false),
                        Note = c.String(),
                        Adjustment = c.Decimal(nullable: false, precision: 18, scale: 2),
                        House_ID = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.HouseModels", t => t.House_ID)
                .Index(t => t.House_ID);
            
            CreateTable(
                "dbo.HouseModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Housemates = c.Int(nullable: false),
                        Address_Street = c.String(),
                        Address_City = c.String(),
                        Address_Postcode = c.String(),
                        Address_County = c.String(),
                        Address_Position = c.Geography(),
                        Electricity = c.Boolean(nullable: false),
                        Gas = c.Boolean(nullable: false),
                        Water = c.Boolean(nullable: false),
                        Media = c.Boolean(nullable: false),
                        Note = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        RegisteredDate = c.DateTime(nullable: false),
                        Coupon_Id = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Coupons", t => t.Coupon_Id)
                .Index(t => t.Coupon_Id);
            
            CreateTable(
                "dbo.Coupons",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        Text = c.String(),
                        Type = c.Int(nullable: false),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        RefTitle = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        AccountNumber = c.Int(nullable: false),
                        AccountSortCode = c.Int(nullable: false),
                        AccountHolder = c.String(),
                        AccountSoleHolder = c.Boolean(nullable: false),
                        DOB = c.DateTime(nullable: false),
                        HomeAddress_Street = c.String(),
                        HomeAddress_City = c.String(),
                        HomeAddress_Postcode = c.String(),
                        HomeAddress_County = c.String(),
                        HomeAddress_Position = c.Geography(),
                        Note = c.String(),
                        NewsletterSubscription = c.Boolean(nullable: false),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                        House_ID = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.HouseModels", t => t.House_ID)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex")
                .Index(t => t.House_ID);
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Email = c.String(),
                        Subject = c.String(),
                        MessageText = c.String(),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Premises",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PAId = c.Int(nullable: false),
                        StreetAddress = c.String(),
                        Place = c.String(),
                        Postcode = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Bills", "House_ID", "dbo.HouseModels");
            DropForeignKey("dbo.HouseModels", "Coupon_Id", "dbo.Coupons");
            DropForeignKey("dbo.Coupons", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "House_ID", "dbo.HouseModels");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", new[] { "House_ID" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.Coupons", new[] { "User_Id" });
            DropIndex("dbo.HouseModels", new[] { "Coupon_Id" });
            DropIndex("dbo.Bills", new[] { "House_ID" });
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Premises");
            DropTable("dbo.Messages");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Coupons");
            DropTable("dbo.HouseModels");
            DropTable("dbo.Bills");
        }
    }
}
