namespace DividaBill.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RefactorModels : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BillBases", "House_ID", "dbo.HouseModels");
            DropForeignKey("dbo.UsersLandlinePhoneBillsBillsPercentages", "BillId", "dbo.LandlinePhoneBills");
            DropForeignKey("dbo.UsersLandlinePhoneBillsBillsPercentages", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UsersLineRentalBillsBillsPercentages", "BillId", "dbo.LandlinePhoneBills");
            DropForeignKey("dbo.UsersLineRentalBillsBillsPercentages", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UsersLineRentalBillsBillsPercentages", "LineRentalBill_Id", "dbo.LineRentalBills");
            DropForeignKey("dbo.UsersElectricityBillsPercentages", "BillId", "dbo.ElectricityBills");
            DropForeignKey("dbo.UsersElectricityBillsPercentages", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UsersGasBillsPercentages", "BillId", "dbo.GasBills");
            DropForeignKey("dbo.UsersGasBillsPercentages", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UsersNetflixTVBillsBillsPercentages", "BillId", "dbo.NetflixTVBills");
            DropForeignKey("dbo.UsersNetflixTVBillsBillsPercentages", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UsersSkyTVBillsBillsPercentages", "BillId", "dbo.SkyTVBills");
            DropForeignKey("dbo.UsersSkyTVBillsBillsPercentages", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UsersTVLicenseBillsBillsPercentages", "BillId", "dbo.TVLicenseBills");
            DropForeignKey("dbo.UsersTVLicenseBillsBillsPercentages", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UsersWaterBillsPercentages", "BillId", "dbo.WaterBills");
            DropForeignKey("dbo.UsersWaterBillsPercentages", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UsersBroadbandBillsBillsPercentages", "BillId", "dbo.BroadbandBills");
            DropForeignKey("dbo.UsersBroadbandBillsBillsPercentages", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UsersDeliveryBillsPercentages", "BillId", "dbo.DeliveryBills");
            DropForeignKey("dbo.UsersDeliveryBillsPercentages", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Bills", "HouseModelID", "dbo.HouseModels");
            DropIndex("dbo.BillBases", new[] { "House_ID" });
            DropIndex("dbo.Contracts", "ID_HouseContractType");
            DropIndex("dbo.UsersLandlinePhoneBillsBillsPercentages", new[] { "UserId" });
            DropIndex("dbo.UsersLandlinePhoneBillsBillsPercentages", new[] { "BillId" });
            DropIndex("dbo.UsersLineRentalBillsBillsPercentages", new[] { "UserId" });
            DropIndex("dbo.UsersLineRentalBillsBillsPercentages", new[] { "BillId" });
            DropIndex("dbo.UsersLineRentalBillsBillsPercentages", new[] { "LineRentalBill_Id" });
            DropIndex("dbo.UsersElectricityBillsPercentages", new[] { "UserId" });
            DropIndex("dbo.UsersElectricityBillsPercentages", new[] { "BillId" });
            DropIndex("dbo.UsersGasBillsPercentages", new[] { "UserId" });
            DropIndex("dbo.UsersGasBillsPercentages", new[] { "BillId" });
            DropIndex("dbo.UsersNetflixTVBillsBillsPercentages", new[] { "UserId" });
            DropIndex("dbo.UsersNetflixTVBillsBillsPercentages", new[] { "BillId" });
            DropIndex("dbo.UsersSkyTVBillsBillsPercentages", new[] { "UserId" });
            DropIndex("dbo.UsersSkyTVBillsBillsPercentages", new[] { "BillId" });
            DropIndex("dbo.UsersTVLicenseBillsBillsPercentages", new[] { "UserId" });
            DropIndex("dbo.UsersTVLicenseBillsBillsPercentages", new[] { "BillId" });
            DropIndex("dbo.UsersWaterBillsPercentages", new[] { "UserId" });
            DropIndex("dbo.UsersWaterBillsPercentages", new[] { "BillId" });
            DropIndex("dbo.UsersBroadbandBillsBillsPercentages", new[] { "UserId" });
            DropIndex("dbo.UsersBroadbandBillsBillsPercentages", new[] { "BillId" });
            DropIndex("dbo.UsersDeliveryBillsPercentages", new[] { "UserId" });
            DropIndex("dbo.UsersDeliveryBillsPercentages", new[] { "BillId" });
            DropIndex("dbo.Bills", new[] { "HouseModelID" });
            DropIndex("dbo.ElectricityBills", new[] { "ElectricityContract_ID" });
            DropIndex("dbo.GasBills", new[] { "GasContract_ID" });
            DropIndex("dbo.WaterBills", new[] { "WaterContract_ID" });
            DropIndex("dbo.BroadbandBills", new[] { "BroadbandContract_ID" });
            DropIndex("dbo.LandlinePhoneBills", new[] { "LandlinePhoneContract_ID" });
            DropIndex("dbo.LineRentalBills", new[] { "LineRentalContract_ID" });
            DropIndex("dbo.NetflixTVBills", new[] { "NetflixTVContract_ID" });
            DropIndex("dbo.SkyTVBills", new[] { "SkyTVContract_ID" });
            DropIndex("dbo.TVLicenseBills", new[] { "TVLicenseContract_ID" });
            //RenameColumn(table: "dbo.BillBases", name: "BroadbandContract_ID", newName: "Contract_Id");
            //RenameColumn(table: "dbo.BillBases", name: "LandlinePhoneContract_ID", newName: "Contract_Id");
            //RenameColumn(table: "dbo.BillBases", name: "LineRentalContract_ID", newName: "Contract_Id");
            //RenameColumn(table: "dbo.BillBases", name: "ElectricityContract_ID", newName: "Contract_Id");
            //RenameColumn(table: "dbo.BillBases", name: "GasContract_ID", newName: "Contract_Id");
            //RenameColumn(table: "dbo.BillBases", name: "NetflixTVContract_ID", newName: "Contract_Id");
            //RenameColumn(table: "dbo.BillBases", name: "SkyTVContract_ID", newName: "Contract_Id");
            //RenameColumn(table: "dbo.BillBases", name: "TVLicenseContract_ID", newName: "Contract_Id");
            //RenameColumn(table: "dbo.BillBases", name: "WaterContract_ID", newName: "Contract_Id");
            AddColumn("dbo.BillBases", "Contract_Id", c => c.Int());

            CreateTable(
                "dbo.ServiceTypes",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Name = c.String(),
                        Abbreviation = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.BillPayments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Bill_Id = c.Int(nullable: false),
                        User_Id = c.String(maxLength: 128),
                        PercentageOfBill = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsPaid = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BillBases", t => t.Bill_Id, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => new { t.Bill_Id, t.User_Id }, unique: true, name: "IX_Payment_UserBill");
            
            CreateTable(
                "dbo.HouseholdPercentages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        House_ID = c.Int(),
                        Service_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.HouseModels", t => t.House_ID)
                .ForeignKey("dbo.ServiceTypes", t => t.Service_Id)
                .Index(t => t.House_ID)
                .Index(t => t.Service_Id);
            
            CreateTable(
                "dbo.UserPercentages",
                c => new
                    {
                        HousePercentage_Id = c.Int(nullable: false),
                        User_Id = c.String(nullable: false, maxLength: 128),
                        PercentageOfBill = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DecisionDate = c.DateTimeOffset(precision: 7),
                        IsApproved = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.HousePercentage_Id, t.User_Id })
                .ForeignKey("dbo.HouseholdPercentages", t => t.HousePercentage_Id, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id, cascadeDelete: true)
                .Index(t => t.HousePercentage_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.HouseholdServiceEstimates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        House_Id = c.Int(nullable: false),
                        Service_Id = c.Int(nullable: false),
                        Mean = c.Double(nullable: false),
                        Variance = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.HouseModels", t => t.House_Id, cascadeDelete: true)
                .ForeignKey("dbo.ServiceTypes", t => t.Service_Id, cascadeDelete: true)
                .Index(t => t.House_Id)
                .Index(t => t.Service_Id);
            
            CreateTable(
                "dbo.ServiceTypeProviders",
                c => new
                    {
                        ServiceType_Id = c.Int(nullable: false),
                        Provider_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ServiceType_Id, t.Provider_ID })
                .ForeignKey("dbo.ServiceTypes", t => t.ServiceType_Id, cascadeDelete: true)
                .ForeignKey("dbo.Providers", t => t.Provider_ID, cascadeDelete: true)
                .Index(t => t.ServiceType_Id)
                .Index(t => t.Provider_ID);
            
            AddColumn("dbo.BillBases", "HouseholdBill_Id", c => c.Int());
            AddColumn("dbo.Contracts", "ServiceNew_ID", c => c.Int());
            AddColumn("dbo.Contracts", "PackageRaw", c => c.Int(nullable: false));
            AddColumn("dbo.Contracts", "RequestedStartDate", c => c.DateTimeOffset(nullable: false, precision: 7));
            AddColumn("dbo.Providers", "AverageInstallationDelay", c => c.Int(nullable: false));
            AddColumn("dbo.HouseholdBills", "Month", c => c.Int(nullable: false));
            AddColumn("dbo.HouseholdBills", "Year", c => c.Int(nullable: false));
            AlterColumn("dbo.Contracts", "StartDate", c => c.DateTimeOffset(precision: 7));
            CreateIndex("dbo.Contracts", "House_ID");
            CreateIndex("dbo.Contracts", "ServiceNew_ID");
            CreateIndex("dbo.BillBases", "Contract_Id");
            CreateIndex("dbo.BillBases", "HouseholdBill_Id");
            AddForeignKey("dbo.Contracts", "ServiceNew_ID", "dbo.ServiceTypes", "Id");
            AddForeignKey("dbo.BillBases", "HouseholdBill_Id", "dbo.HouseholdBills", "ID");
            DropColumn("dbo.HouseModels", "SetUpDate");
            DropColumn("dbo.HouseModels", "Archived");
            DropColumn("dbo.HouseModels", "EazyCollectAdded");
            DropColumn("dbo.BillBases", "Adjustment");
            DropColumn("dbo.BillBases", "Status");
            DropColumn("dbo.BillBases", "EndDate");
            DropColumn("dbo.BillBases", "Type");
            DropColumn("dbo.BroadbandContracts", "Speed");
            DropColumn("dbo.Contracts", "Status");
            DropColumn("dbo.LandlinePhoneContracts", "Package");
            DropColumn("dbo.HouseholdBills", "StartDate");
            DropColumn("dbo.HouseholdBills", "EndDate");
            DropTable("dbo.UsersLandlinePhoneBillsBillsPercentages");
            DropTable("dbo.UsersLineRentalBillsBillsPercentages");
            DropTable("dbo.UsersElectricityBillsPercentages");
            DropTable("dbo.UsersGasBillsPercentages");
            DropTable("dbo.UsersNetflixTVBillsBillsPercentages");
            DropTable("dbo.UsersSkyTVBillsBillsPercentages");
            DropTable("dbo.UsersTVLicenseBillsBillsPercentages");
            DropTable("dbo.UsersWaterBillsPercentages");
            DropTable("dbo.UsersBroadbandBillsBillsPercentages");
            DropTable("dbo.UsersDeliveryBillsPercentages");
            DropTable("dbo.Bills");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Bills",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ElectricBill = c.Decimal(nullable: false, precision: 18, scale: 2),
                        GasBill = c.Decimal(nullable: false, precision: 18, scale: 2),
                        WaterBill = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DeliveryFee = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Month = c.DateTime(),
                        HouseModelID = c.Int(nullable: false),
                        Note = c.String(),
                        Adjustment = c.Decimal(precision: 18, scale: 2),
                        Status = c.Int(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        PaymentDate = c.DateTime(nullable: false),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UsersDeliveryBillsPercentages",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        BillId = c.Int(nullable: false),
                        Percentage = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.BillId });
            
            CreateTable(
                "dbo.UsersBroadbandBillsBillsPercentages",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        BillId = c.Int(nullable: false),
                        Percentage = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.BillId });
            
            CreateTable(
                "dbo.UsersWaterBillsPercentages",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        BillId = c.Int(nullable: false),
                        Percentage = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.BillId });
            
            CreateTable(
                "dbo.UsersTVLicenseBillsBillsPercentages",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        BillId = c.Int(nullable: false),
                        Percentage = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.BillId });
            
            CreateTable(
                "dbo.UsersSkyTVBillsBillsPercentages",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        BillId = c.Int(nullable: false),
                        Percentage = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.BillId });
            
            CreateTable(
                "dbo.UsersNetflixTVBillsBillsPercentages",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        BillId = c.Int(nullable: false),
                        Percentage = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.BillId });
            
            CreateTable(
                "dbo.UsersGasBillsPercentages",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        BillId = c.Int(nullable: false),
                        Percentage = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.BillId });
            
            CreateTable(
                "dbo.UsersElectricityBillsPercentages",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        BillId = c.Int(nullable: false),
                        Percentage = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.BillId });
            
            CreateTable(
                "dbo.UsersLineRentalBillsBillsPercentages",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        BillId = c.Int(nullable: false),
                        Percentage = c.Int(nullable: false),
                        LineRentalBill_Id = c.Int(),
                    })
                .PrimaryKey(t => new { t.UserId, t.BillId });
            
            CreateTable(
                "dbo.UsersLandlinePhoneBillsBillsPercentages",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        BillId = c.Int(nullable: false),
                        Percentage = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.BillId });
            
            AddColumn("dbo.HouseholdBills", "EndDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.HouseholdBills", "StartDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.LandlinePhoneContracts", "Package", c => c.Int(nullable: false));
            AddColumn("dbo.Contracts", "Status", c => c.Int(nullable: false));
            AddColumn("dbo.BroadbandContracts", "Speed", c => c.Int(nullable: false));
            AddColumn("dbo.BillBases", "Type", c => c.Int(nullable: false));
            AddColumn("dbo.BillBases", "EndDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.BillBases", "Status", c => c.Int(nullable: false));
            AddColumn("dbo.BillBases", "Adjustment", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.HouseModels", "EazyCollectAdded", c => c.Boolean(nullable: false));
            AddColumn("dbo.HouseModels", "Archived", c => c.Boolean(nullable: false));
            AddColumn("dbo.HouseModels", "SetUpDate", c => c.DateTime());
            DropForeignKey("dbo.HouseholdServiceEstimates", "Service_Id", "dbo.ServiceTypes");
            DropForeignKey("dbo.HouseholdServiceEstimates", "House_Id", "dbo.HouseModels");
            DropForeignKey("dbo.UserPercentages", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserPercentages", "HousePercentage_Id", "dbo.HouseholdPercentages");
            DropForeignKey("dbo.HouseholdPercentages", "Service_Id", "dbo.ServiceTypes");
            DropForeignKey("dbo.HouseholdPercentages", "House_ID", "dbo.HouseModels");
            DropForeignKey("dbo.BillPayments", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.BillPayments", "Bill_Id", "dbo.BillBases");
            DropForeignKey("dbo.BillBases", "HouseholdBill_Id", "dbo.HouseholdBills");
            DropForeignKey("dbo.Contracts", "ServiceNew_ID", "dbo.ServiceTypes");
            DropForeignKey("dbo.ServiceTypeProviders", "Provider_ID", "dbo.Providers");
            DropForeignKey("dbo.ServiceTypeProviders", "ServiceType_Id", "dbo.ServiceTypes");
            DropIndex("dbo.ServiceTypeProviders", new[] { "Provider_ID" });
            DropIndex("dbo.ServiceTypeProviders", new[] { "ServiceType_Id" });
            DropIndex("dbo.HouseholdServiceEstimates", new[] { "Service_Id" });
            DropIndex("dbo.HouseholdServiceEstimates", new[] { "House_Id" });
            DropIndex("dbo.UserPercentages", new[] { "User_Id" });
            DropIndex("dbo.UserPercentages", new[] { "HousePercentage_Id" });
            DropIndex("dbo.HouseholdPercentages", new[] { "Service_Id" });
            DropIndex("dbo.HouseholdPercentages", new[] { "House_ID" });
            DropIndex("dbo.BillPayments", "IX_Payment_UserBill");
            DropIndex("dbo.BillBases", new[] { "HouseholdBill_Id" });
            DropIndex("dbo.BillBases", new[] { "Contract_Id" });
            DropIndex("dbo.Contracts", new[] { "ServiceNew_ID" });
            DropIndex("dbo.Contracts", new[] { "House_ID" });
            AlterColumn("dbo.Contracts", "StartDate", c => c.DateTimeOffset(nullable: false, precision: 7));
            DropColumn("dbo.HouseholdBills", "Year");
            DropColumn("dbo.HouseholdBills", "Month");
            DropColumn("dbo.Providers", "AverageInstallationDelay");
            DropColumn("dbo.Contracts", "RequestedStartDate");
            DropColumn("dbo.Contracts", "PackageRaw");
            DropColumn("dbo.Contracts", "ServiceNew_ID");
            DropColumn("dbo.BillBases", "HouseholdBill_Id");
            DropTable("dbo.ServiceTypeProviders");
            DropTable("dbo.HouseholdServiceEstimates");
            DropTable("dbo.UserPercentages");
            DropTable("dbo.HouseholdPercentages");
            DropTable("dbo.BillPayments");
            DropTable("dbo.ServiceTypes");
            RenameColumn(table: "dbo.BillBases", name: "Contract_Id", newName: "WaterContract_ID");
            RenameColumn(table: "dbo.BillBases", name: "Contract_Id", newName: "TVLicenseContract_ID");
            RenameColumn(table: "dbo.BillBases", name: "Contract_Id", newName: "SkyTVContract_ID");
            RenameColumn(table: "dbo.BillBases", name: "Contract_Id", newName: "NetflixTVContract_ID");
            RenameColumn(table: "dbo.BillBases", name: "Contract_Id", newName: "GasContract_ID");
            RenameColumn(table: "dbo.BillBases", name: "Contract_Id", newName: "ElectricityContract_ID");
            RenameColumn(table: "dbo.BillBases", name: "Contract_Id", newName: "LineRentalContract_ID");
            RenameColumn(table: "dbo.BillBases", name: "Contract_Id", newName: "LandlinePhoneContract_ID");
            RenameColumn(table: "dbo.BillBases", name: "Contract_Id", newName: "BroadbandContract_ID");
            CreateIndex("dbo.TVLicenseBills", "TVLicenseContract_ID");
            CreateIndex("dbo.SkyTVBills", "SkyTVContract_ID");
            CreateIndex("dbo.NetflixTVBills", "NetflixTVContract_ID");
            CreateIndex("dbo.LineRentalBills", "LineRentalContract_ID");
            CreateIndex("dbo.LandlinePhoneBills", "LandlinePhoneContract_ID");
            CreateIndex("dbo.BroadbandBills", "BroadbandContract_ID");
            CreateIndex("dbo.WaterBills", "WaterContract_ID");
            CreateIndex("dbo.GasBills", "GasContract_ID");
            CreateIndex("dbo.ElectricityBills", "ElectricityContract_ID");
            CreateIndex("dbo.Bills", "HouseModelID");
            CreateIndex("dbo.UsersDeliveryBillsPercentages", "BillId");
            CreateIndex("dbo.UsersDeliveryBillsPercentages", "UserId");
            CreateIndex("dbo.UsersBroadbandBillsBillsPercentages", "BillId");
            CreateIndex("dbo.UsersBroadbandBillsBillsPercentages", "UserId");
            CreateIndex("dbo.UsersWaterBillsPercentages", "BillId");
            CreateIndex("dbo.UsersWaterBillsPercentages", "UserId");
            CreateIndex("dbo.UsersTVLicenseBillsBillsPercentages", "BillId");
            CreateIndex("dbo.UsersTVLicenseBillsBillsPercentages", "UserId");
            CreateIndex("dbo.UsersSkyTVBillsBillsPercentages", "BillId");
            CreateIndex("dbo.UsersSkyTVBillsBillsPercentages", "UserId");
            CreateIndex("dbo.UsersNetflixTVBillsBillsPercentages", "BillId");
            CreateIndex("dbo.UsersNetflixTVBillsBillsPercentages", "UserId");
            CreateIndex("dbo.UsersGasBillsPercentages", "BillId");
            CreateIndex("dbo.UsersGasBillsPercentages", "UserId");
            CreateIndex("dbo.UsersElectricityBillsPercentages", "BillId");
            CreateIndex("dbo.UsersElectricityBillsPercentages", "UserId");
            CreateIndex("dbo.UsersLineRentalBillsBillsPercentages", "LineRentalBill_Id");
            CreateIndex("dbo.UsersLineRentalBillsBillsPercentages", "BillId");
            CreateIndex("dbo.UsersLineRentalBillsBillsPercentages", "UserId");
            CreateIndex("dbo.UsersLandlinePhoneBillsBillsPercentages", "BillId");
            CreateIndex("dbo.UsersLandlinePhoneBillsBillsPercentages", "UserId");
            CreateIndex("dbo.Contracts", new[] { "House_ID", "Service" }, unique: true, name: "ID_HouseContractType");
            CreateIndex("dbo.BillBases", "House_ID");
            AddForeignKey("dbo.Bills", "HouseModelID", "dbo.HouseModels", "ID", cascadeDelete: true);
            AddForeignKey("dbo.UsersDeliveryBillsPercentages", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.UsersDeliveryBillsPercentages", "BillId", "dbo.DeliveryBills", "Id");
            AddForeignKey("dbo.UsersBroadbandBillsBillsPercentages", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.UsersBroadbandBillsBillsPercentages", "BillId", "dbo.BroadbandBills", "Id");
            AddForeignKey("dbo.UsersWaterBillsPercentages", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.UsersWaterBillsPercentages", "BillId", "dbo.WaterBills", "Id");
            AddForeignKey("dbo.UsersTVLicenseBillsBillsPercentages", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.UsersTVLicenseBillsBillsPercentages", "BillId", "dbo.TVLicenseBills", "Id");
            AddForeignKey("dbo.UsersSkyTVBillsBillsPercentages", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.UsersSkyTVBillsBillsPercentages", "BillId", "dbo.SkyTVBills", "Id");
            AddForeignKey("dbo.UsersNetflixTVBillsBillsPercentages", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.UsersNetflixTVBillsBillsPercentages", "BillId", "dbo.NetflixTVBills", "Id");
            AddForeignKey("dbo.UsersGasBillsPercentages", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.UsersGasBillsPercentages", "BillId", "dbo.GasBills", "Id");
            AddForeignKey("dbo.UsersElectricityBillsPercentages", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.UsersElectricityBillsPercentages", "BillId", "dbo.ElectricityBills", "Id");
            AddForeignKey("dbo.UsersLineRentalBillsBillsPercentages", "LineRentalBill_Id", "dbo.LineRentalBills", "Id");
            AddForeignKey("dbo.UsersLineRentalBillsBillsPercentages", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.UsersLineRentalBillsBillsPercentages", "BillId", "dbo.LandlinePhoneBills", "Id");
            AddForeignKey("dbo.UsersLandlinePhoneBillsBillsPercentages", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.UsersLandlinePhoneBillsBillsPercentages", "BillId", "dbo.LandlinePhoneBills", "Id");
            AddForeignKey("dbo.BillBases", "House_ID", "dbo.HouseModels", "ID", cascadeDelete: true);
        }
    }
}
