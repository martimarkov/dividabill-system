namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEmailToBill : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BillEmailMessages",
                c => new
                    {
                        ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.HouseEmailMessages", t => t.ID)
                .Index(t => t.ID);
            
            AddColumn("dbo.HouseholdBills", "SentEmail_ID", c => c.Int(nullable: false));
            CreateIndex("dbo.HouseholdBills", "SentEmail_ID");
            AddForeignKey("dbo.HouseholdBills", "SentEmail_ID", "dbo.BillEmailMessages", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BillEmailMessages", "ID", "dbo.HouseEmailMessages");
            DropForeignKey("dbo.HouseholdBills", "SentEmail_ID", "dbo.BillEmailMessages");
            DropIndex("dbo.BillEmailMessages", new[] { "ID" });
            DropIndex("dbo.HouseholdBills", new[] { "SentEmail_ID" });
            DropColumn("dbo.HouseholdBills", "SentEmail_ID");
            DropTable("dbo.BillEmailMessages");
        }
    }
}
