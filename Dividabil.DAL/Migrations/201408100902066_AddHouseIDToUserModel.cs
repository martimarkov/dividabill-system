namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddHouseIDToUserModel : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AspNetUsers", "House_ID", "dbo.HouseModels");
            DropIndex("dbo.AspNetUsers", new[] { "House_ID" });
            AlterColumn("dbo.AspNetUsers", "House_ID", c => c.Int(nullable: true));
            CreateIndex("dbo.AspNetUsers", "House_ID");
            AddForeignKey("dbo.AspNetUsers", "House_ID", "dbo.HouseModels", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "House_ID", "dbo.HouseModels");
            DropIndex("dbo.AspNetUsers", new[] { "House_ID" });
            AlterColumn("dbo.AspNetUsers", "House_ID", c => c.Int());
            CreateIndex("dbo.AspNetUsers", "House_ID");
            AddForeignKey("dbo.AspNetUsers", "House_ID", "dbo.HouseModels", "ID");
        }
    }
}
