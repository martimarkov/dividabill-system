namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSentUserEmailsToBill : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserEmailMessages", "HouseholdBill_ID", c => c.Int());
            CreateIndex("dbo.UserEmailMessages", "HouseholdBill_ID");
            AddForeignKey("dbo.UserEmailMessages", "HouseholdBill_ID", "dbo.HouseholdBills", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserEmailMessages", "HouseholdBill_ID", "dbo.HouseholdBills");
            DropIndex("dbo.UserEmailMessages", new[] { "HouseholdBill_ID" });
            DropColumn("dbo.UserEmailMessages", "HouseholdBill_ID");
        }
    }
}
