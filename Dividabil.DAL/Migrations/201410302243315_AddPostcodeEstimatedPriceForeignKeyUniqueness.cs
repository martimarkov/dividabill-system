namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPostcodeEstimatedPriceForeignKeyUniqueness : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.PostcodeEstimatedPrices", new[] { "PostCode_ID" });
            CreateIndex("dbo.PostcodeEstimatedPrices", "PostCode_ID", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("dbo.PostcodeEstimatedPrices", new[] { "PostCode_ID" });
            CreateIndex("dbo.PostcodeEstimatedPrices", "PostCode_ID");
        }
    }
}
