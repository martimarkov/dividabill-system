namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUniquenessToHouseIDInContracts : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Contracts", new[] { "House_ID" });
            CreateIndex("dbo.Contracts", new[] { "House_ID", "Service" }, unique: true, name: "ID_HouseContractType");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Contracts", "ID_HouseContractType");
            CreateIndex("dbo.Contracts", "House_ID");
        }
    }
}
