namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUDPRNToHouseModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "HomeAddress_UDPRN", c => c.String());
            AddColumn("dbo.HouseModels", "Address_UDPRN", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.HouseModels", "Address_UDPRN");
            DropColumn("dbo.AspNetUsers", "HomeAddress_UDPRN");
        }
    }
}
