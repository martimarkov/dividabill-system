namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDeliveryFeeToBills : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Bills", "DeliveryFee", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Bills", "DeliveryFee");
        }
    }
}
