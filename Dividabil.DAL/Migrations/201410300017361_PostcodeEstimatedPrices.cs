namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PostcodeEstimatedPrices : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PostcodeEstimatedPrices",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PostCode_ID = c.Guid(nullable: false),
                        type = c.Int(nullable: false),
                        internalPrice = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.PostCodes", t => t.PostCode_ID, cascadeDelete: true)
                .Index(t => t.PostCode_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PostcodeEstimatedPrices", "PostCode_ID", "dbo.PostCodes");
            DropIndex("dbo.PostcodeEstimatedPrices", new[] { "PostCode_ID" });
            DropTable("dbo.PostcodeEstimatedPrices");
        }
    }
}
