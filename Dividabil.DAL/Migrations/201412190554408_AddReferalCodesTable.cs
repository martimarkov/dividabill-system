namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddReferalCodesTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ReferalCodes",
                c => new
                    {
                        ID = c.String(nullable: false, maxLength: 128),
                        DiscountCode_Id = c.Int(),
                        Owner_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Coupons", t => t.DiscountCode_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.Owner_Id)
                .Index(t => t.DiscountCode_Id)
                .Index(t => t.Owner_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ReferalCodes", "Owner_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.ReferalCodes", "DiscountCode_Id", "dbo.Coupons");
            DropIndex("dbo.ReferalCodes", new[] { "Owner_Id" });
            DropIndex("dbo.ReferalCodes", new[] { "DiscountCode_Id" });
            DropTable("dbo.ReferalCodes");
        }
    }
}
