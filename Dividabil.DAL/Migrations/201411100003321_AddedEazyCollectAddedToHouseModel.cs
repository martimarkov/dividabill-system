namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedEazyCollectAddedToHouseModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HouseModels", "EazyCollectAdded", c => c.Boolean(nullable: false));
            AlterColumn("dbo.AspNetUsers", "AccountNumber", c => c.String(maxLength: 8));
            AlterColumn("dbo.AspNetUsers", "AccountSortCode", c => c.String(maxLength: 6));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AspNetUsers", "AccountSortCode", c => c.String());
            AlterColumn("dbo.AspNetUsers", "AccountNumber", c => c.String());
            DropColumn("dbo.HouseModels", "EazyCollectAdded");
        }
    }
}
