namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveRequiredFromBankAccountTable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BankAccounts", "account_number", c => c.String());
            AlterColumn("dbo.BankAccounts", "sort_code", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.BankAccounts", "sort_code", c => c.String(nullable: false));
            AlterColumn("dbo.BankAccounts", "account_number", c => c.String(nullable: false));
        }
    }
}
