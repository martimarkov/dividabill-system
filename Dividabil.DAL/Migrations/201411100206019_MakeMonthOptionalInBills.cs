namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MakeMonthOptionalInBills : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Bills", "Month", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Bills", "Month", c => c.DateTime(nullable: false));
        }
    }
}
