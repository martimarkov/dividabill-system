namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangePremisesPAIdToDouble : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Premises", "PAId", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Premises", "PAId", c => c.Single(nullable: false));
        }
    }
}
