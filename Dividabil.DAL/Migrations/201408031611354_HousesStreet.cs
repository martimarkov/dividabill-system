namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class HousesStreet : DbMigration
    {
        public override void Up()
        {
            //AddColumn("dbo.HouseModels", "Address_Line1", c => c.String());
            AddColumn("dbo.HouseModels", "Address_Line2", c => c.String());
            AddColumn("dbo.HouseModels", "Address_Line3", c => c.String());
            AddColumn("dbo.HouseModels", "Address_Line4", c => c.String());
            //AddColumn("dbo.AspNetUsers", "HomeAddress_Line1", c => c.String());
            AddColumn("dbo.AspNetUsers", "HomeAddress_Line2", c => c.String());
            AddColumn("dbo.AspNetUsers", "HomeAddress_Line3", c => c.String());
            AddColumn("dbo.AspNetUsers", "HomeAddress_Line4", c => c.String());
            RenameColumn("dbo.HouseModels", "Address_Street", "Address_Line1");
            RenameColumn("dbo.AspNetUsers", "HomeAddress_Street", "HomeAddress_Line1");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "HomeAddress_Street", c => c.String());
            AddColumn("dbo.HouseModels", "Address_Street", c => c.String());
            DropColumn("dbo.AspNetUsers", "HomeAddress_Line4");
            DropColumn("dbo.AspNetUsers", "HomeAddress_Line3");
            DropColumn("dbo.AspNetUsers", "HomeAddress_Line2");
            DropColumn("dbo.AspNetUsers", "HomeAddress_Line1");
            DropColumn("dbo.HouseModels", "Address_Line4");
            DropColumn("dbo.HouseModels", "Address_Line3");
            DropColumn("dbo.HouseModels", "Address_Line2");
            DropColumn("dbo.HouseModels", "Address_Line1");
        }
    }
}
