namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddStartEndDatesAndTypeToBills : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Bills", "StartDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Bills", "EndDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Bills", "Type", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Bills", "Type");
            DropColumn("dbo.Bills", "EndDate");
            DropColumn("dbo.Bills", "StartDate");
        }
    }
}
