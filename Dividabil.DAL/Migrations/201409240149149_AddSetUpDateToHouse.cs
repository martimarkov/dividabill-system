namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSetUpDateToHouse : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HouseModels", "SetUpDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.HouseModels", "SetUpDate");
        }
    }
}
