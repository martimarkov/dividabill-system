namespace DividaBill.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BankMandateChangeableColumns : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BankAccounts", "CreatedAt", c => c.DateTime());
            AddColumn("dbo.BankAccounts", "LastModified", c => c.DateTime());
            AddColumn("dbo.BankMandates", "CreatedAt", c => c.DateTime());
            AddColumn("dbo.BankMandates", "LastModified", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.BankMandates", "LastModified");
            DropColumn("dbo.BankMandates", "CreatedAt");
            DropColumn("dbo.BankAccounts", "LastModified");
            DropColumn("dbo.BankAccounts", "CreatedAt");
        }
    }
}
