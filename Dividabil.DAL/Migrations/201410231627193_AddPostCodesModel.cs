namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPostCodesModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PostCodes",
                c => new
                    {
                        ID = c.Guid(nullable: false, identity: true),
                        Postcode = c.String(),
                        Latitude = c.String(),
                        Longitude = c.String(),
                        Easting = c.String(),
                        Northing = c.String(),
                        GridRef = c.String(),
                        County = c.String(),
                        District = c.String(),
                        Ward = c.String(),
                        DistrictCode = c.String(),
                        WardCode = c.String(),
                        Country = c.String(),
                        CountyCode = c.String(),
                        Constituency = c.String(),
                        Introduced = c.String(),
                        Terminated = c.String(),
                        Parish = c.String(),
                        NationalPark = c.String(),
                        Population = c.String(),
                        Households = c.String(),
                        Built_up_area = c.String(),
                        Built_up_sub_division = c.String(),
                        Lower_layer_super_output_area = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.PostCodes");
        }
    }
}
