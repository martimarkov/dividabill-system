namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddGoCardlessFieldsToUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "BankAccountID", c => c.String());
            AddColumn("dbo.AspNetUsers", "GoCardlessCustomerID", c => c.String());
            AddColumn("dbo.AspNetUsers", "MandateID", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "MandateID");
            DropColumn("dbo.AspNetUsers", "GoCardlessCustomerID");
            DropColumn("dbo.AspNetUsers", "BankAccountID");
        }
    }
}
