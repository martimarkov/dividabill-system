namespace Dividabil.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddStandingChargeToEstimatedTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HouseEstimatedPrices", "ElectricityStandingCharge", c => c.Decimal(nullable: false, precision: 18, scale: 2, defaultValue: 0));
            AddColumn("dbo.HouseEstimatedPrices", "GasStandingCharge", c => c.Decimal(nullable: false, precision: 18, scale: 2, defaultValue: 0));
            AddColumn("dbo.PostcodeEstimatedPrices", "ElectricityStandingCharge", c => c.Decimal(nullable: false, precision: 18, scale: 2, defaultValue: 0));
            AddColumn("dbo.PostcodeEstimatedPrices", "GasStandingCharge", c => c.Decimal(nullable: false, precision: 18, scale: 2, defaultValue: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PostcodeEstimatedPrices", "GasStandingCharge");
            DropColumn("dbo.PostcodeEstimatedPrices", "ElectricityStandingCharge");
            DropColumn("dbo.HouseEstimatedPrices", "GasStandingCharge");
            DropColumn("dbo.HouseEstimatedPrices", "ElectricityStandingCharge");
        }
    }
}
