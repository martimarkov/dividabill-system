﻿using System;
using NUnit.Framework;

namespace SeleniumTesting
{
    [TestFixture]
    public class WhyItWorks : Init
    {

        private String whyItWorksPageTitle = "Why it Works";
        private String firstItem = "Less stress";
        private String secondItem = "Save time";
        private String thirdItem = "Save money";
        private String fourthItem = "Quickly";
        private String signUpHref = "https://dividabill-dev.azurewebsites.net/Account/SignUp";

        [Test]
        public void Open_WhyItWorksPage_FromHeader()
        {

            HomePage homePage = new HomePage(driver);
            WhyItWorksPage whyPage = homePage.openWhyItWorksPage("header");

            Assert.IsTrue(whyPage.getPageTitle().Contains(whyItWorksPageTitle));
        }

        [Test]
        public void Open_WhyItWorksPage_FromFooter()
        {
            
            HomePage homePage = new HomePage(driver);
            WhyItWorksPage whyPage = homePage.openWhyItWorksPage("footer");

            Assert.IsTrue(whyPage.getPageTitle().Contains(whyItWorksPageTitle));
        }

        [Test]
        public void LeftSidebar_LessStress_Item()
        {

            HomePage homePage = new HomePage(driver);
            WhyItWorksPage whyPage = homePage.openWhyItWorksPage(header);

            Assert.IsTrue(whyPage.isSidebarItemActive(firstItem));
            Assert.IsFalse(whyPage.isSidebarItemActive(secondItem));
            Assert.IsFalse(whyPage.isSidebarItemActive(thirdItem));
            Assert.IsFalse(whyPage.isSidebarItemActive(fourthItem));
        }

        [Test]
        public void LeftSidebar_SaveTime_Item()
        {

            HomePage homePage = new HomePage(driver);
            WhyItWorksPage whyPage = homePage.openWhyItWorksPage(header);
            whyPage.selectItem(secondItem);

            Assert.IsFalse(whyPage.isSidebarItemActive(firstItem));
            Assert.IsTrue(whyPage.isSidebarItemActive(secondItem));
            Assert.IsFalse(whyPage.isSidebarItemActive(thirdItem));
            Assert.IsFalse(whyPage.isSidebarItemActive(fourthItem));
        }

        [Test]
        public void LeftSidebar_SaveMoney_Item()
        {

            HomePage homePage = new HomePage(driver);
            WhyItWorksPage whyPage = homePage.openWhyItWorksPage(header);
            whyPage.selectItem(thirdItem);

            Assert.IsFalse(whyPage.isSidebarItemActive(firstItem));
            Assert.IsFalse(whyPage.isSidebarItemActive(secondItem));
            Assert.IsTrue(whyPage.isSidebarItemActive(thirdItem));
            Assert.IsFalse(whyPage.isSidebarItemActive(fourthItem));
        }

        [Test]
        public void LeftSidebar_Quickly_Item()
        {

            HomePage homePage = new HomePage(driver);
            WhyItWorksPage whyPage = homePage.openWhyItWorksPage(header);
            whyPage.selectItem(fourthItem);

            Assert.IsFalse(whyPage.isSidebarItemActive(firstItem));
            Assert.IsFalse(whyPage.isSidebarItemActive(secondItem));
            Assert.IsFalse(whyPage.isSidebarItemActive(thirdItem));
            Assert.IsTrue(whyPage.isSidebarItemActive(fourthItem));
        }

        [Test]
        public void LeftSidebar_Scrolling_LessStress_Item()
        {

            HomePage homePage = new HomePage(driver);
            WhyItWorksPage whyPage = homePage.openWhyItWorksPage(header);
            whyPage.scrollPage(firstItem);

            Assert.IsTrue(whyPage.isSidebarItemActive(firstItem));
            Assert.IsFalse(whyPage.isSidebarItemActive(secondItem));
            Assert.IsFalse(whyPage.isSidebarItemActive(thirdItem));
            Assert.IsFalse(whyPage.isSidebarItemActive(fourthItem));
        }

        [Test]
        public void LeftSidebar_Scrolling_SaveTime_Item()
        {

            HomePage homePage = new HomePage(driver);
            WhyItWorksPage whyPage = homePage.openWhyItWorksPage(header);
            whyPage.scrollPage(secondItem);

            Assert.IsFalse(whyPage.isSidebarItemActive(firstItem));
            Assert.IsTrue(whyPage.isSidebarItemActive(secondItem));
            Assert.IsFalse(whyPage.isSidebarItemActive(thirdItem));
            Assert.IsFalse(whyPage.isSidebarItemActive(fourthItem));
        }

        [Test]
        public void LeftSidebar_Scrolling_SaveMoney_Item()
        {

            HomePage homePage = new HomePage(driver);
            WhyItWorksPage whyPage = homePage.openWhyItWorksPage(header);
            whyPage.scrollPage(thirdItem);

            Assert.IsFalse(whyPage.isSidebarItemActive(firstItem));
            Assert.IsFalse(whyPage.isSidebarItemActive(secondItem));
            Assert.IsTrue(whyPage.isSidebarItemActive(thirdItem));
            Assert.IsFalse(whyPage.isSidebarItemActive(fourthItem));
        }

        [Test]
        public void LeftSidebar_Scrolling_Quickly_Item()
        {

            HomePage homePage = new HomePage(driver);
            WhyItWorksPage whyPage = homePage.openWhyItWorksPage(header);
            whyPage.scrollPage(fourthItem);

            Assert.IsFalse(whyPage.isSidebarItemActive(firstItem));
            Assert.IsFalse(whyPage.isSidebarItemActive(secondItem));
            Assert.IsFalse(whyPage.isSidebarItemActive(thirdItem));
            Assert.IsTrue(whyPage.isSidebarItemActive(fourthItem));
        }

        [Test]
        public void CheckSignUpHrefAttribute()
        {
            HomePage homePage = new HomePage(driver);
            WhyItWorksPage whyPage = homePage.openWhyItWorksPage(header);

            Assert.AreEqual(signUpHref, whyPage.getHrefAttribute(firstItem));
            Assert.AreEqual(signUpHref, whyPage.getHrefAttribute(fourthItem));
            Assert.AreEqual(signUpHref, whyPage.getHrefAttribute(fourthItem));
        }

	}
}
