﻿using System;
using NUnit.Framework;

namespace SeleniumTesting
{
    [TestFixture]
    public class Services : Init
    {
        private String servicesTitle = "Our Services";
        private String signUpTitle = "House Information";
        private String postcode = "SO17 1QG";
        String gasUnitRate = "GasUnitRate";
        String gasStandingChange = "GasStandingChange";
        String electricityUnitRate = "ElectricityUnitRate";
        String electricityStandingChange = "ElectricityStandingChange";

        [Test]
        public void Open_ServicesPage_FromHeader()
        {

            HomePage homePage = new HomePage(driver);
            ServicesPage servicesPage = homePage.openServicesPage(header);

            Assert.IsTrue(servicesPage.getPageTitle().Contains(servicesTitle));
        }

        [Test]
        public void Open_ServicesPage_FromFooter()
        {

            HomePage homePage = new HomePage(driver);
            ServicesPage servicesPage = homePage.openServicesPage(footer);

            Assert.IsTrue(servicesPage.getPageTitle().Contains(servicesTitle));
        }

        [Test]
        public void LeftSidebar_DividaBill_Item()
        {

            HomePage homePage = new HomePage(driver);
            ServicesPage servicesPage = homePage.openServicesPage(header);

            Assert.AreEqual("true", servicesPage.getArialExpandedAttributeValue(dividabillItem));
        }

        [Test]
        public void LeftSidebar_GasAndElectricity_Item()
        {

            HomePage homePage = new HomePage(driver);
            ServicesPage servicesPage = homePage.openServicesPage(header);

            Assert.AreEqual("false", servicesPage.getArialExpandedAttributeValue(gasItem));

            servicesPage.selectItem(gasItem);

            Assert.AreEqual("true", servicesPage.getArialExpandedAttributeValue(gasItem));
            Assert.AreEqual("false", servicesPage.getArialExpandedAttributeValue(dividabillItem));
        }

        [Test]
        public void LeftSidebar_Water_Item()
        {

            HomePage homePage = new HomePage(driver);
            ServicesPage servicesPage = homePage.openServicesPage(header);

            Assert.AreEqual("false", servicesPage.getArialExpandedAttributeValue(waterItem));

            servicesPage.selectItem(waterItem);

            Assert.AreEqual("true", servicesPage.getArialExpandedAttributeValue(waterItem));
        }

        [Test]
        public void LeftSidebar_Internet_Item()
        {

            HomePage homePage = new HomePage(driver);
            ServicesPage servicesPage = homePage.openServicesPage(header);

            Assert.AreEqual("false", servicesPage.getArialExpandedAttributeValue(internetlItem));

            servicesPage.selectItem(internetlItem);

            Assert.AreEqual("true", servicesPage.getArialExpandedAttributeValue(internetlItem));
        }

        [Test]
        public void LeftSidebar_LandlinePhone_Item()
        {

            HomePage homePage = new HomePage(driver);
            ServicesPage servicesPage = homePage.openServicesPage(header);

            Assert.AreEqual("false", servicesPage.getArialExpandedAttributeValue(phoneItem));

            servicesPage.selectItem(phoneItem);

            Assert.AreEqual("true", servicesPage.getArialExpandedAttributeValue(phoneItem));
        }

        [Test]
        public void LeftSidebar_Sky_Item()
        {

            HomePage homePage = new HomePage(driver);
            ServicesPage servicesPage = homePage.openServicesPage(header);

            Assert.AreEqual("false", servicesPage.getArialExpandedAttributeValue(skyItem));

            servicesPage.selectItem(skyItem);

            Assert.AreEqual("true", servicesPage.getArialExpandedAttributeValue(skyItem));
        }

        [Test]
        public void LeftSidebar_Netflix_Item()
        {

            HomePage homePage = new HomePage(driver);
            ServicesPage servicesPage = homePage.openServicesPage(header);

            Assert.AreEqual("false", servicesPage.getArialExpandedAttributeValue(netflixItem));

            servicesPage.selectItem(netflixItem);

            Assert.AreEqual("true", servicesPage.getArialExpandedAttributeValue(netflixItem));
        }

        [Test]
        public void LeftSidebar_TvLicense_Item()
        {

            HomePage homePage = new HomePage(driver);
            ServicesPage servicesPage = homePage.openServicesPage(header);

            Assert.AreEqual("false", servicesPage.getArialExpandedAttributeValue(tvItem));

            servicesPage.selectItem(tvItem);

            Assert.AreEqual("true", servicesPage.getArialExpandedAttributeValue(tvItem));
        }

        [Test]
        public void MoreInformation_DividaBill_Item()
        {

            HomePage homePage = new HomePage(driver);
            ServicesPage servicesPage = homePage.openServicesPage(header);
            servicesPage.clickMoreInformationButton(dividabillItem);

            Assert.IsTrue(servicesPage.isMoreInformationModalDialogDisplayed(dividabillItem));

            servicesPage.clickMoreInformationCloseButton(dividabillItem);

            Assert.IsFalse(servicesPage.isMoreInformationModalDialogDisplayed(dividabillItem));
        }

        [Test]
        public void MoreInformation_GasAndElectricity_Item()
        {

            HomePage homePage = new HomePage(driver);
            ServicesPage servicesPage = homePage.openServicesPage(header);
            servicesPage.selectItem(gasItem);
            servicesPage.clickMoreInformationButton(gasItem);

            Assert.IsTrue(servicesPage.isMoreInformationModalDialogDisplayed(gasItem));
            servicesPage.clickMoreInformationCloseButton(gasItem);
            Assert.IsFalse(servicesPage.isMoreInformationModalDialogDisplayed(gasItem));
        }

        [Test]
        public void MoreInformation_Water_Item()
        {

            HomePage homePage = new HomePage(driver);
            ServicesPage servicesPage = homePage.openServicesPage(header);
            servicesPage.selectItem(waterItem);
            servicesPage.clickMoreInformationButton(waterItem);

            Assert.IsTrue(servicesPage.isMoreInformationModalDialogDisplayed(waterItem));

            servicesPage.clickMoreInformationCloseButton(waterItem);

            Assert.IsFalse(servicesPage.isMoreInformationModalDialogDisplayed(waterItem));
        }

        [Test]
        public void MoreInformation_Internet_Item()
        {

            HomePage homePage = new HomePage(driver);
            ServicesPage servicesPage = homePage.openServicesPage(header);
            servicesPage.selectItem(internetlItem);
            servicesPage.clickMoreInformationButton(internetlItem);

            Assert.IsTrue(servicesPage.isMoreInformationModalDialogDisplayed(internetlItem));

            servicesPage.clickMoreInformationCloseButton(internetlItem);

            Assert.IsFalse(servicesPage.isMoreInformationModalDialogDisplayed(internetlItem));
        }

        [Test]
        public void MoreInformation_LandlinePhone_Item()
        {

            HomePage homePage = new HomePage(driver);
            ServicesPage servicesPage = homePage.openServicesPage(header);
            servicesPage.selectItem(phoneItem);
            servicesPage.clickMoreInformationButton(phoneItem);

            Assert.IsTrue(servicesPage.isMoreInformationModalDialogDisplayed(phoneItem));

            servicesPage.clickMoreInformationCloseButton(phoneItem);

            Assert.IsFalse(servicesPage.isMoreInformationModalDialogDisplayed(phoneItem));
        }

        [Test]
        public void MoreInformation_Sky_Item()
        {

            HomePage homePage = new HomePage(driver);
            ServicesPage servicesPage = homePage.openServicesPage(header);
            servicesPage.selectItem(skyItem);
            servicesPage.clickMoreInformationButton(skyItem);

            Assert.IsTrue(servicesPage.isMoreInformationModalDialogDisplayed(skyItem));

            servicesPage.clickMoreInformationCloseButton(skyItem);

            Assert.IsFalse(servicesPage.isMoreInformationModalDialogDisplayed(skyItem));
        }

        [Test]
        public void MoreInformation_Netflix_Item()
        {

            HomePage homePage = new HomePage(driver);
            ServicesPage servicesPage = homePage.openServicesPage(header);
            servicesPage.selectItem(netflixItem);
            servicesPage.clickMoreInformationButton(netflixItem);

            Assert.IsTrue(servicesPage.isMoreInformationModalDialogDisplayed(netflixItem));

            servicesPage.clickMoreInformationCloseButton(netflixItem);

            Assert.IsFalse(servicesPage.isMoreInformationModalDialogDisplayed(netflixItem));
        }

        [Test]
        public void MoreInformation_TvLicense_Item()
        {

            HomePage homePage = new HomePage(driver);
            ServicesPage servicesPage = homePage.openServicesPage(header);
            servicesPage.selectItem(tvItem);
            servicesPage.clickMoreInformationButton(tvItem);

            Assert.IsTrue(servicesPage.isMoreInformationModalDialogDisplayed(tvItem));

            servicesPage.clickMoreInformationCloseButton(tvItem);

            Assert.IsFalse(servicesPage.isMoreInformationModalDialogDisplayed(tvItem));
        }

        [Test]
        public void Open_SignUpPage_FromDividaBillItem()
        {

            HomePage homePage = new HomePage(driver);
            ServicesPage servicesPage = homePage.openServicesPage(header);
            SignUpPage signUpPage = servicesPage.clickSignUpLink(dividabillItem);

            Assert.IsTrue(signUpPage.getPageTitle().Contains(signUpTitle));
        }

        [Test]
        public void Open_SignUpPage_FromTvItem()
        {

            HomePage homePage = new HomePage(driver);
            ServicesPage servicesPage = homePage.openServicesPage(header);
            servicesPage.selectItem(tvItem);
            SignUpPage signUpPage = servicesPage.clickSignUpLink(tvItem);

            Assert.IsTrue(signUpPage.getPageTitle().Contains(signUpTitle));
        }

        [Test]
        public void GasAndElectricity_PostCode_Search()
        {

            HomePage homePage = new HomePage(driver);
            ServicesPage servicesPage = homePage.openServicesPage(header);
            servicesPage.selectItem(gasItem);
            servicesPage.typePostCodeInForm(postcode);
            servicesPage.clickSearch();

            Assert.AreEqual("3.3p", servicesPage.checkSearch(gasUnitRate));
            Assert.AreEqual("28.5p", servicesPage.checkSearch(gasStandingChange));
            Assert.AreEqual("11.05p", servicesPage.checkSearch(electricityUnitRate));
            Assert.AreEqual("23.7p", servicesPage.checkSearch(electricityStandingChange));
        }

        [Test]
        public void GasAndElectricity_PostCode_EmptySearch()
        {

            String postCode = " ";

            HomePage homePage = new HomePage(driver);
            ServicesPage servicesPage = homePage.openServicesPage(header);
            servicesPage.selectItem(gasItem);
            servicesPage.clickSearch();

            Assert.AreEqual("3.465p", servicesPage.checkSearch(gasUnitRate));
            Assert.AreEqual("27.975p", servicesPage.checkSearch(gasStandingChange));
            Assert.AreEqual("11.60p", servicesPage.checkSearch(electricityUnitRate));
            Assert.AreEqual("28.67", servicesPage.checkSearch(electricityStandingChange));

            servicesPage.typePostCodeInForm(postCode);
            servicesPage.clickSearch();

            Assert.AreEqual("3.465p", servicesPage.checkSearch(gasUnitRate));
            Assert.AreEqual("27.975p", servicesPage.checkSearch(gasStandingChange));
            Assert.AreEqual("11.60p", servicesPage.checkSearch(electricityUnitRate));
            Assert.AreEqual("28.67", servicesPage.checkSearch(electricityStandingChange));
        }

        [Test]
        public void GasAndElectricity_PostCode_InvalidData()
        {

            String invalidPostCode = "asdflkajh";

            HomePage homePage = new HomePage(driver);
            ServicesPage servicesPage = homePage.openServicesPage(header);
            servicesPage.selectItem(gasItem);
            servicesPage.typePostCodeInForm(invalidPostCode);
            servicesPage.clickSearch();

            Assert.AreEqual("0p", servicesPage.checkSearch(gasUnitRate));
            Assert.AreEqual("0p", servicesPage.checkSearch(gasStandingChange));
            Assert.AreEqual("0p", servicesPage.checkSearch(electricityUnitRate));
            Assert.AreEqual("0p", servicesPage.checkSearch(electricityStandingChange));
        }

        [Test]
        public void GasAndElectricity_PostCode_Reserch()
        {

            String postCode2 = "HP2 7DX";

            HomePage homePage = new HomePage(driver);
            ServicesPage servicesPage = homePage.openServicesPage(header);
            servicesPage.selectItem(gasItem);
            servicesPage.typePostCodeInForm(postcode);
            servicesPage.clickSearch();

            Assert.AreEqual("3.3p", servicesPage.checkSearch(gasUnitRate));
            Assert.AreEqual("28.5p", servicesPage.checkSearch(gasStandingChange));
            Assert.AreEqual("11.05p", servicesPage.checkSearch(electricityUnitRate));
            Assert.AreEqual("23.7p", servicesPage.checkSearch(electricityStandingChange));

            servicesPage.typePostCodeInForm(postCode2);
            servicesPage.clickSearch();

            Assert.AreEqual("3.3p", servicesPage.checkSearch(gasUnitRate));
            Assert.AreEqual("26p", servicesPage.checkSearch(gasStandingChange));
            Assert.AreEqual("11.05p", servicesPage.checkSearch(electricityUnitRate));
            Assert.AreEqual("22.7p", servicesPage.checkSearch(electricityStandingChange));
        }
    }
}

