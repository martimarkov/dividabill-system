﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SeleniumTesting
{

    public class WhyItWorksPage : AbstractPage
	{
        private By first_selected_item_locator = By.CssSelector(".active>[href='#info-one']");
        private By second_selected_item_locator = By.CssSelector(".active>[href='#info-two']");
        private By third_selected_item_locator = By.CssSelector(".active>[href='#info-three']");
        private By fourth_selected_item_locator = By.CssSelector(".active>[href='#info-four']");
        private By first_item_locator = By.CssSelector("[href='#info-one']");
        private By second_item_locator = By.CssSelector("[href='#info-two']");
        private By third_item_locator = By.CssSelector("[href='#info-three']");
        private By fourth_item_locator = By.CssSelector("[href='#info-four']");
        private By first_item_chapter_locator = By.Id("info-one");
        private By second_item_chapter_locator = By.Id("info-two");
        private By third_item_chapter_locator = By.Id("info-three");
        private By fourth_item_chapter_locator = By.Id("info-four");
        private By info_one_sign_up_link_locator = By.CssSelector("#info-one [href='/Account/SignUp']");
        private By infot_four_start_sign_up_link_locator = By.CssSelector("#info-four ul[class='cust-para'] [href='/Account/SignUp']");
        private By infot_four_end_sign_up_link_locator = By.CssSelector("#info-four p[class='cust-para'] [href='/Account/SignUp']");

        public WhyItWorksPage(IWebDriver driver)
            : base(driver)
        {
        }

        public Boolean isSidebarItemActive(String item)
        {
            By element = null;

            if (item == "Less stress")
            {
                element = first_selected_item_locator;
            }
            else if (item == "Save time")
            {
                element = second_selected_item_locator;
            }
            else if (item == "Save money")
            {
                element = third_selected_item_locator;
            }
            else if (item == "Quickly")
            {
                element = fourth_selected_item_locator;
            }
            else throw new System.ArgumentException(itemError);

            return isElementDisplayed(element);
        }

        public void selectItem(String item)
        {
            pause(1000);
            if (item == "Less stress")
            {
                click(first_item_locator);
            }
            else if (item == "Save time")
            {
                click(second_item_locator);
            }
            else if (item == "Save money")
            {
                click(third_item_locator);
            }
            else if (item == "Quickly")
            {
                click(fourth_item_locator);
            }
            else throw new System.ArgumentException(itemError);
        }

        public void scrollPage(String chapter)
        {
            IWebElement element;
            if (chapter == "Less stress")
            {
                element = driver.FindElement(first_item_chapter_locator);
            }
            else if (chapter == "Save time")
            {
                element = driver.FindElement(second_item_chapter_locator);
            }
            else if (chapter == "Save money")
            {
                element = driver.FindElement(third_item_chapter_locator);
            }
            else if (chapter == "Quickly")
            {
                element = driver.FindElement(fourth_item_chapter_locator);
            }
            else throw new System.ArgumentException(itemError);
            
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            executor.ExecuteScript("arguments[0].scrollIntoView();", element);
        }

        public String getHrefAttribute(String item)
        {
            String value = null;
            String start = "start";
            String end = "end";
            pause(1000);
            if (item == "Less stress")
            {
                value = driver.FindElement(info_one_sign_up_link_locator).GetAttribute("href").ToString();
            }
            else if ((item == "Quickly") && (start == "start"))
            {
                value = driver.FindElement(infot_four_start_sign_up_link_locator).GetAttribute("href").ToString();
            }
            else if ((item == "Quickly") && (end == "end"))
            {
                value = driver.FindElement(infot_four_end_sign_up_link_locator).GetAttribute("href").ToString();
            }
            else throw new System.ArgumentException(itemError);

            return value;
        }

	}
}
