﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SeleniumTesting
{

    public class HomePage : AbstractPage
	{

        private By postcode_field_locator = By.Id("quote_postcode");
        private By get_quote_button_locator = By.Id("quote_search");

        private String[] carousel_src_array = new String[] { "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/anglian%20water.png?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/British_Gas_logo.png?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/BT.jpg?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/GB%20energy%20.png?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/Go-Cardless-1.png?width=112", "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/Netflix.png?width=112", "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/Northumbrian-Water.jpg?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/scottish%20water.gif?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/severn%20trent.jpg?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/sky.jpg?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/south%20west%20water.jpg?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/southern%20water.jpg?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/thames%20water.png?width=112", "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/TV%20licensing.jpg?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/united_utilities_logo.gif?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/welshwater-logo.jpg?width=112", "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/wessex%20water.png?width=112", "https://az754941.vo.msecnd.net/Content/Images/v4/Partners/yorkshire%20water.jpg?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Whoworkswith/enactushqlogo.png?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Whoworkswith/nasma.jpg?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Whoworkswith/student%20loans.jpg?width=112", "https://az754941.vo.msecnd.net/Content/Images/v4/Whoworkswith/Student%20money%20saver.png?width=112", "https://az754941.vo.msecnd.net/Content/Images/v4/Whoworkswith/student%20pad.png?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Whoworkswith/the%20university%20paper.jpg?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Whoworkswith/Bournemouth-University-Logo.jpg?width=112", "https://az754941.vo.msecnd.net/Content/Images/v4/Whoworkswith/BSU-logo---generic-Cropped-transparent-233x317.png?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Whoworkswith/Portsmouth%20su.jpg?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Whoworkswith/Solent-LEP.jpg?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Whoworkswith/SolentSU.jpg?width=160", "https://az754941.vo.msecnd.net/Content/Images/v4/Whoworkswith/susu.jpg?width=112" };
        private String[] footer_hrefs_array = new String[] { "https://www.linkedin.com/company/dividabill", "http://twitter.com/dividabill/", "http://facebook.com/dividabill/", "https://plus.google.com/117719664197593106714/about", "tel:02380223596", "mailto:support@dividabill.co.uk" };


        private By first_slide_locator_indicator = By.CssSelector("[data-slide-to='0']");
        private By second_slide_locator_indicator = By.CssSelector("[data-slide-to='1']");
        private By third_slide_locator_indicator = By.CssSelector("[data-slide-to='2']");
        private By fours_slide_locator_indicator = By.CssSelector("[data-slide-to='3']");
        private By fifth_slide_locator_indicator = By.CssSelector("[data-slide-to='4']");
        private By sixth_slide_locator_indicator = By.CssSelector("[data-slide-to='5']");
        private By sevenss_slide_locator_indicator = By.CssSelector("[data-slide-to='6']");

        private By first_slide_image_locator = By.CssSelector("[src*='JadeRef.jpg']");
        private By second_slide_image_locator = By.CssSelector("[src*='JoRegUniPortsmouth.jpg']");
        private By thirdth_slide_image_lLocator = By.CssSelector("[src*='ValRef.jpg']");
        private By fourth_slide_image_locator = By.CssSelector("[src*='JoshRef.jpg']");
        private By fifth_slide_image_locator = By.CssSelector("[src*='RachNaRef.jpg']");
        private By sixth_slide_image_locator = By.CssSelector("[src*='AlexRefUniBristol.jpg']");
        private By sevenths_slide_image_locator = By.CssSelector("[src*='GeorgieRef.jpg']");

        private By carousel_first_image_locator = By.CssSelector("[src*='anglian water.png']");
        private By carousel_second_image_locator = By.CssSelector("[src*='British_Gas_logo.png']");
        private By carousel_third_image_locator = By.CssSelector("[src*='BT.jpg']");
        private By carousel_fourth_image_locator = By.CssSelector("[src*='GB energy .png']");
        private By carousel_fifth_image_locator = By.CssSelector("[src*='Go-Cardless-1.png']");
        private By carousel_sixth_image_locator = By.CssSelector("[src*='Netflix.png']");
        
        private By carousel_seventh_image_locator = By.CssSelector("[src*='Northumbrian-Water.jpg']");
        private By carousel_eight_image_locator = By.CssSelector("[src*='scottish water.gif']");
        private By carousel_nineth_image_locator = By.CssSelector("[src*='severn trent.jpg']");
        private By carousel_tenth_image_locator = By.CssSelector("[src*='sky.jpg']");
        private By carousel_eleven_image_locator = By.CssSelector("[src*='south west water.jpg']");
        private By caruosel_twelve_image_locator = By.CssSelector("[src*='southern water.jpg']");
        
        private By carousel_thirteen_image_locator = By.CssSelector("[src*='thames water.png']");
        private By carousel_fourteen_image_locator = By.CssSelector("[src*='TV licensing.jpg']");
        private By carousel_fifteen_image_locator = By.CssSelector("[src*='united_utilities_logo.gif']");
        private By carousel_sixteen_image_locator = By.CssSelector("[src*='welshwater-logo.jpg']");
        private By carousel_seventeen_image_locator = By.CssSelector("[src*='wessex water.png']");
        private By carousel_eighteen_image_locator = By.CssSelector("[src*='yorkshire water.jpg']");
        
        private By carousel_nineteen_image_locator = By.CssSelector("[src*='enactushqlogo.png']");
        private By carousel_twenty_image_locator = By.CssSelector("[src*='nasma.jpg']");
        private By carousel_twentyone_image_locator = By.CssSelector("[src*='student loans.jpg']");
        private By carousel_twentytwo_image_locator = By.CssSelector("[src*='Student money saver.png']");
        private By carousel_twentythree_image_locator = By.CssSelector("[src*='student pad.png']");
        private By carousel_twentyfour_image_locator = By.CssSelector("[src*='the university paper.jpg']");
        
        private By carousel_twentyfive_image_locator = By.CssSelector("[src*='Bournemouth-University-Logo.jpg']");
        private By carousel_twentysix_image_locator = By.CssSelector("[src*='BSU-logo---generic-Cropped-transparent-233x317.png']");
        private By carousel_twentyseven_image_locator = By.CssSelector("[src*='Portsmouth su.jpg']");
        private By carousel_twentyeight_image_locator = By.CssSelector("[src*='Solent-LEP.jpg']");
        private By carousel_twentynine_image_locator = By.CssSelector("[src*='SolentSU.jpg']");
        private By carousel_thirty_image_locator = By.CssSelector("[src*='susu.jpg']");

        /*Footer link locators*/
        private By footer_linkedin_link_locator = By.CssSelector("[href='https://www.linkedin.com/company/dividabill']");
        private By footer_twitter_link_locator = By.CssSelector("[href='http://twitter.com/dividabill/']");
        private By footer_facebook_link_locator = By.CssSelector("[href='http://facebook.com/dividabill/']");
        private By footer_google_link_locator = By.CssSelector("[href='https://plus.google.com/117719664197593106714/about']");
        private By footer_phone_link_locator = By.CssSelector("[href='tel:02380223596']");
        private By footer_email_link_locator = By.CssSelector("[href='mailto:support@dividabill.co.uk']");

        /*Partners carosel locators*/
        private By first_list_partners_locator = By.XPath("//*[@id='PartnersCarousel']/div/div[1]");
        private By second_list_partnerslocator = By.XPath("//*[@id='PartnersCarousel']/div/div[2]");
        private By third_list_partners_locator = By.XPath("//*[@id='PartnersCarousel']/div/div[3]");

        /*Who works with us carosel locator*/
        private By first_list_who_works_with_us_locator = By.XPath(".//*[@id='WorkWithCarousel']/div/div[1]");
        private By second_list_who_works_with_us_locator = By.XPath(".//*[@id='WorkWithCarousel']/div/div[2]");

        
		public HomePage(IWebDriver driver) : base(driver){
        }

        public void fillPostcodeField(String postcode)
        {
            type(postcode_field_locator, postcode);
        }

        public SignUpPage clickGetQuoteButton()
        {
            click(get_quote_button_locator);
            return new SignUpPage(driver);
        }

        public void clickToSlideElements(String item) 
        {
            if (item == "0")
            {
                click(first_slide_locator_indicator);
                pause(500);
            }
            else if (item == "1")
            {
                click(second_slide_locator_indicator);
                pause(500);
            }
            else if (item == "2")
            {
                click(third_slide_locator_indicator);
                pause(500);
            }
            else if (item == "3")
            {
                click(fours_slide_locator_indicator);
                pause(500);
            }
            else if (item == "4")
            {
                click(fifth_slide_locator_indicator);
                pause(500);
            }
            else if (item == "5")
            {
                click(sixth_slide_locator_indicator);
                pause(500);
            }
            else if (item == "6")
            {
                click(sevenss_slide_locator_indicator);
                pause(500);
            }
            else throw new System.ArgumentException(itemError);
        }

        public String getSrcAttribute(String item) 
        {
            String value = null;

            if (item == "0")
            {
                value = driver.FindElement(first_slide_image_locator).GetAttribute("src").ToString();
            }
            else if (item == "1")
            {
                value = driver.FindElement(second_slide_image_locator).GetAttribute("src").ToString();
            }
            else if (item == "2")
            {
                value = driver.FindElement(thirdth_slide_image_lLocator).GetAttribute("src").ToString();
            }
            else if (item == "3")
            {
                value = driver.FindElement(fourth_slide_image_locator).GetAttribute("src").ToString();
            }
            else if (item == "4")
            {
                value = driver.FindElement(fifth_slide_image_locator).GetAttribute("src").ToString();
            }
            else if (item == "5")
            {
                value = driver.FindElement(sixth_slide_image_locator).GetAttribute("src").ToString();
            }
            else if (item == "6")
            {
                value = driver.FindElement(sevenths_slide_image_locator).GetAttribute("src").ToString();
            }
            else throw new System.ArgumentException(itemError);

            return value;
        }

        public String getClassAttribute(String item) 
        {
            String value = null;

            if (item == "0")
            {
                value = driver.FindElement(first_slide_locator_indicator).GetAttribute("class").ToString();
            }
            else if (item == "1")
            {
                value = driver.FindElement(second_slide_locator_indicator).GetAttribute("class").ToString();
            }
            else if (item == "2")
            {
                value = driver.FindElement(third_slide_locator_indicator).GetAttribute("class").ToString();
            }
            else if (item == "3")
            {
                value = driver.FindElement(fours_slide_locator_indicator).GetAttribute("class").ToString();
            }
            else if (item == "4")
            {
                value = driver.FindElement(fifth_slide_locator_indicator).GetAttribute("class").ToString();
            }
            else if (item == "5")
            {
                value = driver.FindElement(sixth_slide_locator_indicator).GetAttribute("class").ToString();
            }
            else if (item == "6")
            {
                value = driver.FindElement(sevenss_slide_locator_indicator).GetAttribute("class").ToString();
            }
            else throw new System.ArgumentException(itemError);

            return value;
        }

        public String getSrcAttributeCarosel(String item)
        {
            String value = null;

            if (item == carousel_src_array[0])
            {
                value = driver.FindElement(carousel_first_image_locator).GetAttribute("src").ToString();
            }
            else if (item == carousel_src_array[1])
            {
                value = driver.FindElement(carousel_second_image_locator).GetAttribute("src").ToString();
            }
            else if (item == carousel_src_array[2])
            {
                value = driver.FindElement(carousel_third_image_locator).GetAttribute("src").ToString();
            }
            else if (item == carousel_src_array[3])
            {
                value = driver.FindElement(carousel_fourth_image_locator).GetAttribute("src").ToString();
            }
            else if (item == carousel_src_array[4])
            {
                value = driver.FindElement(carousel_fifth_image_locator).GetAttribute("src").ToString();
            }
            else if (item == carousel_src_array[5])
            {
                value = driver.FindElement(carousel_sixth_image_locator).GetAttribute("src").ToString();
            }
            else if (item == carousel_src_array[6])
            {
                value = driver.FindElement(carousel_seventh_image_locator).GetAttribute("src").ToString();
            }
            else if (item == carousel_src_array[7])
            {
                value = driver.FindElement(carousel_eight_image_locator).GetAttribute("src").ToString();
            }
            else if (item == carousel_src_array[8])
            {
                value = driver.FindElement(carousel_nineth_image_locator).GetAttribute("src").ToString();
            }
            else if (item == carousel_src_array[9])
            {
                value = driver.FindElement(carousel_tenth_image_locator).GetAttribute("src").ToString();
            }
            else if (item == carousel_src_array[10])
            {
                value = driver.FindElement(carousel_eleven_image_locator).GetAttribute("src").ToString();
            }
            else if (item == carousel_src_array[11])
            {
                value = driver.FindElement(caruosel_twelve_image_locator).GetAttribute("src").ToString();
            }
            else if (item == carousel_src_array[12])
            {
                value = driver.FindElement(carousel_thirteen_image_locator).GetAttribute("src").ToString();
            }
            else if (item == carousel_src_array[13])
            {
                value = driver.FindElement(carousel_fourteen_image_locator).GetAttribute("src").ToString();
            }
            else if (item == carousel_src_array[14])
            {
                value = driver.FindElement(carousel_fifteen_image_locator).GetAttribute("src").ToString();
            }
            else if (item == carousel_src_array[15])
            {
                value = driver.FindElement(carousel_sixteen_image_locator).GetAttribute("src").ToString();
            }
            else if (item == carousel_src_array[16])
            {
                value = driver.FindElement(carousel_seventeen_image_locator).GetAttribute("src").ToString();
            }
            else if (item == carousel_src_array[17])
            {
                value = driver.FindElement(carousel_eighteen_image_locator).GetAttribute("src").ToString();
            }
            else if (item == carousel_src_array[18])
            {
                value = driver.FindElement(carousel_nineteen_image_locator).GetAttribute("src").ToString();
            }
            else if (item == carousel_src_array[19])
            {
                value = driver.FindElement(carousel_twenty_image_locator).GetAttribute("src").ToString();
            }
            else if (item == carousel_src_array[20])
            {
                value = driver.FindElement(carousel_twentyone_image_locator).GetAttribute("src").ToString();
            }
            else if (item == carousel_src_array[21])
            {
                value = driver.FindElement(carousel_twentytwo_image_locator).GetAttribute("src").ToString();
            }
            else if (item == carousel_src_array[22])
            {
                value = driver.FindElement(carousel_twentythree_image_locator).GetAttribute("src").ToString();
            }
            else if (item == carousel_src_array[23])
            {
                value = driver.FindElement(carousel_twentyfour_image_locator).GetAttribute("src").ToString();
            }
            else if (item == carousel_src_array[24])
            {
                value = driver.FindElement(carousel_twentyfive_image_locator).GetAttribute("src").ToString();
            }
            else if (item == carousel_src_array[25])
            {
                value = driver.FindElement(carousel_twentysix_image_locator).GetAttribute("src").ToString();
            }
            else if (item == carousel_src_array[26])
            {
                value = driver.FindElement(carousel_twentyseven_image_locator).GetAttribute("src").ToString();
            }
            else if (item == carousel_src_array[27])
            {
                value = driver.FindElement(carousel_twentyeight_image_locator).GetAttribute("src").ToString();
            }
            else if (item == carousel_src_array[28])
            {
                value = driver.FindElement(carousel_twentynine_image_locator).GetAttribute("src").ToString();
            }
            else if (item == carousel_src_array[29])
            {
                value = driver.FindElement(carousel_thirty_image_locator).GetAttribute("src").ToString();
            }
            else throw new System.ArgumentException(itemError);

            return value;
        }

        public SignUpPage clickEnterButton()
        {
            driver.FindElement(get_quote_button_locator).SendKeys(Keys.Enter);
            return new SignUpPage(driver);
        }

        public String getHrefAttributeFooter(String item)
        {
            String value = null;
            if (item == footer_hrefs_array[0])
            {
                value = driver.FindElement(footer_linkedin_link_locator).GetAttribute("href").ToString();
            }
            else if (item == footer_hrefs_array[1])
            {
                value = driver.FindElement(footer_twitter_link_locator).GetAttribute("href").ToString();
            }
            else if (item == footer_hrefs_array[2])
            {
                value = driver.FindElement(footer_facebook_link_locator).GetAttribute("href").ToString();
            }
            else if (item == footer_hrefs_array[3])
            {
                value = driver.FindElement(footer_google_link_locator).GetAttribute("href").ToString();
            }
            else if (item == footer_hrefs_array[4])
            {
                value = driver.FindElement(footer_phone_link_locator).GetAttribute("href").ToString();
            }
            else if (item == footer_hrefs_array[5])
            {
                value = driver.FindElement(footer_email_link_locator).GetAttribute("href").ToString();
            }
            else throw new System.ArgumentException(itemError);
            return value;
        }

        public void typeInvalidPostCode(String item) 
        {
            waitUntilElementIsNotVisible(postcode_field_locator);
            clearAndType(postcode_field_locator, item);
        }

        public String getClassAttributePartnersCarosel(String item) 
        {
            String value = null;
            if(item == "first")
            {
                value = driver.FindElement(first_list_partners_locator).GetAttribute("class").ToString();
            }
            else if(item == "second")
            {
                value = driver.FindElement(second_list_partnerslocator).GetAttribute("class").ToString();
            }
            else if(item == "third")
            {
                value = driver.FindElement(third_list_partners_locator).GetAttribute("class").ToString();
            }
            else throw new System.ArgumentException(itemError);

            return value; 
        }

        public String getClassAttributeWhoWorkWithUsCarosel(String item) 
        {
            String value = null;
            if(item == "first")
            {
                value = driver.FindElement(first_list_who_works_with_us_locator).GetAttribute("class").ToString();
            }
            else if(item == "second")
            {
                value = driver.FindElement(second_list_who_works_with_us_locator).GetAttribute("class").ToString();
            }
            else throw new System.ArgumentException(itemError);

            return value;
        }

        public void moveToCarosel()
        {
            IWebElement element;
            element = driver.FindElement(By.Id("PartnersCarousel"));
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            executor.ExecuteScript("arguments[0].scrollIntoView();", element);
        }
	}
}
