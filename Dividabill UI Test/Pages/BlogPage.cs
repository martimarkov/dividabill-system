﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SeleniumTesting
{

    public class BlogPage : AbstractPage
	{

        private By titleLocator = By.CssSelector("h1>[href='http://blog.dividabill.co.uk/']");

        public BlogPage(IWebDriver driver)
            : base(driver)
        {
        }

        public Boolean isTitleDisplayed()
        {
            return isElementDisplayed(titleLocator);
        }
	}
}
