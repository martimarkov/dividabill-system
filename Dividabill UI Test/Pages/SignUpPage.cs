﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SeleniumTesting
{

    public class SignUpPage : AbstractPage
	{
        
        /*Steps locators*/
        private By first_step_locator = By.Id("step-one");
        private By second_step_locator = By.Id("step-two");
        private By third_step_locator = By.Id("step-three");

        /*required locator*/
        private By reauired_post_code_bar_locator = By.Id("PostCode");

        /*quickfind button*/
        private By quickFind_button_locator = By.CssSelector(".quick-find");

        /*spinner locator*/
        private By quick_find_spinner_locator = By.Id("quick-find");

        /*drop-down list locator*/
        private By select_buildings_button_locator = By.CssSelector("[data-id='building']");
        private By select_end_date_button_locator = By.CssSelector("[data-id='Period']");
        private By select_nuber_of_tenats_button_locator = By.CssSelector("[data-id='Housemates']");
        private By select_ref_title_button_locator = By.CssSelector("[data-id='RefTitle']");
        private By select_start_data_button_locator = By.Id("StartTime");

        private By select_buildings_locator = By.Id("building");
        private By select_nuber_of_tenats_locator = By.Id("Housemates");
        private By select_end_date_locator = By.Id("Period");

        /*fields locators*/
        private By selected_address_locator = By.Id("AddressTT1");
        private By city_locator = By.Id("CityTT");
        private By country_locator = By.Id("CountyTT");
        private By second_selected_address_locator = By.Id("AddressTT2");

        /*Services locators*/
        private By electrisity_service_locator = By.Id("electricity");
        private By gas_service_locator = By.Id("gas");
        private By water_service_locator = By.Id("water");
        private By internet_service_locator = By.Id("internet");
        private By tv_service_locator = By.Id("television");
        private By phone_service_locator = By.Id("telephone");

        /*Servises rates locators*/
        private By electricity_rates_locators = By.Id("fee-electricity");
        private By gas_rates_locators = By.Id("fee-gas");
        private By gas_massage_locator = By.CssSelector(".gas-notshow-msg");
        private By water_rates_locators = By.Id("fee-water");
        private By internet_rates_locators = By.Id("fee-internet");
        private By tv_rates_locators = By.Id("fee-tv");
        private By phone_rates_locators = By.Id("fee-phone");

        /*calendar locators*/
        private By active_date = By.CssSelector(".active.day");

        /*checkbox Ongoing Locator*/
        private By check_box_ongoing_locator = By.Id("Ongoing");

        /*error post code alert button*/
        private By error_alert_button = By.CssSelector("#postcodeError #reggedHouse");

        /*bills for house and person*/
        private By house_bill_locator = By.Id("housing-cost");
        private By person_bill_locator = By.Id("(person-cost");

        /*radiobuttons internet*/
        private By basic_broudbend_locator = By.Id("BroadbandTypeADSL20");
        private By home_plus_locator = By.Id("BroadbandTypeFibre40");

        /*checkbox tv*/
        private By sky_tv_locator = By.Id("SkyTV");
        private By netflix_tv_locator = By.Id("NetflixTV");
        private By tvlicense_tv_locator = By.Id("TVLicense");

        /*checkbox phone */
        private By basic_phone_locator = By.Id("LandlineTypeBasic");
        private By medium_phone_locator = By.Id("LandlineTypeMedium");


        public SignUpPage(IWebDriver driver)
            : base(driver)
        {
        }

        public Boolean isSelectBuildingDropdownPresent()
        {
            waitUntilElementIsNotVisible(select_buildings_button_locator);
            return isElementDisplayed(select_buildings_button_locator);
        }

        public Boolean isSpinnerPresent() 
        {
            return isElementPresent(quick_find_spinner_locator);
        }

        public Boolean isBuildingDropdownPresent()
        {
            pause(5000);
            return isElementDisplayed(select_buildings_locator);
        }

        public void clickOnDropDownList(String item) 
        {
            if(item == "building")
            {
                click(select_buildings_button_locator);
            }
            else if(item == "tenats")
            {
                click(select_nuber_of_tenats_button_locator);
            }
            else if(item == "end date")
            {
                click(select_end_date_button_locator);
            }
            else if (item == "start date")
            {
                click(select_start_data_button_locator);
            }
            else throw new System.ArgumentException(itemError);
        }

        public String getAriaExpandedAttributeDropDownList(String item)
        {
            String value = null;

            if (item == "building")
            {
                value = driver.FindElement(select_buildings_button_locator).GetAttribute("aria-expanded").ToString();
            }
            else if (item == "tenats")
            {
                value = driver.FindElement(select_nuber_of_tenats_button_locator).GetAttribute("aria-expanded").ToString();
            }
            else if (item == "end date")
            {
                value = driver.FindElement(select_end_date_button_locator).GetAttribute("aria-expanded").ToString();
            }
            else throw new System.ArgumentException(itemError);
            return value;
        }

        public String getClassAttributeSteps(String item)
        {
            String value = null;
            if(item == "first")
            {
                value = driver.FindElement(first_step_locator).GetAttribute("class").ToString();
            }
            else if(item == "second")
            {
                value = driver.FindElement(second_step_locator).GetAttribute("class").ToString();
            }
            else if(item == "third")
            {
                value = driver.FindElement(third_step_locator).GetAttribute("class").ToString();
            }
            else throw new System.ArgumentException(itemError);
            return value;
        }

        public void clickElementOfDropDownList(String item, String list_item)
        {
            if (item == "building")
            {
                click(By.XPath("//span[text()='" + list_item + "']"));
            }
            else if (item == "tenats")
            {
                click(By.XPath("//span[text()='" + list_item + "']"));
            }
            else if (item == "end date")
            {
                click(By.XPath("//span[text()='" + list_item + "']"));
            }
            else if (item == "start date")
            {
                click(active_date);
            }
            else throw new System.ArgumentException(itemError);
        }

        public String getFieldsDataValueAttribute(String item) 
        {
            String value = null;
            if(item == "Selected address")
            {
                value = driver.FindElement(selected_address_locator).GetAttribute("value").ToString();
            }
            else if(item == "City")
            {
                value = driver.FindElement(city_locator).GetAttribute("value").ToString();
            }
            else if (item == "Country")
            {
                value = driver.FindElement(country_locator).GetAttribute("value").ToString();
            }
            else if (item == "Selected address 2")
            {
                value = driver.FindElement(second_selected_address_locator).GetAttribute("value").ToString();
            }
            else if(item == "start date")
            {
                value = driver.FindElement(select_start_data_button_locator).GetAttribute("value").ToString();
            }
            else throw new System.ArgumentException(itemError);

            return value;
        }

        public String getClassAttributeEndDate() 
        {
            pause(2000);
            return driver.FindElement(select_end_date_locator).GetAttribute("class").ToString();
        }

        public void clickOngoingCheckBox() 
        {
            click(check_box_ongoing_locator);
        }

        public void typeDayMounthYear(String item) 
        {
            jsType(select_start_data_button_locator, item);
            pause(2000);
        }

        public String getClassAttributeServices(String item) 
        {
            String value = null;
            if(item == "electricity")
            {
                value = driver.FindElement(electrisity_service_locator).GetAttribute("class").ToString();
            }
            else if(item == "gas")
            {
                value = driver.FindElement(gas_service_locator).GetAttribute("class").ToString();
            }
            else if(item == "water")
            {
                value = driver.FindElement(water_service_locator).GetAttribute("class").ToString();
            }
            else if(item == "internet")
            {
                value = driver.FindElement(internet_service_locator).GetAttribute("class").ToString();           
            }
            else if(item == "tv")
            {
                value = driver.FindElement(tv_service_locator).GetAttribute("class").ToString();
            }
            else if (item == "phone")
            {
                value = driver.FindElement(phone_service_locator).GetAttribute("class").ToString();
            }
            else throw new System.ArgumentException(itemError);
            return value;
        }

        public void clickServices(String item)
        {
            pause(2000);
            if (item == "electricity")
            {
                click(electrisity_service_locator);
            }
            else if (item == "gas")
            {
                click(gas_service_locator);
            }
            else if (item == "water")
            {
                click(water_service_locator);
            }
            else if (item == "internet")
            {
                click(internet_service_locator);
            }
            else if (item == "tv")
            {
                click(tv_service_locator);
            }
            else if (item == "phone")
            {
                click(phone_service_locator);
            }
            else throw new System.ArgumentException(itemError);
        }

        public void moveToElement() 
        {
            IWebElement element;
            element = driver.FindElement(electrisity_service_locator);
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            executor.ExecuteScript("arguments[0].scrollIntoView();", element);
        }

        public void clickAlertErrorInvalidPostCode()
        {
            waitUntilElementIsNotVisible(error_alert_button);
            click(error_alert_button);
        }

        public Boolean IsTextFieldPresent()
        {
            waitUntilElementIsNotVisible(selected_address_locator);
            pause(500);
            return isElementDisplayed(selected_address_locator);
        }

        public String getValueAttributeBill(String item) 
        {
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;

            String value = null;
            if (item == "house")
            {
                value = executor.ExecuteScript("return $('#housing-cost').prop('value');").ToString();
            }
            else if (item == "person")
            {
                value = executor.ExecuteScript("return $('#person-cost').prop('value');").ToString();
            }
            else throw new System.ArgumentException(itemError);

            return value;
        }

        public void clickRadioButtonAndCheckBox(String item) 
        {
            if(item == "internet basic")
            {
                click(basic_broudbend_locator);
            }
            else if(item == "internet home")
            {
                click(home_plus_locator);
            }
            else if(item == "tv sky")
            {
                click(sky_tv_locator);
            }
            else if(item == "tv netflix")
            {
                click(netflix_tv_locator);
            }
             else if(item == "tv license")
            {
                click(tvlicense_tv_locator);
            }
             else if(item == "phone basic")
            {
                click(basic_phone_locator);
            }
            else if(item == "phone medium")
            {
                click(medium_phone_locator);
            }
            else throw new System.ArgumentException(itemError);
        }

        public Boolean isRadioButtonOrCheckBoxPresent(String item)
        {
            By item_locator = By.Id("'" + item + "'");
            waitUntilElementIsNotVisible(item_locator);
            pause(500);
            return isElementDisplayed(item_locator);
        }

        public Boolean isInputFieldsDisplayed() 
        {
            waitUntilElementIsNotVisible(house_bill_locator);
            pause(1000);
            return isElementDisplayed(house_bill_locator);
        }

        public Boolean isInputFieldsPresent() 
        {
            pause(2000);
            return isElementDisplayed(house_bill_locator);
        }

        public String getTextAttribute(String item)
        {
            String value = null;
            if(item == "electricity rate")
            {
                value = getTextAttribute(electricity_rates_locators).ToString();
            }
            else if(item =="gas rate")
            {
                value = getTextAttribute(gas_rates_locators).ToString();
            }
            else if(item == "water rate")
            {
                value = getTextAttribute(water_rates_locators).ToString();
            }
            else if(item == "internet base rate")
            {
                value = getTextAttribute(internet_rates_locators).ToString();
            }
            else if(item == "internet home rate")
            {
                value = getTextAttribute(internet_rates_locators).ToString();
            }
            else if(item == "tv sky rate")
            {
                value = getTextAttribute(tv_rates_locators).ToString();
            }
            else if(item == "tv netflix rate")
            {
                value = getTextAttribute(tv_rates_locators).ToString();
            }
            else if(item == "tv license rate")
            {
                value = getTextAttribute(tv_rates_locators).ToString();
            }
            else if(item == "phone base rate")
            {
                value = getTextAttribute(phone_rates_locators).ToString();
            }
            else if(item == "phone medium rate")
            {
                value = getTextAttribute(phone_rates_locators).ToString();
            }
            else if(item == "gas massage")
            {
                value = getTextAttribute(gas_massage_locator).ToString();
            }
            else throw new System.ArgumentException(itemError);
            return value;
        }
	}
}
