﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SeleniumTesting
{

    public class ContactUsPage : AbstractPage
	{
        /*Hrefs Locators*/
        private By facebook_href_locator = By.CssSelector("[href='http://facebook.com/dividabill']");
        private By twitter_href_locator = By.CssSelector("[href='http://twitter.com/dividabill']");
        private By email_href_locator = By.CssSelector("[href='mailto:support@dividabill.co.uk']");
        private By call_us_href_locator = By.CssSelector("[href='tel:02380223596']");
        private By live_chat_href_locator = By.CssSelector("[href*='javascript:void(0)']");

        /*Image Locators*/
        private By facebook_image_locator = By.CssSelector("[src*='FBIcon.png']");
        private By twitter_image_locator = By.CssSelector("[src*='TwitterIcon.png']");
        private By email_image_locator = By.CssSelector("[src*='EmailIcon.png']");
        private By call_us_image_locator = By.CssSelector("[src*='PhoneIcon.png']");
        private By live_chat_image_locator = By.CssSelector("[src*='LiveChatIcon.png']");

        private By quick_chat_locator = By.CssSelector("#habla_window_state_div");

        private By chat_arrow_locator = By.CssSelector("#habla_sizebutton_a");

        private By google_map_coordinats_locator = By.Id("map");

        public ContactUsPage(IWebDriver driver)
            : base(driver)
        {
        }

        public String getTextAttribute(String item) 
        {
            String value = null;

            if (item.Contains("Facebook"))
            {
                value = getTextAttribute(facebook_href_locator).ToString();
            }
            else if (item.Contains("Twitter"))
            {
                value = getTextAttribute(twitter_href_locator).ToString();
            }
            else if (item.Contains("Write us an email"))
            {
                value = getTextAttribute(email_href_locator).ToString();
            }
            else if (item.Contains("Call us"))
            {
                value = getTextAttribute(call_us_href_locator).ToString();
            }
            else if (item.Contains("Live Chat"))
            {
                value = getTextAttribute(live_chat_href_locator).ToString();
            }
            else throw new System.ArgumentException(itemError);

            return value;
        }

        public String getHrefAttribute(String item)
        {
            String value = null;
            pause(2000);

            if (item.Contains("Facebook"))
            {
                value = driver.FindElement(facebook_href_locator).GetAttribute("href").ToString();
            }
            else if (item.Contains("Twitter"))
            {
                value = driver.FindElement(twitter_href_locator).GetAttribute("href").ToString();
            }
            else if (item.Contains("Write us an email"))
            {
                value = driver.FindElement(email_href_locator).GetAttribute("href").ToString();
            }
            else if (item.Contains("Call us"))
            {
                value = driver.FindElement(call_us_href_locator).GetAttribute("href").ToString();
            }
            else if (item.Contains("Live Chat"))
            {
                value = driver.FindElement(live_chat_href_locator).GetAttribute("href").ToString();
            }
            else throw new System.ArgumentException(itemError);

            return value;
        }

        public String getSrcAttribute(String item)
        {
            String value = null;

            if (item.Contains("Facebook"))
            {
                value = driver.FindElement(facebook_image_locator).GetAttribute("src").ToString();
            }
            else if (item.Contains("Twitter"))
            {
                value = driver.FindElement(twitter_image_locator).GetAttribute("src").ToString();
            }
            else if (item.Contains("Write us an email"))
            {
                value = driver.FindElement(email_image_locator).GetAttribute("src").ToString();
            }
            else if (item.Contains("Call us"))
            {
                value = driver.FindElement(call_us_image_locator).GetAttribute("src").ToString();
            }
            else if (item.Contains("Live Chat"))
            {
                value = driver.FindElement(live_chat_image_locator).GetAttribute("src").ToString();
            }
            else throw new System.ArgumentException(itemError);

            return value;
        }

        public void clickOnChatElement(String item)
        {
            pause(1000);
            if (item == "icon")
            {
                jsClick(live_chat_href_locator);
            }
            else if (item == "arrow")
            {
                jsClick(chat_arrow_locator);
            }
            else throw new System.ArgumentException(itemError);
            
            pause(1000);
        }

        public String getClassAttributeQuickChat()
        {
            return driver.FindElement(quick_chat_locator).GetAttribute("class").ToString();
        }

        public Boolean isGoogleMapPresent() 
        {
            pause(3000);
            return isElementDisplayed(google_map_coordinats_locator);
        }
	}
}
