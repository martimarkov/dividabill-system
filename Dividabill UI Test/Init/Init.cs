﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using System.IO;
using System.Drawing.Imaging;

namespace SeleniumTesting
{
    [TestFixture]
    public class Init : AdditionalData
    {
        public String baseUrl = "http://dividabill-dev.azurewebsites.net/";
        public static IWebDriver driver;
        private DefaultWait<IWebDriver> driverWait;
        private static int ELEMENT_LOAD_TIMEOUT_SEC = 45;
        private static int PAGE_LOAD_TIMEOUT_SEC = 120;
        private static int SCRIPT_LOAD_TIMEOUT_SEC = 15;
        private static int WEBDRIVER_TIMEOUT = 3;

        [SetUp]
        public void FixtureSetUp()
        {
            driver = new ChromeDriver(@"..\..\Files");
            driver.Manage().Window.Maximize();
            driver.Manage().Cookies.DeleteAllCookies();
            driver.Navigate().GoToUrl(baseUrl);

            driver.Manage().Timeouts().SetPageLoadTimeout(TimeSpan.FromSeconds(PAGE_LOAD_TIMEOUT_SEC));
            driver.Manage().Timeouts().SetScriptTimeout(TimeSpan.FromSeconds(SCRIPT_LOAD_TIMEOUT_SEC));
            this.driverWait = new WebDriverWait(driver, TimeSpan.FromSeconds(ELEMENT_LOAD_TIMEOUT_SEC));
            driverWait.IgnoreExceptionTypes(typeof(WebDriverException));
        }

        [TearDown]
        public void FixtureTearDown()
        {
            // closes every window associated with this driver
            driver.Quit();
        }

        private void refreshPage(IWebDriver driver)
        {
            driver.Navigate().Refresh();
        }

        public void returnBack(IWebDriver driver)
        {
            driver.Navigate().Back();
        }

        public void navigateTo(IWebDriver driver, String url)
        {
            driver.Navigate().GoToUrl(url);
        }

        public virtual void check(By by)
        {
            if (!isChecked(by))
            {
                click(by);
            }
        }

        public virtual void unCheck(By by)
        {
            if (isChecked(by))
            {
                click(by);
            }
        }

        public virtual void interactWithCheckBox(By by, Boolean action)
        {
            IWebElement element = driver.FindElement(by);
            if (action)
            {
                check(by);
            }
            else unCheck(by);
        }

        public virtual bool isChecked(By by)
        {
            Boolean result = false;
            IWebElement element = driver.FindElement(by);
            try
            {
                if (element.GetAttribute("checked").Equals("true"))
                    result = true;
            }
            catch (Exception e)
            {
                result = false;
            }
            return result;
        }

        public Boolean isSelected(By by)
        {
            Boolean result = false;
            IWebElement element = driver.FindElement(by);
            if (element.GetAttribute("selected").Equals("true"))
                result = true;

            return result;
        }

        public void clearAndType(By by, String text)
        {
            IWebElement element = driver.FindElement(by);
            element.Click();
            element.Clear();
            if (!text.Contains("empty"))
            {
                element.SendKeys(text);
            }
        }

        public void type(By by, String text)
        {
            IWebElement element = driver.FindElement(by);
            element.SendKeys(text);
        }

        public void click(By locator)
        {
            IWebElement element = driver.FindElement(locator);
            element.Click();
        }

        public Boolean isElementDisplayed(By by)
        {
            Boolean elementVisible = false;
            if (isElementPresent(by))
            {
                elementVisible = driver.FindElement(by).Displayed;
            }
            return elementVisible;
        }

        public Boolean isElementPresent(By by)
        {
            Boolean elementState;
            try
            {
                driver.FindElement(by);
                elementState = true;
            }
            catch (NoSuchElementException e)
            {
                elementState = false;
            }
            return elementState;
        }


        public void jsClick(By by)
        {
            IWebElement element = driver.FindElement(by);
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            executor.ExecuteScript("arguments[0].click();", element);
        }

        public int getNumberOfElements(String containerId, String tagName)
        {
            return driver.FindElements(By.XPath("//*[@id='" + containerId + "']//" + tagName)).Count();
        }

        public IJavaScriptExecutor getJs(IWebDriver driver)
        {
            return (IJavaScriptExecutor)driver;
        }

        public void setValueUsingJS(IWebDriver driver, String attribute, String value, IWebElement element)
        {
            getJs(driver).ExecuteScript("arguments[0].setAttribute(\"" + attribute + "\",\"" + value + "\")", element);
        }

        public void jsType(By by, String text)
        {
            IWebElement element = driver.FindElement(by);
            ((IJavaScriptExecutor)driver).ExecuteScript("return arguments[0].value=\"" + text + "\";", element);
        }

        public void jsSpecificType(By by, String text)
        {
            IWebElement element = driver.FindElement(by);
            jsClick(by);
            pause(1000);
            ((IJavaScriptExecutor)driver).ExecuteScript("return arguments[0].value=\"" + text + "\".trigger('change');", element);
        }

        public virtual void selectDropDownValue(String container, String value)
        {
            if (!String.IsNullOrEmpty(value))
            {
                driver.FindElement(By.XPath(container + "/a[@class='chosen-single']")).Click();
                driver.FindElement(By.XPath(container + "//li[text()='" + value + "']")).Click();
                pause(1000);
            }
        }

        public void selectDropDownValueByText(By element, String text)
        {
            if (!String.IsNullOrEmpty(text))
            {
                IWebElement dropDown = driver.FindElement(element);
                SelectElement selector = new SelectElement(dropDown);
                selector.SelectByText(text);
                pause(500);
            }
        }

        public void selectDropDownValueByText(String id, String text)
        {
            selectDropDownValueByText(By.Id(id), text);
        }

        public void specificDropDownSelect(By dropdown, String text)
        {
            if (!String.IsNullOrEmpty(text))
            {
                SelectElement selector = new SelectElement(driver.FindElement(dropdown));
                selector.SelectByText(text);
                pause(500);
            }
        }

        public void selectDropDownByValue(By element, String value)
        {
            if (!String.IsNullOrEmpty(value))
            {
                IWebElement dropDown = driver.FindElement(element);
                SelectElement selector = new SelectElement(dropDown);
                selector.SelectByValue(value);
                pause(500);
            }
        }

        public void selectDropDownValueByIndex(String id, int index)
        {
            if (!String.IsNullOrEmpty(index.ToString()))
            {
                IWebElement dropDown = driver.FindElement(By.Id(id));
                SelectElement selector = new SelectElement(dropDown);
                selector.SelectByIndex(index);
                pause(500);
            }
        }

        public void selectValue(String id, String value)
        {
            if (!String.IsNullOrEmpty(value))
            {
                IWebElement dropDown = driver.FindElement(By.Id(id));
                SelectElement selector = new SelectElement(dropDown);
                selector.SelectByValue(value);
                pause(500);
            }
        }

        public void waitUntillElementIsPresent(By by)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(ELEMENT_LOAD_TIMEOUT_SEC));
            wait.Until(drv => drv.FindElement(by));
        }

        public IWebElement waitUntilElementIsNotVisible(By by)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(ELEMENT_LOAD_TIMEOUT_SEC));
            return wait.Until(ExpectedConditions.ElementIsVisible(by));
        }

        public void waitUntillElementIsVisible(By by)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(ELEMENT_LOAD_TIMEOUT_SEC));
            wait.Until(drv => !drv.FindElement(by).Displayed);
        }

        public void WaitUntillElementDisplayed(By by)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(ELEMENT_LOAD_TIMEOUT_SEC));
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(by));
        }

        public void WaitUntillElementIsClickable(By by)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(ELEMENT_LOAD_TIMEOUT_SEC));
            wait.Until(ExpectedConditions.ElementToBeClickable(by));
        }

        public static String getValueAttribute(By by)
        {
            IWebElement element = driver.FindElement(by);
            return element.GetAttribute("value").ToString();
        }

        public String getColorAttribute(By by)
        {
            IWebElement element = driver.FindElement(by);
            return element.GetAttribute("color").ToString();
        }

        public String getFontTypeAttribute(By by)
        {
            IWebElement element = driver.FindElement(by);
            return element.GetAttribute("face").ToString();
        }

        public String getClassAttribute(By by)
        {
            waitUntillElementIsPresent(by);
            IWebElement element = driver.FindElement(by);
            return element.GetAttribute("class").ToString();
        }

        public static string getTextAttribute(By by)
        {
            IWebElement element = driver.FindElement(by);
            return element.Text.ToString();
        }

        public string getSelectedOption(By dropdownBy)
        {
            string result = "";
            SelectElement select = new SelectElement(driver.FindElement(dropdownBy));
            result = select.SelectedOption.Text.ToString();
            return result;
        }

        public static string getIdAttribute(By by)
        {
            IWebElement element = driver.FindElement(by);
            return element.GetAttribute("id").ToString();
        }

        public void doubleClick(By by)
        {
            IWebElement element = driver.FindElement(by);
            Actions action = new Actions(driver);
            action.DoubleClick(element);
            action.Perform();
            action.Build();
        }

        public void moveToElement(By by)
        {
            waitUntillElementIsPresent(by);
            IWebElement element = driver.FindElement(by);
            Actions action = new Actions(driver);
            action.MoveToElement(element);
            action.Perform();
            action.Build();
        }

        public void dragAndDropTheElement(By source, By destination)
        {
            IWebElement sourceElement = driver.FindElement(source);
            IWebElement destinationElement = driver.FindElement(destination);
            Actions action = new Actions(driver);
            action.DragAndDrop(sourceElement, destinationElement);
            action.Perform();
            action.Build();
        }

        public void switchToFrame(By by)
        {
            IWebElement element = driver.FindElement(by);
            driver.SwitchTo().Frame(element);
        }

        public void switchToActiveElement()
        {
            driver.SwitchTo().ActiveElement();
        }

        public String getAlertMessageText()
        {
            IAlert alert = driver.SwitchTo().Alert();
            return alert.Text;
        }

        public void jsTypeToFrame(By textArea, String text)
        {
            IWebElement element = driver.FindElement(textArea);
            ((IJavaScriptExecutor)driver).ExecuteScript("document.body.innerHTML = '<br>'");
            element.SendKeys(text);
        }

        public static string getMessageContent(By fieldLocator)
        {
            IWebElement element = driver.FindElement(fieldLocator);
            return element.GetAttribute("data-content").ToString();
        }

        public void switchToNextWindow()
        {
            ReadOnlyCollection<string> windowHandles = driver.WindowHandles;
            foreach (string handle in windowHandles)
            {
                try
                {
                    driver.SwitchTo().Window(handle);
                    pause(2000);
                }
                catch (NoSuchWindowException e)
                {
                    pause(2000);
                    driver.SwitchTo().Window(handle);
                }
            }
        }

        public void pause(int msec)
        {
            try
            {
                Thread.Sleep(msec);
            }
            catch (Exception e) { }
        }

        public void waitForAjax()
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(ELEMENT_LOAD_TIMEOUT_SEC));
            wait.Until(d => (bool)(d as IJavaScriptExecutor).ExecuteScript("return jQuery.active == 0"));
        }

        public void clickToFrame()
        {
            IWebElement element = driver.FindElement(By.TagName("body"));
            Actions action = new Actions(driver);
            action.MoveByOffset(element.Location.X, element.Location.Y);
            action.Click();
            action.Perform();
            action.Build();
        }

        public Boolean isValueInDropDownSelected(By dropDownLocator, String dropDownValue)
        {
            By optionLocator = By.XPath("//*[@id='" + getIdAttribute(dropDownLocator) + "']//*[text()='" + dropDownValue + "']");
            return isSelected(optionLocator);
        }

        public String getPlaceholderValue(By by)
        {
            waitUntillElementIsPresent(by);
            IWebElement element = driver.FindElement(by);
            return element.GetAttribute("placeholder").ToString();
        }

        public void switchToPopUp()
        {
            String popWindowHandle = "";
            //get the current window handles
            String mainWindow = driver.CurrentWindowHandle;


            //get the collection of all open windows
            ReadOnlyCollection<string> windowHandles = driver.WindowHandles;
            foreach (string handle in windowHandles)
            {
                if (handle != mainWindow)
                {
                    popWindowHandle = handle;
                    break;
                }
            }
            driver.SwitchTo().Window(popWindowHandle);
        }

        public void mouseOver(By by)
        {
            IWebElement element = driver.FindElement(by);
            Actions action = new Actions(driver);
            action.MoveToElement(element).Perform();
        }

        public void displayBlock(By by)
        {
            IWebElement element = driver.FindElement(by);
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            executor.ExecuteScript("arguments[0].style.display = 'block';", element);
        }
    }
}
