﻿using System.Net;
using System.Threading.Tasks;
using API.Library.Helpers;
using Dividabill.Tests.Core;
using DividaBill.Models.GoCardless;
using GoCardless_API.api;
using NUnit.Framework;

namespace GoCardless_API.IntegrationTests.api
{
    [TestFixture]
    public class GoCardlessApiClientTests : BaseAutoMock<GoCardlessApiClient>
    {
        [TestCase("00000000", "00-00-00")]
        [TestCase("wrong account number", "wrong account sort code")]
        public async Task GivenWrongBankAccount_WhenCheckBankAccountInvoked_ThenApiExceptionThrownAndStatusEqual422(string accountNumber, string accountSortCode)
        {
            var inputModel = new GCBankAccountDetailsInputModel
            {
                bank_details_lookups = new BankAccountDetailsInputModel
                {
                    branch_code = accountSortCode,
                    country_code = "GB",
                    account_number = accountNumber
                }
            };

            try
            {
                await Task.Run(async () => await ClassUnderTest.CheckBankAccountDetails(inputModel));
            }
            catch (ApiException exception)
            {
                Assert.AreEqual((HttpStatusCode) 422, exception.StatusCode);
            }
        }
    }
}