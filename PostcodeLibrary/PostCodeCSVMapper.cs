﻿using CsvHelper.Configuration;
using DividaBill.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostcodeLibrary
{
    public sealed class PostCodeCSVMapper : CsvClassMap<PostCode>
    {
        public PostCodeCSVMapper()
        {
            Map(m => m.Constituency);
            Map(m => m.Country);
            Map(m => m.County);
            Map(m => m.CountyCode);
            Map(m => m.District);
            Map(m => m.DistrictCode);
            Map(m => m.Easting);
            Map(m => m.GridRef);
            Map(m => m.Households);
            Map(m => m.Introduced);
            Map(m => m.Latitude);
            Map(m => m.Longitude);
            Map(m => m.Northing);
            Map(m => m.NationalPark);
            Map(m => m.Parish);
            Map(m => m.Population);
            Map(m => m.Postcode);
            Map(m => m.Terminated);
            Map(m => m.Ward);
            Map(m => m.WardCode);
            Map(m => m.Built_up_area).Name("Built up area");
            Map(m => m.Built_up_sub_division).Name("Built up sub-division");
            Map(m => m.Lower_layer_super_output_area).Name("Lower layer super output area");
        }
    }
}
