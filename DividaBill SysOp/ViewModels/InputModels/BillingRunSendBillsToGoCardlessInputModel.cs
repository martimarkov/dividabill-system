using System.ComponentModel.DataAnnotations;

namespace DividaBill.SysOp.ViewModels.InputModels
{
    public class BillingRunSendBillsToGoCardlessInputModel
    {
        [Required]
        public int BillingRunId { get; set; }

        [Required]
        public GoCardless_API.GoCardless.Environments Environment { get; set; }
    }
}