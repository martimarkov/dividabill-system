﻿using System;

namespace DividaBill.SysOp.ViewModels
{
    public class GenerateBillViewModel
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime PaymentDate { get; set; }

        public Boolean SendEmails { get; set; }
    }
}