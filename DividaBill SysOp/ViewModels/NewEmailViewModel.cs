﻿using System;
using System.Web.Mvc;

namespace DividaBill.Areas.Admin.ViewModels
{
    public class NewEmailViewModel
    {
        public String To { get; set; }
        public String Subject { get; set; }
        [AllowHtml]
        public String Message { get; set; }
        public int HouseId { get; set; }
    }
}