﻿namespace DividaBill.SysOp.Models
{
    public class IssuingAuthorityKey
    {
        public string Id { get; set; }
    }

    public class Tenant
    {
        public string Id { get; set; }
    }
}