﻿using System.Data.Entity;

namespace DividaBill.SysOp.Models
{
    public class TenantDbContext : DbContext
    {
        public TenantDbContext()
            : base("SysOpConnection")
        {
        }

        public DbSet<IssuingAuthorityKey> IssuingAuthorityKeys { get; set; }

        public DbSet<Tenant> Tenants { get; set; }
    }
}