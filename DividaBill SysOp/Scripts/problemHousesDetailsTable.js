﻿var TableManaged = function () {

    return {

        //main function to initiate the module
        init: function () {

            if (!jQuery().dataTable) {
                return;
            }

            // begin first table
            var oTable = $('#problemHousesTable').dataTable({
                "aoColumns": [
                  { "bSortable": false },
                  null,
                  { "bSortable": false, "sType": "text" },
                  null,
                  null
                ],
                "aLengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [0] },
                    { "bSearchable": false, "aTargets": [0] }
                ]
            });

            jQuery('#problemHousesTable .group-checkable').change(function () {
                var set = jQuery(this).attr("data-set");
                var checked = jQuery(this).is(":checked");
                jQuery(set).each(function () {
                    if (checked) {
                        $(this).attr("checked", true);
                        $(this).parents('tr').addClass("active");
                    } else {
                        $(this).attr("checked", false);
                        $(this).parents('tr').removeClass("active");
                    }
                });
                jQuery.uniform.update(set);
            });

            jQuery('#problemHousesTable').on('change', 'tbody tr .checkboxes', function () {
                $(this).parents('tr').toggleClass("active");
            });

            jQuery('#problemHousesTable_wrapper .dataTables_filter input').addClass("form-control input-medium input-inline"); // modify table search input
            jQuery('#problemHousesTable_wrapper .dataTables_length select').addClass("form-control input-xsmall"); // modify table per page dropdown
            jQuery('#problemHousesTable_wrapper .dataTables_length select').select2(); // initialize select2 dropdown



            // Delete handling

            $('#problemHousesTable').on('click', 'a.archive', function (e) {
                e.preventDefault();
                var nRow = $(this).parents('tr')[0];
                var id = $(nRow).data("id");
                var url = $(this).data("request-url");

                bootbox.confirm("Are you sure to <b>archive</b> this row?", function (result) {
                    if (result == true) {
                        $.ajax({
                            url: url,
                            data: { id: id },
                            type: 'POST',
                            success: function (data) {
                                if (data == "success") {
                                    oTable.fnDeleteRow(nRow);
                                }
                            }
                        });
                    }
                    return;
                });


            });

            $('#problemHousesTable').on('click', 'a.unarchive', function (e) {
                e.preventDefault();
                var nRow = $(this).parents('tr')[0];
                var id = $(nRow).data("id");
                var url = $(this).data("request-url");

                bootbox.confirm("Are you sure to <b>unarchive</b> this row?", function (result) {
                    if (result == true) {
                        $.ajax({
                            url: url,
                            data: { id: id },
                            type: 'POST',
                            success: function (data) {
                                if (data == "success") {
                                    oTable.fnDeleteRow(nRow);
                                }
                            }
                        });
                    }
                    return;
                });


            });

            $('#problemHousesTable').on('click', 'a.delete', function (e) {
                e.preventDefault();
                var nRow = $(this).parents('tr')[0];
                var id = $(nRow).data("id");
                var url = $(this).data("request-url");

                bootbox.confirm("Are you sure to <b>delete</b> this row?", function (result) {
                    if (result == true) {
                        $.ajax({
                            url: url,
                            data: { id: id },
                            type: 'POST',
                            success: function (data) {
                                if (data == "success") {
                                    oTable.fnDeleteRow(nRow);
                                }
                            }
                        });
                    }
                    return;
                });


            });
        }

    };

}();

