﻿$("#ValidateBankDetails").click(
    function(event) {
        event.preventDefault();
        var accountHolder = $("#AccountHolder").val();
        var accountNumber = $("#AccountNumber").val();
        var accountSortCode = $("#AccountSortCode").val();
        $.getJSON("/api/BankAccount/ValidateBankAccount/" + accountNumber + "/" + accountSortCode + "/" + accountHolder,
            function(data) {
                if (data != null) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].Key.toLowerCase().indexOf("accountholder") > 0) {
                            $("#AccountHolderLabel").text(data[i].ErrorMessage);
                        } else if (data[i].Key.toLowerCase().indexOf("accountsortcode") > 0) {
                            $("#AccountSortCodeLabel").text(data[i].ErrorMessage);
                        } else if (data[i].Key.toLowerCase().indexOf("accountnumber") > 0) {
                            $("#AccountNumberLabel").text(data[i].ErrorMessage);
                        } else {
                            $("#ValidateBankDetailsErrorLabel").text(data[i].ErrorMessage);
                        }
                    }
                    $("#ValidateBankDetailsLabel").text("");
                } else {
                    $("#ValidateBankDetailsLabel").text("Bank account is valid");
                    $("#AccountNumberLabel").text("");
                    $("#AccountHolderLabel").text("");
                    $("#AccountSortCodeLabel").text("");
                    $("#ValidateBankDetailsErrorLabel").text("");
                }
            }
        ).fail(function(xhr, textStatus, errorThrown) {
            alert("Error: " + xhr.responseText);
        });
    }
);