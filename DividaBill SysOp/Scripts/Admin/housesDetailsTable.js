﻿var TableManaged = function () {

    return {

        //main function to initiate the module
        init: function () {

            if (!jQuery().dataTable) {
                return;
            }

            $.fn.dataTable.moment('D/M/YYYY');

            // begin first table
            var oTable = $('#housesTable').dataTable({
                "columnDefs": [
                    //BUG: Not working. Need to find a fix.
                    { "orderDataType": "dom-checkbox", "targets": [2, 3, 4, 5] } 

                ],
                "aLengthMenu": [
                    [25, 50, 100, -1],
                    [25, 50, 100, "All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 25,
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                }
            });

            jQuery('#housesTable .group-checkable').change(function () {
                var set = jQuery(this).attr("data-set");
                var checked = jQuery(this).is(":checked");
                jQuery(set).each(function () {
                    if (checked) {
                        $(this).attr("checked", true);
                        $(this).parents('tr').addClass("active");
                    } else {
                        $(this).attr("checked", false);
                        $(this).parents('tr').removeClass("active");
                    }
                });
                jQuery.uniform.update(set);
            });

            jQuery('#housesTable').on('change', 'tbody tr .checkboxes', function () {
                $(this).parents('tr').toggleClass("active");
            });

            jQuery('#housesTable_wrapper .dataTables_filter input').addClass("form-control input-medium input-inline"); // modify table search input
            jQuery('#housesTable_wrapper .dataTables_length select').addClass("form-control input-xsmall"); // modify table per page dropdown
            jQuery('#housesTable_wrapper .dataTables_length select').select2(); // initialize select2 dropdown



            $('#housesTable_new').click(function (e) {


                e.preventDefault();

                window.location.href = "/Admin/Houses/Create"

                //var aiNew = oTable.fnAddData(['', '', '', '',

                //        '<a class="edit" href="">Get</a>', '<a class="cancel" data-mode="new" href="">Cancel</a>'

                //]);

                //var nRow = oTable.fnGetNodes(aiNew[0]);

                //editRow(oTable, nRow);

                //nEditing = nRow;

            });

            // Delete handling

            $('#housesTable').on('click', 'a.archive', function (e) {
                e.preventDefault();
                var nRow = $(this).parents('tr')[0];
                var id = $(nRow).data("id");
                var url = $(this).data("request-url");

                bootbox.confirm("Are you sure to <b>archive</b> this row?", function (result) {
                    if (result == true) {
                        $.ajax({
                            url: url,
                            data: { id: id },
                            type: 'POST',
                            success: function (data) {
                                if (data == "success") {
                                    oTable.fnDeleteRow(nRow);
                                }
                            }
                        });
                    }
                    return;
                });


            });
        }

    };

}();