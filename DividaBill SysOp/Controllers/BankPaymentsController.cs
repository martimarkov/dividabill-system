﻿using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using DividaBill.Models;
using DividaBill.DAL;
using Mvc.JQuery.DataTables;
using AutoMapper;
using DividaBill.DAL.DbContext;
using DividaBill.ViewModels.DataTables;

namespace DividaBill.SysOp.Controllers
{
    public class BankPaymentsController : AdminController
    {
        

        private ApplicationDbContext db = new ApplicationDbContext();

        public DataTablesResult<BankPaymentsViewModel> GetPayments(DataTablesParam dataTableParam)
        {
            IQueryable<BankPaymentsViewModel> payments = db.BankPayments.Include("Mandate").AsEnumerable()
                .Select(
                p =>
                {
                    var map = Mapper.Map<BankPaymentsViewModel>(p);
                    map.ClientID = p.Mandate.User_ID;
                    string link = HtmlHelper.GenerateLink(
                        this.ControllerContext.RequestContext,
                        System.Web.Routing.RouteTable.Routes,
                        p.Mandate.User.FullName,
                        "",
                        "Details", "Tenants", new System.Web.Routing.RouteValueDictionary(new { id = map.ClientID }), null);

                    map.Client = link;

                    return map;
                }
                    ).AsQueryable();
            return DataTablesResult.Create(payments, dataTableParam,  Mvc.JQuery.DataTables.Models.ArrayOutputType.ArrayOfObjects);  //...and return a DataTablesResult
        }
        // GET: BankPayments
        public ActionResult Index()
        {
            return View();
        }

        // GET: BankPayments/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BankPayment bankPayment = db.BankPayments.Find(id);
            if (bankPayment == null)
            {
                return HttpNotFound();
            }
            return View(bankPayment);
        }

        // GET: BankPayments/Create
        public ActionResult Create()
        {
            ViewBag.Mandate_ID = new SelectList(db.BankMandates, "id", "created_at");
            return View();
        }

        // POST: BankPayments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,created_at,status,amount_refunded,amount,currency,description,charge_date,reference,Mandate_ID")] BankPayment bankPayment)
        {
            if (ModelState.IsValid)
            {
                db.BankPayments.Add(bankPayment);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Mandate_ID = new SelectList(db.BankMandates, "id", "created_at", bankPayment.Mandate_ID);
            return View(bankPayment);
        }

        // GET: BankPayments/Get/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BankPayment bankPayment = db.BankPayments.Find(id);
            if (bankPayment == null)
            {
                return HttpNotFound();
            }
            ViewBag.Mandate_ID = new SelectList(db.BankMandates, "id", "created_at", bankPayment.Mandate_ID);
            return View(bankPayment);
        }

        // POST: BankPayments/Get/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,created_at,status,amount_refunded,amount,currency,description,charge_date,reference,Mandate_ID")] BankPayment bankPayment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bankPayment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Mandate_ID = new SelectList(db.BankMandates, "id", "created_at", bankPayment.Mandate_ID);
            return View(bankPayment);
        }

        // GET: BankPayments/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BankPayment bankPayment = db.BankPayments.Find(id);
            if (bankPayment == null)
            {
                return HttpNotFound();
            }
            return View(bankPayment);
        }

        // POST: BankPayments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            BankPayment bankPayment = db.BankPayments.Find(id);
            db.BankPayments.Remove(bankPayment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
