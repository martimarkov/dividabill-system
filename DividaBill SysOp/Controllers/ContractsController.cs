﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using DividaBill.Models;
using DividaBill.Services.Abstraction;
using DividaBill.Services.Factories;
using DividaBill.Services.Interfaces;
using DividaBill.ViewModels;

namespace DividaBill.SysOp.Controllers
{
    public class ContractsController : AdminController
    {
        private readonly IEntityToViewModelMappingFactory<Contract, ContractRemoveViewModel> _contractToContractRemoveOutputViewModelMappingFactory;
        private readonly IContractService _contractService;
        private readonly IHouseService _houseService;
        private readonly ITenantEncryptionService _tenantEncryptionService;
        private readonly IProviderService _providerService;

        public ContractsController(IHouseService houseService, IContractService contractService, IProviderService providerService, ITenantEncryptionService tenantEncryptionService, IEntityToViewModelMappingFactory<Contract, ContractRemoveViewModel> contractToContractRemoveOutputViewModelMappingFactory)
        {
            _providerService = providerService;
            _contractToContractRemoveOutputViewModelMappingFactory = contractToContractRemoveOutputViewModelMappingFactory;
            _tenantEncryptionService = tenantEncryptionService;
            _houseService = houseService;
            _contractService = contractService;
        }

        public ActionResult ContractEdit(int id)
        {
            var contract = _contractService.GetContract(id);
            var contractViewModel = Mapper.Map<ContractViewModel>(contract);
            var providers = _providerService.GetProvidersByServiceTypeId(contractViewModel.Service);
            var tenants = _houseService.GetTenantsForHouse(contract.House_ID);
            _tenantEncryptionService.DecryptUsersList(tenants.AsQueryable());

            var contractLengthItems = (from object item in Enum.GetValues(typeof (ContractLength))
                select new SelectListItem
                {
                    Value = Convert.ToInt32(item).ToString(), Text = item.ToString()
                }).ToList();
            var packages = new SelectList(new List<object>
            {
                new {value = 0, text = "N/A"},
                new {value = 1, text = "Tier 1 (ADSL/Basic)"},
                new {value = 2, text = "Tier 2 (Fibre/Medium)"}
            },
                "value",
                "text");


            ViewBag.Parameters = new ContractViewsViewBagParameters
            {
                Providers = new SelectList(providers, "ID", "Name"),
                Houses = new SelectList(_houseService.GetAllHouses(), "ID", "Address.Line1"),
                ContractLengths = new SelectList(contractLengthItems, "Value", "Text"),
                Packages = packages,
                HouseTenants = new SelectList(tenants, "Id", "FullName")
            };
            return PartialView("_ContractEdit", contractViewModel);
        }

        public ActionResult ContractRemove(int id)
        {
            var contract = _contractService.GetContract(id);
            var contractViewModel = _contractToContractRemoveOutputViewModelMappingFactory.Map(contract);
            return PartialView("_ContractRemove", contractViewModel);
        }

        public ActionResult ContractAdd(int houseId)
        {
            var services = ServiceType.All;
            var providers = _providerService.GetProvidersByServiceTypeId(services.First().Id);

            var contractLengthItems = (from ContractLength item in Enum.GetValues(typeof (ContractLength))
                select new SelectListItem
                {
                    Value = Convert.ToInt32(item).ToString(), Text = item.ToString()
                }).ToList();

            var tenants = _houseService.GetTenantsForHouse(houseId);
            _tenantEncryptionService.DecryptUsersList(tenants.AsQueryable());

            ViewBag.Parameters = new ContractViewsViewBagParameters
            {
                Providers = new SelectList(providers, "ID", "Name"),
                Services = new SelectList(services, "ID", "Name"),
                ContractLengths = new SelectList(contractLengthItems, "Value", "Text"),
                HouseTenants = new SelectList(tenants, "Id", "FullName")
            };

            var contract = new ContractViewModel
            {
                House_ID = houseId
            };

            return PartialView("_ContractAdd", contract);
        }
    }
}