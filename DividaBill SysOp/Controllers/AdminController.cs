﻿using DividaBill.Areas.Admin.ViewModels;
using DividaBill.SysOp.Utils;
using Microsoft.Azure.ActiveDirectory.GraphClient;
using System.Collections.Generic;
using System.Web.Mvc;

namespace DividaBill.SysOp.Controllers
{
    [Authorize]
    //[RequireHttps]
    public abstract class AdminController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            ActiveDirectoryClient client = AuthenticationHelper.GetActiveDirectoryClient();
            //using (MemoryStream s = new MemoryStream(rs.Properties["thumbnailPhoto"][0] as byte[]))
            //{
            //    byte[] imageBytes = s.ToArray();
            //    string base64String = Convert.ToBase64String(imageBytes);
            //}


            var user = new UserMinimalViewModel
            {
                FirstName = "",
                LastName = ""
            };

            var notifications = new List<INotificationViewModel>();

            //using (ApplicationDbContext db = new ApplicationDbContext())
            //{
                //UrlHelper u = new UrlHelper(this.ControllerContext.RequestContext);
                //foreach (HouseModel house in db.Houses.Where(h => h.RegisteredDate >= currentUser.LastActivityDate).OrderByDescending(h => h.RegisteredDate))
                //{
                //    notifications.Add(new NotificationViewModel
                //    {
                //        Type = "label-success",
                //        Title = "New house registered",
                //        Created = Functions.TimeAgo(house.RegisteredDate),
                //        Link = u.Action("Details", "Houses", new { Area = "Admin", @id = house.ID }),
                //        Action = "fa-plus",
                //    });
                //}

                //foreach (HouseModel house in db.Users.Where(h => h. >= currentUser.LastActivityDate))
                //{
                //    notifications.Add(new NotificationViewModel
                //    {
                //        Type = NotificationType.Success,
                //        Title = "New client registered to a house",
                //        Created = house.RegisteredDate,
                //        Link = u.Action("Details", "Houses", new { Area = "Admin", @id = house.ID }),
                //        Action = "fa-add"

                //    });
                //}

            //}
            Session["AdminLeftMenuViewModel"] = new AdminLeftMenuViewModel
            {
                User = user,
                Notifications = notifications
            };

            base.OnActionExecuting(filterContext);
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {

            base.OnActionExecuted(filterContext);
        }
    }
}