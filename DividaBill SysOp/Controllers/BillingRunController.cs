﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Mvc;
using DividaBill.Services.Exceptions;
using DividaBill.Services.Factories;
using DividaBill.Services.Impl;
using DividaBill.Services.Interfaces;
using DividaBill.Services.NewBillingSystem;
using DividaBill.Services.NewBillingSystem.Impl;
using DividaBill.Services.ThirdPartyServices.Interfaces;
using DividaBill.SysOp.ViewModels.InputModels;
using DividaBill.ViewModels.SysOp.InputModels;
using GoCardless_API;
using SendGrid.API.Models;

namespace DividaBill.SysOp.Controllers
{
    public class BillingRunController : Controller
    {
        private readonly IBillingRunCreateInputModelToBillingRunMappingFactory _billingRunCreateInputModelToBillingRunMappingFactory;
        private readonly IBillingRunService _billingRunService;
        private readonly IBillingService _billingService;
        private readonly IDistributedBillingService _distributedBillingService;
        private readonly IEmailThirdPartyService _emailThirdPartyService;
        private readonly IGoCardlessService _goCardlessService;
        private readonly IRefreshPaymentStatusService _refreshPaymentStatusService;
        private readonly ISendBillEmailsService _sendBillEmailsService;
        private readonly ISendBillsToGoCardlessService _sendBillsToGoCardlessService;

        public BillingRunController(IBillingRunService billingRunService,
            IBillingService billingService, ISendBillEmailsService sendBillEmailsService,
            IRefreshPaymentStatusService refreshPaymentStatusService, IDistributedBillingService distributedBillingService,
            ISendBillsToGoCardlessService sendBillsToGoCardlessService, IGoCardlessService goCardlessService, IEmailThirdPartyService emailThirdPartyService,
            IBillingRunCreateInputModelToBillingRunMappingFactory billingRunCreateInputModelToBillingRunMappingFactory)
        {
            _billingRunCreateInputModelToBillingRunMappingFactory = billingRunCreateInputModelToBillingRunMappingFactory;
            _emailThirdPartyService = emailThirdPartyService;
            _billingRunService = billingRunService;
            _billingService = billingService;
            _sendBillEmailsService = sendBillEmailsService;
            _refreshPaymentStatusService = refreshPaymentStatusService;
            _distributedBillingService = distributedBillingService;
            _sendBillsToGoCardlessService = sendBillsToGoCardlessService;
            _goCardlessService = goCardlessService;
        }

        [System.Web.Mvc.HttpGet]
        public async Task<ActionResult> Index()
        {
            var billingRuns = await _billingRunService.GetAllBillingRunsAsync();
            return View(billingRuns);
        }

        [System.Web.Mvc.HttpGet]
        public ActionResult Create()
        {
            var model = new BillingRunCreateInputModel
            {
                SendFromEmail = "billing@dividabill.co.uk",
                SendFromEmailDisplayName = "DividaBill Billing",
                EmailTemplateId = "e9fc4b83-ab0d-43d2-979a-fe9d0c0eef52",
                EmailSubject = "Your {Month} {Year} DividaBill"
            };
            return View(model);
        }

        [ValidateAntiForgeryToken]
        [System.Web.Mvc.HttpPost]
        public async Task<ActionResult> Create([Bind] BillingRunCreateInputModel input)
        {
            if (!ModelState.IsValid)
                return View(input);
            SendGridTemplate sendGridTemplate;
            try
            {
                sendGridTemplate = await _emailThirdPartyService.GetTemplateFromThirdParty(input.EmailTemplateId);
            }
            catch (TemplateNotFound execptionNotFound)
            {
                ModelState.AddModelError("EmailTemplateId", execptionNotFound.Message);
                return View(input);
            }

            var emailTemplate = _emailThirdPartyService.CheckEmailTemplate(sendGridTemplate);

            var billingRun = _billingRunCreateInputModelToBillingRunMappingFactory.Map(input, User, emailTemplate);

            billingRun = await _billingRunService.Insert(billingRun);
            return RedirectToAction("Index");
        }

        [System.Web.Mvc.HttpGet]
        public ActionResult HousesToBeBilled([FromUri] int id)
        {
            ViewBag.BillingRunId = id;
            //var houses = _billingRunService.GetHousesToBeBilled(id);
            return View();
        }

        [System.Web.Mvc.HttpPost]
        public async Task<List<int>> HousesToBeBilled(int billingRunId, List<int> houseIds)
        {
            var result = await _billingRunService.SetHousesToBeBilledInBillingRunAsync(billingRunId, houseIds);
            return result;
        }

        [System.Web.Mvc.HttpGet]
        public ActionResult UpdateMandate()
        {
            return View();
        }

        [System.Web.Mvc.HttpGet]
        public async Task<ActionResult> SetActiveHousesToBeBilled(int id)
        {
            await _billingRunService.SetActiveHousesToBeBilledInBillingRunAsync(id);
            return RedirectToAction("HousesToBeBilled", new {id});
        }

        [System.Web.Mvc.HttpGet]
        [System.Web.Mvc.Route("{id:int}/HousesThatHaventBeenBilledYet")]
        public ActionResult HousesThatHaventBeenBilledYet(int id)
        {
            ViewBag.BillingRunId = id;
            return View();
        }

        
        [System.Web.Mvc.HttpGet]
        [System.Web.Mvc.Route("{id:int}/GenerateMissingBills")]
        public ActionResult GenerateMissingBills(int id)
        {
            
            _billingService.GenerateBills(id);
            return RedirectToAction($"HousesThatHaventBeenBilledYet/{id}");
        }

        [NoAsyncTimeout]
        [System.Web.Mvc.HttpGet]
        [System.Web.Mvc.Route("{id:int}/DistributedGenerateMissingBills")]
        public async Task<ActionResult> DistributedGenerateMissingBills(int id)
        {
            await _distributedBillingService.DistributedGenerateBillsAsync(id);
            return View("HousesToBeBilled");
        }

        [NoAsyncTimeout]
        [System.Web.Mvc.HttpGet]
        [System.Web.Mvc.Route("{id:int}/SendTestEmail")]
        public ActionResult SendTestEmail(int id)
        {
            ViewBag.BillingRunId = id;
            return View("SendTestEmail");
        }

        [NoAsyncTimeout]
        [System.Web.Mvc.HttpPost]
        [System.Web.Mvc.Route("SendTestEmail")]
        public async Task<ActionResult> SendTestEmail([Bind] BillingRunSendTestEmailInputModel inputModel)
        {
            await _sendBillEmailsService.SendTestEmailAsync(inputModel.BillingRunId, new List<int> {inputModel.HouseId}, inputModel.EmailAddress);
            return View();
        }

        [NoAsyncTimeout]
        [System.Web.Mvc.HttpGet]
        [System.Web.Mvc.Route("{id:int}/SendTestBillingRunEmails")]
        public ActionResult SendTestBillingRunEmails(int id)
        {
            ViewBag.BillingRunId = id;
            return View();
        }

        [NoAsyncTimeout]
        [System.Web.Mvc.HttpPost]
        [System.Web.Mvc.Route("SendBillsToGoCardless")]
        public async Task<ActionResult> SendBillsToGoCardless([Bind] BillingRunSendBillsToGoCardlessInputModel inputModel)
        {
            await _sendBillsToGoCardlessService.SendBillsToGoCardlessAsync(inputModel.BillingRunId, GoCardless.Environments.Sandbox);
            return View();
        }

        [NoAsyncTimeout]
        [System.Web.Mvc.HttpGet]
        [System.Web.Mvc.Route("{id:int}/AddTenantsToGoCardless")]
        public ActionResult AddTenantsToGoCardless(int id)
        {
            ViewBag.BillingRunId = id;

            return View();
        }

        [NoAsyncTimeout]
        [System.Web.Mvc.HttpPost]
        [System.Web.Mvc.Route("AddTenantsToGoCardless")]
        public async Task<ActionResult> AddTenantsToGoCardless([Bind] BillingRunAddTenantsToGoCardlessInputModel inputModel)
        {
            try
            {
                await _goCardlessService.AddBillingRunTenantsToGoCardlessAsync(inputModel.BillingRunId, GoCardless.Environments.Production);
            }
            catch (Exception ex)
            {
            }

            return View();
        }

        [NoAsyncTimeout]
        [System.Web.Mvc.HttpGet]
        [System.Web.Mvc.Route("{id:int}/SendBillsToGoCardless")]
        public ActionResult SendBillsToGoCardless(int id)
        {
            ViewBag.BillingRunId = id;

            return View();
        }

        [NoAsyncTimeout]
        [System.Web.Mvc.HttpPost]
        [System.Web.Mvc.Route("SendTestBillingRunEmails")]
        public async Task<ActionResult> SendTestBillingRunEmails([Bind] BillingRunSendTestEmailInputModel inputModel)
        {
            await _sendBillEmailsService.SendBillEmailsAsync(inputModel.BillingRunId, string.IsNullOrEmpty(inputModel.EmailAddress) ? null : inputModel.EmailAddress);
            return View();
        }

        [NoAsyncTimeout]
        [System.Web.Mvc.HttpGet]
        [System.Web.Mvc.Route("{id:int}/GoCardlessManualExport")]
        public ActionResult GoCardlessManualExport(int id)
        {
            ViewBag.BillingRunId = id;

            return View();
        }


        //[NoAsyncTimeout]
        //[System.Web.Mvc.HttpGet]
        //[System.Web.Mvc.Route("RefreshPaymentsAndMandates")]
        //public ActionResult RefreshPaymentsAndMandates()
        //{
        //    ViewBag.Environments = new List<GoCardless.Environments>
        //    {
        //        GoCardless.Environments.Sandbox,
        //        GoCardless.Environments.Production
        //    };
        //    return View();
        //}


        [NoAsyncTimeout]
        [System.Web.Mvc.HttpGet]
        [System.Web.Mvc.Route("RefreshPaymentsAndMandates")]
        public async Task<ActionResult> RefreshPaymentsAndMandates()
        {
            await _refreshPaymentStatusService.RefreshAllPaymentStatusAsync(GoCardless.Environments.Production);
            return View("HousesToBeBilled");
        }
    }
}