﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using DividaBill.Models;
using DividaBill.Services.Exceptions;
using DividaBill.Services.Factories;
using DividaBill.Services.Impl;
using DividaBill.Services.Interfaces;
using DividaBill.ViewModels;
using Mvc.JQuery.DataTables;

namespace DividaBill.SysOp.Controllers
{
    public class TenantsController : AdminController
    {
        private readonly IGoCardlessService _goCardlessService;
        private readonly ITenantService _tenantService;
        private readonly IViewModelToEntityMappingFactory<TenantViewModel, User> _tenantViewModelToUserMappingFactory;
        private readonly IEntityToViewModelMappingFactory<User, TenantViewModel> _userToTenantViewModelMappingFactory;

        public TenantsController(ITenantService tenantService, IGoCardlessService goCardlessService,
            IViewModelToEntityMappingFactory<TenantViewModel, User> tenantViewModelToUserMappingFactory,
            IEntityToViewModelMappingFactory<User, TenantViewModel> userToTenantViewModelMappingFactory)
        {
            _userToTenantViewModelMappingFactory = userToTenantViewModelMappingFactory;
            _tenantViewModelToUserMappingFactory = tenantViewModelToUserMappingFactory;
            _goCardlessService = goCardlessService;
            _tenantService = tenantService;
        }

        [HttpPost]
        public DataTablesResult<User> GetTenantsDataTable(DataTablesParam dataTableParam)
        {
            var tenants = _tenantService.GetAllTenantsDecrypted().AsQueryable();
            return DataTablesResult.Create(tenants, dataTableParam, uv => new
            {
            }); //...and return a DataTablesResult
        }

        // GET: Admin/Tenants
        public ActionResult Index()
        {
            return RedirectToActionPermanent(nameof(All));
        }

        // GET: Admin/Tenants/Active
        public ActionResult Active()
        {
            return View("Active");
        }

        // GET: Admin/Tenants/All
        public ActionResult All()
        {
            return View("All");
        }

        // GET: Admin/Tenants/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            try
            {
                var tenant = _tenantService.GetTenantDecrypted(id);
                return View(tenant);
            }
            catch (UserNotFound)
            {
                return HttpNotFound();
            }
        }

        // GET: Admin/Tenants/Get/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            try
            {
                var entity = _tenantService.GetTenantDecrypted(id);
                var userViewModel = _userToTenantViewModelMappingFactory.Map(entity);
                return View(userViewModel);
            }
            catch (UserNotFound)
            {
                return HttpNotFound();
            }
        }

        // POST: Admin/Tenants/Get/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind] TenantViewModel tenant)
        {
            if (ModelState.IsValid)
            {
                var entity = _tenantService.GetTenant(tenant.Id);
                entity = _tenantViewModelToUserMappingFactory.Map(tenant, entity);
                var validationError = await _goCardlessService.AddToGoCardlessAsync(entity);
                if (validationError != null)
                {
                    foreach (var uiError in validationError)
                    {
                        ModelState.AddModelError(uiError.Key, uiError.ErrorMessage);
                    }
                    return View(tenant);
                }
                _tenantService.UpdateAndSave(entity);
                return RedirectToAction("Edit", "Tenants", new {id = tenant.Id});
            }

            return View(tenant);
        }

        // GET: Admin/Tenants/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            try
            {
                var tenant = _tenantService.GetTenantDecrypted(id);
                return View(tenant);
            }
            catch (UserNotFound)
            {
                return HttpNotFound();
            }
        }

        // POST: Admin/Tenants/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            // unitOfWork.UserRepository.Delete(id);

            return RedirectToAction("Index");
        }


        public ActionResult Export(string FileType = "Excel", string Type = "all", int days = 100)
        {
            var tenants = new DataTable("TenantsTable");

            tenants.Columns.Add("Id", typeof (string));
            tenants.Columns.Add("Email", typeof (string));
            tenants.Columns.Add("Title", typeof (string));
            tenants.Columns.Add("FirstName", typeof (string));
            tenants.Columns.Add("LastName", typeof (string));
            tenants.Columns.Add("AccountNumber", typeof (string));
            tenants.Columns.Add("AccountSortCode", typeof (string));
            tenants.Columns.Add("AccountHolder", typeof (string));
            tenants.Columns.Add("AccountSoleHolder", typeof (bool));
            tenants.Columns.Add("DOB", typeof (DateTime));
            tenants.Columns.Add("HomeAddress", typeof (string));
            tenants.Columns.Add("PhoneNumber", typeof (string));
            tenants.Columns.Add("SignupDate", typeof (DateTime));
            tenants.Columns.Add("Housemates", typeof (int));
            tenants.Columns.Add("Address", typeof (string));
            tenants.Columns.Add("Electricity", typeof (bool));
            tenants.Columns.Add("Gas", typeof (bool));
            tenants.Columns.Add("Water", typeof (bool));
            tenants.Columns.Add("RegisteredDate", typeof (DateTime));

            List<User> dbClients;
            var filename = "AllTenants.xlsx";
            switch (Type)
            {
                default:
                    dbClients = _tenantService.GetAllTenantsDecrypted();
                    break;
                case "active":
                    dbClients = _tenantService.GetActiveTenantsDecrypted().ToList();
                    filename = "ActiveTenants.xlsx";
                    break;
            }

            foreach (var tenant in dbClients)
            {
                tenants.Rows.Add(
                    tenant.Id,
                    tenant.Email,
                    tenant.RefTitle,
                    tenant.FirstName,
                    tenant.LastName,
                    tenant.AccountNumber,
                    tenant.AccountSortCode,
                    tenant.AccountHolder,
                    tenant.AccountSoleHolder,
                    tenant.DOB,
                    tenant.HomeAddress,
                    tenant.PhoneNumber,
                    tenant.SignupDate,
                    tenant.House?.Housemates ?? 0,
                    tenant.House?.Address,
                    tenant.House?.HasElectricity,
                    tenant.House?.HasGas,
                    tenant.House?.HasWater,
                    tenant.House?.RegisteredDate
                    );
            }

            var grid = new GridView();
            grid.DataSource = tenants;
            grid.DataBind();

            Response.ClearContent();
            Response.Buffer = true;

            Response.AddHeader("content-disposition", "attachment; filename=" + filename);

            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            var sw = new StringWriter();
            var htw = new HtmlTextWriter(sw);
            grid.RenderControl(htw);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();

            return RedirectToAction("Index");
        }

        [AllowAnonymous]
        public ActionResult TenantAdd(int houseId)
        {
            var tenantViewModel = new SignUpViewModel
            {
                HouseId = houseId
            };
            return PartialView("_TenantAdd", tenantViewModel);
        }
    }
}