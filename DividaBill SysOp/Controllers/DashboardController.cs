﻿using System;
using DividaBill.Services;
using DividaBill.Services.Interfaces;
using System.Linq;
using System.Web.Mvc;
using DividaBill.Services.NewBillingSystem;
using DividaBill.ViewModels.ControllerViewModels.Dashboard;

namespace DividaBill.SysOp.Controllers
{

    public class DashboardController : AdminController
    {
        private readonly IHouseService _houseService;
        private readonly IReportingService _reportingService;

        public DashboardController(IHouseService houseService)
        {
            _houseService = houseService;
        }

        // GET: Admin/Dashboard
        public ActionResult Index()
        {
            var vm = new DashboardIndexViewModel
            {
               RegisteredHousesToday = _houseService.GetTodayHouses().Count(),
               ActiveHouses = _houseService.GetActiveHouses().Count(),
               TotalHouses = _houseService.GetAllHouses().Count(),
               YesterdayHouses = _houseService.GetYesterdayHouses().Count(),
               LastWeekHouses = _houseService.GetLastWeekHouses().Count(),
            };

            return View(vm);
        }


        public ActionResult Logs()
        {
            return View();
        }

        public ActionResult Sheets()
        {
            return View();
        }
    }
}
