using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using DividaBill.AppConstants;
using DividaBill.DAL.Interfaces;
using DividaBill.DAL.Unity;
using DividaBill.Models;
using DividaBill.Services.Impl;
using DividaBill.Services.Interfaces;
using DividaBill.Services.NewBillingSystem;
using DividaBill.ViewModels.API;
using GoCardless_API.api;

namespace DividaBill.SysOp.Controllers.api
{
    [RoutePrefix("api/BankMandate")]
    public class BankMandateApiController : ApiController
    {
        private readonly IBankMandateService _bankMandateService;
        private readonly IGoCardlessBankMandateService _goCardlessBankMandateService;
        private readonly IGoCardlessService _goCardlessService;
        private readonly IMultiThreadingService _multiThreadingService;
        private readonly ITenantService _tenantService;
        private readonly IUnitOfWork _unitOfWork;

        public BankMandateApiController(IUnitOfWork unitOfWork,
            IGoCardlessService goCardlessService, IBankMandateService bankMandateService, ITenantService tenantService, IMultiThreadingService multiThreadingService,
            IGoCardlessBankMandateService goCardlessBankMandateService)
        {
            _unitOfWork = unitOfWork;
            _goCardlessBankMandateService = goCardlessBankMandateService;
            _tenantService = tenantService;
            _bankMandateService = bankMandateService;
            _multiThreadingService = multiThreadingService;
            _goCardlessService = goCardlessService;
        }

        [HttpPost]
        [ResponseType(typeof (List<string>))]
        [Route("UpdateMandatesInfo")]
        public async Task<List<string>> UpdateMandatesInfo([FromBody] List<string> userIDs)
        {
            if (userIDs != null)
            {
                foreach (var tenant in userIDs.Select(userId => _tenantService.GetTenant(userId)))
                {
                    await _goCardlessBankMandateService.UpdateAllExistingBankMandatesFromGc(tenant);
                }
                _unitOfWork.Save();
            }
            return userIDs;
        }

        [HttpPost]
        [Route("UpdateAllMandatesInfo")]
        [ResponseType(typeof (UpdateAllMandatesApiOutputViewModel))]
        public async Task<UpdateAllMandatesApiOutputViewModel> UpdateAllMandatesInfo()
        {
            var tenants = _tenantService.GetAllTenants().ToList();
            var result = await _multiThreadingService.RunTaskInParallelAsync<User, GoCardlessApiClient>(10, tenants,
                async (tenant, unitOfWork, goCardlessApi, bulkOperationResult) =>
                {
                    try
                    {
                        var asyncExecutionParam = new AsyncExecutionParam(unitOfWork);
                        await _goCardlessBankMandateService.UpdateAllExistingBankMandatesFromGc(tenant, asyncExecutionParam, goCardlessApi);
                        unitOfWork.Save();
                        unitOfWork.Dispose();
                    }
                    catch (Exception exception)
                    {
                        if (exception is DbEntityValidationException)
                        {
                            //foreach (var eve in (exception as DbEntityValidationException).EntityValidationErrors)
                            //{
                            //    Debug.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors: ", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                            //    foreach (var ve in eve.ValidationErrors)
                            //    {
                            //        Debug.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            //            ve.PropertyName, ve.ErrorMessage);
                            //    }
                            //}
                        }
                        throw;
                    }
                });

            return new UpdateAllMandatesApiOutputViewModel
            {
                Failed = result.Failed.Count,
                Successful = result.Successful.Count
            };
        }

        [HttpPost]
        [Route("ForceRegenerateGcMandate")]
        [ResponseType(typeof (ForceRegenerateGcMandateApiOutputViewModel))]
        public async Task<ForceRegenerateGcMandateApiOutputViewModel> ForceRegenerateGcMandate([FromBody] List<string> userIDs)
        {
            var success = new List<ForceRegenerateGcMandateApiOutputViewElement>();
            var failed = new List<ForceRegenerateGcMandateApiOutputViewElement>();
            if (userIDs != null)
            {
                foreach (var tenant in userIDs.Select(userId => _tenantService.GetTenant(userId)))
                {
                    var validationError = await _goCardlessService.AddToGoCardlessAsync(tenant);
                    if (validationError != null)
                    {
                        failed.AddRange(validationError.Select(uiError => new ForceRegenerateGcMandateApiOutputViewElement
                        {
                            Key = $"{tenant.Id}-[{uiError.Key}]", Value = uiError.ErrorMessage
                        }));
                    }
                    else
                    {
                        var gcMandate = await _goCardlessBankMandateService.GetExistingOrCreateGcMandate(tenant);

                        if (gcMandate.status == GoCardlessMandateStatus.Failed
                            || gcMandate.status == GoCardlessMandateStatus.Expired
                            || gcMandate.status == GoCardlessMandateStatus.Cancelled
                            || gcMandate.status == GoCardlessMandateStatus.Submitted
                            )
                        {
                            var newBankMandate = await _goCardlessBankMandateService.CreateAndInsertBankMandateFromGc(tenant);
                            _bankMandateService.LinkOrUnlinkBankMandateToUser(tenant, newBankMandate);
                        }
                        _unitOfWork.Save();

                        success.Add(new ForceRegenerateGcMandateApiOutputViewElement
                        {
                            Key = tenant.Id,
                            Value = string.Empty
                        });
                    }
                }
            }
            return new ForceRegenerateGcMandateApiOutputViewModel
            {
                Failed = failed,
                Successful = success
            };
        }
    }
}