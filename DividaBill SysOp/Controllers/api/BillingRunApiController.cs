﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using AutoMapper.QueryableExtensions;
using DividaBill.Extensions;
using DividaBill.Library.DataTable;
using DividaBill.Security.Abstractions;
using DividaBill.Services.Exceptions;
using DividaBill.Services.Factories;
using DividaBill.Services.Interfaces;
using DividaBill.Services.NewBillingSystem;
using DividaBill.Services.NewBillingSystem.Impl;
using DividaBill.Services.NewBillingSystem.Model;
using DividaBill.ViewModels.DataTables;
using Mvc.JQuery.DataTables;
using Mvc.JQuery.DataTables.Models;
using WebGrease.Css.Extensions;

namespace DividaBill.SysOp.Controllers.api
{
    [RoutePrefix("api/billingrun")]
    public class BillingRunApiController : ApiController
    {
        private readonly IAccountService _bankAccountService;
        private readonly IBankMandateService _bankMandateService;
        private readonly IUserGcInfoUpdateViewModelMappingFactory _bankMandateToMandateForUpdateViewModelMappingFactory;
        private readonly IBillingRunService _billingRunService;
        private readonly IEncryptionService _encryptionService;
        private readonly IHouseService _houseService;
        private readonly IMappingFactory<HousesToBeBilledModel, HousesToBeBilledViewModel> _housesToBeBilledModelToHousesToBeBilledViewModelMappingFactory;
        private readonly ITenantService _tenantService;

        public BillingRunApiController(IBillingRunService billingRunService, IHouseService houseService,
            IBankMandateService bankMandateService,
            IMappingFactory<HousesToBeBilledModel, HousesToBeBilledViewModel> housesToBeBilledModelToHousesToBeBilledViewModelMappingFactory,
            IUserGcInfoUpdateViewModelMappingFactory bankMandateToMandateForUpdateViewModelMappingFactory,
            ITenantService tenantService, IAccountService bankAccountService,
            IEncryptionService encryptionService
            )
        {
            _encryptionService = encryptionService;
            _bankAccountService = bankAccountService;
            _tenantService = tenantService;
            _bankMandateToMandateForUpdateViewModelMappingFactory = bankMandateToMandateForUpdateViewModelMappingFactory;
            _bankMandateService = bankMandateService;
            _housesToBeBilledModelToHousesToBeBilledViewModelMappingFactory = housesToBeBilledModelToHousesToBeBilledViewModelMappingFactory;
            _houseService = houseService;
            _billingRunService = billingRunService;
        }

        [Route("SetActiveHousesToBeBilledInBillingRun/{id:int}")]
        [HttpGet]
        public async Task<List<int>> SetActiveHousesToBeBilledInBillingRun([FromUri] int id)
        {
            var activeHouseIds = _houseService.GetActiveHouses().Select(s => s.ID).ToList();
            var result = await _billingRunService.SetHousesToBeBilledInBillingRunAsync(id, activeHouseIds);
            return result;
        }

        #region DataTables

        [Route("HousesToBeBilled/{id:int}")]
        [HttpPost]
        [ResponseType(typeof (DataTablesResponseData))]
        public DataTablesResponseData GetActiveHousesDataTable([FromUri] int id, [ModelBinder(typeof (DataTablesModelBinder))] DataTablesParam dataTableParam)
        {
            var result =
                _billingRunService.GetAllHousesToBeBilled(id)
                    .AsEnumerable()
                    .Select(x => _housesToBeBilledModelToHousesToBeBilledViewModelMappingFactory.Map(x))
                    .AsQueryable().SetComparer(StringComparison.CurrentCultureIgnoreCase);

            var filteredResult = dataTableParam.GetDataTablesResponse(result);

            var convertedData = filteredResult.ConvertResults<HousesToBeBilledViewModel>();

            return filteredResult.Create(convertedData, uv => new
            {
                ID = $"<a href=\"/Houses/Details/{uv.ID}\" class=\"btn btn - xs default btn- editable details\" id=\"details-{uv.ID}\">{uv.ID}</a>",
                Address = $"<a href=\"/Houses/Edit/{uv.ID}\" class=\"btn btn - xs default btn- editable edit\" id=\"edit-{uv.ID}\">{uv.Address}</a>",
                Options = $"<input type=\"checkbox\" name=\"editCheckBox\" value=\"{uv.ID}\">"
            }).Data;
        }

        [Route("MandatesToUpdate")]
        [HttpPost]
        [ResponseType(typeof (DataTablesResponseData))]
        public DataTablesResponseData GetMandatesToUpdate([ModelBinder(typeof (DataTablesModelBinder))] DataTablesParam dataTableParam)
        {
            var result =
                _tenantService.GetAllTenants()
                    .AsEnumerable()
                    .Select(tenant => _bankMandateToMandateForUpdateViewModelMappingFactory.Map(tenant))
                    .AsQueryable().SetComparer(StringComparison.CurrentCultureIgnoreCase);

            var filteredResult = dataTableParam.GetDataTablesResponse(result);

            var convertedData = filteredResult.ConvertResults<UserGcInfoUpdateViewModel>();

            foreach (var item in convertedData)
            {
                try
                {
                    var bankMandate = _bankMandateService.GetById(item.MandateId);
                    item.MandateStatus = bankMandate.status;
                    item.MandateLastUpdatedTime = bankMandate.LastModified;
                }
                catch (BankMandateNotFound)
                {
                }
            }

            return filteredResult.Create(convertedData, uv => new
            {
                UserId = $"<a href=\"/Tenants/Edit/{uv.UserId}\" class=\"btn btn - xs default btn- editable edit\" id=\"edit-{uv.UserId}\">{uv.UserId}</a>",
                UserFullName = $"{uv.UserRefTitle} {_encryptionService.Decrypt(uv.UserFirstName)} {_encryptionService.Decrypt(uv.UserLastName)}",
                BankAccountDetails = $"{_encryptionService.Decrypt(uv.BankAccountHolder)}<br/>{_encryptionService.Decrypt(uv.BankAccountNumber)} - {_encryptionService.Decrypt(uv.BankAccountSortCode)}",
                Options = $"<input type=\"checkbox\" name=\"editCheckBox\" value=\"{uv.UserId}\">"
            }).Data;
        }

        [Route("GetHousesThatHaventBeenBilled/{id:int}")]
        [HttpPost]
        [ResponseType(typeof (DataTablesResponseData))]
        public DataTablesResponseData GetHousesThatHaventBeenBilledDataTable([FromUri] int id, [ModelBinder(typeof (DataTablesModelBinder))] DataTablesParam dataTableParam)
        {
            var result = _billingRunService.GetHousesThatHaventBeenBilled(id);
            var data = result.Project().To<HouseViewModel>();
            //NewRelic.Api.Agent.NewRelic.
            return DataTablesResult.Create(data, dataTableParam, uv => new
            {
                Electricity = uv.Electricity ? "Yes" : "No",
                Gas = uv.Gas ? "Yes" : "No",
                SkyTV = uv.SkyTV ? "Yes" : "No",
                Broadband = uv.Broadband ? "Yes" : "No",
                LandlinePhone = uv.LandlinePhone ? "Yes" : "No",
                Water = uv.Water ? "Yes" : "No",
                TVLicense = uv.TVLicense ? "Yes" : "No",
                Options = "<a href=\"/Houses/Edit/" + uv.ID + "\" class=\"btn btn - xs default btn- editable edit\" id=\"edit-" + uv.ID + "\">Edit</a>" +
                          "<a href=\"/Houses/Details/" + uv.ID + "\" class=\"btn btn - xs default btn- editable details\" id=\"details-" + uv.ID + "\">Details</a>" +
                          ""
            }).Data; //...and return a DataTablesResult
        }

        [Route("GetHousesThatHaveBeenBilled/{id:int}")]
        [HttpPost]
        [ResponseType(typeof (DataTablesResponseData))]
        public DataTablesResponseData GetHousesThatHaveBeenBilledDataTable([FromUri] int id, [ModelBinder(typeof (DataTablesModelBinder))] DataTablesParam dataTableParam)
        {
            var result = _billingRunService.GetHousesThatHaveBeenBilled(id);
            var data = result.Project().To<HouseViewModel>();
            //NewRelic.Api.Agent.NewRelic.
            return DataTablesResult.Create(data, dataTableParam, uv => new
            {
                Electricity = uv.Electricity ? "Yes" : "No",
                Gas = uv.Gas ? "Yes" : "No",
                SkyTV = uv.SkyTV ? "Yes" : "No",
                Broadband = uv.Broadband ? "Yes" : "No",
                LandlinePhone = uv.LandlinePhone ? "Yes" : "No",
                Water = uv.Water ? "Yes" : "No",
                TVLicense = uv.TVLicense ? "Yes" : "No",
                Options = "<a href=\"/Houses/Edit/" + uv.ID + "\" class=\"btn btn - xs default btn- editable edit\" id=\"edit-" + uv.ID + "\">Edit</a>" +
                          "<a href=\"/Houses/Details/" + uv.ID + "\" class=\"btn btn - xs default btn- editable details\" id=\"details-" + uv.ID + "\">Details</a>" +
                          ""
            }).Data; //...and return a DataTablesResult
        }

        [Route("GetGoCardlessManualExport/{id:int}")]
        [HttpPost]
        [ResponseType(typeof (DataTablesResponseData))]
        public DataTablesResponseData GetGoCardlessManualExport([FromUri] int id, [ModelBinder(typeof (DataTablesModelBinder))] DataTablesParam dataTableParam)
        {
            List<ManualMandateExportOutputModel> data = null;
            try
            {
                data = _billingRunService.GetGoCardlessManualExport(id);
            }
            catch (Exception ex)
            {
            }

            //NewRelic.Api.Agent.NewRelic.
            return DataTablesResult.Create(data.AsQueryable(), dataTableParam, uv => new
            {
            }).Data; //...and return a DataTablesResult
        }

        //[Route("GetGoCardlessMandates/{id:int}")]
        //[HttpPost]
        //[ResponseType(typeof(DataTablesResponseData))]
        //public DataTablesResponseData GetGoCardlessMandates([FromUri]int id, [ModelBinder(typeof(DataTablesModelBinder))]DataTablesParam dataTableParam)
        //{
        //    var result = _billingRunService.(id);
        //    var data = result.Project().To<HouseViewModel>();
        //    //NewRelic.Api.Agent.NewRelic.
        //    return DataTablesResult.Create(data, dataTableParam, uv => new
        //    {
        //        Electricity = uv.Electricity ? "Yes" : "No",
        //        Gas = uv.Gas ? "Yes" : "No",
        //        SkyTV = uv.SkyTV ? "Yes" : "No",
        //        Broadband = uv.Broadband ? "Yes" : "No",
        //        LandlinePhone = uv.LandlinePhone ? "Yes" : "No",
        //        Water = uv.Water ? "Yes" : "No",
        //        TVLicense = uv.TVLicense ? "Yes" : "No",
        //        Options = "<a href=\"/Houses/Edit/" + uv.ID + "\" class=\"btn btn - xs default btn- editable edit\" id=\"edit-" + uv.ID + "\">Edit</a>" +
        //                  "<a href=\"/Houses/Details/" + uv.ID + "\" class=\"btn btn - xs default btn- editable details\" id=\"details-" + uv.ID + "\">Details</a>" +
        //                  "",
        //    }).Data;  //...and return a DataTablesResult
        //}

        #endregion
    }
}