using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using DividaBill.Models;
using DividaBill.Services.Interfaces;
using DividaBill.ViewModels;

namespace DividaBill.SysOp.Controllers.api
{
    [RoutePrefix("api/BankAccount")]
    public class BankAccountApiController : ApiController
    {
        private readonly IBankAccountRegistrationService _bankAccountRegistrationService;

        public BankAccountApiController(IBankAccountRegistrationService bankAccountRegistrationService)
        {
            _bankAccountRegistrationService = bankAccountRegistrationService;
        }

        [HttpGet]
        [Route("ValidateBankAccount/{bankNumber}/{sortCode}/{accountName}")]
        public async Task<List<UIError>> ValidateBankAccount([FromUri] string bankNumber, string sortCode, string accountName)
        {
            var bankAccountDetails = new BankAccountDetails
            {
                AccountSortCode = sortCode,
                AccountNumber = bankNumber,
                AccountName = accountName
            };
            return await _bankAccountRegistrationService.ValidateBankAccountDetails(bankAccountDetails);
        }
    }
}