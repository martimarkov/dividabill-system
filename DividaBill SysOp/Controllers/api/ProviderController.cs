﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using DividaBill.Models;
using DividaBill.Services.Abstraction;
using DividaBill.Services.Exceptions;
using DividaBill.Services.Factories;
using DividaBill.ViewModels.API;

namespace DividaBill.SysOp.Controllers.api
{
    [RoutePrefix("api/providers")]
    public class ProviderController : ApiController
    {
        private readonly IProviderService _providerService;
        private readonly IEntityToApiOutputViewModelMappingFactory<Provider, ProviderApiOutputViewModel> _factory;

        public ProviderController(IProviderService providerService, IEntityToApiOutputViewModelMappingFactory<Provider, ProviderApiOutputViewModel> factory)
        {
            _factory = factory;
            _providerService = providerService;
        }

        [HttpGet]
        [Route("GetByServiceId/{id:int}")]
        [ResponseType(typeof (List<ProviderApiOutputViewModel>))]
        public IHttpActionResult GetByServiceId(int id)
        {
            try
            {
                var providers = _providerService.GetProvidersByServiceTypeId(id);
                var convertedValues = _factory.Map(providers).ToList();
                return Ok(convertedValues);
            }
            catch (ServiceNotFound)
            {
                return NotFound();
            }
        }
    }
}