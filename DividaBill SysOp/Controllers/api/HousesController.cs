﻿using System;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DividaBill.Extensions;
using DividaBill.Library.DataTable;
using DividaBill.Models;
using DividaBill.Services.Factories;
using DividaBill.Services.Interfaces;
using DividaBill.ViewModels.DataTables;
using Mvc.JQuery.DataTables;
using Mvc.JQuery.DataTables.Models;

namespace DividaBill.SysOp.Controllers.api
{
    [RoutePrefix("api/houses")]
    public class HousesController : ApiController
    {
        private readonly IEntityToViewModelMappingFactory<HouseModel, HouseViewModel> _houseModelToHouseViewModelMappingFactory;
        private readonly IHouseService _houseService;

        public HousesController(IHouseService houseService, IEntityToViewModelMappingFactory<HouseModel, HouseViewModel> houseModelToHouseViewModelMappingFactory)
        {
            _houseService = houseService;
            _houseModelToHouseViewModelMappingFactory = houseModelToHouseViewModelMappingFactory;
        }

        [Route("ActiveHousesAsDataTable")]
        [HttpPost]
        [ResponseType(typeof (DataTablesResponseData))]
        public DataTablesResponseData GetActiveHousesDataTable([ModelBinder(typeof (DataTablesModelBinder))] DataTablesParam dataTableParam)
        {
            var houses = _houseService.GetActiveHouses().AsEnumerable().Select(x => _houseModelToHouseViewModelMappingFactory.Map(x)).AsQueryable().SetComparer(StringComparison.CurrentCultureIgnoreCase);
            var filteredResult = dataTableParam.GetDataTablesResponse(houses);
            var convertedData = filteredResult.ConvertResults<HouseViewModel>();

            return filteredResult.Create(convertedData, uv => new
            {
                Electricity = uv.Electricity ? "Yes" : "No",
                Gas = uv.Gas ? "Yes" : "No",
                SkyTV = uv.SkyTV ? "Yes" : "No",
                Broadband = uv.Broadband ? "Yes" : "No",
                LandlinePhone = uv.LandlinePhone ? "Yes" : "No",
                Water = uv.Water ? "Yes" : "No",
                TVLicense = uv.TVLicense ? "Yes" : "No",
                Options = "<a href=\"/Houses/Get/" + uv.ID + "\" class=\"btn btn - xs default btn- editable edit\" id=\"edit-" + uv.ID + "\">Get</a>" +
                          "<a href=\"/Houses/Details/" + uv.ID + "\" class=\"btn btn - xs default btn- editable details\" id=\"details-" + uv.ID + "\">Details</a>" +
                          ""
            }).Data;
        }


        [Route("AllHousesAsDataTable")]
        [HttpPost]
        [ResponseType(typeof (DataTablesResponseData))]
        public DataTablesResponseData GetAllHousesDataTable([ModelBinder(typeof (DataTablesModelBinder))] DataTablesParam dataTableParam)
        {
            var houses = _houseService.GetAllHouses().AsEnumerable().Select(x => _houseModelToHouseViewModelMappingFactory.Map(x)).AsQueryable().SetComparer(StringComparison.CurrentCultureIgnoreCase);
            var filteredResult = dataTableParam.GetDataTablesResponse(houses);
            var convertedData = filteredResult.ConvertResults<HouseViewModel>();

            return filteredResult.Create(convertedData, uv => new
            {
                Electricity = uv.Electricity ? "Yes" : "No",
                Gas = uv.Gas ? "Yes" : uv.Gas.ToString(),
                SkyTV = uv.SkyTV ? "Yes" : "No",
                Broadband = uv.Broadband ? "Yes" : "No",
                LandlinePhone = uv.LandlinePhone ? "Yes" : "No",
                Water = uv.Water ? "Yes" : "No",
                TVLicense = uv.TVLicense ? "Yes" : "No",
                Options = "<a href=\"/Houses/Get/" + uv.ID + "\" class=\"btn btn - xs default btn- editable edit\" id=\"edit-" + uv.ID + "\">Get</a>" +
                          "<a href=\"/Houses/Details/" + uv.ID + "\" class=\"btn btn - xs default btn- editable details\" id=\"details-" + uv.ID + "\">Details</a>" +
                          ""
            }).Data;
        }


        [Route("LastWeekHousesAsDataTable")]
        [HttpPost]
        [ResponseType(typeof (DataTablesResponseData))]
        public DataTablesResponseData GetLastWeekHousesDataTable([ModelBinder(typeof (DataTablesModelBinder))] DataTablesParam dataTableParam)
        {
            var houses = _houseService.GetLastWeekHouses().AsEnumerable().Select(x => _houseModelToHouseViewModelMappingFactory.Map(x)).AsQueryable().SetComparer(StringComparison.CurrentCultureIgnoreCase);
            var filteredResult = dataTableParam.GetDataTablesResponse(houses);
            var convertedData = filteredResult.ConvertResults<HouseViewModel>();

            return filteredResult.Create(convertedData, uv => new
            {
                Electricity = uv.Electricity ? "Yes" : "No",
                Gas = uv.Gas ? "Yes" : "No",
                SkyTV = uv.SkyTV ? "Yes" : "No",
                Broadband = uv.Broadband ? "Yes" : "No",
                LandlinePhone = uv.LandlinePhone ? "Yes" : "No",
                Water = uv.Water ? "Yes" : "No",
                TVLicense = uv.TVLicense ? "Yes" : "No",
                Options = "<a href=\"/Houses/Get/" + uv.ID + "\" class=\"btn btn - xs default btn- editable edit\" id=\"edit-" + uv.ID + "\">Get</a>" +
                          "<a href=\"/Houses/Details/" + uv.ID + "\" class=\"btn btn - xs default btn- editable details\" id=\"details-" + uv.ID + "\">Details</a>" +
                          ""
            }).Data;
        }

        [Route("TodayHousesAsDataTable")]
        [HttpPost]
        [ResponseType(typeof (DataTablesResponseData))]
        public DataTablesResponseData GetTodayHousesDataTable([ModelBinder(typeof (DataTablesModelBinder))] DataTablesParam dataTableParam)
        {
            var houses = _houseService.GetTodayHouses().AsEnumerable().Select(x => _houseModelToHouseViewModelMappingFactory.Map(x)).AsQueryable().SetComparer(StringComparison.CurrentCultureIgnoreCase);
            var filteredResult = dataTableParam.GetDataTablesResponse(houses);
            var convertedData = filteredResult.ConvertResults<HouseViewModel>();

            return filteredResult.Create(convertedData, uv => new
            {
                Electricity = uv.Electricity ? "Yes" : "No",
                Gas = uv.Gas ? "Yes" : "No",
                SkyTV = uv.SkyTV ? "Yes" : "No",
                Broadband = uv.Broadband ? "Yes" : "No",
                LandlinePhone = uv.LandlinePhone ? "Yes" : "No",
                Water = uv.Water ? "Yes" : "No",
                TVLicense = uv.TVLicense ? "Yes" : "No",
                Options = "<a href=\"/Houses/Get/" + uv.ID + "\" class=\"btn btn - xs default btn- editable edit\" id=\"edit-" + uv.ID + "\">Get</a>" +
                          "<a href=\"/Houses/Details/" + uv.ID + "\" class=\"btn btn - xs default btn- editable details\" id=\"details-" + uv.ID + "\">Details</a>" +
                          ""
            }).Data;
        }

        [Route("YesterdayHousesAsDataTable")]
        [HttpPost]
        [ResponseType(typeof (DataTablesResponseData))]
        public DataTablesResponseData GetYesterdayHousesDataTable([ModelBinder(typeof (DataTablesModelBinder))] DataTablesParam dataTableParam)
        {
            var houses = _houseService.GetYesterdayHouses().AsEnumerable().Select(x => _houseModelToHouseViewModelMappingFactory.Map(x)).AsQueryable().SetComparer(StringComparison.CurrentCultureIgnoreCase);
            var filteredResult = dataTableParam.GetDataTablesResponse(houses);
            var convertedData = filteredResult.ConvertResults<HouseViewModel>();

            return filteredResult.Create(convertedData, uv => new
            {
                Electricity = uv.Electricity ? "Yes" : "No",
                Gas = uv.Gas ? "Yes" : "No",
                SkyTV = uv.SkyTV ? "Yes" : "No",
                Broadband = uv.Broadband ? "Yes" : "No",
                LandlinePhone = uv.LandlinePhone ? "Yes" : "No",
                Water = uv.Water ? "Yes" : "No",
                TVLicense = uv.TVLicense ? "Yes" : "No",
                Options = "<a href=\"/Houses/Get/" + uv.ID + "\" class=\"btn btn - xs default btn- editable edit\" id=\"edit-" + uv.ID + "\">Get</a>" +
                          "<a href=\"/Houses/Details/" + uv.ID + "\" class=\"btn btn - xs default btn- editable details\" id=\"details-" + uv.ID + "\">Details</a>" +
                          ""
            }).Data;
        }
    }
}