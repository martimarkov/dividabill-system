using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using DividaBill.Services.NewBillingSystem;

namespace DividaBill.SysOp.Controllers.api
{
    [RoutePrefix("api/BillingQueue")]
    public class BillingQueueApiController : ApiController
    {
        private readonly IHousesToBeBilledService _housesToBeBilledService;

        public BillingQueueApiController(IHousesToBeBilledService housesToBeBilledService)
        {
            _housesToBeBilledService = housesToBeBilledService;
        }

        [Route("AddHousesToBeBilled/{billingRunId:int}")]
        [HttpPost]
        [ResponseType(typeof (List<int>))]
        public async Task<List<int>> AddHousesToBeBilled([FromUri] int billingRunId, [FromBody] List<int> houseIds)
        {
            await _housesToBeBilledService.SetHousesToBeBilledInBillingRunAsync(billingRunId, houseIds);

            return houseIds;
        }

        [Route("DeleteHousesToBeBilled/{billingRunId:int}")]
        [HttpPost]
        [ResponseType(typeof(List<int>))]
        public async Task<List<int>> DeleteHousesToBeBilled([FromUri] int billingRunId, [FromBody] List<int> houseIds)
        {
            var result = await _housesToBeBilledService.DeleteHousesToBeBilledFromBillingRunAsync(billingRunId, houseIds);

            return result;
        }
    }
}