﻿using DividaBill.Services.Interfaces;
using DividaBill.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace DividaBill.SysOp.Controllers
{
    [RoutePrefix("api/logs")]
    public class LogsController : ApiController
    {
        readonly ILoggingService logService;

        public LogsController(ILoggingService logService)
        {
            this.logService = logService;
        }


        [HttpGet, Route("list")]
        public string[] ListFormatters()
        {
            return logService.GetFormatters();
        }

        [HttpGet, Route("last")]
        public LogResponseViewModel GetLastLines(int feedId, int nItems = 50)
        {
            return logService.GetLastLines(feedId, nItems);
        }

        [HttpGet, Route("previous")]
        public LogResponseViewModel GetPreviousLines(int feedId, int upToRev, int nItems = 50)
        {
            return logService.GetLastLines(feedId, nItems, upToRev);
        }

        [HttpGet, Route("newest")]
        public LogResponseViewModel GetNewestLines(int feedId, int lastSeenId)
        {
            return logService.GetNewestLines(feedId, lastSeenId);
        }
    }
}