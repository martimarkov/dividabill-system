﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;
using AutoMapper;
using DividaBill.Models;
using DividaBill.Services.Abstraction;
using DividaBill.Services.Exceptions;
using DividaBill.Services.Factories;
using DividaBill.Services.Factories.Impl;
using DividaBill.Services.Interfaces;
using DividaBill.ViewModels;
using DividaBill.ViewModels.API;

namespace DividaBill.SysOp.Controllers.api
{
    [RoutePrefix("api/contracts")]
    public class ContractController : ApiController
    {
        private readonly IViewModelToEntityMappingFactory<ContractRemoveViewModel, Contract> _contractRemoveViewModelToContractMappingFactory;
        private readonly IContractService _contractService;
        private readonly IMappingFactory<Contract, ContractApiViewModel> _contractToContractApiViewModelMappingFactory;
        private readonly IMappingFactory<ContractViewModel, Contract> _contractViewModelToContractMappingFactory;
        private readonly IMappingFactory<ContractViewModel, ContractRequest> _contractViewModelToContractRequestMappingFactory;
        private readonly IHouseService _houseService;
        private readonly IProviderService _providerService;

        public ContractController(IHouseService houseService, IContractService contractService, IProviderService providerService, IViewModelToEntityMappingFactory<ContractRemoveViewModel, Contract> contractRemoveViewModelToContractMappingFactory, IMappingEngine mappingEngine)
        {
            _contractToContractApiViewModelMappingFactory = new AutoMapperFactory<Contract, ContractApiViewModel>(mappingEngine);
            _contractViewModelToContractRequestMappingFactory = new AutoMapperFactory<ContractViewModel, ContractRequest>(mappingEngine);
            _contractViewModelToContractMappingFactory = new AutoMapperFactory<ContractViewModel, Contract>(mappingEngine);
            _contractRemoveViewModelToContractMappingFactory = contractRemoveViewModelToContractMappingFactory;
            _providerService = providerService;
            _contractService = contractService;
            _houseService = houseService;
        }

        // GET: api/contracts/5
        [HttpGet]
        [Route("{id:int}")]
        [ResponseType(typeof (ContractViewModel))]
        public IHttpActionResult Get(int id)
        {
            try
            {
                _contractService.GetContract(id);
                return Ok();
            }
            catch (ContractNotFound)
            {
                return NotFound();
            }
        }


        // GET: api/contracts/GetByHouseId/8
        [HttpGet]
        [Route("GetByHouseId/{id:int}")]
        [ResponseType(typeof (List<ContractApiViewModel>))]
        public IHttpActionResult GetByHouseId(int id)
        {
            try
            {
                var contracts = _houseService.GetContractsForHouse(id).ToList();
                var contractViewModels = contracts.Select(item => _contractToContractApiViewModelMappingFactory.Map(item)).ToList();

                return Ok(contractViewModels);
            }
            catch (HouseNotFoundException)
            {
                return NotFound();
            }
        }

        [Route("")]
        [HttpPost]
        [ValidateModelState]
        [ResponseType(typeof (ContractViewModel))]
        // POST: api/contracts
        public IHttpActionResult AddContract([FromBody] ContractViewModel contract)
        {
            try
            {
                FillInMissingTenantData(contract);
                var contractRequest = _contractViewModelToContractRequestMappingFactory.Map(contract);

                contractRequest.Provider = _providerService.GetProvider(contract.Provider_ID.Value);
                contractRequest.Service = ServiceType.GetById(contract.Service);
                _contractService.AddContract(contractRequest);
                return Ok();
            }
            catch (ContractNotFound)
            {
                return NotFound();
            }
            catch (InvalidOperationException ex)
            {
                return new BadRequestErrorMessageResult(ex.Message, this);
            }
        }

        [Route("{id:int}")]
        [HttpPut]
        [ValidateModelState]
        // PUT: api/contracts/5
        public IHttpActionResult EditContract(int id, [FromBody] ContractViewModel contract)
        {
            try
            {
                FillInMissingTenantData(contract);

                var oldContract = _contractService.GetContract(id);
                var newContract = _contractViewModelToContractMappingFactory.Map(contract, oldContract);

                _contractService.EditContract(newContract);
                return Ok(id);
            }
            catch (ContractNotFound)
            {
                return NotFound();
            }
        }

        // DELETE: api/contracts/5
        [Route("{id:int}")]
        [HttpDelete]
        public IHttpActionResult Remove([FromBody] ContractRemoveViewModel contract)
        {
            try
            {
                var contractOld = _contractService.GetContract(contract.ID);
                var contractNew = _contractRemoveViewModelToContractMappingFactory.Map(contract, contractOld);

                _contractService.EditContract(contractNew);

                return Ok(contract.ID);
            }
            catch (ContractNotFound)
            {
                return NotFound();
            }
        }

        /// <summary>
        ///     Adds TriggeringTenant to a viewmodel.
        /// </summary>
        /// <param name="contract"></param>
        private void FillInMissingTenantData(ContractViewModel contract)
        {
            if (contract.RequestedTenantId == null)
            {
                var tenant = _houseService.GetHouse(contract.House_ID).Tenants.First();
                contract.RequestedTenantId = tenant.Id;
            }
            if (contract.TriggeringTenant == null)
            {
                var tenant = _houseService.GetHouse(contract.House_ID).Tenants.FirstOrDefault(x => x.Id == contract.RequestedTenantId);
                if (tenant == null)
                    tenant = _houseService.GetHouse(contract.House_ID).Tenants.First();
                contract.TriggeringTenant = tenant;
                contract.RequestedTenantId = tenant.Id;
            }
        }
    }
}