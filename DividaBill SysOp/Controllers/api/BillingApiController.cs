﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using DividaBill.DAL;
using DividaBill.DAL.Interfaces;
using DividaBill.Security.Utilities;
using DividaBill.Services.Impl;
using DividaBill.Services.Interfaces;
using DividaBill.Services.NewBillingSystem;
using DividaBill.Services.NewBillingSystem.Impl;
using DividaBill_Library;
using GoCardless_API;

namespace DividaBill.SysOp.Controllers.api
{
    [RoutePrefix("api/billing")]
    public class BillingApiController : ApiController
    {
        private readonly ISendBillsToGoCardlessService _sendBillsToGoCardlessService;
        private IBillingService _billingService;

        public BillingApiController(ISendBillsToGoCardlessService sendBillsToGoCardlessService, IBillingService billingService)
        {
            _sendBillsToGoCardlessService = sendBillsToGoCardlessService;
            _billingService = billingService;
            //_unitOfWork = new UnitOfWork();
            //var billingQueueTransactionLogService = new BillingQueueTransactionLogService(_unitOfWork);
            //var tenantEncryptionService = new TenantEncryptionService(new AesEncryptionService(), new AddressEncryptionService(new AesEncryptionService()));
            //var hardCodedEstimatedUtilityPricesService = new HardCodedEstimatedUtilityPricesService();
            //var calculateEstimatesPricesService = new CalculateEstimatesPricesService(hardCodedEstimatedUtilityPricesService);
            //var priceService = new PriceService(_unitOfWork, calculateEstimatesPricesService, hardCodedEstimatedUtilityPricesService);
            //var billingQueueService = new BillingQueueService(_unitOfWork, billingQueueTransactionLogService);
            //var housesToBeBilledService = new HousesToBeBilledService(_unitOfWork,
            //    new HouseToBeBilledManager(billingQueueService), new HouseService(_unitOfWork, new ContractService(_unitOfWork), tenantEncryptionService));
            //var billPaymentsService = new BillPaymentsService(_unitOfWork, billingQueueTransactionLogService);
            //_billingService = new BillingService(_unitOfWork, billingQueueTransactionLogService, priceService, billPaymentsService, housesToBeBilledService, new HouseService(_unitOfWork, new ContractService(_unitOfWork), tenantEncryptionService), new MultiThreadingService());
        }

        //~BillingApiController()
        //{
        //    _unitOfWork = null;
        //    _billingService = null;
        //}

        [Route("SendBillsToGoCardless/{billingRunId:int}/{houseId:int}/{period:DateTime}")]
        [HttpGet]
        public async Task SendBillsToGoCardless(int billingRunId, int houseId, DateTime period)
        {
            try
            {
                await _sendBillsToGoCardlessService.SendBillsToGoCardlessAsync(billingRunId, GoCardless.Environments.Sandbox);
            }
            catch (Exception ex)
            {
            }
        }
    }
}
