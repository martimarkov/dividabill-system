﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using DividaBill.DAL.Interfaces;
using DividaBill.Models;
using DividaBill.Models.Sheets;
using DividaBill.Services.Interfaces;
using DividaBill.ViewModels.GDrive;

namespace DividaBill.SysOp.Controllers.api
{
    [RoutePrefix("api/sheets")]
    public class SheetsController : ApiController
    {
        private readonly IGDriveService _gDriveService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IHouseService _houseService;

        public SheetsController(IGDriveService gDriveService, IUnitOfWork unitOfWork, IHouseService houseService)
        {
            this._gDriveService = gDriveService;
            this._unitOfWork = unitOfWork;
            this._houseService = houseService;
        }

        private File getFileObject(int id)
        {
            var file = _unitOfWork.GDriveFileRepository.GetByID(id);
            return file;
        }

        [HttpGet, Route("info")]
        public GDriveResponseViewModel GetFilesInfo()
        {
            var files = _unitOfWork.GDriveFileRepository.Get().ToArray();
            return new GDriveResponseViewModel
            {
                LogLines = new string[0],
                Files = files
                    .Select(f => new GDriveFileViewModel(f))
                    .ToArray(),
            };
        }

        [HttpGet, Route("pull")]
        public void userPull(int id, bool saveToDB)
        {
            _gDriveService.Pull(getFileObject(id), saveToDB);
            return;
        }

        [HttpGet, Route("push")]
        public void userPush(int id, bool saveToDB)
        {
            IEnumerable<HouseModel> houses = _houseService.GetAllHouses().Where(h => h.StartDate.Year < 2020);
            _gDriveService.Push(getFileObject(id), saveToDB, houses);
            return;
        }
    }
}