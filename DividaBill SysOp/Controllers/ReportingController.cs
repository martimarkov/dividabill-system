﻿using System;
using System.Web.Mvc;
using DividaBill.Services.NewBillingSystem;

namespace DividaBill.SysOp.Controllers
{
    public class ReportingController : Controller
    {
        private readonly IReportingService _reportingService;

        public ReportingController(IReportingService reportingService)
        {
            _reportingService = reportingService;
        }

        // GET: Reporting
        public ActionResult MonthlyBillingReports()
        {
            var monthlyBillingReports = _reportingService.GetTotalBilledForPeriod(DateTime.UtcNow.AddMonths(-6), DateTime.UtcNow);
            return View(monthlyBillingReports);
        }
    }
}