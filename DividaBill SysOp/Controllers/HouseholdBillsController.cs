﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using DividaBill.Helpers;
using DividaBill.Library;
using DividaBill.Services.Exceptions;
using DividaBill.Services.Factories;
using DividaBill.Services.Interfaces;
using DividaBill.SysOp.ViewModels;
using DividaBill.ViewModels.DataTables;
using Mvc.JQuery.DataTables;
using Mvc.JQuery.DataTables.Models;

namespace DividaBill.SysOp.Controllers
{
    public class HouseholdBillsController : AdminController
    {
        private readonly IBillService _billService;
        private readonly IHouseService _houseService;
        private readonly IHouseHoldBillViewFactory _billViewFactory;

        public HouseholdBillsController(IBillService billService, IHouseService houseService, IHouseHoldBillViewFactory billViewFactory)
        {
            _billService = billService;
            _houseService = houseService;
            _billViewFactory = billViewFactory;
        }

        public DataTablesResult<HouseHoldBillsViewModel> GetBill(DataTablesParam dataTableParam)
        {
            IQueryable<HouseHoldBillsViewModel> bills = null;
            return DataTablesResult.Create(bills, dataTableParam, ArrayOutputType.ArrayOfObjects);
            //...and return a DataTablesResult
        }



        // GET: Admin/HouseholdBills
        public ActionResult Index(string date)
        {
            var parsedSelectedTime = DateTime.Now;
            if (!DateTime.TryParse(date, out parsedSelectedTime))
            {
                parsedSelectedTime = DateTime.Now;
            }

            //Get the bills for the selected month
            var selectedBillsEntity = _billService.GetHouseholdBillsForMonth(parsedSelectedTime);
            var selectedBillsViewModel = _billViewFactory.Create(selectedBillsEntity).AsEnumerable();

            ViewData["YearsList"] = _billService.GetHouseholdBillsAllYearsList().Select(x => new SelectListItem
            {
                Text = x.ToString(),
                Value = x.ToString(),
                Selected = parsedSelectedTime.Year == x
            }).ToList();
            ViewData["SelectedDate"] = parsedSelectedTime.ToStringDateFormat();
            return View(selectedBillsViewModel);
        }

        // GET: Admin/HouseholdBills/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            try
            {
                var bill = _billService.GetHouseholdBill(id.Value);
                return View(bill);
            }
            catch (BillNotFound)
            {
                return HttpNotFound();
            }
        }


        [RequireRequestValue("houseId")]
        public ActionResult Create(int houseId)
        {
            //HouseModel house = unitOfWork.ActiveHouses.GetByID(houseId);
            //if (house == null)
            //{
            //    return HttpNotFound();
            //}
            //var bill = new Bill();
            //bill.HouseModelID = houseId;
            //bill.Month = DateTime.Now;
            //ViewBag.Houses = new SelectList(unitOfWork.ActiveHouses.Get(h => !h.Archived && (h.EndDate > DateTime.Now)).ToList(), "ID", "Address.Line1");
            //ViewBag.DisabledDropdownList = true;
            return View();
        }

        // POST: Admin/Bills/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create(Bill mbill)
        //{
        //    //if (bill.House == null)
        //    //{
        //    //    ModelState.AddModelError("House", "The house can't be empty.");
        //    //}
        //    if (ModelState.IsValid)
        //    {
        //        if (mbill.Type == BillType.Estimated)
        //        {
        //            BillService _service = new BillService(unitOfWork);
        //            _service.GenerateMonthlyBill(mbill.StartDate, mbill.EndDate, mbill.HouseModelID);
        //        }
        //        else
        //        {
        //            unitOfWork.BillRepository.Insert(mbill);
        //            unitOfWork.Save();
        //        }
        //        return RedirectToAction("Index");
        //    }
        //    else
        //    {
        //        ViewBag.Houses = new SelectList(unitOfWork.ActiveHouses.Get(h => !h.Archived && (h.EndDate > DateTime.Now)).ToList(), "ID", "Address.Street");
        //    }

        //    return View(mbill);
        //}

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GenerateMonthlyBills(GenerateBillViewModel model)
        {
            if (ModelState.IsValid)
            {
                throw new NotImplementedException();
                // List<HouseModel> dbActiveHouses = _houseService.GetActiveHouses().Where(p => p.SetUpDate <= DateTime.Today && p.StartDate <= DateTime.Today && p.Housemates == p.Tenants.Count()).Include("Tenants").ToList();
                //foreach (HouseModel house in dbActiveHouses)
                //{
                //    _billService.GenerateMonthlyBill(model.StartDate.Date, model.EndDate.Date, house.ID, model.PaymentDate);
                //}
                //return RedirectToAction("Index");
            }

            return View(model);
        }

        public ActionResult GenerateMonthlyBills()
        {
            var olddate = DateTime.Today.AddMonths(1);
            var nextMonth = new DateTime(olddate.Year, olddate.Month, 1, 0, 0, 0, olddate.Kind);
            var model = new GenerateBillViewModel
            {
                StartDate = nextMonth,
                EndDate = nextMonth.AddDays(DateTime.DaysInMonth(nextMonth.Year, nextMonth.Month)),
                PaymentDate = nextMonth
            };

            return View(model);
        }
    }
}