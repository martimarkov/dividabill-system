﻿using System.IdentityModel.Tokens;
using System.Linq;
using DividaBill.SysOp.Models;
using System;

namespace DividaBill.SysOp.Utils
{
    public class DatabaseIssuerNameRegistry : ValidatingIssuerNameRegistry
    {
        public static bool ContainsTenant(string tenantId)
        {
            using (TenantDbContext context = new TenantDbContext())
            {
                return context.Tenants
                    .Any(tenant => tenant.Id == tenantId);
            }
        }

        public static bool ContainsKey(string thumbprint)
        {
            using (TenantDbContext context = new TenantDbContext())
            {
                return context.IssuingAuthorityKeys
                    .Any(key => key.Id == thumbprint);
            }
        }

        public static void RefreshKeys(string metadataLocation)
        {
            IssuingAuthority issuingAuthority = ValidatingIssuerNameRegistry.GetIssuingAuthority(metadataLocation);

            bool newKeys = false;
            bool refreshTenant = false;
            foreach (string thumbprint in issuingAuthority.Thumbprints)
            {
                if (!ContainsKey(thumbprint))
                {
                    newKeys = true;
                    refreshTenant = true;
                    break;
                }
            }

            foreach (string issuer in issuingAuthority.Issuers)
            {
                if (!ContainsTenant(GetIssuerId(issuer)))
                {
                    refreshTenant = true;
                    break;
                }
            }

            if (newKeys || refreshTenant)
            {
                using (TenantDbContext context = new TenantDbContext())
                {
                    if (newKeys)
                    {
                      context.IssuingAuthorityKeys.RemoveRange(context.IssuingAuthorityKeys);
                      foreach (string thumbprint in issuingAuthority.Thumbprints)
                      {
                          context.IssuingAuthorityKeys.Add(new IssuingAuthorityKey { Id = thumbprint });
                      }
                    }

                    if (refreshTenant)
                    {
                        foreach (string issuer in issuingAuthority.Issuers)
                        {
                            string issuerId = GetIssuerId(issuer);
                            if (!ContainsTenant(issuerId))
                            {
                                context.Tenants.Add(new Tenant { Id = issuerId });
                            }
                        }
                    }
                    context.SaveChanges();
                }
            }
        }

        private static string GetIssuerId(string issuer)
        {
            return issuer.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).Last();
        }

        protected override bool IsThumbprintValid(string thumbprint, string issuer)
        {
            var tenantId = GetIssuerId(issuer);
            var hasTenant = ContainsTenant(tenantId);
            var hasKey = ContainsKey(thumbprint);
            return hasTenant && hasKey;
        }
    }
}
