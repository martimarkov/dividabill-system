﻿using System.Web.Mvc;
using DividaBill.SysOp.Exceptions;

namespace DividaBill.SysOp
{
    public static class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //filters.Add(new RequireHttpsAttribute());
            filters.Add(new HandleErrorAttribute());
            filters.Add(new CustomExceptionAttribute());
        }
    }
}