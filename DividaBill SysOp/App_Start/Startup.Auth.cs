﻿using DividaBill.SysOp.Utils;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System;
using System.Globalization;
using System.Threading.Tasks;
using System.Web;
using Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OpenIdConnect;
using System.Configuration;

namespace DividaBill.SysOp
{
    public partial class Startup
    {
        //
        // The Client ID is used by the application to uniquely identify itself to Azure AD.
        // The App Key is a credential used to authenticate the application to Azure AD.  Azure AD supports password and certificate credentials.
        // The Metadata Address is used by the application to retrieve the signing keys used by Azure AD.
        // The AAD Instance is the instance of Azure, for example public Azure or Azure China.
        // The Authority is the sign-in URL of the tenant.
        // The Post Logout Redirect Uri is the URL where the user will be redirected after they sign out.
        //

        //private static string clientId = Environment.GetEnvironmentVariable("APPSETTING_ida:ClientId");
        //private static string appKey = Environment.GetEnvironmentVariable("APPSETTING_ida:AppKey");
        //private static string aadInstance = Environment.GetEnvironmentVariable("APPSETTING_ida:AADInstance");
        //private static string tenant = Environment.GetEnvironmentVariable("APPSETTING_ida:Tenant");
        //private static string postLogoutRedirectUri = Environment.GetEnvironmentVariable("APPSETTING_ida:PostLogoutRedirectUri");


        // This is the resource ID of the AAD Graph API.  We'll need this to request a token to call the Graph API.
        string graphResourceId = Environment.GetEnvironmentVariable("APPSETTING_ida:GraphUrl");

        public void ConfigureAuth(IAppBuilder app)
        {
            var clientId              = ConfigurationManager.AppSettings["ida:ClientId"];
            var appKey                = ConfigurationManager.AppSettings["ida:AppKey"];
            var aadInstance           = ConfigurationManager.AppSettings["ida:AADInstance"];
            var tenant                = ConfigurationManager.AppSettings["ida:Tenant"];
            var postLogoutRedirectUri = ConfigurationManager.AppSettings["ida:PostLogoutRedirectUri"];

            var authority = string.Format(CultureInfo.InvariantCulture, aadInstance, tenant);

            app.SetDefaultSignInAsAuthenticationType(CookieAuthenticationDefaults.AuthenticationType);

            app.UseCookieAuthentication(new CookieAuthenticationOptions());

            app.UseOpenIdConnectAuthentication(
                new OpenIdConnectAuthenticationOptions
                {
                    ClientId = clientId,
                    Authority = authority,
                    PostLogoutRedirectUri = postLogoutRedirectUri,

                    Notifications = new OpenIdConnectAuthenticationNotifications()
                    {
                        //
                        // If there is a code in the OpenID Connect response, redeem it for an access token and refresh token, and store those away.
                        //

                        AuthorizationCodeReceived = (context) =>
                        {
                            var code = context.Code;

                            var credential = new ClientCredential(clientId, appKey);
                            var userObjectID = context.AuthenticationTicket.Identity
                                .FindFirst("http://schemas.microsoft.com/identity/claims/objectidentifier")
                                .Value;
                            var authContext = new AuthenticationContext(authority, new NaiveSessionCache(userObjectID));
                            var result = authContext.AcquireTokenByAuthorizationCode(
                                code, new Uri(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Path)), credential, graphResourceId);

                            AuthenticationHelper.token = result.AccessToken;

                            return Task.FromResult(0);
                        }

                    }

                });
        }
    }
}