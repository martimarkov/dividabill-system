﻿using System.Globalization;
using System.Web.Http;
using DividaBill.SysOp.Exceptions;
using Newtonsoft.Json;

namespace DividaBill.SysOp
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MessageHandlers.Add(new MethodOverrideHandler());
            config.MapHttpAttributeRoutes();

            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);

            var jSettings = new JsonSerializerSettings
            {
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                DateTimeZoneHandling = DateTimeZoneHandling.Unspecified,
                Culture = CultureInfo.GetCultureInfo("en-GB")
            };

            config.Formatters.JsonFormatter.SerializerSettings = jSettings;
            config.Formatters.Remove(config.Formatters.XmlFormatter);
            config.Filters.Add(new UnhandledExceptionFilter());
        }
    }
}