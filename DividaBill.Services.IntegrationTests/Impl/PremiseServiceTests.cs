﻿using System.Collections.Generic;
using Dividabill.Tests.Core;
using DividaBill.AppConstants;
using DividaBill.Library.AppConfiguration.Impl;
using DividaBill.Services.Impl;
using DividaBill.Services.ThirdPartyServices.Configurations;
using DividaBill.Services.ThirdPartyServices.Impl;
using DividaBill.Services.ThirdPartyServices.Interfaces;
using NUnit.Framework;

namespace DividaBill.Services.IntegrationTests.Impl
{
    [TestFixture]
    public class PremiseServiceTests : BaseAutoMock<PremiseService>
    {
        public PremiseServiceTests()
        {
            var thirdPartyReader = new ThirdPartyReader();
            var appConfigurationReader = new AppConfigurationReader();
            var testDataService = new TestDataService(appConfigurationReader);

            var postCodeAnyWhereConfig = new PostCodeAnyWhereConfig
            {
                ApiKey = appConfigurationReader.GetThirdPartyConfig(ThirdPartyServicesEnum.PostCodeAnyWhere, ThirdPartyServicesConfigEnum.ApiKey),
                BaseUrl = appConfigurationReader.GetThirdPartyConfig(ThirdPartyServicesEnum.PostCodeAnyWhere, ThirdPartyServicesConfigEnum.BaseUrl)
            };

            _postCodeAnyWhereService = new PostCodeAnyWhereService(postCodeAnyWhereConfig, thirdPartyReader, testDataService);

            var idealPostCodesConfig = new IdealPostCodesConfig
            {
                ApiKey = appConfigurationReader.GetThirdPartyConfig(ThirdPartyServicesEnum.IdealPostCodes, ThirdPartyServicesConfigEnum.ApiKey),
                BaseUrl = appConfigurationReader.GetThirdPartyConfig(ThirdPartyServicesEnum.IdealPostCodes, ThirdPartyServicesConfigEnum.BaseUrl),
                Version = appConfigurationReader.GetThirdPartyConfig(ThirdPartyServicesEnum.IdealPostCodes, ThirdPartyServicesConfigEnum.Version)
            };
            _idealPostCodesService = new IdealPostCodesService(idealPostCodesConfig, thirdPartyReader);
        }

        private readonly PostCodeAnyWhereService _postCodeAnyWhereService;
        private readonly IdealPostCodesService _idealPostCodesService;

        [TestCase("E27QH", 59)]
        [TestCase("Sw153ph", 31)]
        [TestCase("B139DQ", 43)]
        [TestCase("B297BU", 28)]
        [TestCase("PO51HB", 38)]
        [TestCase("RG304EE", 13)]
        public void GivenNonExistingInDBPostCodeAndExistingOnThirdParty_WhenPostCodeAnyWhereGetPremisesForPostcodeFromThirdPartyInvoked_ThenInsertPremisesIntoDBAndInvokedExactTimes(string postCode, int premisesCount)
        {
            GetMock<IThirdPartiesListService>().Setup(x => x.Services).Returns(new List<IThirdPartyService>
            {
                _postCodeAnyWhereService
            });

            var result = ClassUnderTest.GetPremisesForPostcodeFromThirdParty(postCode);

            Assert.NotNull(result);
            Assert.AreEqual(premisesCount, result.Count);
        }

        [TestCase("ID1 1QD", 7)] //Returns a successful postcode lookup response 2000
        public void GivenNonExistingInDBPostCodeAndExistingOnThirdParty_WhenIdealPostCodesServiceGetPremisesForPostcodeInvoked_ThenInsertPremisesIntoDBAndInvokedExactTimes(string postCode, int premisesCount)
        {
            GetMock<IThirdPartiesListService>().Setup(x => x.Services).Returns(new List<IThirdPartyService>
            {
                _idealPostCodesService
            });

            var result = ClassUnderTest.GetPremisesForPostcodeFromThirdParty(postCode);

            Assert.NotNull(result);
            Assert.AreEqual(premisesCount, result.Count);
        }

        [TestCase("WHATEVER string just for test")]
        [TestCase("ID1 KFA")]
        [TestCase("478923748239")]
        [TestCase("ID1 CHOP")]
        public void GivenNonExistingInDBPostCodeAndNonExistingOnThirdParty_WhenPostCodeAnyWhereGetPremisesForPostcodeFromThirdPartyInvoked_ThenNullReturned(string postCode)
        {
            GetMock<IThirdPartiesListService>().Setup(x => x.Services).Returns(new List<IThirdPartyService>
            {
                _postCodeAnyWhereService
            });

            var result = ClassUnderTest.GetPremisesForPostcodeFromThirdParty(postCode);

            Assert.IsNull(result);
        }

        [TestCase("WHATEVER string just for test")]
        [TestCase("ID1 KFA")] //Returns "postcode not found" error 4040
        [TestCase("ID1 CLIP")] //Returns "no lookups remaining" error 4020
        [TestCase("ID1 CHOP")] //Returns "daily (or individual) lookup limit breached" error 4021
        public void GivenNonExistingInDBPostCodeAndNonExistingOnThirdParty_WhenIdealPostCodesServiceGetPremisesForPostcodeFromThirdPartyInvoked_ThenNullReturned(string postCode)
        {
            GetMock<IThirdPartiesListService>().Setup(x => x.Services).Returns(new List<IThirdPartyService>
            {
                _idealPostCodesService
            });

            var result = ClassUnderTest.GetPremisesForPostcodeFromThirdParty(postCode);

            Assert.IsNull(result);
        }

        [TestCase(242, "AB10 1HW", "Aberdeenshire", "Aberdeen", "St. Nicholas Centre", "", "", "")]
        [TestCase(263450, "AL4 0QS", "Hertfordshire", "St. Albans", "16 Bullens Green Lane", "Colney Heath", "", "")]
        [TestCase(1000977564, "CO10 7LS", "Essex", "Sudbury", "The Flat", "The Henny Swan", "Henny Street", "Great Henny")]
        public void GivenNonExistingInDBBuildingAndExistingOnThirdParty_WhenPostCodeAnyWhereGetBuildingsForPremiseFromThirdPartyInvoked_ThenInsertOneBuildingIntoDBAndInvokedOnce(double premise, string postCode, string county, string postTown, string line1, string line2, string line3, string line4)
        {
            GetMock<IThirdPartiesListService>().Setup(x => x.Services).Returns(new List<IThirdPartyService>
            {
                _postCodeAnyWhereService
            });

            var result = ClassUnderTest.GetBuildingsForPremiseFromThirdParty(premise);

            Assert.NotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(premise, result[0].Udprn);
            Assert.AreEqual(postCode, result[0].PostCode);
            Assert.AreEqual(county, result[0].County);
            Assert.AreEqual(postTown, result[0].PostTown);
            Assert.AreEqual(line1, result[0].Line1);
            Assert.AreEqual(line2, result[0].Line2);
            Assert.AreEqual(line3, result[0].Line3);
            Assert.AreEqual(line4, result[0].Line4);
        }

        [TestCase(0)]
        [TestCase(-1)]
        [TestCase(10)]
        public void GivenNonExistingInDBBuildingAndNonExistingOnThirdParty_WhenPostCodeAnyWhereGetBuildingsForPremiseFromThirdPartyInvoked_ThenNullReturned(double premise)
        {
            GetMock<IThirdPartiesListService>().Setup(x => x.Services).Returns(new List<IThirdPartyService>
            {
                _postCodeAnyWhereService
            });

            var result = ClassUnderTest.GetBuildingsForPremiseFromThirdParty(premise);

            Assert.Null(result);
        }

        [TestCase(0, "ID1 1QD", "Greater London", "LONDON", "2 Barons Court Road", "", "")] //Returns a successful UDPRN lookup response 2000
        public void GivenNonExistingInDBBuildingAndExistingOnThirdParty_WhenIdealPostCodesGetBuildingsForPremiseFromThirdPartyInvoked_ThenInsertOneBuildingIntoDBAndInvokedOnce(double premise, string postCode, string county, string postTown, string line1, string line2, string line3)
        {
            GetMock<IThirdPartiesListService>().Setup(x => x.Services).Returns(new List<IThirdPartyService>
            {
                _idealPostCodesService
            });

            var result = ClassUnderTest.GetBuildingsForPremiseFromThirdParty(premise);

            Assert.NotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(premise, result[0].Udprn);
            Assert.AreEqual(postCode, result[0].PostCode);
            Assert.AreEqual(county, result[0].County);
            Assert.AreEqual(postTown, result[0].PostTown);
            Assert.AreEqual(line1, result[0].Line1);
            Assert.AreEqual(line2, result[0].Line2);
            Assert.AreEqual(line3, result[0].Line3);
        }

        [TestCase(-1)] //Returns "UDPRN not found" error 4044
        [TestCase(-2)] //Returns "no lookups remaining" error 4020
        [TestCase(-3)] //Returns "daily (or individual) lookup limit breached" error 4021
        public void GivenNonExistingInDBBuildingAndNonExistingOnThirdParty_WhenIdealPostCodesGetBuildingsForPremiseFromThirdPartyInvoked_ThenNullReturned(double premise)
        {
            GetMock<IThirdPartiesListService>().Setup(x => x.Services).Returns(new List<IThirdPartyService>
            {
                _idealPostCodesService
            });

            var result = ClassUnderTest.GetBuildingsForPremiseFromThirdParty(premise);

            Assert.Null(result);
        }
    }
}