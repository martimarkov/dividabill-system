using DividaBill.AppConstants;
using DividaBill.Library.AppConfiguration.Impl;
using DividaBill.Services.ThirdPartyServices.Configurations;
using DividaBill.Services.ThirdPartyServices.Impl;
using DividaBill.Services.ThirdPartyServices.Interfaces;
using NUnit.Framework;

namespace DividaBill.Services.IntegrationTests.ThirdPartyServices.Impl
{
    [TestFixture]
    public class DbIpServiceTests
    {
        [SetUp]
        public void SetUp()
        {
            var appConfigurationReader = new AppConfigurationReader();
            var dbIpConfig = new DbIpConfig
            {
                BaseUrl = appConfigurationReader.GetThirdPartyConfig(ThirdPartyServicesEnum.DbIp, ThirdPartyServicesConfigEnum.BaseUrl),
                ApiKey = appConfigurationReader.GetThirdPartyConfig(ThirdPartyServicesEnum.DbIp, ThirdPartyServicesConfigEnum.ApiKey)
            };
            _classUnderTest = new DbIpService(dbIpConfig, new ThirdPartyReader());
        }

        private IDbIpService _classUnderTest;

        [TestCase("82.163.124.86", 51.5236f, -0.0834383f)]
        public void GivenCorrectIp_WhenGetIpAddressInfoInvoked_ThenLocationReturned(string ip, float latitude, float longitude)
        {
            var result = _classUnderTest.GetIpAddressInfo(ip);

            Assert.NotNull(result);
            Assert.AreEqual(latitude, result.latitude);
            Assert.AreEqual(longitude, result.longitude);
        }

        [TestCase("any wrong ip")]
        [TestCase("local")]
        public void GivenWrongIp_WhenGetIpAddressInfoInvoked_ThenNullReturned(string ip)
        {
            var result = _classUnderTest.GetIpAddressInfo(ip);

            Assert.IsNull(result);
        }
    }
}