﻿using DividaBill.AppConstants;
using DividaBill.Library.AppConfiguration.Impl;
using DividaBill.Services.Interfaces;
using DividaBill.Services.ThirdPartyServices.Configurations;
using DividaBill.Services.ThirdPartyServices.Impl;
using DividaBill.Services.ThirdPartyServices.Interfaces;
using Moq.AutoMock;
using NUnit.Framework;

namespace DividaBill.Services.IntegrationTests.ThirdPartyServices.Impl
{
    [TestFixture]
    public class PostCodeAnyWhereServiceTests
    {
        private readonly IPostCodeAnyWhereService _classUnderTest;

        public PostCodeAnyWhereServiceTests()
        {
            var mocker = new AutoMocker();

            var thirdPartyReader = mocker.GetMock<IThirdPartyReader>();
            var testDataService = mocker.GetMock<ITestDataService>();

            var appConfigurationReader = new AppConfigurationReader();

            var postCodeAnyWhereConfig = new PostCodeAnyWhereConfig
            {
                BaseUrl = appConfigurationReader.GetThirdPartyConfig(ThirdPartyServicesEnum.PostCodeAnyWhere, ThirdPartyServicesConfigEnum.BaseUrl),
                ApiKey = appConfigurationReader.GetThirdPartyConfig(ThirdPartyServicesEnum.PostCodeAnyWhere, ThirdPartyServicesConfigEnum.ApiKey)
            };
            _classUnderTest = new PostCodeAnyWhereService(postCodeAnyWhereConfig, thirdPartyReader.Object, testDataService.Object);
        }

        [TestCase(51.5236f, -0.0834383f, "EC2A 4HJ")]
        [TestCase(51.528892f, -0.078062f, "E2 8AE")]
        public void GetPostCodeFromCoords(float latitude, float longitude, string postCode)
        {
            var result = _classUnderTest.GetLocationByCoords(latitude, longitude);

            Assert.NotNull(result);
            Assert.AreEqual(postCode, result.Postcode);
        }
    }
}