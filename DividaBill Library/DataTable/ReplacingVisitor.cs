using System;
using System.Linq.Expressions;

namespace DividaBill.Library.DataTable
{
    public class ReplacingVisitor : ExpressionVisitor
    {
        private readonly Func<Expression, Expression> _createReplacement;
        private readonly Func<Expression, bool> _match;

        public ReplacingVisitor(Expression from, Expression to)
        {
            _match = node => from == node;
            _createReplacement = node => to;
        }

        public override Expression Visit(Expression node)
        {
            if (_match(node)) return _createReplacement(node);
            return base.Visit(node);
        }
    }
}