using System;
using System.Linq;
using QueryInterceptor;

namespace DividaBill.Library.DataTable
{
    public static class IQueryableCaseExtensions
    {
        public static IQueryable<T> SetComparer<T>(this IQueryable<T> q, StringComparison sc)
        {
            return q
                .InterceptWith(new SetComparerExpressionVisitor(sc));
        }
    }
}