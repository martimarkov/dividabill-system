using DividaBill.Library.AppConfiguration;
using DividaBill.Library.AppConfiguration.Impl;
using Microsoft.Practices.Unity;

namespace DividaBill.Library.Unity
{
    public static class Config
    {
        public static void RegisterLibrary(this IUnityContainer container)
        {
            container.RegisterType<IAppConfigurationReader, AppConfigurationReader>();
        }
    }
}