﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;

//using System.Web.Mvc;

namespace DividaBill.Library
{
    public static class Functions
    {
        /// <summary>
        ///     Adds the given number of business days to the <see cref="DateTime" />.
        /// </summary>
        /// <param name="current">The date to be changed.</param>
        /// <param name="days">Number of business days to be added.</param>
        /// <returns>A <see cref="DateTime" /> increased by a given number of business days.</returns>
        public static DateTime AddBusinessDays(this DateTime current, int days)
        {
            var sign = Math.Sign(days);
            var unsignedDays = Math.Abs(days);
            for (var i = 0; i < unsignedDays; i++)
            {
                do
                {
                    current = current.AddDays(sign);
                } while (current.DayOfWeek == DayOfWeek.Saturday ||
                         current.DayOfWeek == DayOfWeek.Sunday);
            }
            return current;
        }


        /// <summary>
        ///     Subtracts the given number of business days to the <see cref="DateTime" />.
        /// </summary>
        /// <param name="current">The date to be changed.</param>
        /// <param name="days">Number of business days to be subtracted.</param>
        /// <returns>A <see cref="DateTime" /> increased by a given number of business days.</returns>
        public static DateTime SubtractBusinessDays(this DateTime current, int days)
        {
            return AddBusinessDays(current, -days);
        }


        public static decimal grossPrice(decimal ElectricBill, decimal GasBill, decimal WaterBill, int p)
        {
            return (ElectricBill + GasBill + WaterBill)/p + p*(decimal) 4.29;
        }


        public static string TimeAgo(DateTime date)
        {
            var timeSince = DateTime.Now.Subtract(date);
            if (timeSince.TotalMilliseconds < 1) return "not yet";
            if (timeSince.TotalMinutes < 1) return "just now";
            if (timeSince.TotalMinutes < 2) return "1 minute ago";
            if (timeSince.TotalMinutes < 60) return string.Format("{0} minutes ago", timeSince.Minutes);
            if (timeSince.TotalMinutes < 120) return "1 hour ago";
            if (timeSince.TotalHours < 24) return string.Format("{0} hours ago", timeSince.Hours);
            if (timeSince.TotalDays < 2) return "yesterday";
            if (timeSince.TotalDays < 7) return string.Format("{0} days ago", timeSince.Days);
            if (timeSince.TotalDays < 14) return "last week";
            if (timeSince.TotalDays < 21) return "2 weeks ago";
            if (timeSince.TotalDays < 28) return "3 weeks ago";
            if (timeSince.TotalDays < 60) return "last month";
            if (timeSince.TotalDays < 365) return string.Format("{0} months ago", Math.Round(timeSince.TotalDays/30));
            if (timeSince.TotalDays < 730) return "last year"; //last but not least...
            return string.Format("{0} years ago", Math.Round(timeSince.TotalDays/365));
        }


        public static int MonthsBetweenTwoDates(DateTime StartDate, DateTime EndDate)
        {
            return (EndDate.Year - StartDate.Year)*12 + (EndDate.Month - StartDate.Month)
                   + (EndDate.Day - StartDate.Day == 0 ? 0 : 1);
        }


        public static string GetCustomDescription(object objEnum)
        {
            var fi = objEnum.GetType().GetField(objEnum.ToString());
            var attributes = (DescriptionAttribute[]) fi.GetCustomAttributes(typeof (DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : objEnum.ToString();
        }

        /// <summary>
        ///     Extension to Enum to return the String Description of Enum
        /// </summary>
        public static string Description(this Enum value)
        {
            return GetCustomDescription(value);
        }

        /// <summary>
        ///     Maps A to 0, B to 1, and so forth.
        /// </summary>
        /// <param name="columnName"></param>
        /// <returns></returns>
        public static int ColumnStringToIndex(string columnName)
        {
            // http://stackoverflow.com/questions/667802/what-is-the-algorithm-to-convert-an-excel-column-letter-into-its-number
            if (string.IsNullOrEmpty(columnName)) throw new ArgumentNullException("columnName");

            columnName = columnName.ToUpperInvariant();

            var sum = 0;
            for (var i = 0; i < columnName.Length; i++)
            {
                sum *= 26;
                sum += columnName[i] - 'A' + 1;
            }

            return sum - 1;
        }

        public static string GetAssemblyVersionInfo(Assembly assembly)
        {
            if (assembly == null)
                return null;
            const int hashMaxLength = 7;
            var assemblyInformationalVersion = FileVersionInfo.GetVersionInfo(assembly.Location).ProductVersion;
            if (!string.IsNullOrEmpty(assemblyInformationalVersion)
                && assemblyInformationalVersion.Length > hashMaxLength)
            {
                assemblyInformationalVersion = assemblyInformationalVersion.Substring(0, hashMaxLength);
            }
            var assemblyVersion = assembly.GetName().Version;
            return $"{assemblyVersion} - {assemblyInformationalVersion}";
        }

        public static string GetAssemblyFileVersion(Type type)
        {
            var assembly = Assembly.GetAssembly(type);
            var attributes = assembly.GetCustomAttributes(typeof (AssemblyFileVersionAttribute), false)
                as AssemblyFileVersionAttribute[];

            if (attributes != null && attributes.Length == 1)
                return attributes[0].Version;
            return "";
        }
    }
}