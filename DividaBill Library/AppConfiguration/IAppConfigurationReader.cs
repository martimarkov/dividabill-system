using DividaBill.AppConstants;

namespace DividaBill.Library.AppConfiguration
{
    public interface IAppConfigurationReader
    {
        string GetFromAppConfig(string key);
        string GetFromAppConfig(AppSettingsEnum keyEnum);
        string GetThirdPartyConfig(ThirdPartyServicesEnum serviceType, ThirdPartyServicesConfigEnum configName);
    }
}