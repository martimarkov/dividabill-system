﻿using DividaBill.Models.GoCardless;

namespace GoCardless_API.Models
{
    public class PaymentResponse
    {
        public GCPayment GcPayments { get; set; }
    }
}