﻿using DividaBill.Models.GoCardless;

namespace GoCardless_API.Models
{
    public class MandateResponse
    {
        public GCMandate Mandates { get; set; }
    }
}