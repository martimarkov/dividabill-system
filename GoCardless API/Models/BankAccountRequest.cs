﻿using DividaBill.Models.GoCardless;

namespace GoCardless_API.Models
{
    public class BankAccountRequest
    {
        public NewBankAccount customer_bank_accounts { get; set; }
    }
}