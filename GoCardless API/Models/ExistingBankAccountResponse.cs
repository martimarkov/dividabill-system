﻿using System.Collections.Generic;
using DividaBill.Models.GoCardless;

namespace GoCardless_API.Models
{
    public class ExistingBankAccountResponse
    {
        public List<GCExistingBankAccount> customer_bank_accounts { get; set; }
    }
}