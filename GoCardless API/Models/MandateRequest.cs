﻿using DividaBill.Models.GoCardless;

namespace GoCardless_API.Models
{
    public class MandateRequest
    {
        public NewMandate mandates { get; set; }
    }
}