﻿using System.Collections.Generic;
using DividaBill.Models.GoCardless;

namespace GoCardless_API.Models
{
    public class BankAccountsResponse
    {
        public List<GCBankAccount> customer_bank_accounts { get; set; }
    }
}