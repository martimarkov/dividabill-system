﻿using DividaBill.Models.GoCardless;

namespace GoCardless_API.Models
{
    public class BankAccountResponse
    {
        public GCBankAccount customer_bank_accounts { get; set; }
    }
}