﻿using DividaBill.Models.GoCardless;

namespace GoCardless_API.Models
{
    public class PaymentRequest
    {
        public NewPayment payments { get; set; }
    }
}