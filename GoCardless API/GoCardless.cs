﻿using System.Collections.Generic;
using System.Reflection;
using DividaBill.Library;

namespace GoCardless_API
{
    public static class PaymentStatus
    {
        public static string Paid => "paid_out";
    }

    public static class GoCardless
    {
        public enum Environments
        {
            Production = 0,
            Sandbox = 1
        }

        public static readonly Dictionary<Environments, string> BaseUrls =
            new Dictionary<Environments, string>
            {
                {Environments.Production, "https://api.gocardless.com"},
                {Environments.Sandbox, "https://api-sandbox.gocardless.com/"}
            };

        private static string _baseUrl;

        internal static string UserAgent = GetUserAgent();

        public static string BaseUrl
        {
            get { return _baseUrl ?? BaseUrls[Environment ?? Environments.Production]; }
            set { _baseUrl = value.Trim(); }
        }

        public static Environments? Environment { get; set; }

        private static string GetUserAgent()
        {
            try
            {
                return "dividabill-client/v" + Functions.GetAssemblyFileVersion(typeof(GoCardless));
            }
            catch
            {
                return "dividabill-client";
            }
        }
    }
}