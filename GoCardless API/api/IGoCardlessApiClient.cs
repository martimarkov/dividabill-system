using System.Collections.Generic;
using System.Threading.Tasks;
using DividaBill.Models.GoCardless;

namespace GoCardless_API.api
{
    public interface IGoCardlessApiClient
    {
        Task<Customer> CreateCustomerAsync(NewCustomerInfo customer);
        Task<GCBankAccount> CreateBankAccountAsync(NewBankAccount bankAccount);
        Task<GCBankAccount> GetBankAccountAsync(string accountId);
        Task<GCBankAccount> DisableBankAccountAsync(string bankAccountId);
        Task<GCMandate> CreateMandateAsync(NewMandate mandate);
        Task<GCMandate> GetMandateAsync(string mandateId);
        Task<List<GCMandate>> GetMandatesForBankAccount(string bankAccountId);
        Task<GCMandate> CancelMandateAsync(string mandateId);
        Task<GCPayment> CreatePaymentAsync(NewPayment payment);
        Task<GCPayment> GetPaymentAsync(string paymentId);
        Task<GCPayment> CancelPaymentAsync(string paymentId);
        void SetGoCardlessEnvironment(GoCardless.Environments goCardlessEnvironment);
        Task<List<GCPayment>> GetPaymentsAsync();
        Task<List<GCExistingBankAccount>> GetBankAccountsAsync(string customerId);


        Task<BankAccountDetailsLookupsOutputModel> CheckBankAccountDetails(GCBankAccountDetailsInputModel bankDetails);
    }
}