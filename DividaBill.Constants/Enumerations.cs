﻿namespace DividaBill.AppConstants
{
    public enum ThirdPartyServicesEnum
    {
        PostCodeAnyWhere = 0,
        IdealPostCodes = 1,

        DbIp = 2
    }

    public enum ThirdPartyServicesConfigEnum
    {
        BaseUrl,
        ApiKey,
        Version
    }

    public enum AppSettingsEnum
    {
        TestBankAccountNumber,
        TestBankAccountSortCode,

        SendGridApiKey,
        SendGridEnabled,

        ExceptionsSendToEmail,
    }

    public enum EmailEnum
    {
        EmailVerification = 1,
        UserRegisteredToNewHouse = 2,
        UserRegisteredToExistingHouse = 3,

        EmailForException = 4,
    }
}