namespace DividaBill.AppConstants
{
    public abstract class ConstantClass
    {
        protected readonly string _name;

        protected ConstantClass(string name)
        {
            _name = name;
        }

        public override string ToString()
        {
            return _name;
        }
    }

    public class BankAccountField : ConstantClass
    {
        public static readonly BankAccountField AccountNumber = new BankAccountField("BankAccountDetails.AccountNumber");
        public static readonly BankAccountField AccountSortCode = new BankAccountField("BankAccountDetails.AccountSortCode");

        private BankAccountField(string name) : base(name)
        {
        }

        public static implicit operator string(BankAccountField bankAccountField)
        {
            return bankAccountField.ToString();
        }
    }

    public class GoCardlessMandateStatus : ConstantClass
    {
        public static readonly GoCardlessMandateStatus Failed = new GoCardlessMandateStatus("failed");
        public static readonly GoCardlessMandateStatus Expired = new GoCardlessMandateStatus("expired");
        public static readonly GoCardlessMandateStatus Cancelled = new GoCardlessMandateStatus("cancelled");
        public static readonly GoCardlessMandateStatus Submitted = new GoCardlessMandateStatus("submitted");
        public static readonly GoCardlessMandateStatus Active = new GoCardlessMandateStatus("active");
        public static readonly GoCardlessMandateStatus PendingSubmission = new GoCardlessMandateStatus("pending_submission");

        private GoCardlessMandateStatus(string name) : base(name)
        {
        }

        public static implicit operator string(GoCardlessMandateStatus constClass)
        {
            return constClass.ToString();
        }
    }
}