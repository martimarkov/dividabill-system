using AutoMapper;
using DividaBill.BusinessLogic.MappingProfiles;
using DividaBill.Models;
using DividaBill.Models.Common;
using NUnit.Framework;

namespace DividaBill.BusinessLogic.Tests.MappingProfiles
{
    [TestFixture]
    public class SimpleAddressProfileTests
    {
        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            Mapper.Initialize(m => m.AddProfile<SimpleAddressProfile>());
        }

        [Test]
        public void GivenInitialisedProfile_WhenAssertConfigurationIsValidInvoked_ThenNoExceptionThrown()
        {
            // Arrange

            // Act
            Mapper.AssertConfigurationIsValid();

            // Assert
            Assert.Pass();
        }

        [Test]
        public void GivenAddress_WhenMapInvoked_ThenCityMapped()
        {
            // Arrange
            var expected = "expected";
            var source = new Address {City = expected};

            // Act
            var result = Mapper.Map<SimpleAddress>(source);

            // Assert
            Assert.AreEqual(result.City, expected);
        }

        [Test]
        public void GivenAddress_WhenMapInvoked_ThenCountryMapped()
        {
            // Arrange
            var expected = "expected";
            var source = new Address {County = expected};

            // Act
            var result = Mapper.Map<SimpleAddress>(source);

            // Assert
            Assert.AreEqual(result.County, expected);
        }

        [Test]
        public void GivenAddress_WhenMapInvoked_ThenLine1Mapped()
        {
            // Arrange
            var expected = "expected";
            var source = new Address {Line1 = expected};

            // Act
            var result = Mapper.Map<SimpleAddress>(source);

            // Assert
            Assert.AreEqual(result.Line1, expected);
        }

        [Test]
        public void GivenAddress_WhenMapInvoked_ThenLine2Mapped()
        {
            // Arrange
            var expected = "expected";
            var source = new Address {Line2 = expected};

            // Act
            var result = Mapper.Map<SimpleAddress>(source);

            // Assert
            Assert.AreEqual(result.Line2, expected);
        }

        [Test]
        public void GivenAddress_WhenMapInvoked_ThenLine3Mapped()
        {
            // Arrange
            var expected = "expected";
            var source = new Address {Line3 = expected};

            // Act
            var result = Mapper.Map<SimpleAddress>(source);

            // Assert
            Assert.AreEqual(result.Line3, expected);
        }

        [Test]
        public void GivenAddress_WhenMapInvoked_ThenLine4Mapped()
        {
            // Arrange
            var expected = "expected";
            var source = new Address {Line4 = expected};

            // Act
            var result = Mapper.Map<SimpleAddress>(source);

            // Assert
            Assert.AreEqual(result.Line4, expected);
        }

        [Test]
        public void GivenAddress_WhenMapInvoked_ThenPostcodeMapped()
        {
            // Arrange
            var expected = "expected";
            var source = new Address {Postcode = expected};

            // Act
            var result = Mapper.Map<SimpleAddress>(source);

            // Assert
            Assert.AreEqual(result.Postcode, expected);
        }
    }
}