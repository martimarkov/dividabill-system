﻿using AutoMapper;
using DividaBill.Models;
using DividaBill.ViewModels.API;

namespace DividaBill.BusinessLogic.MappingProfiles
{
    public class ContractAPIViewModelProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Contract, ContractApiViewModel>()
                .ForMember(dto => dto.ServiceName, conf => conf.MapFrom(dm => dm.ServiceNew.Name))
                .ForMember(dto => dto.LengthName, conf => conf.MapFrom(dm => dm.Length.ToString()))
                .ForMember(dto => dto.SubmissionStatusName, conf => conf.MapFrom(dm => dm.SubmissionStatus.ToString()));
        }
    }
}