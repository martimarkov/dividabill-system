﻿using DividaBill.DAL.Interfaces;
using DividaBill.Client.ViewModels;
using DividaBill.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security;
using DividaBill.Services;
using DividaBill.Services.Interfaces;

namespace DividaBill.Client.Controllers.Api.Services
{
    [Authorize]
    public class ServicesController : BaseApiController
    {
        public ServicesController(IHouseService houseService, IUnitOfWork unitOfWork)
            : base(houseService, unitOfWork)
        { }

        // GET: api/Services/
        public ServicesSummaryViewModel Get()
        {
            return new ServicesSummaryViewModel(this.CurrentHouse());
        }
    }
}
