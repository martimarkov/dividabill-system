﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DividaBill.DAL.Interfaces;
using System.Web.Http;
using DividaBill.Client.ViewModels.Tenant;
using Microsoft.Owin.Security;
using DividaBill.Models;
using Microsoft.AspNet.Identity;
using DividaBill.Services;
using DividaBill.Services.Interfaces;

namespace DividaBill.Client.Controllers.Api.Tenant
{
    [Authorize]
    public class PaymentController : BaseApiController
    {
        public PaymentController(IHouseService houseService, IUnitOfWork unitOfWork)
            : base(houseService, unitOfWork)
        { }

        //GET: Api/Payment
        public PaymentViewModel Get()
        {
            return new PaymentViewModel(CurrentUser);
        }
    }
}