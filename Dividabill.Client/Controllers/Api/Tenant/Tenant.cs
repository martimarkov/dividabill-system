﻿using DividaBill.DAL.Interfaces;
using DividaBill.Client.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Microsoft.Owin.Security;
using DividaBill.Services;
using Microsoft.AspNet.Identity;
using DividaBill.Models;
using DividaBill.Services.Interfaces;

namespace DividaBill.Client.Controllers.Api
{
    [Authorize]
    public class TenantController : BaseApiController
    {
        public TenantController(IHouseService houseService, IUnitOfWork unitOfWork)
            : base(houseService, unitOfWork)
        { }

        //GET: api/tenant
        public TenantViewModel Get()
        {
            return new TenantViewModel(CurrentUser);
        }
    }
}