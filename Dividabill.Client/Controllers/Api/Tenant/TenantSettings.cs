﻿using DividaBill.DAL.Interfaces;
using DividaBill.Client.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity;
using DividaBill.Models;
using DividaBill.Services;
using DividaBill.Services.Interfaces;

namespace DividaBill.Client.Controllers.Api.Tenant
{
    [Authorize]
    public class TenantSettingsController : BaseApiController
    {
        public TenantSettingsController(IHouseService houseService, IUnitOfWork unitOfWork)
            : base(houseService, unitOfWork)
        { }

        //GET: Api/TenantSettings
        public TenantSettingsViewModel Get()
        {
            return new TenantSettingsViewModel(CurrentUser);
        }

        public void Post(TenantSettingsViewModel model)
        {
            Console.WriteLine("POLUCHIH POSTA BRAT");
        }
    }
}