﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using DividaBill.DAL.Interfaces;
using DividaBill.Client.ViewModels;
using Microsoft.Owin.Security;
using DividaBill.Services;
using DividaBill.Services.Interfaces;

namespace DividaBill.Client.Controllers.Api.House
{
    [Authorize]
    public class HouseTenantsController : BaseApiController
    {
        public HouseTenantsController(IHouseService houseService, IUnitOfWork unitOfWork)
            : base(houseService, unitOfWork)
        { }

        //GET: api/housetenant
        public HouseTenantsViewModel Get()
        {
            return new HouseTenantsViewModel(this.CurrentHouse());
        }
    }
}