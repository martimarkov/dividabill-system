﻿using DividaBill.DAL.Interfaces;
using DividaBill.Services;
using DividaBill.Client.ViewModels;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using DividaBill.Services.Interfaces;

namespace DividaBill.Client.Controllers.Api
{
    [Authorize]
    public class HouseController : BaseApiController
    {
        public HouseController(IHouseService houseService, IUnitOfWork unitOfWork)
            : base(houseService, unitOfWork)
        { }

        //GET: api/house
        public async Task<HouseViewModel> Get()
        {
            return new HouseViewModel(CurrentHouse());
        }
    }
}