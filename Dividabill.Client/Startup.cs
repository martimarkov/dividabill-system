﻿using Microsoft.Owin;
using Microsoft.Practices.Unity;
using Owin;

[assembly: OwinStartupAttribute(typeof(DividaBill.Client.Startup))]
namespace DividaBill.Client
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }

    static class UnityExt
    {
        public static T Resolve<T>(this IUnityContainer container)
        {
            return (T)container.Resolve(typeof(T), nameof(T));
        }
    }
}
