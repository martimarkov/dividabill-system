﻿using DividaBill.Models;
using DividaBill.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DividaBill.Client.ViewModels.Services
{
    /// <summary>
    /// Displays billing data for a given service to the user. 
    /// </summary>
    public class BillingViewModel
    {
        public string LastPayment { get; set; } = "N/A";
        public string NextPayment { get; set; } = "N/A";

        private BillingViewModel() { }

        public BillingViewModel(HouseModel house, ServiceType ty)
        {
            var lastPaidBill = house.GetActiveContract(ty)?.Bills
                .Where(b => b.IsPaid)
                .OrderByDescending(b => b.StartDate)
                .FirstOrDefault();

            var firstUnpaidBill = house.GetActiveContract(ty)?.Bills
                .Where(b => !b.IsPaid)
                .OrderBy(b => b.StartDate)
                .FirstOrDefault();

            if (lastPaidBill != null)
                LastPayment = "£" + lastPaidBill.Ammount;

            if (firstUnpaidBill != null)
                LastPayment = "£" + firstUnpaidBill.Ammount;
        }
    }
}