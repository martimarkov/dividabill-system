﻿using DividaBill.Models;
using DividaBill.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DividaBill.Client.ViewModels
{
    /// <summary>
    /// Displays a bill to the user. 
    /// </summary>
    public class BillViewModel
    {
        public int ID { get; set; }
        public String Name { get; set; }
        public String BillNo { get; set; }
        public DateTime Date { get; set; }
        public string Status { get; set; }
        public double Amount { get; set; }

        public string Service { get; set; }


        private BillViewModel() { }

        public BillViewModel(IUtilityBill bill, ServiceType ty)
        {
            //TODO: fix end date
            ID = bill.Id;
            Name = bill.StartDate + " " + bill.StartDate.AddMonths(1);
            BillNo = bill.Id.ToString();
            Date = bill.PaymentDate;
            Status = "wtf";
            Amount = (double)bill.Ammount;

            Service = ty.Name;
        }
    }
}