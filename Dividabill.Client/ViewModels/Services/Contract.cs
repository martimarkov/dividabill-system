﻿using DividaBill.Models;

namespace DividaBill.Client.ViewModels
{
    /// <summary>
    ///     Displays info about the given service contract
    /// </summary>
    public class ContractViewModel
    {
        private ContractViewModel()
        {
        }

        public ContractViewModel(Contract c, ServiceType ty)
        {
            StartDate = c.RequestedStartDate.Date.ToShortDateString();
            EndDate = c.EndDate.Date.ToShortDateString();
            Status = c.SubmissionStatus.ToString();

            Service = ty.ToString();
        }

        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Status { get; set; }

        public string Service { get; set; }
    }
}