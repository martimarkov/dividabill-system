﻿using DividaBill.Models;
using DividaBill.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DividaBill.Client.ViewModels.Services
{
    /// <summary>
    /// Displays info for the given service. 
    /// </summary>
    public class ServiceViewModel
    {
        public bool IsActive { get; set; } = false;

        public string EstimatedBill { get; set; } = "N/A";

        public string BillStatus { get; set; } = "N/A";

        private ServiceViewModel() { }

        public ServiceViewModel(HouseModel house, ServiceType ty)
        {
            var contract = house.GetRequestedContract(ty);

            if (contract == null)
                return;

            var lastBill = house.Contracts
                .Where(c => c.ServiceNew == ty)
                .SelectMany(c => c.Bills)
                .OrderByDescending(b => b.StartDate)
                .FirstOrDefault();

            IsActive = contract.IsRequested(DateTime.Now);
            BillStatus = contract.IsActive(DateTime.Now) ? "Active" : "Pending";

            if (lastBill != null)
            {
                EstimatedBill = "£" + lastBill.Ammount;
            }

        }
    }
}