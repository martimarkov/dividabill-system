﻿using DividaBill.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DividaBill.Client.ViewModels
{
    public class HouseTenantsViewModel
    {
        public int TenantCount
        {
            get { return Tenants.Count(); }
        }

        public IEnumerable<HouseTenantViewModel> Tenants; 

        //private HouseTenantsViewModel() { }

        public HouseTenantsViewModel(HouseModel house)
        {
            Tenants = house.Tenants.Select(t => new HouseTenantViewModel(t));
        }

    }

    public class HouseTenantViewModel
    {
        public string FullName { get; set; }

        public string StartDate { get; set; }
        public string EndDate { get; set; }

        public string LastPaymentAmount { get; set; }

        public string PaymentStatus { get; set; }

        private HouseTenantViewModel() { }

        public HouseTenantViewModel(User u)
        {
            FullName = u.FullName;
            StartDate = u.StartDate.ToShortDateString();
            EndDate = u.EndDate.ToShortDateString();

            LastPaymentAmount = "???";
            PaymentStatus = "???";
        }
    }
}