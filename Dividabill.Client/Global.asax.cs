﻿using DividaBill.DAL.Interfaces;
using DividaBill.Client.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Microsoft.Practices.Unity;
using DividaBill.DAL;
//using Elmah.Contrib.WebApi;

namespace DividaBill.Client
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {

            //GlobalConfiguration.Configuration.Filters.Add(new ElmahHandleErrorApiAttribute());

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);


            //ControllerHelper.Init(UnityConfig.Container.Resolve<IUnitOfWork>());
        }
    }
}
