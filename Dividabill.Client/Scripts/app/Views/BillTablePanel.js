﻿/*
    Displays the 'Bills' page for a service. 
*/

(function (App) {
    'use strict';

    var BillTablePanel = Backbone.Marionette.LayoutView.extend({
        template: '#bill-table-tpl',

        regions: {
            BillsTable: "#bill-table",
            SearchOptions: "#search-options",
        },

        initialize: function (options) {
            this.billCollection = options.model;
            options.model.fetch();
        },

        onShow: function () {
            if (this.billCollection == null)
                return;

            var billTableView = new App.View.BillTableView({
                collection: this.billCollection
            });
            this.BillsTable.show(billTableView);
            //this.SearchOptions.show(new App.View.BillSearchOptions());
        }
    });


    App.View.BillTablePanel = BillTablePanel;

})(window.App);