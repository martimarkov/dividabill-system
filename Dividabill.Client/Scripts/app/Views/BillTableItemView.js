﻿(function (App) {
    'use strict';

    var BillTableItemView = Backbone.Marionette.ItemView.extend({
        template: '#table-item-tpl',
        tagName: 'tr'
    });



    App.View.BillTableItemView = BillTableItemView;

})(window.App);
