﻿(function (App) {
    'use strict';

    var HouseAccount = Backbone.Marionette.ItemView.extend({
        template: '#house-account-tpl',

        events: {
            'click .front': 'flipFront',
            'click .backButton': 'flipBack',
            'click #changeOfService': 'changeOfService',
            'click #tenantManagement': 'tenantManagement',
            /*'click #houseEmails': 'houseEmails',*/
        },
        initialize: function () {
            this.model = new App.Model.House();
            this.model.fetch().done(this.render);
        },

        onRender: function () {
            $('#card', this.$el).flip({
                trigger: 'manual'
            });
        },

        onShow: function () {
            console.log("House tile show");
        },

        flipFront: function (e) {
            this.$el.find('#card').flip(true);
        },

        flipBack: function (e) {
            this.$el.find('#card').flip(false);
        },

        changeOfService: function () {
            App.vent.trigger("show:houseSettings");
        },

        tenantManagement: function () {
            App.vent.trigger('show:houseTenants');
        }

    });

    App.View.HouseTile = HouseAccount;
})(window.App);
