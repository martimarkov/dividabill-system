﻿/*
    Displays all individual pages for a service:
    bills, usage, contract, billing
*/

(function (App) {
    'use strict';

    var _this;

    var BillView = Backbone.Marionette.LayoutView.extend({

        template: '#service-page-tpl',

        className: "white-bg",

        events: {
            'click .bills': 'showBills',
            //'click .usage': 'showUsage',
            'click .contract': 'showContract',
            'click .billing': 'showBilling'
        },

        regions: {
            PageHeader: "#page-header",
            SecondNav: "#second-nav",
            PageItem: "#page-item"
        },

        initialize: function () {
        },

        onShow: function () {
            _this = this;
            this.SecondNav.show(new App.View.BillSecondNav());
            this.PageHeader.show(new App.View.PageHeader({
                backRoute: 'Services',
                title: this.headerTitle,
            }));

            //this.showBills();

            App.Router.navigate('Services/' + this.headerTitle);
            this.showBills();

        },

        showBills: function (e) {
            this.PageItem.empty();
            var billView = new App.View.BillTablePanel({
                model: this.modelPrototype(),
            });

            this.PageItem.show(billView);
            return false;
        },

        showUsage: function (e) {
            e.preventDefault();
            this.PageItem.empty();

            this.PageItem.show(new App.View.Usage());
        },

        showContract: function (e) {
            e.preventDefault();
            this.PageItem.empty();

            var contract = new App.Model.Contract({ id: this.serviceId }); 
            this.PageItem.show(new App.View.Contract({ model: contract }));
        },

        showBilling: function (e) {
            e.preventDefault();
            this.PageItem.empty();

            var billing = new App.Model.Billing({ id: this.serviceId });
            this.PageItem.show(new App.View.Billing({ model: billing }));
        }
    });


    App.View.ElBillView = BillView.extend({
        serviceId: '1',
        headerTitle: 'Electricity',
        modelPrototype: function () { return new App.Model.ElectricityBillCollection() }
    });

    App.View.GasBillView = BillView.extend({
        serviceId: '2',
        headerTitle: "Gas",
        modelPrototype: function () { return new App.Model.GasBillCollection() }
    });

    App.View.WaterBillView = BillView.extend({
        serviceId: '3',
        headerTitle: "Water",
        modelPrototype: function () { return new App.Model.WaterBillCollection() }
    });


    App.View.BbBillView = BillView.extend({
        serviceId: '4',
        headerTitle: "Broadband",
        modelPrototype: function () { return new App.Model.BbBillCollection() }
    });

    App.View.SkyBillView = BillView.extend({
        serviceId: '6',
        headerTitle: "Sky TV",
        modelPrototype: function () { return new App.Model.SkyBillCollection() }
    });

    App.View.TvBillView = BillView.extend({
        serviceId: '7',
        headerTitle: "Tv License",
        modelPrototype: function () { return new App.Model.TvBillCollection() }
    });

    App.View.PhoneBillView = BillView.extend({
        serviceId: '8',
        headerTitle: "Phone", 
        modelPrototype: function () { return new App.Model.PhoneBillCollection() }
    });

})(window.App);