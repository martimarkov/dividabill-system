﻿(function (App) {
    'use strict';

    var Billing = Backbone.Marionette.ItemView.extend({
        template: '#billing-item-tpl',
        model: App.Model.Billing,
        className: 'gas-body-full',

        initialize: function () {
            console.log("Init ElBilling");
            var that = this;
            var fetching = this.model.fetch();

            fetching.done(function () { that.render(); });
        }
    });

    App.View.Billing = Billing;

})(window.App);
