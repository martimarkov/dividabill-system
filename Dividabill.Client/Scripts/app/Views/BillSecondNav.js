﻿(function (App) {
    'use strict';

    var BillSecondNav = Backbone.Marionette.ItemView.extend({
        template: '#billing-contract-usage-nav-tpl',
        tagName: 'nav',
        className: 'col-lg-12 water-nav',

        initialize: function () {
            console.log("Init SecondNav");
        }
    });

    App.View.BillSecondNav = BillSecondNav;

})(window.App);
