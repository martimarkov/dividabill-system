﻿(function (App) {
    'use strict';

    var HouseSettings = Backbone.Marionette.LayoutView.extend({
        template: '#tenant-settings-tpl',
        events: {
            'click #changeService': 'showChangeOfService',
            'click #tenantManagement': 'showTenantManagement',
        },
        regions: {
            PageHeader: "#page-header",
            PageItem: "#page-item"
        },
        initialize: function () {
            this.menu = new App.Model.PageHeaderMenu([
                {
                    linkTitle: 'Change of Service',
                    id: 'changeService'
                },
                {
                    linkTitle: 'Tenant Manangement',
                    id: 'tenantManagement'
                }
            ]);

            console.log("Init House Settings");
        },

        showChangeOfService: function () {
            this.$el.find('ul li .active').removeClass("active");
            this.$el.find('#changeService').addClass("active");

            this.PageItem.show(new App.View.ChangeService());

            //var tenantView = new App.View.TenantAccount();

            App.Router.navigate('House/Settings')
        },


        showTenantManagement: function () {
            this.$el.find('ul li .active').removeClass("active");
            this.$el.find('#tenantManagement').addClass("active");

            this.PageItem.show(new App.View.TenantManagement());

            App.Router.navigate('House/Tenants')
        },

        onShow: function () {
            console.log("TenantSettings show");
            $("main").addClass("white-bg");
            this.PageHeader.show(new App.View.PageHeader({
                title: "House Account",
                menu: this.menu,
                backRoute: '#',
            }));
        },

    });

    App.View.HouseSettings = HouseSettings;
})(window.App);
