﻿(function (App) {
    'use strict';

    var _this;

    var serviceViews =
    {
        "electricity": function () { return new App.View.ElBillView(); },
        "water": function () { return new App.View.WaterBillView(); },
        "gas": function () { return new App.View.GasBillView(); },
        "broadband": function () { return new App.View.BbBillView(); },

        "sky tv": function () { return new App.View.SkyBillView(); },
        "tv license": function () { return new App.View.TvBillView(); },
        "phone": function () { return new App.View.PhoneBillView(); },
    };

    var MainWindow = Backbone.Marionette.LayoutView.extend({
        template: '#main-window-tpl',

        id: 'main-window',

        regions: {
            Header: '#header',
            Content: '#content'
        },

        initialize: function () {
            _this = this;

            //// Application events
            App.vent.on('show:dashboard', this.showDashboard);


            // tenant pages
            App.vent.on('show:paymentDetails', this.showPaymentDetails);
            App.vent.on('show:tenantAccount', this.showTenantDetails);

            //House pages
            App.vent.on("show:houseSettings", this.showHouseSettings);
            App.vent.on("show:houseTenants", this.showHouseTenants);

            // services pages
            App.vent.on('show:servicesDashboard', this.showServicesDashboard);
            App.vent.on("show:showService", this.showService);


            console.log("Main window init")
        },
        
        onShow: function () {
            console.log("Main window show");
            //this.showDashboard();
            $('[data-toggle="tooltip"]').tooltip();

        },

        showDashboard: function () {
            _this.Content.empty();
            _this.Content.show(new App.View.Dashboard());
        },

        showService: function(srvc) {
            var viewProto = serviceViews[srvc];

            if(viewProto)
            {
                var view = viewProto();
                _this.Content.empty();
                _this.Content.show(view);
            }
        },


        // tenant 
        showTenantDetails: function () {
            _this.Content.empty();

            var tenantPanel = new App.View.TenantSettings();
            _this.Content.show(tenantPanel);

            tenantPanel.showTenantAccount();
        },

        showPaymentDetails: function () {
            _this.Content.empty();

            var tenantPanel = new App.View.TenantSettings();
            _this.Content.show(tenantPanel);

            tenantPanel.showPaymentDetails();
        },

        //house 
        showHouseSettings: function () {
            _this.Content.empty();

            var housePanel = new App.View.HouseSettings();
            _this.Content.show(housePanel);

            housePanel.showChangeOfService();
        },

        showHouseTenants: function () {
            _this.Content.empty();

            var housePanel = new App.View.HouseSettings();
            _this.Content.show(housePanel);

            housePanel.showTenantManagement();
        },

        //services
        showServicesDashboard: function () {

            _this.Content.empty();
            _this.Content.show(new App.View.ServicesDashboard());
        },
    });

    App.View.MainWindow = MainWindow;
})(window.App);
