﻿(function (App) {
    'use strict';

    var BillTableView = Backbone.Marionette.CompositeView.extend({
        template: '#table-tpl',
        className: 'water-table-section col-lg-12',
        tagName: 'section',

        childView: App.View.BillTableItemView,
        childViewContainer: "tbody",

        initialize: function () {
            console.log("Init TableView");
        },
    });


    App.View.BillTableView = BillTableView;

})(window.App);
