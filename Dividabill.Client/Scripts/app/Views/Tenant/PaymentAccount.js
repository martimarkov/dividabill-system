﻿(function (App) {
    'use strict';

    var PaymentAccount = Backbone.Marionette.ItemView.extend({
        template: '#paydetails-tpl',

        initialize: function () {
            console.log("Show Payment Details");
        }
    });

    App.View.PaymentAccount = PaymentAccount;

})(window.App);
