﻿(function (App) {
    'use strict';

    var PaymentDetails = Backbone.Marionette.LayoutView.extend({
        template: '#payment-details-tpl',
        events:{ 
            'click #edit-paydetails-btn': 'showEditPaymentDetails',
            'click #cancel-paydetails': 'showPaymentAccount'
        },
        regions: {
            TenantAccountDetails: '#tenant_paydetails',
        },
        initialize: function () {
            console.log("Init PaymentDetails");
            var that = this;
            this.model = new App.Model.PaymentDetails();
            this.model.fetch().done(function () {
                that.render();
                that.onShow();
            });
        },

        showEditPaymentDetails: function () {
            var editPayAcc = new App.View.EditPaymentDetails({model: this.model})
            this.TenantAccountDetails.show(editPayAcc);


        },
        showPaymentAccount: function () {
            var payAcc = new App.View.PaymentAccount({ model: this.model});
            this.TenantAccountDetails.show(payAcc);
            
        },

        onShow: function () {
            console.log("PaymentDetails show");
            var payAccDet = new App.View.PaymentAccount({model: this.model})
            this.TenantAccountDetails.show(payAccDet);
        }

    });

    App.View.PaymentDetails = PaymentDetails;
})(window.App);
