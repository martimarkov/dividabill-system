﻿(function (App) {
    'use strict';

    var EditTenantDetails = Backbone.Marionette.ItemView.extend({
        template: '#edit-details-tpl',

        initialize: function () {
            console.log("Show Edit Details");
        }
    });

    App.View.EditTenantDetails = EditTenantDetails;

})(window.App);
