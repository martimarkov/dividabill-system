﻿(function (App) {
    'use strict';

    var TenantAccount = Backbone.Marionette.LayoutView.extend({
        template: '#tenant-account-tpl',
        events: {
            'click #edit-details-btn': 'showEditDetails',
            'click #cancel-edit': 'showTenantMainDetails',
        },
       regions:{
           PageItem: "#tenant-details",
        },
        initialize: function () {

            //load the model
            var that = this;
            this.model = new App.Model.TenantSettings();
            this.model.fetch().done(function () {
                that.render();
                that.onShow();
            });


            console.log("Init TenantAccount");
        },

        showEditDetails: function () {
            var editDetails = new App.View.EditTenantDetails({ model: this.model });
            this.PageItem.show(editDetails);

        },
        showTenantMainDetails: function ( ){
            var tenantDetails = new App.View.TenantDetails({ model: this.model });
            this.PageItem.show(tenantDetails);
        },
        
        onShow: function () {
            console.log("TenantAccount show");
            var tenantDetails = new App.View.TenantDetails({ model: this.model});
            this.PageItem.show(tenantDetails);
        }

    });

    App.View.TenantAccount = TenantAccount;
})(window.App);
