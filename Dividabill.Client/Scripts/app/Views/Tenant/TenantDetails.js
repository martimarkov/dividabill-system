﻿(function (App) {
    'use strict';

    var TenantDetails = Backbone.Marionette.ItemView.extend({
        template: '#tenant-details-tpl',

        initialize: function () {
            console.log("Init tenant Details");
           
        }
    });

    App.View.TenantDetails = TenantDetails;

})(window.App);
