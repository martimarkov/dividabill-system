﻿(function (App) {
    'use strict';

    var TenantSettings = Backbone.Marionette.LayoutView.extend({
        template: '#tenant-settings-tpl',
        events: {
            'click #tenantAccount': 'showTenantAccount',
            'click #paymentDetails': 'showPaymentDetails',
        },
        regions: {
            PageHeader: "#page-header",
            PageItem: "#page-item"
        },
        initialize: function () {
            this.menu = new App.Model.PageHeaderMenu([
                {
                    linkTitle: 'Tenant Account',
                    id: 'tenantAccount'
                },
                {
                    linkTitle: 'Payment Details',
                    id: 'paymentDetails'
                }
            ]);

            console.log("Init TenantSettings");
        },

        showTenantAccount: function () {
            this.$el.find('ul li .active').removeClass("active");
            this.$el.find('#tenantAccount').addClass("active");

            this.PageItem.show(new App.View.TenantAccount());

            //var tenantView = new App.View.TenantAccount();

            App.Router.navigate('Tenant/Account')
        },


        showPaymentDetails: function () {
            this.$el.find('ul li .active').removeClass("active");
            this.$el.find('#paymentDetails').addClass("active");

            this.PageItem.show(new App.View.PaymentDetails());

            App.Router.navigate('Tenant/Payment')
        },


        showPaymentHistory: function () {
            this.$el.find('ul li .active').removeClass("active");
            this.$el.find('#paymentDetails').addClass("active");

            this.PageItem.show(new App.View.PaymentHistory());

            App.Router.navigate('Tenant/History')
        },

        onShow: function () {
            console.log("TenantSettings show");
            $("main").addClass("white-bg");
            this.PageHeader.show(new App.View.PageHeader({
                title: "Tenant Settings",
                menu: this.menu,
                backRoute: '#',
            }));
        },

    });

    App.View.TenantSettings = TenantSettings;
})(window.App);
