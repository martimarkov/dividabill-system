﻿(function (App) {
    'use strict';


    var myController = {
        
        getTheApp: _.once(function() {
            return new App.View.MainWindow();
        }),

        initApp: function () {
            try {
                App.Window.show(this.getTheApp());
            } catch (e) {
                console.error('Couldn\'t start app: ', e, e.stack);
            }
        },

        showIndex: function () {
            this.initApp();
            App.vent.trigger('show:dashboard');
        },


        //tenant
        showTenantPayment: function () {
            this.initApp();
            App.vent.trigger('show:paymentDetails');
        },

        showTenantAccount: function () {
            this.initApp();
            App.vent.trigger('show:tenantAccount');
        },

        showTenantHistory: function () {
            //TODO
            //this.initApp();
            //App.vent.trigger('show:tenantAccount');
        },


        //services
        showServicesDashboard: function () {
            this.initApp();
            App.vent.trigger('show:servicesDashboard');
        },

        showService: function (service) {
            this.initApp();
            App.vent.trigger("show:showService", service.toLowerCase());
        },


        //house
        showHouseSettings: function () {
            this.initApp();
            App.vent.trigger('show:houseSettings');
        },

        showHouseTenants: function () {
            this.initApp();
            App.vent.trigger('show:houseTenants');
        },
    };

    var MyRouter = new Marionette.AppRouter({
        controller: myController,
        appRoutes: {
            "": "showIndex",
            
            //tenant
            "Tenant/Payment": "showTenantPayment",
            "Tenant/Account": "showTenantAccount",
            "Tenant/History": "showTenantHistory",

            //house
            "House/Settings": "showHouseSettings",
            "House/Tenants": "showHouseTenants",

            //services
            "Services": "showServicesDashboard",
            "Services/:service": "showService",
        },
    });


    App.Router = MyRouter;

})(window.App);
