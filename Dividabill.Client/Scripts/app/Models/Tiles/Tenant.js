﻿(function (App) {
    'use strict';

    App.Model.Tenant = Backbone.Model.extend({
        url: "/api/tenant",
        defaults: {
            "StartDate": "???",
            "EndDate": "???",
            "Duration": "???",
        }
    });
})(window.App);