﻿(function (App) {
    'use strict';

    App.Model.Services = Backbone.Model.extend({
        url: "/api/services",
        defaults: {
            "LastBillPayment": "...",
            "CurrentBill": "...",
            "PreviousBill": "...",
        }
    });
})(window.App);