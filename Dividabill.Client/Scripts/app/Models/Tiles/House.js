﻿(function (App) {
    'use strict';

    App.Model.House = Backbone.Model.extend({
        url: "/api/house",
        defaults: {
            "CreationDate": "...",
            "StartDate": "...",
            "EndDate": "...",
            "Services": ["..."],
            "ServiceInitials": ["..."],
            "TenantCount": "...",
            "Tenants": null,
        }
    });
})(window.App);