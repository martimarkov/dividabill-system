﻿(function (App) {
    'use strict';

    //DB Data - 
    var PaymentDetails = Backbone.Model.extend({
        urlRoot: '/api/Payment',
        defaults: {
            "LastPayment": "...",
            "NextPayment": "...",
            "AccountNumber" : "...",
            "AccountSortCode" : "...",
            "PaymentMethod" : "...",
            "PaymentType" : "..."          
        }
    });

    App.Model.PaymentDetails = PaymentDetails;
})(window.App);