﻿(function (App) {
    'use strict';

    App.Model.TenantSettings = Backbone.Model.extend({
        url: "/api/tenantsettings",
        defaults: {
            "FullName": "...",
            "EMail": "...",
            "PhoneNumber": "...",
            "StartDate": "...",
            "EndDate": "...",
        }
    });
})(window.App);