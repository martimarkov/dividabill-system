﻿(function (App) {
    'use strict';

    var PageHeaderMenu = Backbone.Collection.extend({
        model: App.Model.MenuItem
    })

    App.Model.PageHeaderMenu = PageHeaderMenu;
})(window.App);