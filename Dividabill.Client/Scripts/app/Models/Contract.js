﻿(function (App) {
    'use strict';

    //DB Data - 
    var Contract = Backbone.Model.extend({
        urlRoot: '/api/Contract',
        defaults: {
            "Status": "",
            "StartDate": "",
            "EndDate": ""
        }
    });

    App.Model.Contract = Contract;
})(window.App);