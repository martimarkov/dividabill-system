﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dividabill.Tests.Core;
using DividaBill.DAL;
using DividaBill.DAL.Interfaces;
using DividaBill.Models;
using DividaBill.Services.Exceptions;
using DividaBill.Services.Impl;
using NUnit.Framework;

namespace DividaBill.Services.Tests.Impl
{
    [TestFixture]
    public class CouponServiceTests : BaseAutoMock<CouponService>
    {
        [Test]
        public void GivenExistingCouponInDB_WhenGetInvoked_ThenCouponReturned()
        {
            var coupon = new Coupon
            {
                Code = Guid.NewGuid().ToString()
            };

            var couponRepository = GetMock<GenericRepository<Coupon, User, IApplicationContext>>();
            couponRepository.Setup(x => x.Get()).Returns(new List<Coupon> {coupon}.AsQueryable());

            GetMock<IUnitOfWork>().Setup(x => x.CouponRepository).Returns(couponRepository.Object);

            var result = ClassUnderTest.Get(coupon.Code);

            Assert.AreSame(coupon, result);
        }

        [Test]
        public void GivenNonExistingCouponInDB_WhenGetInvoked_ThenCouponNotFoundExceptionThrown()
        {
            var couponCode = Guid.NewGuid().ToString();

            var couponRepository = GetMock<GenericRepository<Coupon, User, IApplicationContext>>();
            couponRepository.Setup(x => x.Get()).Returns(new List<Coupon>().AsQueryable());

            GetMock<IUnitOfWork>().Setup(x => x.CouponRepository).Returns(couponRepository.Object);

            var exception = Assert.Throws<CouponNotFound>(() => ClassUnderTest.Get(couponCode));
            Assert.AreEqual(couponCode, exception.Message);
        }
    }
}