﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dividabill.Tests.Core;
using DividaBill.DAL;
using DividaBill.DAL.Interfaces;
using DividaBill.DAL.Unity;
using DividaBill.Models;
using DividaBill.Models.GoCardless;
using DividaBill.Services.Exceptions;
using DividaBill.Services.Factories;
using DividaBill.Services.Impl;
using DividaBill.Services.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.AutoMock;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace DividaBill.Services.Tests.Impl
{
    [TestFixture]
    public class BankMandateServiceTests : BaseAutoMock<BankMandateService>
    {
        [Test]
        public void GivenAnyBankMandate_WhenInsertAndSaveInvoked_ThenInsertAndSaveFunctionsInvoked()
        {
            var entityToInsert = new BankMandate();

            var bankMandateRepository = GetMock<GenericRepository<BankMandate, User, IApplicationContext>>();
            GetMock<IUnitOfWork>().Setup(x => x.BankMandateRepository).Returns(bankMandateRepository.Object);

            ClassUnderTest.InsertAndSave(entityToInsert);

            bankMandateRepository.Verify(x => x.Insert(entityToInsert), Times.Once);
            bankMandateRepository.Verify(x => x.SaveChanges(), Times.Once);
        }

        [Test]
        public void GivenAnyBankMandate_WhenUpdateAndSaveInvoked_ThenUpdateAndSaveFunctionsInvoked()
        {
            var entityToUpdate = new BankMandate();

            var bankMandateRepository = GetMock<GenericRepository<BankMandate, User, IApplicationContext>>();
            GetMock<IUnitOfWork>().Setup(x => x.BankMandateRepository).Returns(bankMandateRepository.Object);

            ClassUnderTest.UpdateAndSave(entityToUpdate);

            bankMandateRepository.Verify(x => x.Update(entityToUpdate), Times.Once);
            bankMandateRepository.Verify(x => x.SaveChanges(), Times.Once);
        }

        [Test]
        public void GivenAnyBankMandateForInsert_WhenInsertInvoked_ThenDefaultPropertiesSet()
        {
            var entityToInsert = new BankMandate();

            var bankMandateRepository = GetMock<GenericRepository<BankMandate, User, IApplicationContext>>();
            GetMock<IUnitOfWork>().Setup(x => x.BankMandateRepository).Returns(bankMandateRepository.Object);
            bankMandateRepository.Setup(x => x.Insert(entityToInsert)).Returns(entityToInsert);

            var result = ClassUnderTest.Insert(entityToInsert);

            Assert.NotNull(result.CreatedAt);
            Assert.NotNull(result.LastModified);
        }

        [Test]
        public void GivenAnyBankMandateForInsert_WhenInsertInvoked_ThenInsertInvoked()
        {
            var entityToInsert = new BankMandate();

            var bankMandateRepository = GetMock<GenericRepository<BankMandate, User, IApplicationContext>>();
            GetMock<IUnitOfWork>().Setup(x => x.BankMandateRepository).Returns(bankMandateRepository.Object);

            ClassUnderTest.Insert(entityToInsert);

            bankMandateRepository.Verify(x => x.Insert(entityToInsert), Times.Once);
        }

        [Test]
        public void GivenAnyBankMandateForUpdate_WhenGetActiveInvoked_ThenUpdateInvoked()
        {
            var bankMandateRepository = GetMock<GenericRepository<BankMandate, User, IApplicationContext>>();
            GetMock<IUnitOfWork>().Setup(x => x.BankMandateRepository).Returns(bankMandateRepository.Object);

            ClassUnderTest.GetActive();

            bankMandateRepository.Verify(x => x.Get(), Times.Once);
        }

        [Test]
        public void GivenAnyBankMandateForUpdate_WhenUpdateInvoked_ThenDefaultPropertiesSet()
        {
            var entityToUpdate = new BankMandate();

            var bankMandateRepository = GetMock<GenericRepository<BankMandate, User, IApplicationContext>>();
            GetMock<IUnitOfWork>().Setup(x => x.BankMandateRepository).Returns(bankMandateRepository.Object);
            bankMandateRepository.Setup(x => x.Update(entityToUpdate)).Returns(entityToUpdate);

            var result = ClassUnderTest.Update(entityToUpdate);

            Assert.NotNull(result.LastModified);
        }

        [Test]
        public void GivenAnyBankMandateForUpdate_WhenUpdateInvoked_ThenUpdateInvoked()
        {
            var entityToUpdate = new BankMandate();

            var bankMandateRepository = GetMock<GenericRepository<BankMandate, User, IApplicationContext>>();
            GetMock<IUnitOfWork>().Setup(x => x.BankMandateRepository).Returns(bankMandateRepository.Object);

            ClassUnderTest.Update(entityToUpdate);

            bankMandateRepository.Verify(x => x.Update(entityToUpdate), Times.Once);
        }

        [Test]
        public void GivenAsyncCallingAndMocked_WhenGetActiveInvoked_ThenGetOfAsyncUnitOfWorkInvoked()
        {
            var bankMandateRepository = GetMock<GenericRepository<BankMandate, User, IApplicationContext>>();
            var mocker = new AutoMocker();
            var unitOfWorkAsync = mocker.GetMock<IUnitOfWork>();
            unitOfWorkAsync.Setup(x => x.BankMandateRepository).Returns(bankMandateRepository.Object);

            ClassUnderTest.GetActive(new AsyncExecutionParam(unitOfWorkAsync.Object));

            bankMandateRepository.Verify(x => x.Get(), Times.Once);
        }

        [Test]
        public void GivenAsyncCallingAndMocked_WhenInsertInvoked_ThenInsertOfAsyncUnitOfWorkInvoked()
        {
            var entityToInsert = new BankMandate();

            var bankMandateRepository = GetMock<GenericRepository<BankMandate, User, IApplicationContext>>();
            var mocker = new AutoMocker();
            var unitOfWorkAsync = mocker.GetMock<IUnitOfWork>();
            unitOfWorkAsync.Setup(x => x.BankMandateRepository).Returns(bankMandateRepository.Object);

            ClassUnderTest.Insert(entityToInsert, new AsyncExecutionParam(unitOfWorkAsync.Object));

            bankMandateRepository.Verify(x => x.Insert(entityToInsert), Times.Once);
        }

        [Test]
        public void GivenAsyncCallingAndMocked_WhenUpdateInvoked_ThenUpdateOfAsyncUnitOfWorkInvoked()
        {
            var entityToUpdate = new BankMandate();

            var bankMandateRepository = GetMock<GenericRepository<BankMandate, User, IApplicationContext>>();
            var mocker = new AutoMocker();
            var unitOfWorkAsync = mocker.GetMock<IUnitOfWork>();
            unitOfWorkAsync.Setup(x => x.BankMandateRepository).Returns(bankMandateRepository.Object);

            ClassUnderTest.Update(entityToUpdate, new AsyncExecutionParam(unitOfWorkAsync.Object));

            bankMandateRepository.Verify(x => x.Update(entityToUpdate), Times.Once);
        }

        [Test]
        [ExpectedException(typeof (NullReferenceException))]
        public void GivenAsyncCallingAndNotMocked_WhenGetActiveInvoked_ThenFailed()
        {
            var bankMandateRepository = GetMock<GenericRepository<BankMandate, User, IApplicationContext>>();
            var mocker = new AutoMocker();
            var unitOfWorkAsync = mocker.GetMock<IUnitOfWork>();

            GetMock<IUnitOfWork>().Setup(x => x.BankMandateRepository).Returns(bankMandateRepository.Object);

            Assert.Throws<NullReferenceException>(() => ClassUnderTest.GetActive(new AsyncExecutionParam(unitOfWorkAsync.Object)));
        }

        [Test]
        [ExpectedException(typeof (NullReferenceException))]
        public void GivenAsyncCallingAndNotMocked_WhenGetByIdInvoked_ThenFailed()
        {
            var bankMandateRepository = GetMock<GenericRepository<BankMandate, User, IApplicationContext>>();
            var mocker = new AutoMocker();
            var unitOfWorkAsync = mocker.GetMock<IUnitOfWork>();

            GetMock<IUnitOfWork>().Setup(x => x.BankMandateRepository).Returns(bankMandateRepository.Object);

            Assert.Throws<NullReferenceException>(() => ClassUnderTest.GetById(Guid.NewGuid().ToString(), new AsyncExecutionParam(unitOfWorkAsync.Object)));
        }

        [Test]
        [ExpectedException(typeof (NullReferenceException))]
        public void GivenAsyncCallingAndNotMocked_WhenInsertInvoked_ThenFailed()
        {
            var entityToInsert = new BankMandate();

            var bankMandateRepository = GetMock<GenericRepository<BankMandate, User, IApplicationContext>>();
            var mocker = new AutoMocker();
            var unitOfWorkAsync = mocker.GetMock<IUnitOfWork>();

            GetMock<IUnitOfWork>().Setup(x => x.BankMandateRepository).Returns(bankMandateRepository.Object);

            Assert.Throws<NullReferenceException>(() => ClassUnderTest.Insert(entityToInsert, new AsyncExecutionParam(unitOfWorkAsync.Object)));
        }

        [Test]
        [ExpectedException(typeof (NullReferenceException))]
        public void GivenAsyncCallingAndNotMocked_WhenUpdateInvoked_ThenFailed()
        {
            var entityToUpdate = new BankMandate();

            var bankMandateRepository = GetMock<GenericRepository<BankMandate, User, IApplicationContext>>();
            var mocker = new AutoMocker();
            var unitOfWorkAsync = mocker.GetMock<IUnitOfWork>();

            GetMock<IUnitOfWork>().Setup(x => x.BankMandateRepository).Returns(bankMandateRepository.Object);

            Assert.Throws<NullReferenceException>(() => ClassUnderTest.Update(entityToUpdate, new AsyncExecutionParam(unitOfWorkAsync.Object)));
        }

        [Test]
        public void GivenExistedBankMandate_WhenGetByIdInvoked_ThenBankMandateReturned()
        {
            var bankMandateRepository = GetMock<GenericRepository<BankMandate, User, IApplicationContext>>();
            GetMock<IUnitOfWork>().Setup(x => x.BankMandateRepository).Returns(bankMandateRepository.Object);
            var bankMandate = new BankMandate
            {
                Id = Guid.NewGuid().ToString()
            };

            bankMandateRepository.Setup(x => x.Get()).Returns(new List<BankMandate>
            {
                bankMandate
            }.AsQueryable());

            var result = ClassUnderTest.GetById(bankMandate.Id);

            Assert.AreSame(bankMandate, result);
        }

        [Test]
        public void GivenExistedBankMandate_WhenUpdateBankMandateFromGcMandateInvoked_ThenUpdateInvokedButNotSaved()
        {
            var bankMandateId = Guid.NewGuid().ToString();
            var bankMandate = new BankMandate
            {
                Id = bankMandateId
            };

            var bankMandateRepository = GetMock<GenericRepository<BankMandate, User, IApplicationContext>>();
            bankMandateRepository.Setup(x => x.Get()).Returns(
                new List<BankMandate>
                {
                    bankMandate
                }.AsQueryable()
                );

            var unitOfWork = GetMock<IUnitOfWork>();
            unitOfWork.Setup(x => x.BankMandateRepository).Returns(bankMandateRepository.Object);

            GetMock<IGcMandateToBankMandateMappingFactory>().Setup(x => x.Map(It.IsAny<GCMandate>(), It.IsAny<BankMandate>())).Returns(bankMandate);

            var gcMandate = new GCMandate();
            var result = ClassUnderTest.UpdateBankMandateFromGcMandate(bankMandateId, gcMandate);

            GetMock<IGcMandateToBankMandateMappingFactory>().Verify(x => x.Map(gcMandate, It.IsAny<BankMandate>()), Times.Once);
            bankMandateRepository.Verify(x => x.Update(It.IsAny<BankMandate>()), Times.Once);
            bankMandateRepository.Verify(x => x.Insert(It.IsAny<BankMandate>()), Times.Never);
            bankMandateRepository.Verify(x => x.SaveChanges(), Times.Never);
        }

        [Test]
        public void GivenNotExistBankMandate_WhenInsertBankMandateFromGcMandateInvoked_ThenInsertInvokedAndSaved()
        {
            var bankMandateRepository = GetMock<GenericRepository<BankMandate, User, IApplicationContext>>();
            bankMandateRepository.Setup(x => x.Get()).Returns(new List<BankMandate>().AsQueryable());

            var unitOfWork = GetMock<IUnitOfWork>();
            unitOfWork.Setup(x => x.BankMandateRepository).Returns(bankMandateRepository.Object);

            GetMock<IGcMandateToBankMandateMappingFactory>().Setup(x => x.Map(It.IsAny<User>(), It.IsAny<GCMandate>())).Returns(new BankMandate());

            var gcMandate = new GCMandate();
            var result = ClassUnderTest.InsertAndSaveBankMandateFromGcMandate(new User(), gcMandate);

            GetMock<IGcMandateToBankMandateMappingFactory>().Verify(x => x.Map(It.IsAny<User>(), gcMandate), Times.Once);
            bankMandateRepository.Verify(x => x.Insert(It.IsAny<BankMandate>()), Times.Once);
            bankMandateRepository.Verify(x => x.SaveChanges(), Times.Once);
            bankMandateRepository.Verify(x => x.Update(It.IsAny<BankMandate>()), Times.Never);
        }

        [Test]
        [ExpectedException(typeof (BankMandateNotFound))]
        public void GivenNotExistedBankMandate_WhenGetByIdInvoked_ThenBankMandateNotFoundThrown()
        {
            var bankMandateRepository = GetMock<GenericRepository<BankMandate, User, IApplicationContext>>();
            GetMock<IUnitOfWork>().Setup(x => x.BankMandateRepository).Returns(bankMandateRepository.Object);
            bankMandateRepository.Setup(x => x.Get()).Returns(new List<BankMandate>().AsQueryable());

            Assert.Throws<BankMandateNotFound>(() => ClassUnderTest.GetById(string.Empty));
        }

        [Test]
        public void GivenNotNullBankMandate_WhenLinkOrUnlinkBankMandateToUserInvoked_ThenLinkedToTenant()
        {
            var tenant = new User();
            var bankMandate = new BankMandate
            {
                Id = Guid.NewGuid().ToString()
            };
            ClassUnderTest.LinkOrUnlinkBankMandateToUser(tenant, bankMandate);

            Assert.NotNull(tenant.MandateID);
        }

        [Test]
        public void GivenNotNullBankMandate_WhenLinkOrUnlinkBankMandateToUserInvoked_ThenTenantUpdateInvoked()
        {
            var tenant = new User();
            ClassUnderTest.LinkOrUnlinkBankMandateToUser(tenant, new BankMandate());

            GetMock<ITenantService>().Verify(x => x.Update(tenant, It.IsAny<AsyncExecutionParam>()), Times.Once);
        }

        [Test]
        public void GivenNotNullBankMandate_WhenLinkOrUnlinkBankMandateToUserInvoked_ThenUpdatedTenantReturned()
        {
            var tenant = new User();

            var tenantService = GetMock<ITenantService>();
            tenantService.Setup(x => x.Update(It.IsAny<User>(), It.IsAny<AsyncExecutionParam>())).Returns(tenant);

            var result = ClassUnderTest.LinkOrUnlinkBankMandateToUser(tenant, new BankMandate());

            Assert.AreSame(tenant, result);
        }

        [Test]
        public void GivenNullBankMandate_WhenLinkOrUnlinkBankMandateToUserInvoked_ThenUnLinkedFromTenant()
        {
            var tenant = new User();
            ClassUnderTest.LinkOrUnlinkBankMandateToUser(tenant, null);

            Assert.Null(tenant.MandateID);
        }
    }
}