﻿using System;
using Dividabill.Tests.Core;
using DividaBill.AppConstants;
using DividaBill.Library.AppConfiguration;
using DividaBill.Services.Impl;
using Moq;
using NUnit.Framework;

namespace DividaBill.Services.Tests.Impl
{
    [TestFixture]
    public class TestDataServiceTests : BaseAutoMock<TestDataService>
    {
        [Test]
        public void GivenTestBankAccount_WhenIsTestBankAccountNumberInvoked_ThenTrueReturned()
        {
            var testAccountNumber = Guid.NewGuid().ToString();

            GetMock<IAppConfigurationReader>().Setup(x => x.GetFromAppConfig(It.IsAny<AppSettingsEnum>())).Returns(testAccountNumber);

            var result = ClassUnderTest.IsTestBankAccountNumber(testAccountNumber);

            Assert.IsTrue(result);
        }

        [Test]
        public void GivenHardCodedTestBankAccount_WhenIsTestBankAccountNumberInvoked_ThenTrueReturned()
        {
            var result = ClassUnderTest.IsTestBankAccountNumber("00000000");

            Assert.IsTrue(result);
        }

        [Test]
        public void GivenNonTestBankAccount_WhenIsTestBankAccountNumberInvoked_ThenFalseReturned()
        {
            GetMock<IAppConfigurationReader>().Setup(x => x.GetFromAppConfig(It.IsAny<string>())).Returns(Guid.NewGuid().ToString());

            var result = ClassUnderTest.IsTestBankAccountNumber(Guid.NewGuid().ToString());

            Assert.IsFalse(result);
        }

        [Test]
        public void GivenTestBankAccount_WhenIsTestBankAccountSortCodeInvoked_ThenTrueReturned()
        {
            var accountSortCode = Guid.NewGuid().ToString();

            GetMock<IAppConfigurationReader>().Setup(x => x.GetFromAppConfig(It.IsAny<AppSettingsEnum>())).Returns(accountSortCode.Replace("-", string.Empty));

            var result = ClassUnderTest.IsTestBankAccountSortCode(accountSortCode);

            Assert.IsTrue(result);
        }

        [Test]
        public void GivenHardCodedTestBankAccount_WhenIsTestBankAccountSortCodeInvoked_ThenTrueReturned()
        {
            var result = ClassUnderTest.IsTestBankAccountSortCode("000000");

            Assert.IsTrue(result);
        }

        [Test]
        public void GivenNonTestBankAccount_WhenIsTestBankAccountSortCodeInvoked_ThenFalseReturned()
        {
            GetMock<IAppConfigurationReader>().Setup(x => x.GetFromAppConfig(It.IsAny<string>())).Returns(Guid.NewGuid().ToString());

            var result = ClassUnderTest.IsTestBankAccountSortCode(Guid.NewGuid().ToString());

            Assert.IsFalse(result);
        }
    }
}