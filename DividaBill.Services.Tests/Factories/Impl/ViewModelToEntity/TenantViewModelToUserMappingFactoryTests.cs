﻿using System;
using Dividabill.Tests.Core;
using DividaBill.Models;
using DividaBill.Models.Common;
using DividaBill.Services.Factories.Impl.ViewModelToEntity;
using DividaBill.ViewModels;
using NUnit.Framework;

namespace DividaBill.Services.Tests.Factories.Impl.ViewModelToEntity
{
    [TestFixture]
    public class TenantViewModelToUserMappingFactoryTests : BaseAutoMock<TenantViewModelToUserMappingFactory>
    {
        [Test]
        public void GivenViewModel_WhenMapInvoked_ThenAccountHolderMapped()
        {
            // Arrange
            var expected = "expected";
            var viewModel = new TenantViewModel {AccountHolder = expected};
            var entity = new User {AccountHolder = string.Empty};

            // Act
            var result = ClassUnderTest.Map(viewModel, entity);

            // Assert
            Assert.AreEqual(result.AccountHolder, expected);
        }

        [Test]
        public void GivenViewModel_WhenMapInvoked_ThenAccountNumberMapped()
        {
            // Arrange
            var expected = "expected";
            var viewModel = new TenantViewModel {AccountNumber = expected};
            var entity = new User {AccountNumber = string.Empty};

            // Act
            var result = ClassUnderTest.Map(viewModel, entity);

            // Assert
            Assert.AreEqual(result.AccountNumber, expected);
        }

        [Test]
        public void GivenViewModel_WhenMapInvoked_ThenAccountSoleHolderMapped()
        {
            // Arrange
            var expected = true;
            var viewModel = new TenantViewModel {AccountSoleHolder = expected};
            var entity = new User {AccountSoleHolder = false};

            // Act
            var result = ClassUnderTest.Map(viewModel, entity);

            // Assert
            Assert.AreEqual(result.AccountSoleHolder, expected);
        }

        [Test]
        public void GivenViewModel_WhenMapInvoked_ThenAccountSortCodeMapped()
        {
            // Arrange
            var expected = "expected";
            var viewModel = new TenantViewModel {AccountSortCode = expected};
            var entity = new User {AccountSortCode = string.Empty};

            // Act
            var result = ClassUnderTest.Map(viewModel, entity);

            // Assert
            Assert.AreEqual(result.AccountSortCode, expected);
        }

        [Test]
        public void GivenViewModel_WhenMapInvoked_ThenDDRefNotMapped()
        {
            // Arrange
            var expected = "expected";
            var viewModel = new TenantViewModel {DDRef = "just some string"};
            var entity = new User {DDRef = expected};

            // Act
            var result = ClassUnderTest.Map(viewModel, entity);

            // Assert
            Assert.AreEqual(result.DDRef, expected);
        }

        [Test]
        public void GivenViewModel_WhenMapInvoked_ThenDOBMapped()
        {
            // Arrange
            var expected = DateTime.Now;
            var viewModel = new TenantViewModel {DOB = expected};
            var entity = new User {DOB = new DateTime(1910, 01, 01)};

            // Act
            var result = ClassUnderTest.Map(viewModel, entity);

            // Assert
            Assert.AreEqual(result.DOB, expected);
        }

        [Test]
        public void GivenViewModel_WhenMapInvoked_ThenEmailMapped()
        {
            // Arrange
            var expected = "expected";
            var viewModel = new TenantViewModel {Email = expected};
            var entity = new User {Email = string.Empty};

            // Act
            var result = ClassUnderTest.Map(viewModel, entity);

            // Assert
            Assert.AreEqual(result.Email, expected);
        }

        [Test]
        public void GivenViewModel_WhenMapInvoked_ThenFirstNameMapped()
        {
            // Arrange
            var expected = "expected";
            var viewModel = new TenantViewModel {FirstName = expected};
            var entity = new User {FirstName = string.Empty};

            // Act
            var result = ClassUnderTest.Map(viewModel, entity);

            // Assert
            Assert.AreEqual(result.FirstName, expected);
        }

        [Test]
        public void GivenViewModel_WhenMapInvoked_ThenGoCardlessCustomerIDNotMapped()
        {
            // arrange
            var expected = "expected";
            var entity = new User {GoCardlessCustomerID = expected};
            var viewModel = new TenantViewModel {GoCardlessCustomerID = "new id"};

            // act
            var result = ClassUnderTest.Map(viewModel, entity);

            // assert
            Assert.That(result.GoCardlessCustomerID, Is.EqualTo(expected));
        }

        [Test]
        public void GivenViewModel_WhenMapInvoked_ThenHomeAddressLine1NotMapped()
        {
            // arrange
            var expected = "expected";
            var entity = new User {HomeAddress = new Address {Line1 = expected}};
            var viewModel = new TenantViewModel {HomeAddress = new Address {Line1 = "new line 1"}};

            // act
            var result = ClassUnderTest.Map(viewModel, entity);

            // assert
            Assert.That(result.HomeAddress.Line1, Is.EqualTo(expected));
        }

        [Test]
        public void GivenViewModel_WhenMapInvoked_ThenHomeAddressNotMapped()
        {
            // Arrange
            var expected = new Address();
            var viewModel = new TenantViewModel {HomeAddress = new SimpleAddress()};
            var entity = new User {HomeAddress = expected};

            // Act
            var result = ClassUnderTest.Map(viewModel, entity);

            // Assert
            Assert.AreEqual(result.HomeAddress, expected);
        }

        [Test]
        public void GivenViewModel_WhenMapInvoked_ThenHouse_IDMapped()
        {
            // Arrange
            var expected = 123;
            var viewModel = new TenantViewModel {House_ID = expected};
            var entity = new User {House_ID = 456};

            // Act
            var result = ClassUnderTest.Map(viewModel, entity);

            // Assert
            Assert.AreEqual(result.House_ID, expected);
        }

        [Test]
        public void GivenViewModel_WhenMapInvoked_ThenIdMapped()
        {
            // Arrange
            var expected = "expected";
            var viewModel = new TenantViewModel {Id = expected};
            var entity = new User {Id = string.Empty};

            // Act
            var result = ClassUnderTest.Map(viewModel, entity);

            // Assert
            Assert.AreEqual(result.Id, expected);
        }

        [Test]
        public void GivenViewModel_WhenMapInvoked_ThenLastNameMapped()
        {
            // Arrange
            var expected = "expected";
            var viewModel = new TenantViewModel {LastName = expected};
            var entity = new User {LastName = string.Empty};

            // Act
            var result = ClassUnderTest.Map(viewModel, entity);

            // Assert
            Assert.AreEqual(result.LastName, expected);
        }

        [Test]
        public void GivenViewModel_WhenMapInvoked_ThenLockoutEnabledNotMapped()
        {
            // arrange
            var expected = true;
            var entity = new User {LockoutEnabled = expected};
            var viewModel = new TenantViewModel {LockoutEnabled = false};

            // act
            var result = ClassUnderTest.Map(viewModel, entity);

            // assert
            Assert.That(result.LockoutEnabled, Is.EqualTo(expected));
        }

        [Test]
        public void GivenViewModel_WhenMapInvoked_ThenNewsletterSubscriptionMapped()
        {
            // Arrange
            var expected = true;
            var viewModel = new TenantViewModel {NewsletterSubscription = expected};
            var entity = new User {NewsletterSubscription = false};

            // Act
            var result = ClassUnderTest.Map(viewModel, entity);

            // Assert
            Assert.AreEqual(result.NewsletterSubscription, expected);
        }

        [Test]
        public void GivenViewModel_WhenMapInvoked_ThenNoteMapped()
        {
            // Arrange
            var expected = "expected";
            var viewModel = new TenantViewModel {Note = expected};
            var entity = new User {Note = string.Empty};

            // Act
            var result = ClassUnderTest.Map(viewModel, entity);

            // Assert
            Assert.AreEqual(result.Note, expected);
        }

        [Test]
        public void GivenViewModel_WhenMapInvoked_ThenPhoneNumberMapped()
        {
            // Arrange
            var expected = "expected";
            var viewModel = new TenantViewModel {PhoneNumber = expected};
            var entity = new User {PhoneNumber = string.Empty};

            // Act
            var result = ClassUnderTest.Map(viewModel, entity);

            // Assert
            Assert.AreEqual(result.PhoneNumber, expected);
        }

        [Test]
        public void GivenViewModel_WhenMapInvoked_ThenRefTitleMapped()
        {
            // Arrange
            var expected = "expected";
            var viewModel = new TenantViewModel {RefTitle = expected};
            var entity = new User {RefTitle = string.Empty};

            // Act
            var result = ClassUnderTest.Map(viewModel, entity);

            // Assert
            Assert.AreEqual(result.RefTitle, expected);
        }

        [Test]
        public void GivenViewModel_WhenMapInvoked_ThenSignupDateNotMapped()
        {
            // arrange
            var expected = new DateTime(1989, 03, 22);
            var entity = new User {SignupDate = expected};
            var viewModel = new TenantViewModel {SignupDate = new DateTime(2015, 12, 25)};

            // act
            var result = ClassUnderTest.Map(viewModel, entity);

            // assert
            Assert.That(result.SignupDate, Is.EqualTo(expected));
        }

        [Test]
        public void GivenViewModel_WhenMapInvoked_ThenUserNameMapped()
        {
            // Arrange
            var expected = "expected";
            var viewModel = new TenantViewModel {Email = expected};
            var entity = new User {Email = string.Empty};

            // Act
            var result = ClassUnderTest.Map(viewModel, entity);

            // Assert
            Assert.AreEqual(result.UserName, expected);
        }


        [Test]
        public void GivenViewModel_WhenMap2Invoked_ThenAccountHolderMapped()
        {
            // Arrange
            var expected = "expected";
            var viewModel = new TenantViewModel { AccountHolder = expected };

            // Act
            var result = ClassUnderTest.Map(viewModel);

            // Assert
            Assert.AreEqual(result.AccountHolder, expected);
        }

        [Test]
        public void GivenViewModel_WhenMap2Invoked_ThenAccountNumberMapped()
        {
            // Arrange
            var expected = "expected";
            var viewModel = new TenantViewModel { AccountNumber = expected };

            // Act
            var result = ClassUnderTest.Map(viewModel);

            // Assert
            Assert.AreEqual(result.AccountNumber, expected);
        }

        [Test]
        public void GivenViewModel_WhenMap2Invoked_ThenAccountSoleHolderMapped()
        {
            // Arrange
            var expected = true;
            var viewModel = new TenantViewModel { AccountSoleHolder = expected };

            // Act
            var result = ClassUnderTest.Map(viewModel);

            // Assert
            Assert.AreEqual(result.AccountSoleHolder, expected);
        }

        [Test]
        public void GivenViewModel_WhenMap2Invoked_ThenAccountSortCodeMapped()
        {
            // Arrange
            var expected = "expected";
            var viewModel = new TenantViewModel { AccountSortCode = expected };

            // Act
            var result = ClassUnderTest.Map(viewModel);

            // Assert
            Assert.AreEqual(result.AccountSortCode, expected);
        }

        [Test]
        public void GivenViewModel_WhenMap2Invoked_ThenDOBMapped()
        {
            // Arrange
            var expected = DateTime.Now;
            var viewModel = new TenantViewModel { DOB = expected };

            // Act
            var result = ClassUnderTest.Map(viewModel);

            // Assert
            Assert.AreEqual(result.DOB, expected);
        }

        [Test]
        public void GivenViewModel_WhenMap2Invoked_ThenEmailMapped()
        {
            // Arrange
            var expected = "expected";
            var viewModel = new TenantViewModel { Email = expected };

            // Act
            var result = ClassUnderTest.Map(viewModel);

            // Assert
            Assert.AreEqual(result.Email, expected);
        }

        [Test]
        public void GivenViewModel_WhenMap2Invoked_ThenFirstNameMapped()
        {
            // Arrange
            var expected = "expected";
            var viewModel = new TenantViewModel { FirstName = expected };

            // Act
            var result = ClassUnderTest.Map(viewModel);

            // Assert
            Assert.AreEqual(result.FirstName, expected);
        }

        [Test]
        public void GivenViewModel_WhenMap2Invoked_ThenHouse_IDMapped()
        {
            // Arrange
            var expected = 123;
            var viewModel = new TenantViewModel { House_ID = expected };

            // Act
            var result = ClassUnderTest.Map(viewModel);

            // Assert
            Assert.AreEqual(result.House_ID, expected);
        }

        [Test]
        public void GivenViewModel_WhenMap2Invoked_ThenIdMapped()
        {
            // Arrange
            var expected = "expected";
            var viewModel = new TenantViewModel { Id = expected };

            // Act
            var result = ClassUnderTest.Map(viewModel);

            // Assert
            Assert.AreEqual(result.Id, expected);
        }

        [Test]
        public void GivenViewModel_WhenMap2Invoked_ThenLastNameMapped()
        {
            // Arrange
            var expected = "expected";
            var viewModel = new TenantViewModel { LastName = expected };

            // Act
            var result = ClassUnderTest.Map(viewModel);

            // Assert
            Assert.AreEqual(result.LastName, expected);
        }

        [Test]
        public void GivenViewModel_WhenMap2Invoked_ThenNewsletterSubscriptionMapped()
        {
            // Arrange
            var expected = true;
            var viewModel = new TenantViewModel { NewsletterSubscription = expected };

            // Act
            var result = ClassUnderTest.Map(viewModel);

            // Assert
            Assert.AreEqual(result.NewsletterSubscription, expected);
        }

        [Test]
        public void GivenViewModel_WhenMap2Invoked_ThenNoteMapped()
        {
            // Arrange
            var expected = "expected";
            var viewModel = new TenantViewModel { Note = expected };

            // Act
            var result = ClassUnderTest.Map(viewModel);

            // Assert
            Assert.AreEqual(result.Note, expected);
        }

        [Test]
        public void GivenViewModel_WhenMap2Invoked_ThenPhoneNumberMapped()
        {
            // Arrange
            var expected = "expected";
            var viewModel = new TenantViewModel { PhoneNumber = expected };

            // Act
            var result = ClassUnderTest.Map(viewModel);

            // Assert
            Assert.AreEqual(result.PhoneNumber, expected);
        }

        [Test]
        public void GivenViewModel_WhenMap2Invoked_ThenRefTitleMapped()
        {
            // Arrange
            var expected = "expected";
            var viewModel = new TenantViewModel { RefTitle = expected };

            // Act
            var result = ClassUnderTest.Map(viewModel);

            // Assert
            Assert.AreEqual(result.RefTitle, expected);
        }

        [Test]
        public void GivenViewModel_WhenMap2Invoked_ThenUserNameMapped()
        {
            // Arrange
            var expected = "expected";
            var viewModel = new TenantViewModel { Email = expected };

            // Act
            var result = ClassUnderTest.Map(viewModel);

            // Assert
            Assert.AreEqual(result.UserName, expected);
        }
    }
}