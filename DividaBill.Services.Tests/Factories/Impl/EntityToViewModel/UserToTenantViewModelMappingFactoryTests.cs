﻿using System;
using AutoMapper;
using DividaBill.BusinessLogic.MappingProfiles;
using DividaBill.Models;
using DividaBill.Models.Common;
using DividaBill.Services.Factories.Impl.EntityToViewModel;
using DividaBill.ViewModels;
using NUnit.Framework;

namespace DividaBill.Services.Tests.Factories.Impl.EntityToViewModel
{
    [TestFixture]
    public class UserToTenantViewModelMappingFactoryTests
    {
        [SetUp]
        public void SetUp()
        {
            _mappingEngine = Mapper.Engine;
            Mapper.Initialize(m => m.AddProfile<SimpleAddressProfile>());
            _classUnderTest = new UserToTenantViewModelMappingFactory(_mappingEngine);
        }

        private UserToTenantViewModelMappingFactory _classUnderTest;
        private IMappingEngine _mappingEngine;

        [Test]
        public void GivenEntityModel_WhenMapInvoked_ThenAccountHolderMapped()
        {
            // Arrange
            var expected = "expected";
            var viewModel = new TenantViewModel {AccountHolder = string.Empty};
            var entity = new User {AccountHolder = expected};

            // Act
            var result = _classUnderTest.Map(entity, viewModel);

            // Assert
            Assert.AreEqual(result.AccountHolder, expected);
        }

        [Test]
        public void GivenEntityModel_WhenMapInvoked_ThenAccountNumberMapped()
        {
            // Arrange
            var expected = "expected";
            var viewModel = new TenantViewModel {AccountNumber = string.Empty};
            var entity = new User {AccountNumber = expected};

            // Act
            var result = _classUnderTest.Map(entity, viewModel);

            // Assert
            Assert.AreEqual(result.AccountNumber, expected);
        }

        [Test]
        public void GivenEntityModel_WhenMapInvoked_ThenAccountSoleHolderMapped()
        {
            // Arrange
            var expected = true;
            var viewModel = new TenantViewModel {AccountSoleHolder = false};
            var entity = new User {AccountSoleHolder = expected};

            // Act
            var result = _classUnderTest.Map(entity, viewModel);

            // Assert
            Assert.AreEqual(result.AccountSoleHolder, expected);
        }

        [Test]
        public void GivenEntityModel_WhenMapInvoked_ThenAccountSortCodeMapped()
        {
            // Arrange
            var expected = "expected";
            var viewModel = new TenantViewModel {AccountSortCode = string.Empty};
            var entity = new User {AccountSortCode = expected};

            // Act
            var result = _classUnderTest.Map(entity, viewModel);

            // Assert
            Assert.AreEqual(result.AccountSortCode, expected);
        }

        [Test]
        public void GivenEntityModel_WhenMapInvoked_ThenDDRefMapped()
        {
            // Arrange
            var expected = "expected";
            var viewModel = new TenantViewModel {DDRef = "just some string"};
            var entity = new User {DDRef = expected};

            // Act
            var result = _classUnderTest.Map(entity, viewModel);

            // Assert
            Assert.AreEqual(result.DDRef, expected);
        }

        [Test]
        public void GivenEntityModel_WhenMapInvoked_ThenDOBMapped()
        {
            // Arrange
            var expected = DateTime.Now;
            var viewModel = new TenantViewModel {DOB = new DateTime(1910, 01, 01)};
            var entity = new User {DOB = expected};

            // Act
            var result = _classUnderTest.Map(entity, viewModel);

            // Assert
            Assert.AreEqual(result.DOB, expected);
        }

        [Test]
        public void GivenEntityModel_WhenMapInvoked_ThenEmailMapped()
        {
            // Arrange
            var expected = "expected";
            var viewModel = new TenantViewModel {Email = string.Empty};
            var entity = new User {Email = expected};

            // Act
            var result = _classUnderTest.Map(entity, viewModel);

            // Assert
            Assert.AreEqual(result.Email, expected);
        }

        [Test]
        public void GivenEntityModel_WhenMapInvoked_ThenFirstNameMapped()
        {
            // Arrange
            var expected = "expected";
            var viewModel = new TenantViewModel {FirstName = string.Empty};
            var entity = new User {FirstName = expected};

            // Act
            var result = _classUnderTest.Map(entity, viewModel);

            // Assert
            Assert.AreEqual(result.FirstName, expected);
        }

        [Test]
        public void GivenEntityModel_WhenMapInvoked_ThenFullNameMapped()
        {
            // Arrange
            var refTitle = "some reference title";
            var firstName = "some first name";
            var lastName = "some last name";

            var viewModel = new TenantViewModel
            {
                RefTitle = string.Empty,
                FirstName = string.Empty,
                LastName = string.Empty
            };
            var entity = new User
            {
                RefTitle = refTitle,
                FirstName = firstName,
                LastName = lastName
            };

            // Act
            var result = _classUnderTest.Map(entity, viewModel);

            // Assert
            Assert.AreEqual(result.FullName, $"{refTitle} {firstName} {lastName}");
        }

        [Test]
        public void GivenEntityModel_WhenMapInvoked_ThenGoCardlessCustomerIDMapped()
        {
            // arrange
            var expected = "expected";
            var entity = new User
            {
                GoCardlessCustomerID = expected
            };
            var viewModel = new TenantViewModel();

            // act
            var result = _classUnderTest.Map(entity, viewModel);

            // assert
            Assert.That(result.GoCardlessCustomerID, Is.EqualTo(expected));
        }

        [Test]
        public void GivenEntityModel_WhenMapInvoked_ThenHomeAddressCityMapped()
        {
            // arrange
            var expected = "expected";
            var entity = new User
            {
                HomeAddress =
                    new Address
                    {
                        City = expected
                    }
            };
            var viewModel = new TenantViewModel();

            // act
            var result = _classUnderTest.Map(entity, viewModel);

            // assert
            Assert.That(result.HomeAddress.City, Is.EqualTo(expected));
        }

        [Test]
        public void GivenEntityModel_WhenMapInvoked_ThenHomeAddressCountyMapped()
        {
            // arrange
            var expected = "expected";
            var entity = new User
            {
                HomeAddress =
                    new Address
                    {
                        County = expected
                    }
            };
            var viewModel = new TenantViewModel();

            // act
            var result = _classUnderTest.Map(entity, viewModel);

            // assert
            Assert.That(result.HomeAddress.County, Is.EqualTo(expected));
        }

        [Test]
        public void GivenEntityModel_WhenMapInvoked_ThenHomeAddressLine1Mapped()
        {
            // arrange
            var expected = "expected";
            var entity = new User
            {
                HomeAddress =
                    new Address
                    {
                        Line1 = expected
                    }
            };
            var viewModel = new TenantViewModel();

            // act
            var result = _classUnderTest.Map(entity, viewModel);

            // assert
            Assert.That(result.HomeAddress.Line1, Is.EqualTo(expected));
        }

        [Test]
        public void GivenEntityModel_WhenMapInvoked_ThenHomeAddressLine2Mapped()
        {
            // arrange
            var expected = "expected";
            var entity = new User
            {
                HomeAddress =
                    new Address
                    {
                        Line2 = expected
                    }
            };
            var viewModel = new TenantViewModel();

            // act
            var result = _classUnderTest.Map(entity, viewModel);

            // assert
            Assert.That(result.HomeAddress.Line2, Is.EqualTo(expected));
        }

        [Test]
        public void GivenEntityModel_WhenMapInvoked_ThenHomeAddressLine3Mapped()
        {
            // arrange
            var expected = "expected";
            var entity = new User
            {
                HomeAddress =
                    new Address
                    {
                        Line3 = expected
                    }
            };
            var viewModel = new TenantViewModel();

            // act
            var result= _classUnderTest.Map(entity, viewModel);

            // assert
            Assert.That(result.HomeAddress.Line3, Is.EqualTo(expected));
        }

        [Test]
        public void GivenEntityModel_WhenMapInvoked_ThenHomeAddressLine4Mapped()
        {
            // arrange
            var expected = "expected";
            var entity = new User
            {
                HomeAddress =
                    new Address
                    {
                        Line4 = expected
                    }
            };
            var viewModel = new TenantViewModel();

            // act
            var result = _classUnderTest.Map(entity, viewModel);

            // assert
            Assert.That(result.HomeAddress.Line4, Is.EqualTo(expected));
        }

        [Test]
        public void GivenEntityModel_WhenMapInvoked_ThenHomeAddressPostcodeMapped()
        {
            // arrange
            var expected = "expected";
            var entity = new User
            {
                HomeAddress =
                    new Address
                    {
                        Postcode = expected
                    }
            };
            var viewModel = new TenantViewModel();

            // act
            var result = _classUnderTest.Map(entity, viewModel);

            // assert
            Assert.That(result.HomeAddress.Postcode, Is.EqualTo(expected));
        }

        [Test]
        public void GivenEntityModel_WhenMapInvoked_ThenHomeAddressUDPRNMapped()
        {
            // arrange
            var expected = "expected";
            var entity = new User
            {
                HomeAddress =
                    new Address
                    {
                        UDPRN = expected
                    }
            };
            var viewModel = new TenantViewModel();

            // act
            var result = _classUnderTest.Map(entity, viewModel);

            // assert
            Assert.That(result.HomeAddress.UDPRN, Is.EqualTo(expected));
        }

        [Test]
        public void GivenEntityModel_WhenMapInvoked_ThenHouse_IDMapped()
        {
            // Arrange
            var expected = 123;
            var viewModel = new TenantViewModel {House_ID = 456};
            var entity = new User {House_ID = expected};

            // Act
            var result = _classUnderTest.Map(entity, viewModel);

            // Assert
            Assert.AreEqual(result.House_ID, expected);
        }

        [Test]
        public void GivenEntityModel_WhenMapInvoked_ThenIdMapped()
        {
            // Arrange
            var expected = "expected";
            var viewModel = new TenantViewModel {Id = string.Empty};
            var entity = new User {Id = expected};

            // Act
            var result = _classUnderTest.Map(entity, viewModel);

            // Assert
            Assert.AreEqual(result.Id, expected);
        }

        [Test]
        public void GivenEntityModel_WhenMapInvoked_ThenLastNameMapped()
        {
            // Arrange
            var expected = "expected";
            var viewModel = new TenantViewModel {LastName = string.Empty};
            var entity = new User {LastName = expected};

            // Act
            var result = _classUnderTest.Map(entity, viewModel);

            // Assert
            Assert.AreEqual(result.LastName, expected);
        }

        [Test]
        public void GivenEntityModel_WhenMapInvoked_ThenLockoutEnabledMapped()
        {
            // arrange
            var expected = true;
            var entity = new User
            {
                LockoutEnabled = expected
            };
            var viewModel = new TenantViewModel();

            // act
            var result = _classUnderTest.Map(entity, viewModel);

            // assert
            Assert.That(result.LockoutEnabled, Is.EqualTo(expected));
        }

        [Test]
        public void GivenEntityModel_WhenMapInvoked_ThenNewsletterSubscriptionMapped()
        {
            // Arrange
            var expected = true;
            var viewModel = new TenantViewModel {NewsletterSubscription = false};
            var entity = new User {NewsletterSubscription = expected};

            // Act
            var result = _classUnderTest.Map(entity, viewModel);

            // Assert
            Assert.AreEqual(result.NewsletterSubscription, expected);
        }

        [Test]
        public void GivenEntityModel_WhenMapInvoked_ThenNoteMapped()
        {
            // Arrange
            var expected = "expected";
            var viewModel = new TenantViewModel {Note = string.Empty};
            var entity = new User {Note = expected};

            // Act
            var result = _classUnderTest.Map(entity, viewModel);

            // Assert
            Assert.AreEqual(result.Note, expected);
        }

        [Test]
        public void GivenEntityModel_WhenMapInvoked_ThenPhoneNumberMapped()
        {
            // Arrange
            var expected = "expected";
            var viewModel = new TenantViewModel {PhoneNumber = string.Empty};
            var entity = new User {PhoneNumber = expected};

            // Act
            var result = _classUnderTest.Map(entity, viewModel);

            // Assert
            Assert.AreEqual(result.PhoneNumber, expected);
        }

        [Test]
        public void GivenEntityModel_WhenMapInvoked_ThenRefTitleMapped()
        {
            // Arrange
            var expected = "expected";
            var viewModel = new TenantViewModel {RefTitle = string.Empty};
            var entity = new User {RefTitle = expected};

            // Act
            var result = _classUnderTest.Map(entity, viewModel);

            // Assert
            Assert.AreEqual(result.RefTitle, expected);
        }

        [Test]
        public void GivenEntityModel_WhenMapInvoked_ThenSignupDateMapped()
        {
            // arrange
            var expected = DateTime.Now;
            var entity = new User
            {
                SignupDate = expected
            };

            // act
            var result = _classUnderTest.Map(entity);

            // assert
            Assert.That(result.SignupDate, Is.EqualTo(expected));
        }

        [Test]
        public void GivenEntityModel_WhenMap2Invoked_ThenAccountHolderMapped()
        {
            // Arrange
            var expected = "expected";
            var entity = new User { AccountHolder = expected };

            // Act
            var result = _classUnderTest.Map(entity);

            // Assert
            Assert.AreEqual(result.AccountHolder, expected);
        }

        [Test]
        public void GivenEntityModel_WhenMap2Invoked_ThenAccountNumberMapped()
        {
            // Arrange
            var expected = "expected";
            var entity = new User { AccountNumber = expected };

            // Act
            var result = _classUnderTest.Map(entity);

            // Assert
            Assert.AreEqual(result.AccountNumber, expected);
        }

        [Test]
        public void GivenEntityModel_WhenMap2Invoked_ThenAccountSoleHolderMapped()
        {
            // Arrange
            var expected = true;
            var entity = new User { AccountSoleHolder = expected };

            // Act
            var result = _classUnderTest.Map(entity);

            // Assert
            Assert.AreEqual(result.AccountSoleHolder, expected);
        }

        [Test]
        public void GivenEntityModel_WhenMap2Invoked_ThenAccountSortCodeMapped()
        {
            // Arrange
            var expected = "expected";
            var entity = new User { AccountSortCode = expected };

            // Act
            var result = _classUnderTest.Map(entity);

            // Assert
            Assert.AreEqual(result.AccountSortCode, expected);
        }

        [Test]
        public void GivenEntityModel_WhenMap2Invoked_ThenDDRefMapped()
        {
            // Arrange
            var expected = "expected";
            var entity = new User { DDRef = expected };

            // Act
            var result = _classUnderTest.Map(entity);

            // Assert
            Assert.AreEqual(result.DDRef, expected);
        }

        [Test]
        public void GivenEntityModel_WhenMap2Invoked_ThenDOBMapped()
        {
            // Arrange
            var expected = DateTime.Now;
            var entity = new User { DOB = expected };

            // Act
            var result = _classUnderTest.Map(entity);

            // Assert
            Assert.AreEqual(result.DOB, expected);
        }

        [Test]
        public void GivenEntityModel_WhenMap2Invoked_ThenEmailMapped()
        {
            // Arrange
            var expected = "expected";
            var entity = new User { Email = expected };

            // Act
            var result = _classUnderTest.Map(entity);

            // Assert
            Assert.AreEqual(result.Email, expected);
        }

        [Test]
        public void GivenEntityModel_WhenMap2Invoked_ThenFirstNameMapped()
        {
            // Arrange
            var expected = "expected";
            var entity = new User { FirstName = expected };

            // Act
            var result = _classUnderTest.Map(entity);

            // Assert
            Assert.AreEqual(result.FirstName, expected);
        }

        [Test]
        public void GivenEntityModel_WhenMap2Invoked_ThenFullNameMapped()
        {
            // Arrange
            var refTitle = "some reference title";
            var firstName = "some first name";
            var lastName = "some last name";

            var entity = new User
            {
                RefTitle = refTitle,
                FirstName = firstName,
                LastName = lastName
            };

            // Act
            var result = _classUnderTest.Map(entity);

            // Assert
            Assert.AreEqual(result.FullName, $"{refTitle} {firstName} {lastName}");
        }

        [Test]
        public void GivenEntityModel_WhenMap2Invoked_ThenGoCardlessCustomerIDMapped()
        {
            // arrange
            var expected = "expected";
            var entity = new User
            {
                GoCardlessCustomerID = expected
            };
            var viewModel = new TenantViewModel();

            // act
            var result = _classUnderTest.Map(entity);

            // assert
            Assert.That(result.GoCardlessCustomerID, Is.EqualTo(expected));
        }

        [Test]
        public void GivenEntityModel_WhenMap2Invoked_ThenHomeAddressCityMapped()
        {
            // arrange
            var expected = "expected";
            var entity = new User
            {
                HomeAddress =
                    new Address
                    {
                        City = expected
                    }
            };

            // act
            var result = _classUnderTest.Map(entity);

            // assert
            Assert.That(result.HomeAddress.City, Is.EqualTo(expected));
        }

        [Test]
        public void GivenEntityModel_WhenMap2Invoked_ThenHomeAddressCountyMapped()
        {
            // arrange
            var expected = "expected";
            var entity = new User
            {
                HomeAddress =
                    new Address
                    {
                        County = expected
                    }
            };

            // act
            var result = _classUnderTest.Map(entity);

            // assert
            Assert.That(result.HomeAddress.County, Is.EqualTo(expected));
        }

        [Test]
        public void GivenEntityModel_WhenMap2Invoked_ThenHomeAddressLine1Mapped()
        {
            // arrange
            var expected = "expected";
            var entity = new User
            {
                HomeAddress =
                    new Address
                    {
                        Line1 = expected
                    }
            };

            // act
            var result = _classUnderTest.Map(entity);

            // assert
            Assert.That(result.HomeAddress.Line1, Is.EqualTo(expected));
        }

        [Test]
        public void GivenEntityModel_WhenMap2Invoked_ThenHomeAddressLine2Mapped()
        {
            // arrange
            var expected = "expected";
            var entity = new User
            {
                HomeAddress =
                    new Address
                    {
                        Line2 = expected
                    }
            };

            // act
            var result = _classUnderTest.Map(entity);

            // assert
            Assert.That(result.HomeAddress.Line2, Is.EqualTo(expected));
        }

        [Test]
        public void GivenEntityModel_WhenMap2Invoked_ThenHomeAddressLine3Mapped()
        {
            // arrange
            var expected = "expected";
            var entity = new User
            {
                HomeAddress =
                    new Address
                    {
                        Line3 = expected
                    }
            };

            // act
            var result = _classUnderTest.Map(entity);

            // assert
            Assert.That(result.HomeAddress.Line3, Is.EqualTo(expected));
        }

        [Test]
        public void GivenEntityModel_WhenMap2Invoked_ThenHomeAddressLine4Mapped()
        {
            // arrange
            var expected = "expected";
            var entity = new User
            {
                HomeAddress =
                    new Address
                    {
                        Line4 = expected
                    }
            };

            // act
            var result = _classUnderTest.Map(entity);

            // assert
            Assert.That(result.HomeAddress.Line4, Is.EqualTo(expected));
        }

        [Test]
        public void GivenEntityModel_WhenMap2Invoked_ThenHomeAddressPostcodeMapped()
        {
            // arrange
            var expected = "expected";
            var entity = new User
            {
                HomeAddress =
                    new Address
                    {
                        Postcode = expected
                    }
            };

            // act
            var result = _classUnderTest.Map(entity);

            // assert
            Assert.That(result.HomeAddress.Postcode, Is.EqualTo(expected));
        }

        [Test]
        public void GivenEntityModel_WhenMap2Invoked_ThenHomeAddressUDPRNMapped()
        {
            // arrange
            var expected = "expected";
            var entity = new User
            {
                HomeAddress =
                    new Address
                    {
                        UDPRN = expected
                    }
            };

            // act
            var result = _classUnderTest.Map(entity);

            // assert
            Assert.That(result.HomeAddress.UDPRN, Is.EqualTo(expected));
        }

        [Test]
        public void GivenEntityModel_WhenMap2Invoked_ThenHouse_IDMapped()
        {
            // Arrange
            var expected = 123;
            var entity = new User { House_ID = expected };

            // Act
            var result = _classUnderTest.Map(entity);

            // Assert
            Assert.AreEqual(result.House_ID, expected);
        }

        [Test]
        public void GivenEntityModel_WhenMap2Invoked_ThenIdMapped()
        {
            // Arrange
            var expected = "expected";
            var entity = new User { Id = expected };

            // Act
            var result = _classUnderTest.Map(entity);

            // Assert
            Assert.AreEqual(result.Id, expected);
        }

        [Test]
        public void GivenEntityModel_WhenMap2Invoked_ThenLastNameMapped()
        {
            // Arrange
            var expected = "expected";
            var entity = new User { LastName = expected };

            // Act
            var result = _classUnderTest.Map(entity);

            // Assert
            Assert.AreEqual(result.LastName, expected);
        }

        [Test]
        public void GivenEntityModel_WhenMap2Invoked_ThenLockoutEnabledMapped()
        {
            // arrange
            var expected = true;
            var entity = new User
            {
                LockoutEnabled = expected
            };

            // act
            var result = _classUnderTest.Map(entity);

            // assert
            Assert.That(result.LockoutEnabled, Is.EqualTo(expected));
        }

        [Test]
        public void GivenEntityModel_WhenMap2Invoked_ThenNewsletterSubscriptionMapped()
        {
            // Arrange
            var expected = true;
            var entity = new User { NewsletterSubscription = expected };

            // Act
            var result = _classUnderTest.Map(entity);

            // Assert
            Assert.AreEqual(result.NewsletterSubscription, expected);
        }

        [Test]
        public void GivenEntityModel_WhenMap2Invoked_ThenNoteMapped()
        {
            // Arrange
            var expected = "expected";
            var viewModel = new TenantViewModel { Note = string.Empty };
            var entity = new User { Note = expected };

            // Act
            var result = _classUnderTest.Map(entity);

            // Assert
            Assert.AreEqual(result.Note, expected);
        }

        [Test]
        public void GivenEntityModel_WhenMap2Invoked_ThenPhoneNumberMapped()
        {
            // Arrange
            var expected = "expected";
            var entity = new User { PhoneNumber = expected };

            // Act
            var result = _classUnderTest.Map(entity);

            // Assert
            Assert.AreEqual(result.PhoneNumber, expected);
        }

        [Test]
        public void GivenEntityModel_WhenMap2Invoked_ThenRefTitleMapped()
        {
            // Arrange
            var expected = "expected";
            var entity = new User { RefTitle = expected };

            // Act
            var result = _classUnderTest.Map(entity);

            // Assert
            Assert.AreEqual(result.RefTitle, expected);
        }

        [Test]
        public void GivenEntityModel_WhenMap2Invoked_ThenSignupDateMapped()
        {
            // arrange
            var expected = DateTime.Now;
            var entity = new User
            {
                SignupDate = expected
            };

            // act
            var result = _classUnderTest.Map(entity);

            // assert
            Assert.That(result.SignupDate, Is.EqualTo(expected));
        }
    }
}