﻿using System;
using Dividabill.Tests.Core;
using DividaBill.DAL;
using DividaBill.DAL.Interfaces;
using DividaBill.Models;
using DividaBill.Models.Emails;
using DividaBill.Services.Emails.Impl;
using Moq;
using NUnit.Framework;

namespace DividaBill.Services.Tests.Emails.Impl
{
    [TestFixture]
    public class EmailTemplateServiceTests : BaseAutoMock<EmailTemplateService>
    {
        [Test]
        public void GivenNewTemplate_WhenRenewInvoked_ThenCloseOldOneAndInsertNewOne()
        {
            var oldTemplate = new EmailTemplate
            {
                ID = 1
            };

            var newTemplate = new EmailTemplate();

            var repository = GetMock<GenericRepository<EmailTemplate, User, IApplicationContext>>();
            repository.Setup(x => x.GetByID(oldTemplate.ID)).Returns(oldTemplate);
            repository.Setup(x => x.Insert(It.IsAny<EmailTemplate>())).Returns(newTemplate);
            repository.Setup(x => x.Update(It.IsAny<EmailTemplate>())).Returns(oldTemplate);

            var unitOfWork = GetMock<IUnitOfWork>();
            unitOfWork.Setup(x => x.EmailTemplateRepository).Returns(repository.Object);

            var result = ClassUnderTest.Renew(oldTemplate.ID, newTemplate);

            repository.Verify(templates => templates.GetByID(oldTemplate.ID), Times.Once);
            repository.Verify(templates => templates.Update(It.IsAny<EmailTemplate>()), Times.Once);
            repository.Verify(templates => templates.Insert(It.IsAny<EmailTemplate>()), Times.Once);
            Assert.AreSame(newTemplate, result);
            Assert.AreNotEqual(default(DateTime), result.UsedFrom);
            Assert.AreEqual(true, result.Enabled);
            Assert.IsNull(result.UsedTo);

            Assert.AreEqual(false, oldTemplate.Enabled);
            Assert.IsNotNull(oldTemplate.UsedTo);

            Assert.AreEqual(result.UsedFrom, oldTemplate.UsedTo);
        }

        [Test]
        public void GivenNewTemplate_WhenInsertInvoked_ThenDefaultProperiesSetUpAndNewTemplateReturned()
        {
            var newTemplate = new EmailTemplate();

            var repository = GetMock<GenericRepository<EmailTemplate, User, IApplicationContext>>();
            repository.Setup(x => x.Insert(It.IsAny<EmailTemplate>())).Returns(newTemplate);

            var unitOfWork = GetMock<IUnitOfWork>();
            unitOfWork.Setup(x => x.EmailTemplateRepository).Returns(repository.Object);

            var result = ClassUnderTest.Insert(newTemplate);

            repository.Verify(templates => templates.Insert(It.IsAny<EmailTemplate>()), Times.Once);
            Assert.AreNotEqual(default(DateTime), result.UsedFrom);
            Assert.AreEqual(true, result.Enabled);
            Assert.IsNull(result.UsedTo);
            Assert.IsNotNull(result.CreatedAt);
            Assert.IsNotNull(result.LastModified);
        }
    }
}