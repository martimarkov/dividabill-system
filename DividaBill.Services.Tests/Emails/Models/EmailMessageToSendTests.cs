﻿using System;
using DividaBill.Models;
using DividaBill.Services.Emails.Models;
using NUnit.Framework;

namespace DividaBill.Services.Tests.Emails.Models
{
    [TestFixture]
    public class EmailMessageToSendTests
    {
        [Test]
        public void GivenContructor_WhenContructorInvoked_ThenIsTestSendingFalseByDefaul()
        {
            var instance = new EmailMessageToSend(null, null, string.Empty, null);

            Assert.AreEqual(false, instance.IsTestSending);
        }

        [Test]
        public void GivenUserEmail_WhenContructorInvoked_ThenSendToPropertySet()
        {
            var email = Guid.NewGuid().ToString();
            var sendToUser = new User
            {
                Email = email
            };
            var instance = new EmailMessageToSend(null, null, sendToUser, null);

            Assert.AreEqual(email, instance.SendTo);
        }
    }
}