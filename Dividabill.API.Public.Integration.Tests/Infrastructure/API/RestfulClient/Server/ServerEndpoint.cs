﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Dividabill.API.Public.Integration.Tests.Infrastructure.API.RestfulClient.Server
{
    public class ServerEndPoint
    {
        private IHttpClientFactory _httpClientFactory;
        private InMemoryHost _server;
        private readonly string _baseUrl;

        public ServerEndPoint(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
            _baseUrl = ConfigurationManager.AppSettings["BaseUrl"];
            _server = new InMemoryHost(_baseUrl);
        }

        public async Task<HttpResponseMessage> GetAsync(string url)
        {
            var httpClient = _httpClientFactory.Create(_server.InMemoryHttpServer);
            //httpClient.
            //httpClient.SetBaseUri(new Uri(_baseUrl));
            var tokenSource = new CancellationTokenSource();
            var response = await httpClient.GetAsync(_baseUrl + url, tokenSource.Token);
            return response;
        }

        public async Task<HttpResponseMessage> PostAsync(string url, HttpContent content, string sessionToken = null)
        {
            var httpClient = _httpClientFactory.Create(_server.InMemoryHttpServer);
            
            if(sessionToken != null)
                httpClient.AddHeader("Authorization","Basic " + new Guid(sessionToken.Trim(new []{'"'})));

            var tokenSource = new CancellationTokenSource();
            var response = await httpClient.PostAsync(_baseUrl + url, content, tokenSource.Token);
            return response;
        }

        public async Task<HttpResponseMessage> PostJsonAsync(string url, object obj, string sessionToken = null)
        {
            var json = await JsonConvert.SerializeObjectAsync(obj);

            var response = await PostAsync(url, new StringContent(json, Encoding.UTF8, "application/json"), sessionToken);
            return response;
        }

        public async Task<HttpResponseMessage> PostFileAsync(string url, MultipartFormDataContent multipartFormData, string sessionToken = null)
        {
            var response = await PostAsync(url, multipartFormData, sessionToken);
            return response;
        }

        public async Task<HttpResponseMessage> PutAsync(string url, HttpContent content)
        {
            var httpClient = _httpClientFactory.Create(_server.InMemoryHttpServer);
            //httpClient.SetBaseUri(new Uri(_baseUrl));
            var tokenSource = new CancellationTokenSource();
            var response = await httpClient.PutAsync(url, content, tokenSource.Token);
            return response;
        }

        public async Task<HttpResponseMessage> DeleteAsync(string url)
        {
            var httpClient = _httpClientFactory.Create(_server.InMemoryHttpServer);
            //httpClient.SetBaseUri(new Uri(_baseUrl));
            var tokenSource = new CancellationTokenSource();
            var response = await httpClient.DeleteAsync(url, tokenSource.Token);
            return response;
        }

        public void Dispose()
        {
            _httpClientFactory = null;
            _server.Dispose();
        }
    }
}