﻿using System;
using System.Web.Http;

namespace Dividabill.API.Public.Integration.Tests.Infrastructure.API.RestfulClient.Server
{

    public class InMemoryHost
    {
        private IDisposable _server;
        //private HttpSelfHostServer _server;
        public HttpServer InMemoryHttpServer { get; set; }

        public string BaseUrl { get; set; }

        public InMemoryHost(string baseUrl)
        {
            var config = new HttpConfiguration();
            throw new NotImplementedException();
            //WebApiConfig.Register(config);
            //config.DependencyResolver = AutofacModule.SetupDependencyInjection();
            InMemoryHttpServer = new HttpServer(config);

            BaseUrl = baseUrl;
        }

        public void Dispose()
        {
            if (_server != null)
            {
                //_server.CloseAsync().Wait();
                _server.Dispose();
            }
            _server = null;
        }
    }
}