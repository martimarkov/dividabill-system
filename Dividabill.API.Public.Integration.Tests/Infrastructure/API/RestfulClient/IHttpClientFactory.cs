﻿using System.Net.Http;

namespace Dividabill.API.Public.Integration.Tests.Infrastructure.API.RestfulClient
{
    public interface IHttpClientFactory
    {
        IHttpClient Create();

        IHttpClient Create(HttpMessageHandler messageHandler);
    }
}