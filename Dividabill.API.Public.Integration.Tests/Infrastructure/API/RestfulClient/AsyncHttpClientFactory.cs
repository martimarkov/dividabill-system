﻿using System.Net.Http;

namespace Dividabill.API.Public.Integration.Tests.Infrastructure.API.RestfulClient
{
    public class AsyncHttpClientFactory : IHttpClientFactory
    {
        public IHttpClient Create()
        {
            return new AsyncHttpClient();
        }

        public IHttpClient Create(HttpMessageHandler messageHandler)
        {
            return new AsyncHttpClient(messageHandler);
        }
    }
}