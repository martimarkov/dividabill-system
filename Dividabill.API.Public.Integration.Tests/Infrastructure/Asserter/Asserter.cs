﻿using System.Net;
using System.Net.Http;
using NUnit.Framework;

namespace Dividabill.API.Public.Integration.Tests.Infrastructure.Asserter
{
    public static class Asserter
    {
        public static void AssertHttpStatus(this HttpResponseMessage responseMessage, HttpStatusCode statusCode)
        {
            Assert.That(responseMessage.StatusCode, Is.EqualTo(statusCode));
        }

        public static void AssertHttpStatusIsOk(this HttpResponseMessage responseMessage)
        {
            Assert.That(responseMessage.StatusCode, Is.EqualTo(HttpStatusCode.OK));
        }
    }
}