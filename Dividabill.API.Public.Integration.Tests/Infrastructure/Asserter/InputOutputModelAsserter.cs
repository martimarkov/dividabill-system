﻿using Dividabill.API.Public.Integration.Tests.Infrastructure.Models;

namespace Dividabill.API.Public.Integration.Tests.Infrastructure.Asserter
{
    public class InputOutputModelAsserter
    {
        private readonly InputOutputModels _inputOutputModels;

        public InputOutputModelAsserter(InputOutputModels inputOutputModels)
        {
            _inputOutputModels = inputOutputModels;
        }

        public void SignupPostReturnedOk()
        {
            _inputOutputModels.OutputModels.SignupOutputModel.AssertHttpStatusIsOk();
        }
    }
}