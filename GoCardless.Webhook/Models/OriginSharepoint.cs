﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoCardless.Webhook.Models
{
    public class OriginSharepoint
    {
        public DateTimeOffset ExpectedInstallDate { get; set; }
        public DateTimeOffset ActualStartDate { get; set; }
    }
}