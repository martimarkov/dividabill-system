﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoCardless.Webhook.Models
{
    public class Event : DividaBill.Models.Event
    {
        public Event()
        {
            this.links = new Dictionary<string, string>();
            this.details = new Dictionary<string, string>();
        }
        public virtual Dictionary<string, string> links { get; set; }
        public virtual Dictionary<string, string> details { get; set; }


    }
}