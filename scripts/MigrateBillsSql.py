﻿"""
Reads in all lines from the specified SQL scripts in 'files'. 

Input should use old DividaBill.DAL bills format: a table/file for each bill type with all the relevant bill info per row. 
Output uses the new 'BillBases' table for common data and bill-type-specific tables for additional fields. 

17/07/2015
ix
"""

import re

pattern = r"INSERT INTO \[dbo\]\.\[(\w+)\] \(\[Id\], \[Ammount\], \[House_ID\], \[Note\], \[Adjustment], \[Status], \[StartDate], \[EndDate], \[PaymentDate], \[Type]\,? ?(\[.*])?\) VALUES \((\d+), CAST\(([\d.]+) AS Decimal\(18, 2\)\), (\d+), NULL, NULL, 0, (N'.*'), (N'.*'), (N'.*'), 0,? ?(\d+)?\)"
reg = re.compile(pattern)
files = [
    'db.sql',
]

i = 2362

for fn in files:
    out = []
    outA = []
    outB = []
    tokens = []
    errz = []

    with open(fn) as f:
        lines = [ln.strip() for ln in f.readlines()]
        
    for ln in lines:
        match = reg.match(ln)
        if match:
            table_name = match.group(1)
            contract_id_name = match.group(2)
            old_id = match.group(3)
            amount = match.group(4)
            house_id = match.group(5)
            start = match.group(6)
            end = match.group(7)
            pay = match.group(8)
            contract_id = match.group(9)

            tokens.append((amount, house_id, start, end, pay, table_name, contract_id_name, old_id, contract_id))
        elif not ln.startswith('SET IDENTITY_') and not ln.endswith('NULL)'):
            errz.append(ln)

    print("ERRORZ YO:")

    for e in errz:
        print(e)
            
    for tk in tokens:
        outA.append(r"INSERT INTO [dbo].[BillBases] ([Id], [Ammount], [House_ID], [StartDate], [EndDate], [PaymentDate], [Type], [Status]) VALUES (%d, %s, %s, %s, %s, %s, %s, 0)"
                   % (i, tk[0], tk[1], tk[2], tk[3], tk[4], "0"))

        if tk[6]:
            outB.append(r"INSERT INTO [dbo].%s ([Id], %s) VALUES (%d, %s)" % (tk[5], tk[6], i, tk[8]))
        else:
            outB.append(r"INSERT INTO [dbo].%s ([Id]) VALUES (%d)" % (tk[5], i))


        i = i + 1

        
    out.append("SET IDENTITY_INSERT [dbo].[BillBases] ON")
    for z in outA:
        out.append(z)
    out.append("SET IDENTITY_INSERT [dbo].[BillBases] OFF")
        
    for z in outB:
        out.append(z)

    with open('out_' + fn, 'w') as f:
        f.write("\n".join(out))