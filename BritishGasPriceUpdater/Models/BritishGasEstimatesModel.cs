using System;
using System.Collections.Generic;

namespace BritishGasPriceUpdater.Models
{
    public class BritishGasEstimatesModel
    {
        public List<Prices> prices;

        public List<Prices> getPrices()
        {
            return this.prices;
        }
        public void setPrices(List<Prices> prices)
        {
            this.prices = prices;
        }
    }
}