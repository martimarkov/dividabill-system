﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BritishGasPriceUpdater.Models
{
    class POSTModel
    {
        public String paymentOption { get; set; }
        public String postcode { get; set; }
        public String tariff { get; set; }
        public String fuelType { get; set; }
        public bool economySevenMeter { get; set; }
        public bool dualFuel { get; set; }
    }
}
