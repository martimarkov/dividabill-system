﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BritishGasPriceUpdater.Models
{
    public class TariffInformationDetails
    {

        public String supplier { get; set; }
        public String tariffName { get; set; }
        public String tariffType { get; set; }
        public String cancellationCharge { get; set; }
        public String gasDiscountDescription { get; set; }
        public String electricityDiscountDescription { get; set; }
        public String gasAdditionalProductDescription { get; set; }
        public String elecAdditionalProductDescription { get; set; }
        public bool isOnSaleTariff { get; set; }
        public bool bundledOption { get; set; }
        public String bundledText { get; set; }
        public bool isTouTariff { get; set; }
        public bool isEsmartEligible { get; set; }
        public bool isEshopEligible { get; set; }
        public bool dualTariff { get; set; }

    }
}
