using System;
using System.Collections.Generic;

namespace BritishGasPriceUpdater.Models
{
    public class PostUnitRates
    {
        public Double annualConsumption;
        public Double discount;
        public Double estimatedAnnualCost;
        public String estimatedAnnualCostWithDiscount;
        public String estimatedAnnualCostWithoutDiscout;
        public String standingCharges;
        public Double tariffComparisonRate;
        public String tariffComparisonRateWithDiscount;
        public String tariffComparisonRateWithoutDiscount;
        public List<UnitRates> unitRates;
        public bool zeusTariff;

        public Double getAnnualConsumption()
        {
            return this.annualConsumption;
        }
        public void setAnnualConsumption(Double annualConsumption)
        {
            this.annualConsumption = annualConsumption;
        }
        public Double getDiscount()
        {
            return this.discount;
        }
        public void setDiscount(Double discount)
        {
            this.discount = discount;
        }
        public Double getEstimatedAnnualCost()
        {
            return this.estimatedAnnualCost;
        }
        public void setEstimatedAnnualCost(Double estimatedAnnualCost)
        {
            this.estimatedAnnualCost = estimatedAnnualCost;
        }
        public String getEstimatedAnnualCostWithDiscount()
        {
            return this.estimatedAnnualCostWithDiscount;
        }
        public void setEstimatedAnnualCostWithDiscount(String estimatedAnnualCostWithDiscount)
        {
            this.estimatedAnnualCostWithDiscount = estimatedAnnualCostWithDiscount;
        }
        public String getEstimatedAnnualCostWithoutDiscout()
        {
            return this.estimatedAnnualCostWithoutDiscout;
        }
        public void setEstimatedAnnualCostWithoutDiscout(String estimatedAnnualCostWithoutDiscout)
        {
            this.estimatedAnnualCostWithoutDiscout = estimatedAnnualCostWithoutDiscout;
        }
        public String getStandingCharges()
        {
            return this.standingCharges;
        }
        public void setStandingCharges(String standingCharges)
        {
            this.standingCharges = standingCharges;
        }
        public Double getTariffComparisonRate()
        {
            return this.tariffComparisonRate;
        }
        public void setTariffComparisonRate(Double tariffComparisonRate)
        {
            this.tariffComparisonRate = tariffComparisonRate;
        }
        public String getTariffComparisonRateWithDiscount()
        {
            return this.tariffComparisonRateWithDiscount;
        }
        public void setTariffComparisonRateWithDiscount(String tariffComparisonRateWithDiscount)
        {
            this.tariffComparisonRateWithDiscount = tariffComparisonRateWithDiscount;
        }
        public String getTariffComparisonRateWithoutDiscount()
        {
            return this.tariffComparisonRateWithoutDiscount;
        }
        public void setTariffComparisonRateWithoutDiscount(String tariffComparisonRateWithoutDiscount)
        {
            this.tariffComparisonRateWithoutDiscount = tariffComparisonRateWithoutDiscount;
        }
        public List<UnitRates> getUnitRates()
        {
            return this.unitRates;
        }
        public void setUnitRates(List<UnitRates> unitRates)
        {
            this.unitRates = unitRates;
        }
        public bool getZeusTariff()
        {
            return this.zeusTariff;
        }
        public void setZeusTariff(bool zeusTariff)
        {
            this.zeusTariff = zeusTariff;
        }
    }
}