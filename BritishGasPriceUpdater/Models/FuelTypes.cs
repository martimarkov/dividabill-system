using System;
using System.Collections.Generic;

namespace BritishGasPriceUpdater.Models
{
    public class FuelTypes
    {
        public AveragePrices averagePrices;
        public String fuelType;
        public String postThreshold;
        public TariffRates tariffRates;
        public String threshold;

        public AveragePrices getAveragePrices()
        {
            return this.averagePrices;
        }
        public void setAveragePrices(AveragePrices averagePrices)
        {
            this.averagePrices = averagePrices;
        }
        public String getFuelType()
        {
            return this.fuelType;
        }
        public void setFuelType(String fuelType)
        {
            this.fuelType = fuelType;
        }
        public String getPostThreshold()
        {
            return this.postThreshold;
        }
        public void setPostThreshold(String postThreshold)
        {
            this.postThreshold = postThreshold;
        }
        public TariffRates getTariffRates()
        {
            return this.tariffRates;
        }
        public void setTariffRates(TariffRates tariffRates)
        {
            this.tariffRates = tariffRates;
        }
        public String getThreshold()
        {
            return this.threshold;
        }
        public void setThreshold(String threshold)
        {
            this.threshold = threshold;
        }
    }
}