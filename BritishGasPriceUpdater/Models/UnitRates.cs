using System;
using System.Collections.Generic;

namespace BritishGasPriceUpdater.Models
{
    public class UnitRates
    {
        public MonthlyDirectDebitRates monthlyDirectDebitRates;
        public String registerName;
        public String registerDouble;

        public MonthlyDirectDebitRates getMonthlyDirectDebitRates()
        {
            return this.monthlyDirectDebitRates;
        }
        public void setMonthlyDirectDebitRates(MonthlyDirectDebitRates monthlyDirectDebitRates)
        {
            this.monthlyDirectDebitRates = monthlyDirectDebitRates;
        }
        public String getRegisterName()
        {
            return this.registerName;
        }
        public void setRegisterName(String registerName)
        {
            this.registerName = registerName;
        }
        public String getRegisterDouble()
        {
            return this.registerDouble;
        }
        public void setRegisterDouble(String registerDouble)
        {
            this.registerDouble = registerDouble;
        }
    }
}