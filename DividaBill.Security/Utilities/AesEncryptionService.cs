﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using DividaBill.Security.Abstractions;

namespace DividaBill.Security.Utilities
{
    public class AesEncryptionService : IEncryptionService
    {
        /// name="keySize" Can be 128, 192, or 256
        private const EncryptionKeySize KeySize = EncryptionKeySize.Keysize256;

        private readonly string _appendix = Convert.ToChar(1).ToString() + Convert.ToChar(2);

        /// salt">Salt to encrypt with
        private const string Salt = "Kosher";

        /// Password">Password to encrypt with
        private const string Password = "DividaBill p@ssw0rd";

        /// PasswordIterations">Number of iterations to do
        private const int IterationCount = 1000;

        /// <summary>
        ///     Encrypts a string
        /// </summary>
        /// <param name="plainText">Text to be encrypted</param>
        /// <returns>An encrypted string</returns>
        public string Encrypt(string plainText)
        {
            if(string.IsNullOrEmpty(plainText))
                return plainText;

            if (!IsEncrypted(plainText))
                plainText = encryptAlgorithm(plainText);
            return plainText;
        }

        public bool IsEncrypted(string value)
        {
            if (string.IsNullOrEmpty(value))
                return false;
            value = value.Trim();

            return value.StartsWith(_appendix);
        }

        public string Decrypt(string cipherText)
        {
            if (IsEncrypted(cipherText))
                cipherText = decryptAlgorithm(cipherText);
            return cipherText;
        }

        private string encryptAlgorithm(string plainText)
        {
            if (string.IsNullOrEmpty(plainText))
                return "";

            var saltValueBytes = Encoding.Unicode.GetBytes(Salt);

            var plainTextBytes = Encoding.Unicode.GetBytes(plainText);

            var derivedPassword = new Rfc2898DeriveBytes(Password, saltValueBytes, IterationCount);

            var aes = new RijndaelManaged();

            aes.BlockSize = (int) KeySize;

            var keyBytes = derivedPassword.GetBytes(aes.KeySize/8);
            var ivBytes = derivedPassword.GetBytes(aes.BlockSize/8);

            aes.Mode = CipherMode.CBC;
            aes.Key = keyBytes;
            aes.IV = ivBytes;

            byte[] cipherTextBytes = null;

            using (var encryptor = aes.CreateEncryptor())
            {
                using (var memStream = new MemoryStream())
                {
                    using (var cryptoStream = new CryptoStream(memStream, encryptor, CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);

                        cryptoStream.FlushFinalBlock();

                        cipherTextBytes = memStream.ToArray();

                        memStream.Close();

                        cryptoStream.Close();
                    }
                }
            }

            aes.Clear();

            return _appendix + Convert.ToBase64String(cipherTextBytes);
        }

        /// <summary>
        ///     Decrypts a string
        /// </summary>
        /// <param name="cipherText">Text to be decrypted</param>
        /// <returns>A decrypted string</returns>
        private string decryptAlgorithm(string cipherText)
        {
            if (string.IsNullOrEmpty(cipherText) || !cipherText.StartsWith(_appendix))
                return "";

            cipherText = cipherText.Substring(_appendix.Length, cipherText.Length - _appendix.Length);

            var saltValueBytes = Encoding.Unicode.GetBytes(Salt);

            var cipherTextBytes = Convert.FromBase64String(cipherText);

            var derivedPassword = new Rfc2898DeriveBytes(Password, saltValueBytes, IterationCount);

            var aes = new RijndaelManaged();

            aes.BlockSize = (int) KeySize;

            var keyBytes = derivedPassword.GetBytes(aes.KeySize/8);
            var ivBytes = derivedPassword.GetBytes(aes.BlockSize/8);

            aes.Mode = CipherMode.CBC;
            aes.Key = keyBytes;
            aes.IV = ivBytes;

            var plainTextBytes = new byte[cipherTextBytes.Length];

            var byteCount = 0;

            using (var decryptor = aes.CreateDecryptor())
            {
                using (var memStream = new MemoryStream(cipherTextBytes))
                {
                    using (var cryptoStream = new CryptoStream(memStream, decryptor, CryptoStreamMode.Read))
                    {
                        byteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);

                        memStream.Close();

                        cryptoStream.Close();
                    }
                }
            }

            aes.Clear();

            return Encoding.Unicode.GetString(plainTextBytes, 0, byteCount);
        }

        private enum EncryptionKeySize
        {
            Keysize128 = 128,
            Keysize192 = 192,
            Keysize256 = 256
        }

        #region Copyright AES Encryption by James Craig

        /*http://www.gutgames.com/post/AES-Encryption-in-C.aspx
   Copyright (c) 2010 <a href="http://www.gutgames.com">James Craig</a>
   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.*/

        #endregion
    }
}