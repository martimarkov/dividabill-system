﻿using DividaBill.Security.Abstractions;
using DividaBill.Security.Utilities;
using Microsoft.Practices.Unity;

namespace DividaBill.Security.Unity
{
    public static class Config
    {
        public static void RegisterSecurity(this IUnityContainer container)
        {
            container.RegisterType<IEncryptionService, AesEncryptionService>();
        }
    }
}