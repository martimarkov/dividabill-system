namespace EncryptDB.Abstractions
{
    public interface ILoggingEncryptionService
    {
        void LogProcess(int i, int totalCount, string identificator);
        void LogStart(string nameOfClass, string actionType, int totalCount);
        void LogEnd(string nameOfClass, int updatedTotalCount);
    }
}