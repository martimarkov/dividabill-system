﻿namespace EncryptDB.Abstractions
{
    public interface ITableEncryptor
    {
        void EncryptTableRows();
        void DecryptTableRows();
    }
}