namespace EncryptDB.Abstractions
{
    public interface IScriptRunner
    {
        int PickListItem(string[] itemList);
    }
}