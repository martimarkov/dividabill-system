using System.Linq;
using DividaBill.DAL.Interfaces;
using DividaBill.Services.Abstraction;
using DividaBill.Services.Interfaces;
using EncryptDB.Abstractions;

namespace EncryptDB.EncryptInfo
{
    public class BankAccountInfoEncryptor : ITableEncryptor
    {
        private readonly IBankAccountEncryptionService _bankAccountEncryptionService;
        private readonly ILoggingEncryptionService _loggingEncryptionService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAccountService _bankAccountService;

        public BankAccountInfoEncryptor(IUnitOfWork unitOfWork,
            IBankAccountEncryptionService bankAccountEncryptionService,
            ILoggingEncryptionService loggingEncryptionService,
            IAccountService bankAccountService)
        {
            _bankAccountService = bankAccountService;
            _loggingEncryptionService = loggingEncryptionService;
            _unitOfWork = unitOfWork;
            _bankAccountEncryptionService = bankAccountEncryptionService;
        }

        public void EncryptTableRows()
        {
            var list = _bankAccountService.GetActive().ToList();
            var totalCount = list.Count;
            _loggingEncryptionService.LogStart(nameof(BankAccountInfoEncryptor), "encryption", totalCount);
            var i = 0;
            foreach (var item in list)
            {
                i++;
                _bankAccountEncryptionService.EncryptBankAccount(item);
                _loggingEncryptionService.LogProcess(i, totalCount, item.Id);
                _unitOfWork.Save();
            }
            _loggingEncryptionService.LogEnd(nameof(BankAccountInfoEncryptor), i);
        }

        public void DecryptTableRows()
        {
            var list = _bankAccountService.GetActive().ToList();
            var totalCount = list.Count;
            _loggingEncryptionService.LogStart(nameof(BankAccountInfoEncryptor), "decryption", totalCount);
            var i = 0;
            foreach (var item in list)
            {
                i++;
                _bankAccountEncryptionService.DecryptBankAccount(item);
                _loggingEncryptionService.LogProcess(i, totalCount, item.Id);
                _unitOfWork.Save();
            }
            _loggingEncryptionService.LogEnd(nameof(BankAccountInfoEncryptor), i);
        }
    }
}