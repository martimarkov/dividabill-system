﻿using System.Linq;
using DividaBill.DAL.Interfaces;
using DividaBill.Services.Abstraction;
using EncryptDB.Abstractions;

namespace EncryptDB.EncryptInfo
{
    public class UserInfoEncryptor : ITableEncryptor
    {
        private readonly ILoggingEncryptionService _loggingEncryptionService;
        private readonly ITenantEncryptionService _tenantEncryptionService;
        private readonly IUnitOfWork _unitOfWork;

        public UserInfoEncryptor(IUnitOfWork unitOfWork, ITenantEncryptionService tenantEncryptionService,
            ILoggingEncryptionService loggingEncryptionService)
        {
            _loggingEncryptionService = loggingEncryptionService;
            _unitOfWork = unitOfWork;
            _tenantEncryptionService = tenantEncryptionService;
        }

        public void EncryptTableRows()
        {
            var list = _unitOfWork.UserRepository.Get().ToList();
            var totalCount = list.Count;
            _loggingEncryptionService.LogStart(nameof(UserInfoEncryptor), "encryption", totalCount);

            var i = 0;
            foreach (var item in list)
            {
                i++;
                _tenantEncryptionService.EncryptUser(item);
                _loggingEncryptionService.LogProcess(i, totalCount, item.Id);
                _unitOfWork.Save();
            }
            _loggingEncryptionService.LogEnd(nameof(UserInfoEncryptor), i);
        }

        public void DecryptTableRows()
        {
            var list = _unitOfWork.UserRepository.Get().ToList();
            var totalCount = list.Count;
            _loggingEncryptionService.LogStart(nameof(UserInfoEncryptor), "decryption", totalCount);

            var i = 0;
            foreach (var item in list)
            {
                i++;
                _tenantEncryptionService.DecryptUser(item);
                _loggingEncryptionService.LogProcess(i, totalCount, item.Id);
                _unitOfWork.Save();
            }
            _loggingEncryptionService.LogEnd(nameof(UserInfoEncryptor), i);
        }
    }
}