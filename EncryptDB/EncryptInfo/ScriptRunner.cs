﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EncryptDB.Abstractions;

namespace EncryptDB.EncryptInfo
{
    public class ScriptRunner : IScriptRunner
    {
        public int PickListItem(string[] itemList)
        {
            var id = 0;
            while(true)
            {
                Console.WriteLine("Do you wish to encrypt or decrypt the data? (Enter the action number and press enter...)", itemList);
                for (int i = 0; i < itemList.Length; i++)
                {
                    Console.WriteLine("[{0}] {1}", i, itemList[i]);
                }
                Console.WriteLine(">");

                var input = Console.ReadLine();

                if (int.TryParse(input, out id) && id >= 0 && id <= itemList.Length)
                {
                    Console.WriteLine("Please confirm that you want to {0} the database (yes/no)?", itemList[id]);
                    if (Console.ReadLine() == "yes")
                        break;
                }
                else
                {
                    Console.WriteLine("This is not a valid entry");
                }

            }
            return id;
        }
    }
}
