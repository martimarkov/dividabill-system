using System;
using System.IO;
using EncryptDB.Abstractions;

namespace EncryptDB.EncryptInfo
{
    public class LoggingEncryptionService : ILoggingEncryptionService
    {
        private readonly string _logFileName;

        public LoggingEncryptionService(string logFileName)
        {
            _logFileName = logFileName;
        }

        public void LogStart(string nameOfClass, string actionType, int totalCount)
        {
            var time = GetCurrentTime();
            Log($"{time}: {nameOfClass} {actionType} starting. Total rows to update: {totalCount}");
        }

        public void LogEnd(string nameOfClass, int updatedTotalCount)
        {
            var time = GetCurrentTime();
            Log($"{time}: {nameOfClass} ended. Total rows updated: {updatedTotalCount}");
        }

        public void LogProcess(int i, int totalCount, string identificator)
        {
            Log($"{i}/{totalCount}: {identificator}");
        }

        private void Log(string message)
        {
            Console.WriteLine(message);
            using (var logWriter = File.AppendText(_logFileName))
            {
                logWriter.WriteLine(message);
            }
        }

        private string GetCurrentTime()
        {
            return DateTime.Now.ToString("HH:mm:ss");
        }
    }
}