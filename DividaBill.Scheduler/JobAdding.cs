﻿using Quartz;
using Scheduler.Jobs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DividaBill.Scheduler
{
    class JobAdding
    {
        static JobKey netflixInstallJobKey = new JobKey("NetflixInstallationJob", "Manual Registration");
        static JobKey billGenerationJobKey = new JobKey("BillGeneration", "Billing");
        static TriggerKey billGenerationTriggerKey = new TriggerKey("BillGenerationMonthlyTrigger", "Billing");
        static TriggerKey netflixInstallTriggerKey = new TriggerKey("NetflixInstallationTrigger", "Manual Registration");

        // define the job and tie it to our HelloJob class
        IJobDetail billGenerationJob = JobBuilder.Create<BillGenerationJob>()
            .WithIdentity(billGenerationJobKey)
            .Build();

        // Trigger the job to run now, and then every 40 seconds
        ITrigger billGenerationTrigger = TriggerBuilder.Create()
            .WithIdentity(billGenerationTriggerKey)
            .WithCronSchedule("0 0 12 27 1/1 ?")
            .Build();


        //// Netflix installation job and trigger
        //// runs at 12:00 every day
        //IJobDetail netflixInstallJob = JobBuilder.Create<NetflixRegistrationJob>()
        //    .WithIdentity(netflixInstallJobKey)
        //    .Build();

        //ITrigger netflixInstallTrigger = TriggerBuilder.Create()
        //    .WithIdentity(netflixInstallTriggerKey)
        //    .WithCronSchedule("0 0/1 * * * ?")
        //    //.WithCronSchedule("0 0 12 * * ?")
        //    .Build();


        // Tell quartz to schedule the jobs using our trigger
        public void ScheduleAllJobs(IScheduler sched)
        {
            //sched.AddJob(billGenerationJob, true, true);
            sched.ScheduleJob(billGenerationJob, new Quartz.Collection.HashSet<ITrigger>() { billGenerationTrigger }, true);
            //sched.AddJob(netflixInstallJob, true, true);
            //sched.ScheduleJob(netflixInstallJob, new Quartz.Collection.HashSet<ITrigger>() {netflixInstallTrigger }, true);
        }
    }
}
