﻿using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using API.Library.Helpers;
using DividaBill.Library.AppConfiguration.Impl;
using NUnit.Framework;
using SendGrid.API.api;

namespace SendGrid.API.IntegrationTests.api
{
    [TestFixture]
    public class SendGridApiClientTests
    {
        [SetUp]
        public void SetUp()
        {
            ClassUnderTest = new SendGridApiClient(new AppConfigurationReader());
        }

        private ISendGridApiClient ClassUnderTest { get; set; }

        [TestCase("f82dc9ef-63a1-4286-b131-cef322139f7e", "Final Update (Ns)", "55779911@mailinator.com")]
        [TestCase("a4787c92-273a-415f-bd1c-a1c928d9d47d", "Important Account Information (Ns)", "55779911@mailinator.com")]
        [TestCase("315c4668-9372-4ab5-b193-b62f77ca8c88", "House Account Update (Ns)", "55779911@mailinator.com")]
        [TestCase("8bdfa908-c822-4577-93e4-5276b5cb9a41", "Verify Email Mc", "55779911@mailinator.com")]
        //[TestCase("330885c2-c2a0-4f8c-a649-c2d16a33dbfe", "Verify Email Mc Upload", "55779911@mailinator.com")]
        [TestCase("b362c378-bcf7-4aa0-924b-937081678c5a", "Customer Invoice (Vat)", "55779911@mailinator.com")]
        [TestCase("e2fd1c56-f055-4240-8f3e-b79a47e2649c", "Sky Installation", "55779911@mailinator.com")]
        [TestCase("3676c914-6b2a-439b-864d-f82d80093c8f", "Divida Bill Service Update", "55779911@mailinator.com")]
        [TestCase("99f3280b-49e6-40c3-8800-855c38c458a8", "Account Update", "55779911@mailinator.com")]
        [TestCase("fd64834b-bef2-4edf-9669-d60b9e0f839e", "Welcome Email", "55779911@mailinator.com")]
        [TestCase("80456456-c42d-47fc-8fe2-552d480b0893", "Christmas Message", "55779911@mailinator.com")]
        [TestCase("0ff28ab6-84db-4096-8bab-62cabab63726", "Bill Discount", "55779911@mailinator.com")]
        [TestCase("967b8c59-06d2-4dcb-8b37-1f2652ac8ef3", "Energy Bill", "55779911@mailinator.com")]
        [TestCase("bee427c1-cb2f-4de1-a55c-77ccb56262b5", "Creative Test Campaign", "55779911@mailinator.com")]
        [TestCase("296231e7-6d48-4c66-85cb-4ebd3f035f04", "Basic Template", "55779911@mailinator.com")]
        [TestCase("8013b3aa-6a95-4080-962d-e4c3dd083ed9", "Energy Rebate", "55779911@mailinator.com")]
        [TestCase("afbddfdc-1603-4424-a74e-bcdb2e1fae2a", "Tv Licensing", "55779911@mailinator.com")]
        [TestCase("7d20ecf5-7c6c-4208-9632-abed8d5f51ea", "October Bills", "55779911@mailinator.com")]
        [TestCase("6181766a-9db9-460f-9baa-7801239e3f1b", "Change Of Housemates", "55779911@mailinator.com")]
        [TestCase("0f1a629d-8562-499f-971d-7ac827bd83b5", "House Account Update", "55779911@mailinator.com")]
        [TestCase("538b0e1e-4f96-4f75-8c78-d3d127f3f1fd", "Signup Fix", "55779911@mailinator.com")]
        [TestCase("770e7864-965b-45d8-b2bb-6254de56674f", "Contract Renewal", "55779911@mailinator.com")]
        [TestCase("b65133dc-81ee-4bae-8974-4454909d9491", "Contract End", "55779911@mailinator.com")]
        [TestCase("6428fb87-e653-4864-96ba-a22cab203218", "End Of Contract Reminder", "55779911@mailinator.com")]
        [TestCase("862b81d3-488f-4182-82ad-65a11a1d1e1e", "Service Addition", "55779911@mailinator.com")]
        [TestCase("79517f03-d3a0-4c2d-94fa-c6332cd116a0", "Service Removal", "55779911@mailinator.com")]
        [TestCase("e7bb0193-9f63-422e-88a4-867195e350b2", "Cancellation", "55779911@mailinator.com")]
        [TestCase("12357bc6-74b0-4d43-89fb-eacb28c2491b", "Housemate 12 Day Reminder", "55779911@mailinator.com")]
        [TestCase("03840614-b277-4a59-829e-36c2fb31ffe0", "Housemate 8 Day Reminder", "55779911@mailinator.com")]
        [TestCase("580891b0-de02-40dd-bd9f-f8cc34649370", "General Amended Bill", "55779911@mailinator.com")]
        [TestCase("dad89689-d645-489b-81a2-ed8abe8796e4", "Email Verification 7 Day Reminder", "55779911@mailinator.com")]
        [TestCase("b48d4bf8-5b32-43ba-ae2b-3dd371b67849", "Email Verification", "55779911@mailinator.com")]
        [TestCase("7a65be25-7120-4ebc-86bd-e751758cf2d1", "Energy Switch Day", "55779911@mailinator.com")]
        [TestCase("f9635ac9-c741-4ee5-bf97-5546709ed9ab", "Welcome Email Secondary Tenants", "55779911@mailinator.com")]
        [TestCase("682bafb0-f89f-4ed2-82e6-86a9c50ce55e", "First Bill Email September Starters", "55779911@mailinator.com")]
        [TestCase("45d52210-2607-4d5a-829b-e1ac93990e1b", "Energy Emails", "55779911@mailinator.com")]
        [TestCase("a9f6eb05-3b58-4d36-a15e-f00cb86aaef8", "Amended First Bill", "55779911@mailinator.com")]
        [TestCase("2d9d6dc2-0f78-43fa-880a-a0d9bff8deea", "Billing Error", "55779911@mailinator.com")]
        [TestCase("feed00a4-b0ef-4bfe-a883-8abc99a3f592", "Fibre Optic Installation", "55779911@mailinator.com")]
        [TestCase("ff2db864-6e99-45f3-892c-69637ff00620", "Adsl Installation", "55779911@mailinator.com")]
        [TestCase("7018e591-a2d1-4bf4-aa4d-132b0a7de880", "Fibre Optic Unavailable", "55779911@mailinator.com")]
        [TestCase("df816c5f-8bb0-4dbe-a99f-96c838070696", "New Billing Process", "55779911@mailinator.com")]
        [TestCase("db84e1fd-b6a9-4ac9-aac5-c8d338e55756", "Help Centre", "55779911@mailinator.com")]
        [TestCase("2b2f2d36-76ce-434a-bd02-c52fb17d1b2c", "Initial Billing Estimate And Breakdown", "55779911@mailinator.com")]
        [TestCase("46ad5699-0dfb-4b91-901e-d8f0e459d2e9", "Welcome Email First Tenant", "55779911@mailinator.com")]
        [TestCase("9f296507-0a60-4ab5-b2b7-ecd6adac080a", "Billing Email Apology", "55779911@mailinator.com")]
        [TestCase("719749bb-9d9b-4387-8f47-de48bd763add", "First Bill Template", "55779911@mailinator.com")]
        [TestCase("e9fc4b83-ab0d-43d2-979a-fe9d0c0eef52", "General Bill Template", "55779911@mailinator.com")]
        public async Task SendGridEmailSendFromTemplate(string sendGridTemplateId, string subject, string emailTo)
        {
            await ClassUnderTest.SendEmailFromTemplate(sendGridTemplateId, $"{subject} for Integration Test", new MailAddress("info@dividabill.co.uk", "Info@SendGrid Integration Test"), emailTo, new Dictionary<string, string> {{"{{first_name}},", "Tech Team"}});
        }

        [TestCase("f82dc9ef-63a1-4286-b131-cef322139f7e")]
        [TestCase("a4787c92-273a-415f-bd1c-a1c928d9d47d")]
        [TestCase("315c4668-9372-4ab5-b193-b62f77ca8c88")]
        [TestCase("8bdfa908-c822-4577-93e4-5276b5cb9a41")]
        //[TestCase("330885c2-c2a0-4f8c-a649-c2d16a33dbfe")]
        [TestCase("b362c378-bcf7-4aa0-924b-937081678c5a")]
        [TestCase("e2fd1c56-f055-4240-8f3e-b79a47e2649c")]
        [TestCase("3676c914-6b2a-439b-864d-f82d80093c8f")]
        [TestCase("99f3280b-49e6-40c3-8800-855c38c458a8")]
        [TestCase("fd64834b-bef2-4edf-9669-d60b9e0f839e")]
        [TestCase("80456456-c42d-47fc-8fe2-552d480b0893")]
        [TestCase("0ff28ab6-84db-4096-8bab-62cabab63726")]
        [TestCase("967b8c59-06d2-4dcb-8b37-1f2652ac8ef3")]
        [TestCase("bee427c1-cb2f-4de1-a55c-77ccb56262b5")]
        [TestCase("296231e7-6d48-4c66-85cb-4ebd3f035f04")]
        [TestCase("8013b3aa-6a95-4080-962d-e4c3dd083ed9")]
        [TestCase("afbddfdc-1603-4424-a74e-bcdb2e1fae2a")]
        [TestCase("7d20ecf5-7c6c-4208-9632-abed8d5f51ea")]
        [TestCase("6181766a-9db9-460f-9baa-7801239e3f1b")]
        [TestCase("0f1a629d-8562-499f-971d-7ac827bd83b5")]
        [TestCase("538b0e1e-4f96-4f75-8c78-d3d127f3f1fd")]
        [TestCase("770e7864-965b-45d8-b2bb-6254de56674f")]
        [TestCase("b65133dc-81ee-4bae-8974-4454909d9491")]
        [TestCase("6428fb87-e653-4864-96ba-a22cab203218")]
        [TestCase("862b81d3-488f-4182-82ad-65a11a1d1e1e")]
        [TestCase("79517f03-d3a0-4c2d-94fa-c6332cd116a0")]
        [TestCase("e7bb0193-9f63-422e-88a4-867195e350b2")]
        [TestCase("12357bc6-74b0-4d43-89fb-eacb28c2491b")]
        [TestCase("03840614-b277-4a59-829e-36c2fb31ffe0")]
        [TestCase("580891b0-de02-40dd-bd9f-f8cc34649370")]
        [TestCase("dad89689-d645-489b-81a2-ed8abe8796e4")]
        [TestCase("b48d4bf8-5b32-43ba-ae2b-3dd371b67849")]
        [TestCase("7a65be25-7120-4ebc-86bd-e751758cf2d1")]
        [TestCase("f9635ac9-c741-4ee5-bf97-5546709ed9ab")]
        [TestCase("682bafb0-f89f-4ed2-82e6-86a9c50ce55e")]
        [TestCase("45d52210-2607-4d5a-829b-e1ac93990e1b")]
        [TestCase("a9f6eb05-3b58-4d36-a15e-f00cb86aaef8")]
        //[TestCase("2d9d6dc2-0f78-43fa-880a-a0d9bff8deea")]
        [TestCase("feed00a4-b0ef-4bfe-a883-8abc99a3f592")]
        [TestCase("ff2db864-6e99-45f3-892c-69637ff00620")]
        [TestCase("7018e591-a2d1-4bf4-aa4d-132b0a7de880")]
        [TestCase("df816c5f-8bb0-4dbe-a99f-96c838070696")]
        [TestCase("db84e1fd-b6a9-4ac9-aac5-c8d338e55756")]
        [TestCase("2b2f2d36-76ce-434a-bd02-c52fb17d1b2c")]
        [TestCase("46ad5699-0dfb-4b91-901e-d8f0e459d2e9")]
        [TestCase("9f296507-0a60-4ab5-b2b7-ecd6adac080a")]
        [TestCase("719749bb-9d9b-4387-8f47-de48bd763add")]
        [TestCase("e9fc4b83-ab0d-43d2-979a-fe9d0c0eef52")]
        public async Task GivenSendGridTemplateId_WhenGetSendGridTemplateByIdInvoked_ThanSuccessfulResponse(string sendGridTemplateId)
        {
            var result = await ClassUnderTest.GetSendGridTemplateById(sendGridTemplateId);

            Assert.NotNull(result);
            Assert.IsNotEmpty(result.id);
            Assert.IsNotEmpty(result.name);
            Assert.NotNull(result.versions);
            Assert.Greater(result.versions.Count, 0);
            Assert.IsNotNull(result.GetActiveVersion());
            Assert.IsNotEmpty(result.versions[0].id);
            Assert.IsNotEmpty(result.versions[0].html_content);
            Assert.IsNotNull(result.versions[0].updated_at.Day);
        }

        [TestCase("some wrong template guid")]
        public async Task GivenWrongTemplateId_WhenGetSendGridTemplateByIdInvoked_Than404ErrorThrown(string sendGridTemplateId)
        {
            try
            {
                await ClassUnderTest.GetSendGridTemplateById(sendGridTemplateId);
                Assert.Fail("ApiException wasn't thrown");
            }
            catch (ApiException exception)
            {
                Assert.AreEqual(HttpStatusCode.NotFound, exception.StatusCode);
            }
        }

        [Test]
        public async Task GetSendGridAllTemplates()
        {
            var result = await ClassUnderTest.GetSendGridTemplates();

            Assert.NotNull(result);
            Assert.NotNull(result.templates);
            Assert.Greater(result.templates.Count, 0);
            Assert.NotNull(result.templates[0].id);
            Assert.NotNull(result.templates[0].name);
        }
    }
}