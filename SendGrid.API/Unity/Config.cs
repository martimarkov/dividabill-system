using Microsoft.Practices.Unity;
using SendGrid.API.api;

namespace SendGrid.API.Unity
{
    public static class Config
    {
        public static void RegisterSendGridApi(this IUnityContainer container)
        {
            container.RegisterType<ISendGridApiClient, SendGridApiClient>();
        }
    }
}