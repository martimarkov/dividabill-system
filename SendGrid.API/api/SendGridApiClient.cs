using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using API.Library.Models;
using API.Library.Request.Impl;
using DividaBill.AppConstants;
using DividaBill.Library;
using DividaBill.Library.AppConfiguration;
using RestSharp;
using SendGrid.API.Models;

namespace SendGrid.API.api
{
    public class SendGridApiClient : ISendGridApiClient, IDisposable
    {
        private readonly ApiRestRequest _apiRestRequest;

        public SendGridApiClient(IAppConfigurationReader appConfigurationReader)
        {
            SendGridApiKey = appConfigurationReader.GetFromAppConfig(AppSettingsEnum.SendGridApiKey);

            var sendgridCsharp = $"sendgrid/{Functions.GetAssemblyFileVersion(typeof (SendGridMessage))};csharp"; //6.3.4.0
            var apiRestConfig = new ApiRestConfig(new Uri("https://api.sendgrid.com/v3/"), sendgridCsharp, SendGridApiKey);
            _apiRestRequest = new ApiRestRequest(apiRestConfig);
        }

        private string SendGridApiKey { get; }

        public void Dispose()
        {
            _apiRestRequest.Dispose();
        }

        public async Task SendEmailFromTemplate(string sendGridEmailGuid, string subject, MailAddress from, string emailAddress, Dictionary<string, string> substitutionsList)
        {
            await SendEmailFromTemplate(sendGridEmailGuid, subject, from, new List<string> {emailAddress}, substitutionsList);
        }

        public async Task SendEmailFromTemplate(string sendGridEmailGuid, string subject, MailAddress from, List<string> emailAddresses, Dictionary<string, string> substitutionsList)
        {
            var myMessage = new SendGridMessage {From = @from};

            myMessage.AddTo(emailAddresses);

            myMessage.Subject = subject;
            myMessage.Text = " ";
            myMessage.Html = " ";

            myMessage.EnableTemplateEngine(sendGridEmailGuid);

            if (substitutionsList != null)
                foreach (var key in substitutionsList.Keys.Where(key => substitutionsList[key] != null))
                    myMessage.AddSubstitution(key, new List<string> {substitutionsList[key]});

            //var credentials = new NetworkCredential(SendGridUserName, SendGridPassword);
            var transportWeb = new Web(SendGridApiKey);

            await transportWeb.DeliverAsync(myMessage);
        }

        public async Task<SendGridTemplates> GetSendGridTemplates()
        {
            var restRequest = _apiRestRequest.CreateRestRequest("templates", Method.GET);

            var response = await _apiRestRequest.ExecuteAsync<SendGridTemplates>(restRequest);
            return response;
        }

        public async Task<SendGridTemplate> GetSendGridTemplateById(string templateId)
        {
            var restRequest = _apiRestRequest.CreateRestRequest("templates/{template_id}", Method.GET)
                .AddUrlSegment("template_id", templateId);

            var response = await _apiRestRequest.ExecuteAsync<SendGridTemplate>(restRequest);
            return response;
        }
    }
}