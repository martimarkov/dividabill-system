using System.Collections.Generic;
using System.Net.Mail;
using System.Threading.Tasks;
using SendGrid.API.Models;

namespace SendGrid.API.api
{
    public interface ISendGridApiClient
    {
        Task SendEmailFromTemplate(string sendGridEmailGuid, string subject, MailAddress from, List<string> emailAddresses, Dictionary<string, string> substitutionsList);
        Task SendEmailFromTemplate(string sendGridEmailGuid, string subject, MailAddress from, string emailAddress, Dictionary<string, string> substitutionsList);

        Task<SendGridTemplates> GetSendGridTemplates();
        Task<SendGridTemplate> GetSendGridTemplateById(string templateId);
    }
}