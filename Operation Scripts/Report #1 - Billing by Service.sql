/*
1. a report showing the number of active houses for October and detail the  number of tenants, total amount billed, the number of houses using each service (ie water, gas and elec etc), and the total billed for each of the services.  If you could produce this report for each month back to the start of 2014.  Can you also included on this report the total number of non active houses in the monthly summary
*/

DECLARE @dummyHouse INT = 1270

CREATE TABLE #DateRange (ID INT IDENTITY(1,1), Firstdate DATETIME, LastDate DATETIME, [year] int, [month] int)

DECLARE @N DATETIME = '2015-12-31'
DECLARE @T DATETIME = '2014-06-01'
DECLARE @L DATETIME

WHILE @T < @N 
BEGIN

	SET @T = (SELECT DATEADD(month, DATEDIFF(month, 0, @T), 0))

	SET @L = (SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@T)+1,0)))

	INSERT INTO #DateRange (FirstDate, LastDate)
	SELECT @T, @L

	SET @T = DATEADD(d, 32, @T)
END
UPDATE #DateRange SET [Year] = DATEPART(yy, LastDate), [Month] = DATEPART(mm, LastDate)

--SELECT *, Datename(mm, FirstDate), Datename(mm, LastDate), Datepart(YY, FirstDate), Datepart(YY, LastDate) from #DateRange

select '#DateRange', * from #DateRange



DECLARE @j INT = (SELECT MAX(ID) FROM #DateRange)
DECLARE @i INT = 1

DECLARE @Firstdate DATETIME
DECLARE @LastDate DATETIME
DECLARE @Month INT, @year INT
DECLARE @ActiveHouseCount INT

CREATE TABLE #HouseServiceAnalysis (
[Month] INT
, [Year] INT
, ActiveHouseCount INT
, Housemates INT
, TotalAmounBilled MONEY
, [Nb of Houses with Electricity contract] INT default 0
, [Total Amount billed for Electricity] INT default 0
, [Nb of Houses with Gas contract] INT default 0
, [Total Amount billed for Gas] INT default 0
, [Nb of Houses with Water contract] INT default 0
, [Total Amount billed for Water] INT default 0
, [Nb of Houses with Broadband contract] INT default 0
, [Total Amount billed for Broadband] INT default 0
, [Nb of Houses with NetflixTV contract] INT default 0
, [Total Amount billed for NetflixTV] INT default 0
, [Nb of Houses with SkyTV contract] INT default 0
, [Total Amount billed for SkyTV] INT default 0
, [Nb of Houses with TVLicense contract] INT default 0
, [Total Amount billed for TVLicense] INT default 0
, [Nb of Houses with LandlinePhone contract] INT default 0
, [Total Amount billed for LandlinePhone] INT default 0
, [Nb of Houses with LineRental contract] INT default 0
, [Total Amount billed for LineRental] INT default 0
)

WHILE @i <= @j
BEGIN

	SELECT @Firstdate = Firstdate, @LastDate = LastDate, @Month = [month], @Year = [year]  FROM #DateRange WHERE ID = @i

	SELECT 'ActiveHouseIds' as Status
	, ID --'HouseID' 
	INTO #activehouses
	FROM HouseModels WHERE startdate <= @Firstdate AND enddate > @LastDate and id != @dummyHouse

	SELECT House_ID, ID AS HouseHoldbill_id
	INTO #householdbills
	FROM HouseHoldbills 
	WHERE month = @month and year = @year


	SELECT @ActiveHouseCount = COUNT(*) FROM #activehouses

	INSERT INTO #HouseServiceAnalysis
	SELECT 
		@Month
		, @Year
		, @ActiveHouseCount AS ActiveHouseCount
		, ISNULL((SELECT SUM(h.Housemates) FROM HouseModels h INNER JOIN #activehouses ah ON h.ID = ah.ID), 0) as TotalHousemates
		, ISNULL((SELECT SUM(bb.ammount) FROM #activehouses ah INNER JOIN #householdbills hb ON ah.ID = hb.House_ID INNER JOIN Billbases bb ON hb.HouseHoldbill_id = bb.HouseHoldbill_id), 0) AS TotalAmountBilled
		, ISNULL((SELECT COUNT(ce.House_ID) FROM Contracts ce INNER JOIN #activehouses ah ON ce.House_ID = ah.ID AND ce.ServiceNew_ID = 1), 0) AS 'Nb of Houses with Electricity contract'
		
		, ISNULL((SELECT SUM(bb.Ammount) FROM Contracts ce 
			INNER JOIN #activehouses ah ON ce.House_ID = ah.ID 
			INNER JOIN #householdbills hb ON ah.ID = hb.House_ID 
			INNER JOIN Billbases bb ON bb.Contract_ID = ce.ID 
						AND hb.HouseHoldbill_id = bb.HouseHoldbill_id AND ce.ServiceNew_ID = 1), 0) AS 'Total Amount billed for Electricty'
		
		, ISNULL((SELECT COUNT(cg.House_ID) FROM Contracts cg INNER JOIN #activehouses ah ON cg.House_ID = ah.ID AND cg.ServiceNew_ID = 2), 0) AS 'Nb of Houses with Gas contract'
		, ISNULL((SELECT SUM(bb.Ammount) FROM Contracts cg INNER JOIN #activehouses ah ON cg.House_ID = ah.ID INNER JOIN #householdbills hb ON ah.ID = hb.House_ID INNER JOIN Billbases bb ON bb.Contract_ID = cg.ID AND hb.HouseHoldbill_id = bb.HouseHoldbill_id AND cg.ServiceNew_ID = 2), 0) AS 'Total Amount billed for Gas'
		, ISNULL((SELECT COUNT(cw.House_ID) FROM Contracts cw INNER JOIN #activehouses ah ON cw.House_ID = ah.ID AND cw.ServiceNew_ID = 3), 0) AS 'Nb of Houses with Water contract'
		, ISNULL((SELECT SUM(bb.Ammount) FROM Contracts cw INNER JOIN #activehouses ah ON cw.House_ID = ah.ID INNER JOIN #householdbills hb ON ah.ID = hb.House_ID INNER JOIN Billbases bb ON bb.Contract_ID = cw.ID AND hb.HouseHoldbill_id = bb.HouseHoldbill_id AND cw.ServiceNew_ID = 3), 0) AS 'Total Amount billed for Water'
		, ISNULL((SELECT COUNT(cb.House_ID) FROM Contracts cb INNER JOIN #activehouses ah ON cb.House_ID = ah.ID AND cb.ServiceNew_ID = 4), 0) AS 'Nb of Houses with Broadband contract'
		, ISNULL((SELECT SUM(bb.Ammount) FROM Contracts cb INNER JOIN #activehouses ah ON cb.House_ID = ah.ID INNER JOIN #householdbills hb ON ah.ID = hb.House_ID INNER JOIN Billbases bb ON bb.Contract_ID = cb.ID AND hb.HouseHoldbill_id = bb.HouseHoldbill_id AND cb.ServiceNew_ID = 4), 0) AS 'Total Amount billed for Broadband'
		, ISNULL((SELECT COUNT(cn.House_ID) FROM Contracts cn INNER JOIN #activehouses ah ON cn.House_ID = ah.ID AND cn.ServiceNew_ID = 5), 0) AS 'Nb of Houses with NetflixTV contract'
		, ISNULL((SELECT SUM(bb.Ammount) FROM Contracts cn INNER JOIN #activehouses ah ON cn.House_ID = ah.ID INNER JOIN #householdbills hb ON ah.ID = hb.House_ID INNER JOIN Billbases bb ON bb.Contract_ID = cn.ID AND hb.HouseHoldbill_id = bb.HouseHoldbill_id AND cn.ServiceNew_ID = 5), 0) AS 'Total Amount billed for NetflixTV'
		, ISNULL((SELECT COUNT(cs.House_ID) FROM Contracts cs INNER JOIN #activehouses ah ON cs.House_ID = ah.ID AND cs.ServiceNew_ID = 6), 0) AS 'Nb of Houses with SkyTV contract'
		, ISNULL((SELECT SUM(bb.Ammount) FROM Contracts cs INNER JOIN #activehouses ah ON cs.House_ID = ah.ID INNER JOIN #householdbills hb ON ah.ID = hb.House_ID INNER JOIN Billbases bb ON bb.Contract_ID = cs.ID AND hb.HouseHoldbill_id = bb.HouseHoldbill_id AND cs.ServiceNew_ID = 6), 0) AS 'Total Amount billed for SkyTV'
		, ISNULL((SELECT COUNT(ct.House_ID) FROM Contracts ct INNER JOIN #activehouses ah ON ct.House_ID = ah.ID AND ct.ServiceNew_ID = 7), 0) AS 'Nb of Houses with TVLicense contract'
		, ISNULL((SELECT SUM(bb.Ammount) FROM Contracts ct INNER JOIN #activehouses ah ON ct.House_ID = ah.ID INNER JOIN #householdbills hb ON ah.ID = hb.House_ID INNER JOIN Billbases bb ON bb.Contract_ID = ct.ID AND hb.HouseHoldbill_id = bb.HouseHoldbill_id AND ct.ServiceNew_ID = 7), 0) AS 'Total Amount billed for TVLicense'
		, ISNULL((SELECT COUNT(cla.House_ID) FROM Contracts cla INNER JOIN #activehouses ah ON cla.House_ID = ah.ID AND cla.ServiceNew_ID = 8), 0) AS 'Nb of Houses with LandlinePhone contract'
		, ISNULL((SELECT SUM(bb.Ammount) FROM Contracts cla INNER JOIN #activehouses ah ON cla.House_ID = ah.ID INNER JOIN #householdbills hb ON ah.ID = hb.House_ID INNER JOIN Billbases bb ON bb.Contract_ID = cla.ID AND hb.HouseHoldbill_id = bb.HouseHoldbill_id AND cla.ServiceNew_ID = 8), 0) AS 'Total Amount billed for LandlinePhone'
		, ISNULL((SELECT COUNT(cli.House_ID) FROM Contracts cli INNER JOIN #activehouses ah ON cli.House_ID = ah.ID AND cli.ServiceNew_ID = 9), 0) AS 'Nb of Houses with LineRental contract'
		, ISNULL((SELECT SUM(bb.Ammount) FROM Contracts cli INNER JOIN #activehouses ah ON cli.House_ID = ah.ID INNER JOIN #householdbills hb ON ah.ID = hb.House_ID INNER JOIN Billbases bb ON bb.Contract_ID = cli.ID AND hb.HouseHoldbill_id = bb.HouseHoldbill_id AND cli.ServiceNew_ID = 9), 0) AS 'Total Amount billed for LineRental'
	SET @i = @i +1

	DROP TABLE #activehouses, #householdbills
END

SELECT '#HouseServiceAnalysis', * FROM #HouseServiceAnalysis


DROP TABLE #DateRange, #HouseServiceAnalysis

