﻿using System;
using Moq;
using Moq.AutoMock;
using NUnit.Framework;

namespace Dividabill.Tests.Core
{
    public class BaseAutoMock<T> where T : class
    {
        private T _classUnderTest;

        private AutoMocker _mocker;

        public T ClassUnderTest
        {
            get
            {
                if (_classUnderTest == null)
                    throw new Exception("Access to ClassUnderTest when not initialised.");
                return _classUnderTest;
            }
        }

        [SetUp]
        public void BaseSetUp()
        {
            _mocker = new AutoMocker();
            _classUnderTest = _mocker.CreateInstance<T>();
        }

        [TearDown]
        public void BaseTearDown()
        {
            _classUnderTest = null;
            _mocker = null;
        }

        public Mock<T1> GetMock<T1>() where T1 : class
        {
            return _mocker.GetMock<T1>();
        }

        //public T2 CreateInstance<T2>() where T2 : class
        //{
        //    return _mocker.CreateInstance<T2>();
        //}
    }
}