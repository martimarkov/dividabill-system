﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using CsvHelper;
using DividaBill.DAL.Interfaces;
using DividaBill.Library;
using DividaBill.Models;
using DividaBill.Services.Interfaces;
using DividaBill.Services.ViewModel;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v2;
using Google.Apis.Drive.v2.Data;

namespace DividaBill.Services.Sheets
{
    /// <summary>
    /// Queries files inside GDrive/GSheets for any changes occurred since our last check. 
    /// </summary>
    public class GDriveService : IGDriveService
    {
        // Obtained from Google Developer Console
        const string CertificatePass = "notasecret";

        /// <summary>
        /// The service account identifier. 
        /// </summary>
        const string ServiceAccountEmail = "121736045962-o1gqrkg4limbtqam681dvfnkfcp2j99t@developer.gserviceaccount.com";
        /// <summary>
        /// The user account to log in as. 
        /// </summary>
        const string UserAccountEmail = "google@dividabill.co.uk";
        /// <summary>
        /// The application name as set in the Developer Console. 
        /// </summary>
        const string ApplicationName = "divida-test";
        /// <summary>
        /// The MIME type of a spreadsheet as reported by Google Drive. 
        /// </summary>
        const string SpreadSheetMimeType = "application/vnd.google-apps.spreadsheet";
        /// <summary>
        /// The MIME type of a CSV file. 
        /// </summary>
        const string CsvMimeType = "text/csv";

        public static string CertificateFile { get; private set; }

        static readonly Dictionary<string, Type> sheetDict = new Dictionary<string, Type>
        {
            { "Origin", typeof(OriginRegistrationForm) },
            { "Spark", typeof(SparkOnboarding) },
        };

        struct GFileMetadata
        {
            public Type RowType { get; set; }
            public string Name { get; set; }
            public string ProviderName { get; set; }
            public int KeyColumn { get; set; }
        }

        static readonly GFileMetadata[] FileMetadata = new[]
        {
            new GFileMetadata { Name = "Origin", RowType = typeof(OriginRegistrationForm), ProviderName = "Origin Broadband", KeyColumn = 20 },
            //new GFileMetadata { Name = "Spark", RowType = typeof(SparkOnboarding), ProviderName = "Spark Energy" },
        };




        public static void Initialize(string certificateLocation)
        {
            if (!System.IO.File.Exists(certificateLocation))
                throw new ArgumentException(nameof(certificateLocation), "The file at '{0}' does not exist!".F(certificateLocation));

            CertificateFile = certificateLocation;
        }



        readonly IUnitOfWork unitOfWork;
        readonly ServiceAccountCredential credentials;
        readonly DriveService GDrive;
        readonly CsvComparer csvComparer = new CsvComparer();


        /// <summary>
        /// Creates a new guy, sets up his credentials, 
        /// and finally logs him in to GDrive and GSheets. 
        /// </summary>
        /// <param name="unitOfWork"></param>
        public GDriveService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;

            //setup credentials

            var certificate = new X509Certificate2(CertificateFile, CertificatePass, X509KeyStorageFlags.Exportable);
            credentials = new ServiceAccountCredential(new ServiceAccountCredential.Initializer(ServiceAccountEmail)
            {
                User = UserAccountEmail,
                Scopes = new[] { DriveService.Scope.Drive },
            }.FromCertificate(certificate));
            credentials.RequestAccessTokenAsync(System.Threading.CancellationToken.None).Wait();

            //setup drive service
            GDrive = new DriveService(new DriveService.Initializer()
            {
                HttpClientInitializer = credentials,
                ApplicationName = ApplicationName,
            });

            //create all required file references in the db
            foreach (var d in FileMetadata)
            {
                var dbFile = unitOfWork.GDriveFileRepository.Get().SingleOrDefault(f => f.FileName == d.Name);
                if (dbFile == null)
                {
                    var provider = unitOfWork.ProviderRepository.Get().FirstOrDefault(p => p.Name == d.ProviderName);
                    if (provider == null)
                        throw new Exception("Unable to find a provider named '{0}' for a sheets file '{1}'".F(d.ProviderName, d.Name));

                    dbFile = new Models.Sheets.File(provider, d.Name, d.KeyColumn);
                    unitOfWork.GDriveFileRepository.Insert(dbFile);
                }
                
            }
            unitOfWork.Save();
        }

        public void SyncAll(bool saveChanges)
        {
            var sparkDbFile = unitOfWork.GDriveFileRepository.Get()
                .Single(f => f.FileName == "Spark");

            var houses = unitOfWork.ActiveHouses.Get()
                .Where(h => h.StartDate.Year < 2020)
                .ToArray();

            Sync(sparkDbFile, saveChanges, houses);
        }

        /// <summary>
        /// First pull, then push. 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="file"></param>
        /// <param name="commitToDb"></param>
        /// <param name="houses"></param>
        public void Sync(Models.Sheets.File file, bool commitToDb, IEnumerable<HouseModel> houses)
        {
            Pull(file, commitToDb);
            Push(file, commitToDb, houses);
        }


        public void Pull(Models.Sheets.File file, bool commitToDb)
        {
            var csvRowType = sheetDict[file.FileName];

            unitOfWork.WrapInTransaction(
                () => pullUnsafe(csvRowType, file),
                commitOnSuccess: commitToDb);
        }

        public void Push(Models.Sheets.File file, bool commitToDb, IEnumerable<HouseModel> houses)
        {
            var csvRowType = sheetDict[file.FileName];

            unitOfWork.WrapInTransaction(
                () => pushUnsafe(csvRowType, file, houses, commitToDb),
                commitOnSuccess: commitToDb);
        }

        #region Raw push/pull methods
        void pushUnsafe(Type csvRowType, Models.Sheets.File file, IEnumerable<HouseModel> houses, bool pushToDrive)
        {
            //check type
            if (!typeof(ICsvRow).IsAssignableFrom(csvRowType))
                throw new ArgumentException(nameof(csvRowType), "The provided type must be a ICsvRow!");

            //fetch file, rows
            Console.WriteLine("Fetching the '{0}' file..", file.FileName);
            var fInfo = getFileInfo(file.FileName);
            var fContents = getFileContents(fInfo);
            var fileRows = readCsvData(csvRowType, fContents);

            Console.WriteLine("Start writing {0} houses.. ");
            //update their entries
            foreach (var h in houses)
            {
                // get their requested contracts
                var cs = h.GetRequestedContracts();

                // see if a row exists, or a new one is to be made
                ICsvRow val;
                var wasInside = fileRows.TryGetValue(h.ID, out val);
                if (!wasInside)
                    val = (ICsvRow)Activator.CreateInstance(csvRowType);

                // update the row with data from the database
                if (val.LoadFromDb(cs)) //if it is to be written to the sheet
                {
                    Debug.Assert(val.HouseId == h.ID);

                    // add the model to the sheet, if it wasn't in
                    if (!wasInside)
                    {
                        fileRows.Add(h.ID, val);
                        Console.WriteLine("House {0} added!", h.ID);
                    }
                    else
                        Console.WriteLine("House {0} updated!", h.ID);

                }
                else if (wasInside)     //should not be in the sheet, but was in
                {
                    fileRows.Remove(h.ID);
                    Console.WriteLine("House {0} removed!", h.ID);
                }
            }

            //make the new CSV
            Console.WriteLine("Writing CSV...");
            fContents = writeCsvData(csvRowType, fileRows);

            //get the new revision
            var rev = getFileChanges(file, fContents, doneByOurselves: true);
            if (rev == null)
            {
                Console.WriteLine("No changes were detected. ");
                return;
            }

            Console.WriteLine("Updated {0} rows within the sheet. ", rev.ChangedRows.Count());

            //upload to gdrive
            if (pushToDrive)
            {
                Console.WriteLine("Uploading to GDrive...");
                uploadFileData(fInfo, fContents);
            }
            else
                Console.WriteLine("Skipped uploading to GDrive. ");

            Console.WriteLine("Finished!");
        }

        void pullUnsafe(Type csvRowType, Models.Sheets.File file)
        {
            //check type
            if (!typeof(ICsvRow).IsAssignableFrom(csvRowType))
                throw new ArgumentException(nameof(csvRowType), "The provided type must be a ICsvRow!");

            //fetch file, rows
            Console.WriteLine("Fetching the '{0}' file..", file.FileName);
            var fInfo = getFileInfo(file.FileName);
            var fContents = getFileContents(fInfo);
            var fileRows = readCsvData(csvRowType, fContents);

            //get the new revision
            var rev = getFileChanges(file, fContents, doneByOurselves: false);

            if (rev == null)
            {
                Console.WriteLine("No changes were detected. ");
                return;
            }

            Console.WriteLine("Found {0} changed rows within the sheet. ", rev.ChangedRows.Count());

            //update db with data from the file. 

            var i = 0;
            foreach (var ch in rev.ChangedRows)
            {
                //get the house
                Console.CursorLeft = 0;
                Console.Write("Updating house {0}/{1}", ++i, rev.ChangedRows.Count);
                var h = unitOfWork.ActiveHouses.GetByID(ch.RowId);

                if (h == null)
                {
                    Console.WriteLine("\nWARNING: House #{0} does not exist in the database!", ch.RowId);
                    continue;
                }

                //do the update
                fileRows[ch.RowId].SaveToDb(h);

                //commit the dhanges
                unitOfWork.ActiveHouses.Update(h);
            }

            unitOfWork.Save();
        }
        #endregion


        /// <summary>
        /// Gets all rows that changed in a file since its last revision. 
        /// </summary>
        /// <param name="f">The file reference. </param>
        /// <param name="newDatas">The new data of the CSV file. </param>
        /// <param name="doneByOurselves">Whether to mark these changes as done by ourselves, or a third-party. </param>
        /// <param name="commitToDatabase">If false performs a dry run without affecting the database. </param>
        /// <param name="revision">The revision to add the changes to, or null </param>
        /// <returns></returns>
        Models.Sheets.FileRevision getFileChanges(
            Models.Sheets.File f,
            byte[] newDatas,
            bool doneByOurselves)
        {
            //get old datas
            var oldRev = f.Revisions
                .ArgMax(rev => rev.CreationDate);
            var oldDatas = oldRev?.FileData;

            //create new revision, check changes
            var revision = new Models.Sheets.FileRevision(f, newDatas, doneByOurselves);
            var changes = csvComparer.CheckForChanges(revision, oldDatas, newDatas, f.KeyColumnId);

            if (!changes.Any())
                return null;

            //add changes to revision
            foreach (var ch in changes)
                revision.ChangedRows.Add(ch);

            //add the revision to the file
            f.Revisions.Add(revision);

            //save everything
            unitOfWork.GDriveFileRepository.Update(f);
            unitOfWork.Save();

            return revision;
        }

        /// <summary>
        /// Fetches a file object from google drive given its name. 
        /// </summary>
        /// <param name="name">The file name. Must be unique. </param>
        File getFileInfo(string name)
        {
            // http://stackoverflow.com/questions/16298912/retrieve-list-of-files-google-drive-using-net-sdk
            var request = GDrive.Files.List();
            request.Q = "mimeType='{0}' and title='{1}'".F(SpreadSheetMimeType, name);

            var reqResult = request.Execute();
            var fList = reqResult.Items;

            if (fList.Count > 1)
                throw new Exception("More than 1 file with the name '{0}' was found".F(name));

            if (!fList.Any())
                throw new Exception("No file with the name '{0}' was found".F(name));

            return fList.Single();
        }

        /// <summary>
        /// Gets the raw csv contents of the given <see cref="File"/> from GDrive. 
        /// </summary>
        byte[] getFileContents(File file)
        {
            var url = file.ExportLinks[CsvMimeType];
            return GDrive.HttpClient.GetByteArrayAsync(url).Result;
        }

        /// <summary>
        /// Uploads the raw csv contents of the given <see cref="File"/> to GDrive. 
        /// </summary>
        void uploadFileData(File file, byte[] datas)
        {
            FilesResource.UpdateMediaUpload request;
            using (var ms = new System.IO.MemoryStream(datas))
                request = GDrive.Files.Update(file, file.Id, ms, CsvMimeType);
            request.Upload();
        }

        /// <summary>
        /// Parses a CSV file. 
        /// </summary>
        Dictionary<int, ICsvRow> readCsvData(Type ty, byte[] fContents)
        {
            try
            {
                using (var ms = new System.IO.MemoryStream(fContents))
                using (var rdr = new System.IO.StreamReader(ms))
                using (var csv = new CsvReader(rdr))
                {
                    csv.Configuration.IgnoreHeaderWhiteSpace = true;
                    csv.Configuration.IsHeaderCaseSensitive = false;
                    return csv.GetRecords(ty)
                        .Cast<ICsvRow>()
                        .ToDictionary(m => m.HouseId, m => m);
                }
            }
            catch (Exception e)
            {
                var zz = 125;
                return null;
            }
        }

        byte[] writeCsvData(Type ty, Dictionary<int, ICsvRow> dataz)
        {
            using (var ms = new System.IO.MemoryStream())
            {
                using (var rdr = new System.IO.StreamWriter(ms))
                using (var csv = new CsvWriter(rdr))
                {
                    csv.WriteHeader(ty);
                    foreach (var val in dataz.Values)
                        csv.WriteRecord(ty, val);
                }

                return ms.ToArray();
            }
        }

    }
}
