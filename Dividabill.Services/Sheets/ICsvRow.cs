﻿using DividaBill.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DividaBill.Services.Sheets
{
    /// <summary>
    /// A CSV file row. 
    /// </summary>
    public interface ICsvRow
    {
        int HouseId { get; }

        /// <summary>
        /// Loads data from the db to this row. 
        /// </summary>
        /// <param name="contracts">All the requested contracts for this house. </param>
        /// <returns>Returns whether the row is to be in the final sheet. </returns>
        bool LoadFromDb(IEnumerable<Contract> contracts);

        /// <summary>
        /// Saves this row's data to the db. 
        /// </summary>
        void SaveToDb(HouseModel h);
    }
}
