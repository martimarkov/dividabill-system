﻿using DividaBill.Models.Sheets;
using Google.Apis.Drive.v2;
using Google.Apis.Drive.v2.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DividaBill.Services.Sheets
{
    class CsvComparer
    {
        /// <summary>
        /// Checks two versions of csv files for changes. 
        /// </summary>
        /// <param name="rev">The FileRevision these changes are part of. </param>
        /// <param name="oldDatas">The binary data of the old version of the file. </param>
        /// <param name="newDatas">The binary data of the new version of the file. </param>
        /// <param name="keyCell">The columns which contains the keys for each rows. </param>
        /// <returns>An enumeration of the changed rows, as identified by their key. </returns>
        public IEnumerable<RowChange> CheckForChanges(FileRevision rev, byte[] oldDatas, byte[] newDatas, int keyCell)
        {
            //read the old file
            var oldLines = CsvExt.ParseLines(CsvExt.GetCsvLines(oldDatas), keyCell);
            var newLines = CsvExt.ParseLines(CsvExt.GetCsvLines(newDatas), keyCell);

            var oldKvps = CsvExt.ParseCells(oldLines);
            var newKvps = CsvExt.ParseCells(newLines);

            //no old file => all new lines were added
            if (!oldLines.Any())
            {
                return newKvps
                    .Select((ln, i) => new RowChange(rev, ln.Key, RowChangeEvent.Added));
            }

            var changes = new List<RowChange>();

            var oldIds = new HashSet<int>(oldKvps.Keys);
            foreach(var kvp in newKvps)
            {
                var id = kvp.Key;

                //check for adds (in new, not in old)
                if(!oldIds.Contains(id))
                {
                    changes.Add(new RowChange(rev, id, RowChangeEvent.Added));
                    continue;
                }


                //check for edits (in both, hashes differ, cells differ)
                var newVal = kvp.Value;
                var oldVal = oldKvps[id];

                var wasEdited = (newVal.Length != oldVal.Length) || (CsvExt.memcmp(oldVal, newVal, newVal.Length) != 0);
                if (wasEdited)
                    changes.Add(new RowChange(rev, id, RowChangeEvent.Edited));

            }

            //check for removals
            oldIds.ExceptWith(newKvps.Keys);
            changes.AddRange(oldIds.Select(id => new RowChange(rev, id, RowChangeEvent.Removed)));

            return changes;
        }

    }
}
