﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DividaBill.Services.Sheets
{
    static class CsvExt
    {
        [DllImport("msvcrt.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int memcmp(byte[] b1, byte[] b2, long count);

        // Specific to the csv file format. 
        static readonly char[] NewLineCharacters = new[] { '\r', '\n' };
        static readonly char RowSplitCharacter = ',';

        // Used for hashing a row for faster lookup. 
        static readonly MD5 md5 = MD5.Create();

        public static string[] GetCsvLines(byte[] bytes)
        {
            if (bytes == null)
                return new string[0];

            using (var ms = new System.IO.MemoryStream(bytes))
            using (var sr = new System.IO.StreamReader(ms))
                return sr.ReadToEnd()
                    .Split(NewLineCharacters, StringSplitOptions.RemoveEmptyEntries)    // split on newline
                    .Skip(1)                                                            // !! skip a header row !!
                    .ToArray();
        }

        /// <summary>
        /// For each entry in the dictionary, gets a hash of its value and parses its key as an int. 
        /// </summary>
        public static Dictionary<int, byte[]> ParseCells(Dictionary<int, string> rowDict)
        {
            return rowDict
                .ToDictionary(
                    kvp => kvp.Key,
                    kvp => getFingerprint(kvp.Value)
                );
        }

        /// <summary>
        /// Gets a dictionary of all rows keyed by the value in the <paramref name="keyColumn"/>. 
        /// </summary>
        public static Dictionary<int, string> ParseLines(IEnumerable<string> lines, int keyColumn)
        {
            var splits = lines
                .Select(ln => ln.Split(RowSplitCharacter))
                .Where(ln => !string.IsNullOrEmpty(ln[keyColumn]))
                .ToArray();

            return splits
                .ToDictionary(
                    row => int.Parse(row[keyColumn]),
                    row => string.Join(RowSplitCharacter + "", row));
        }

        public static byte[] getFingerprint(string val)
        {
            return md5.ComputeHash(Encoding.Unicode.GetBytes(val));
        }
    }
}
