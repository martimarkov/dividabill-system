﻿using System.Collections.Generic;
using System.Linq;
using DividaBill.DAL.Interfaces;
using DividaBill.Services.Interfaces;
using DividaBill.ViewModels;
using TrackerEnabledDbContext.Common.Models;

namespace DividaBill.Services.Log
{
    public class LoggingService : ILoggingService
    {
        static ILogFormatter[] formattersInUse = new ILogFormatter[]
        {
            new Log.Formatters.Everything(),
            new Log.Formatters.Registrations(),
        };

        /// <summary>
        /// Gets whether there is a formatter with this id. 
        /// </summary>
        public static bool HasFormatter(int id)
        {
            return id >= 0 && id < formattersInUse.Length;
        }



        IUnitOfWork unitOfWork;

        public LoggingService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }


        /// <summary>
        /// Returns the names of all the formatters in use. 
        /// </summary>
        public string[] GetFormatters()
        {
            return formattersInUse.Select(f => f.FriendlyName).ToArray();
        }

        /// <summary>
        /// Gets the last lines leading up to the specified revision from the given feed. 
        /// If no revision is given, returns the last lines. 
        /// To be used whenever the user scrolls back in history. 
        /// </summary>
        /// <param name="feedId">The id of the feed (aka the formatter). </param>
        /// <param name="nItems">The maximum number of items to return. </param>
        /// <param name="upToRev">The revision immediately after the last revision that is returned. </param>
        public LogResponseViewModel GetLastLines(int feedId, int nItems = 50, int upToRev = int.MaxValue)
        {
            if (!HasFormatter(feedId))
                return LogResponseViewModel.Empty;

            var formatter = formattersInUse[feedId];
            var relevantLogs = unitOfWork.AuditLogs.ToArray()
                .Where(l => formatter.CanParse(l))
                .OrderByDescending(l => l.AuditLogId)
                .SkipWhile(l => l.AuditLogId >= upToRev)
                .Take(nItems)
                .Reverse();


            return parseLogs(formatter, relevantLogs);
        }


        /// <summary>
        /// Gets all new log lines that may have occurred after the given log id. 
        /// </summary>
        /// <param name="feedId">The id of the feed (aka the formatter). </param>
        /// <param name="lastSeenId">The id of the last log line that was seen. </param>
        public LogResponseViewModel GetNewestLines(int feedId, int lastSeenId)
        {
            if (!HasFormatter(feedId))
                return LogResponseViewModel.Empty;

            var formatter = formattersInUse[feedId];
            var relevantLogs = unitOfWork.AuditLogs.ToArray()
                .Where(l => formatter.CanParse(l))
                .OrderBy(l => l.AuditLogId)
                .SkipWhile(l => l.AuditLogId <= lastSeenId);


            return parseLogs(formatter, relevantLogs);
        }

        /// <summary>
        /// Creates a <see cref="LogResponseViewModel"/> from the given logs, using the specified formatter. 
        /// </summary>
        /// <param name="formatter">The formatter to parse the lines. </param>
        /// <param name="relevantLogs">The lines this formatter is to parse. </param>
        static LogResponseViewModel parseLogs(ILogFormatter formatter, IEnumerable<AuditLog> relevantLogs)
        {
            var lines = relevantLogs
                .Select(log => formatter.GetLine(log))
                .ToArray();
            var lastLogId = relevantLogs.LastOrDefault()?.AuditLogId ?? -1;
            var firstLogId = relevantLogs.FirstOrDefault()?.AuditLogId ?? -1;

            return new LogResponseViewModel(lines, firstLogId, lastLogId);
        }
    }



}
