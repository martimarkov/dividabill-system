﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackerEnabledDbContext.Common.Models;

namespace DividaBill.Services.Log
{
    interface ILogFormatter
    {
        string FriendlyName { get; }

        bool CanParse(AuditLog log);

        string GetLine(AuditLog log);
    }
}
