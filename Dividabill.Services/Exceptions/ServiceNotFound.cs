﻿using System;
using System.Runtime.Serialization;

namespace DividaBill.Services.Exceptions
{
    [Serializable]
    public class ServiceNotFound : Exception
    {
        public ServiceNotFound()
        {
        }

        public ServiceNotFound(string message) : base(message)
        {
        }

        public ServiceNotFound(string message, Exception inner) : base(message, inner)
        {
        }

        protected ServiceNotFound(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
        }
    }
}