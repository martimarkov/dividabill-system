﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DividaBill.Services.Exceptions
{


    [global::System.Serializable]
    public class UserNotFound : Exception
    {
        public UserNotFound() { }
        public UserNotFound(string message) : base(message) { }
        public UserNotFound(string message, Exception inner) : base(message, inner) { }
        protected UserNotFound(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
