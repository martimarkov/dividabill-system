using System;
using System.Runtime.Serialization;

namespace DividaBill.Services.Exceptions
{
    [Serializable]
    public class BillingRunNotFound : Exception
    {
        public BillingRunNotFound()
        {
        }

        public BillingRunNotFound(string message) : base(message)
        {
        }

        public BillingRunNotFound(string message, Exception inner) : base(message, inner)
        {
        }

        protected BillingRunNotFound(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
        }
    }
}