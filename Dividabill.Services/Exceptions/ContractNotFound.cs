﻿using System;
using System.Runtime.Serialization;

namespace DividaBill.Services.Exceptions
{
    [Serializable]
    public class ContractNotFound : Exception
    {
        public ContractNotFound()
        {
        }

        public ContractNotFound(string message) : base(message)
        {
        }

        public ContractNotFound(string message, Exception inner) : base(message, inner)
        {
        }

        protected ContractNotFound(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
        }
    }
}