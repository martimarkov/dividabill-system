using System.Collections.Generic;
using DividaBill.Models;

namespace DividaBill.Services.Responses
{
    public class RegisterResult
    {
        public User User { get; set; }
        public bool HouseExisted { get; set; }
        public List<UIError> Errors { get; set; } = new List<UIError>();
    }
}