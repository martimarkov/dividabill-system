using System.Collections.Generic;
using DividaBill.Models;

namespace DividaBill.Services.NewBillingSystem.Model
{
    public class HousesToBeBilledModel
    {
        public int ID { get; set; }

        public int Housemates { get; set; }

        public Address Address { get; set; }

        public List<User> Tenants { get; set; }

        public bool IsAlreadyInQueue { get; set; }
    }
}