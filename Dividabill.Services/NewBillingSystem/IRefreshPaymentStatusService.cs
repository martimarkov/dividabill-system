using System.Threading.Tasks;
using DividaBill.Models.GoCardless;
using DividaBill.Services.NewBillingSystem.Impl;
using GoCardless_API;

namespace DividaBill.Services.NewBillingSystem
{
    public interface IRefreshPaymentStatusService
    {
        Task RefreshPaymentStatusAsync(GoCardless.Environments environment );
        Task<BulkOperationResult<GCPayment>> RefreshAllPaymentStatusAsync(GoCardless.Environments environment);
    }
}