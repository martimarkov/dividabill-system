﻿using DividaBill.Models;
using DividaBill.Models.Bills;
using DividaBill.Models.Notifications;

namespace DividaBill.Services.NewBillingSystem
{
    public interface INotificationFactory
    {
        Notification BillPayment(HouseholdBill bill, User user);
        Notification AllocationChange(UserPercentage perc);
    }
}