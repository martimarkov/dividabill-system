using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DividaBill.DAL.Interfaces;
using DividaBill.Models;
using DividaBill.Services.NewBillingSystem.Impl;
using DividaBill.Services.NewBillingSystem.Model;
using GoCardless_API;

namespace DividaBill.Services.NewBillingSystem
{
    public interface IBillingRunService
    {
        Task<List<BillingRun>> GetAllBillingRunsAsync();
        Task<BillingRun> GetBillingRunAsync(int billingRunId, IUnitOfWork unitOfWork = null);
        Task<BillingRun> Insert(BillingRun billingRun);
        Task<List<int>> SetHousesToBeBilledInBillingRunAsync(int billingRunId, List<int> houseIds);
        IQueryable<HouseModel> GetHousesToBeBilled(int billingRunId);
        Task<List<int>> SetActiveHousesToBeBilledInBillingRunAsync(int billingRunId);
        IQueryable<HouseModel> GetHousesThatHaventBeenBilled(int billingRunId);
        IQueryable<HouseModel> GetHousesThatHaveBeenBilled(int billingRunId);
        IQueryable<HouseModel> GetHousesThatHaveBeenBilledAnAmount(int billingRunId);
        Task AddBillingRunTenantsToGoCardless(int billingRunId, GoCardless.Environments environment);
        List<ManualMandateExportOutputModel> GetGoCardlessManualExport(int billingRunId);
        IQueryable<HousesToBeBilledModel> GetAllHousesToBeBilled(int billingRunId);
    }
}