namespace DividaBill.Services.NewBillingSystem
{
    public interface IEstimatedUtilityPricesService
    {
        decimal[] Water { get; }
        decimal[] Gas { get; }
        decimal[] Electricity1408700964 { get; }
        decimal[] ElectricityUsage { get; }
        decimal[] GasUsage { get; }
    }
}