﻿using System;
using System.Collections.Generic;
using DividaBill.DAL.Interfaces;
using DividaBill.Models;
using DividaBill.ViewModels;

namespace DividaBill.Services.NewBillingSystem
{
    public interface IPriceService
    {
        decimal EstimateElectricityUsage(HouseModel house);
        decimal EstimateGasUsage(HouseModel house);
        EstimatedPriceViewModel GetEstimatedPrice(HouseModel house, IUnitOfWork unitOfWork = null);
        UnitEstimatedPrice GetPriceByPostCode(string postcode, IUnitOfWork unitOfWork = null);
        Dictionary<ServiceType, decimal> GetMonthlyEstdPrice(HouseModel h, IUnitOfWork unitOfWork = null);
        Dictionary<ServiceType, decimal> GetWeeklyEstdPrice(HouseModel h, IUnitOfWork unitOfWork = null);

        EstimatedPriceViewModel GetEstimatedPrice(CouponSearchViewModel id, int? houseId = null, IUnitOfWork unitOfWork = null);
    }
}
