using System.Collections.Generic;

namespace DividaBill.Services.NewBillingSystem
{
    public interface IHouseBlacklistService
    {
        List<int> GetBlackListedHouses();
    }
}