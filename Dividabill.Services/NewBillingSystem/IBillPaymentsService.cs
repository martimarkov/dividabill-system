using System.Collections.Generic;
using System.Threading.Tasks;
using DividaBill.DAL.Interfaces;
using DividaBill.Models;

namespace DividaBill.Services.NewBillingSystem
{
    public interface IBillPaymentsService
    {
        IEnumerable<BillPayment> GenerateBillPayments(UtilityBill utilityBill, int billingQueueId, IUnitOfWork unitOfWork = null);
    }
}