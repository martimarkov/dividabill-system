using System;
using System.Collections.Generic;
using System.Linq;
using DividaBill.Models;
using DividaBill.Models.Bills;

namespace DividaBill.Services.NewBillingSystem
{
    public interface IHouseholdBillService
    {
        decimal GetTenantTotalAmount(User u, HouseholdBill bill);
        decimal GetTenantOutstandingAmount(User u, HouseholdBill bill);
        bool IsPaidByTenant(User u, HouseholdBill bill);
        List<HouseholdBill> GetBillsForHouse(HouseModel h);
        List<HouseholdBill> GetBillsForHouse(HouseModel h, DateTime date);

        /// <summary>
        /// Outstanding amount that hasn't been sent to a payment processor.
        /// </summary>
        /// <param name="u"></param>
        /// <param name="bill"></param>
        /// <returns></returns>
        decimal GetTenantOutstandingAmountNotYetSentToBeDebited(User u, HouseholdBill bill);

        /// <summary>
        /// Outstanding amount that hasn't been sent to a payment processor.
        /// </summary>
        /// <param name="u"></param>
        /// <param name="bill"></param>
        /// <param name="now"></param>
        /// <returns></returns>
        decimal GetTenantOutstandingAmountThatHasFailedToBePaid(User u, HouseholdBill bill, DateTime now);

        IQueryable<HouseholdBill> GetHouseholdBillsForBillingRun(int billingRunId);
    }
}