using DividaBill.Models;
using DividaBill.Models.Abstractions;
using DividaBill.ViewModels;

namespace DividaBill.Services.NewBillingSystem
{
    public interface ICalculateEstimatesPricesService
    {
        decimal GetEstimatedLandlinePhoneMediumPrices(CouponSearchViewModel id, IEstimatedPrice estimatedPrice);
        decimal GetEstimatedLandlinePhoneBasicPrices(CouponSearchViewModel id, IEstimatedPrice estimatedPrice);
        decimal GetEstimatedTvLicensePrices(CouponSearchViewModel id, IEstimatedPrice estimatedPrice);
        decimal GetEstimatedSkyTvPrices(CouponSearchViewModel id, IEstimatedPrice estimatedPrice);
        decimal GetEstimatedNetflixTvPrices(CouponSearchViewModel id, IEstimatedPrice estimatedPrice);
        decimal GetEstimatedLineRental(CouponSearchViewModel id, IEstimatedPrice estimatedPrice);
        decimal GetEstimatedBroadbandFibre40Prices(CouponSearchViewModel id, IEstimatedPrice estimatedPrice);
        decimal GetEstimatedBroadbandAdslPrices(CouponSearchViewModel id, IEstimatedPrice estimatedPrice);
        decimal GetEstimatedGasPrices(CouponSearchViewModel id, IEstimatedPrice estimatedPrice);
        decimal GetEstimatedElectricityPrices(CouponSearchViewModel id, IEstimatedPrice estimatedPrice);
        decimal GetEstimatedWaterPrices(CouponSearchViewModel id);
    }
}