using System.Threading.Tasks;
using DividaBill.Models;

namespace DividaBill.Services.NewBillingSystem
{
    public interface IBillingQueueService
    {
        /// <summary>
        /// Inserts and tracks changes
        /// </summary>
        /// <param name="billingQueue"></param>
        /// <returns></returns>
        Task<bool> InsertAsync(BillingQueue billingQueue);

        Task<bool> SetDistributedProcessorOfBillingQueueAsync(int billingRunId, int houseId, int distributedProcessorId);
    }
}