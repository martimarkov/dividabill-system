using System.Collections.Generic;
using DividaBill.Models;
using DividaBill.Models.Bills;

namespace DividaBill.Services.NewBillingSystem
{
    public interface IUtilityBillService
    {
        /// <summary>
        /// Gets the amount this tenant has to pay. 
        /// Currently returns 1/nth of the whole bill if there are n tenants. 
        /// TODO: use <see cref="BillPayment"/>
        /// </summary>
        decimal GetTenantAmount(UtilityBill bill, User tenant);

        decimal GetTenantUtilityBillAmount(HouseholdBill bill, User tenant, int serviceTypeId);

        /// <summary>
        /// Used to provide a breakdown/summary of the tenant's amount owed per service.
        /// </summary>
        /// <param name="tenant"></param>
        /// <param name="bill"></param>
        /// <returns></returns>
        Dictionary<int, decimal> GetTenantUtilityBillAmountsForAllServiceTypes(User tenant, HouseholdBill bill);
    }
}