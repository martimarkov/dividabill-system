using System.Collections.Generic;

namespace DividaBill.Services.NewBillingSystem.Impl
{
    public interface IThreadSafeList<T>
    {
        List<T> List { get; set; }
    }
}