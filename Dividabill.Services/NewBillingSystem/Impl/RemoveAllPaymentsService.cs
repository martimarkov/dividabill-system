﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DividaBill.DAL.Interfaces;
using DividaBill.Library;
using DividaBill.Services.Interfaces;
using DividaBill.Services.Sheets;

namespace DividaBill.Services.NewBillingSystem.Impl
{
    internal class RemoveAllPaymentsService : IRemoveAllPaymentsService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IHouseholdBillService _householdBillService;
        private readonly IHousesToBeBilledService _housesToBeBilledService;
        private readonly IBillingService _billingService;
        private GDriveService gDriveService;

        public RemoveAllPaymentsService(IUnitOfWork unitOfWork,IHouseholdBillService householdBillService, IHousesToBeBilledService housesToBeBilledService, IBillingService billingService)
        {
            _unitOfWork = unitOfWork;
            _householdBillService = householdBillService;
            _housesToBeBilledService = housesToBeBilledService;
            _billingService = billingService;
        }

        public void RemovePayments(int billingRunId, DateTime? dt)
        {

            var allHouses = _housesToBeBilledService.GetHousesToBeBilled(billingRunId);
            //_householdBilliBillService = new HouseholdBillService(new UtilityBillService());
            //var unitOfWork = new UnitOfWork();
            //var allHouses = unitOfWork.ActiveHouses.ToArray();
            //var calculateEstimatedPriceService =
            //    new CalculateEstimatesPricesService(new HardCodedEstimatedUtilityPricesService());
            //var billingQueueTransactionLogService = new BillingQueueTransactionLogService(unitOfWork);
            //var priceService = new PriceService(unitOfWork, calculateEstimatedPriceService, new HardCodedEstimatedUtilityPricesService());
            //var billService = new BillingService(unitOfWork,billingQueueTransactionLogService, priceService, new BillPaymentsService(unitOfWork, billingQueueTransactionLogService)  );

            var i = 0;
            foreach (var house in allHouses)
            {
                var tenants = house.Tenants;
                var bills = house.HouseholdBills.ToArray();

                foreach (var hb in bills)
                {
                    var diff = hb.TotalAmount - tenants.Sum(t => _householdBillService.GetTenantTotalAmount(t, hb));
                    if (hb.IsPaid == false && Math.Abs(diff) > 0.20m)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("CS FUCKEd UP!!1! (#{0}) (diff: {1})".F(hb.House_ID, diff));

                        foreach (var pay in hb.Payments.ToArray())
                        {
                            _unitOfWork.BillPaymentsRepository.Delete(pay);
                        }



                        foreach (var ub in hb.UtilityBills.ToArray())
                        {
                            _unitOfWork.UtilityBillRepository.Delete(ub);
                        }

                        _unitOfWork.HouseholdBillRepository.Delete(hb);




                        i++;
                    }
                }

               
            }
            _unitOfWork.Save();

            _billingService.GenerateBills(billingRunId);


            Console.WriteLine("CS FUCKED UP {0} times", i);

        }
    }
}
