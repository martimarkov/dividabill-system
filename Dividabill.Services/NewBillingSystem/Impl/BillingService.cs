﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using DividaBill.DAL.Interfaces;
using DividaBill.Models;
using DividaBill.Models.Bills;
using DividaBill.Services.Interfaces;

namespace DividaBill.Services.NewBillingSystem.Impl
{
    // Controls the creation of household and utility bills. 
    public class BillingService : IBillingService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IBillingQueueTransactionLogService _billingQueueTransactionLogService;
        private readonly IPriceService _priceService;
        private readonly IBillPaymentsService _billPaymentsService;
        private readonly IHousesToBeBilledService _housesToBeBilledService;
        private readonly IHouseService _houseService;
        private readonly IMultiThreadingService _multiThreadingService;

        public BillingService(
            IUnitOfWork unitOfWork, IBillingQueueTransactionLogService billingQueueTransactionLogService,
            IPriceService priceService, IBillPaymentsService billPaymentsService, IHousesToBeBilledService housesToBeBilledService,
            IHouseService houseService, IMultiThreadingService multiThreadingService)
        {
            _unitOfWork = unitOfWork;
            _billingQueueTransactionLogService = billingQueueTransactionLogService;
            _priceService = priceService;
            _billPaymentsService = billPaymentsService;
            _housesToBeBilledService = housesToBeBilledService;
            _houseService = houseService;
            _multiThreadingService = multiThreadingService;
        }

        public void GenerateBills(int billingRunId)
        {
            var billingRun = _unitOfWork.BillingRunRepository.Where(w => w.ID == billingRunId).Select(s => s.Period).SingleOrDefault();
            var housesToBill = _housesToBeBilledService.GetHousesThatHaventBeenBilled(billingRunId).Select(s => s.ID).ToList();

            RunTask(billingRunId, housesToBill, billingRun.Value);
        }

        private void RunTask(int billingRunId, List<int> housesToBill, DateTime when)
        {
            //await _multiThreadingService.RunDatabaseTaskInParallelAsync(5, housesToBill, (houseId, work, bulkOperationReuslt) =>
            //{
            for (var i = 0; i< housesToBill.Count; i++)
            {
                var houseId = housesToBill[i];
                GenerateBillForHouse(billingRunId, houseId, when);
            }
                
            //});
        }

        public bool GenerateBillForHouse(int billingRunId, int houseId, DateTime period, IUnitOfWork unitOfWork = null)
        {
            if (unitOfWork == null)
                unitOfWork = _unitOfWork;

            var billingQueueId = unitOfWork.BillingQueueRepository.Get(w => w.BillingRunId == billingRunId && w.HouseId == houseId)
            .Select(s => s.ID)
            .SingleOrDefault();

            var h =
                _houseService.GetActiveHouses(unitOfWork)
                    .Where(w => w.ID == houseId)
                    .Include("Contracts")
                    .Include("HouseholdBills")
                    .SingleOrDefault();
            if (h == null)
            {
                _billingQueueTransactionLogService.TrackChanges(billingQueueId, $" houseId: {houseId} does not have any active contracts", BillingQueueStage.GenerateBills, TransactionLogStatus.Error, unitOfWork);
                return false;
            }

            try
            {
                var success = GenerateMissingBills(h, period, billingQueueId, unitOfWork);
                if (!success)
                    _billingQueueTransactionLogService.TrackChanges(billingQueueId, $" {billingQueueId} failed", BillingQueueStage.GenerateBills, TransactionLogStatus.Warning, unitOfWork);
                return success;
            }
            catch (Exception ex)
            {
                //_billingQueueTransactionLogService.TrackException(billingQueueId, ex, BillingQueueStage.GenerateBills, unitOfWork);
                return false;
                //await _billingQueueTransactionLogService.TrackChangesAsync(billingQueueId, BillingQueueStage.GenerateBills, ex.Message + ex.Source + ex.StackTrace);
            }
        }

        public bool GenerateMissingBills(HouseModel h, DateTime? dt, int billingQueueId, IUnitOfWork unitOfWork = null)
        {

            if (unitOfWork == null)
                unitOfWork = _unitOfWork;

            if (dt == null)
                return false;

            var toMonth = dt.Value.Month;
            var toYear = dt.Value.Year;

            if (toMonth < 1 || toMonth > 12) throw new ArgumentException(nameof(toMonth), "Month must be an integer between 1 and 12. ");
            if (toYear < 2015) throw new ArgumentException(nameof(toYear), "Year must be above 2015. ");

            //check if house has a confirmed start date
            if (!h.ActualStartDate.HasValue)
            {
                TrackChangesOfBillingQueue(billingQueueId, $"House #{h.ID} has no confirmed contracts.", unitOfWork);
                return false;
            }

            //check if there is a bill for this month already. 
            var houseBills = unitOfWork.HouseholdBillRepository.Get()
                .Where(b => b.House_ID == h.ID).ToArray();

            var hasBill = houseBills
                .Any(b => b.Month == toMonth && b.Year == toYear);

            if (!hasBill)
                // if not, create the new household bill
                BillHouse(h, dt.Value, billingQueueId, true, unitOfWork);
            else
            {
                var billingQueue = unitOfWork.BillingQueueRepository.FirstOrDefault(w => w.ID == billingQueueId);

                if (billingQueue == null)
                    throw new Exception();

                if (billingQueue.HouseholdBillId == null)
                    billingQueue.HouseholdBillId = houseBills.Where(b => b.Month == toMonth && b.Year == toYear).Select(s => s.ID).FirstOrDefault();
                unitOfWork.Save();
            }


            return true;
        }

        // Creates a household bill for the given house spanning the specified interval. 
        public void BillHouse(HouseModel house, DateTime date, int billingQueueId, bool writeToDb = true, IUnitOfWork unitOfWork = null)
        {

            if (unitOfWork == null)
                unitOfWork = _unitOfWork;

            //await _unitOfWork.WrapInTransactionAsync(async () => 
            BillHouseUnsafe(house, date, billingQueueId, unitOfWork);
            //, writeToDb);
        }

        void BillHouseUnsafe(HouseModel house, DateTime date, int billingQueueId, IUnitOfWork unitOfWork = null)
        {
            if (unitOfWork == null)
                unitOfWork = _unitOfWork;

            var houseBill = new HouseholdBill(house, date.Year, date.Month);
            houseBill.House_ID = house.ID;

            unitOfWork.HouseholdBillRepository.Insert(houseBill);

            var billingQueue = unitOfWork.BillingQueueRepository.FirstOrDefault(w => w.ID == billingQueueId);

            if (billingQueue == null)
                throw new Exception();

            var defaultPrices = _priceService.GetWeeklyEstdPrice(house, unitOfWork);

            //get all active contracts in that period
            var contracts = house.Contracts.Where(w => date <= w.EndDate && w.StartDate.HasValue && w.StartDate <= date);

            //make em generate a bill + billpayments for this month
            foreach (var c in contracts)
            {
                CreateBillsAndSave(billingQueueId, c, houseBill, defaultPrices, unitOfWork);
            }

            billingQueue.HouseholdBillId = houseBill.ID;

            //save everything
            unitOfWork.Save();

            var billAmount = houseBill.TotalAmount;
        }

        public void CreateBillsAndSave(int billingQueueId, Contract c, HouseholdBill houseBill, Dictionary<ServiceType, decimal> defaultPrices, IUnitOfWork unitOfWork = null)
        {
            if (unitOfWork == null)
                unitOfWork = _unitOfWork;

            //zhe bill
            var bill = new UtilityBill(c, houseBill, defaultPrices);
            var totalAmount = bill.Ammount;
            unitOfWork.UtilityBillRepository.Insert(bill);

            //n zhe payments
            var payments = _billPaymentsService.GenerateBillPayments(bill, billingQueueId, unitOfWork);
            foreach (var p in payments)
                unitOfWork.BillPaymentsRepository.Insert(p);
        }

        // Generates all bills for the house. 


        private bool TrackChangesOfBillingQueue(int billingQueueId, string message, IUnitOfWork unitOfWork = null)
        {
            if (unitOfWork == null)
                unitOfWork = _unitOfWork;

            return _billingQueueTransactionLogService.TrackChanges(billingQueueId, message, BillingQueueStage.GenerateBills, TransactionLogStatus.Information, unitOfWork);
        }

        public void MarkHouseholdBillAsPaidAsync(HouseholdBill bill, User u, IUnitOfWork unitOfWork = null)
        {
            if (unitOfWork == null)
                unitOfWork = _unitOfWork;

            var userPayments = bill.Payments
                .Where(p => p.User_Id == u.Id).ToList();

            if (userPayments.Any(p => p.IsPaid))
                return;

            foreach (var pay in userPayments)
            {
                pay.MarkAsPaid();
                unitOfWork.BillPaymentsRepository.Update(pay);
            }
             unitOfWork.BillPaymentsRepository.SaveChanges();
        }
    }
}
