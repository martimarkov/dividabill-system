﻿using System;
using System.Collections.Generic;
using System.Linq;
using DividaBill.DAL.Interfaces;
using DividaBill.Models;
using DividaBill.Models.Abstractions;
using DividaBill.ViewModels;

namespace DividaBill.Services.NewBillingSystem.Impl
{
    public class PriceService : IPriceService
    {
        private readonly IEstimatedUtilityPricesService _estimatedUtilityPricesService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICalculateEstimatesPricesService _calculateEstimatesPricesService;

        public PriceService(IUnitOfWork unitOfWork, ICalculateEstimatesPricesService calculateEstimatesPricesService, IEstimatedUtilityPricesService estimatedUtilityPricesService)
        {
            _unitOfWork = unitOfWork;
            _calculateEstimatesPricesService = calculateEstimatesPricesService;
            _estimatedUtilityPricesService = estimatedUtilityPricesService;
        }

        public decimal EstimateElectricityUsage(HouseModel house)
        {
            return _estimatedUtilityPricesService.ElectricityUsage[house.Housemates - 1] * 52;
        }

        public decimal EstimateGasUsage(HouseModel house)
        {
            return _estimatedUtilityPricesService.GasUsage[house.Housemates - 1] * 52;
        }

        public EstimatedPriceViewModel GetEstimatedPrice(HouseModel house, IUnitOfWork unitOfWork = null)
        {
            if (unitOfWork == null)
                unitOfWork = _unitOfWork;

            var coupon = unitOfWork.CouponRepository.GetByID(house.Coupon_Id);
            var couponCode = (coupon != null) ? coupon.Code : "";
            var id = new CouponSearchViewModel
            {
                coupon = couponCode,
                electricity = house.HasElectricity,
                postcode = house.Address.Postcode,
                gas = house.HasGas,
                water = house.HasWater,
                tenants = house.Housemates
            };

            return GetEstimatedPrice(id, house.ID);

        }

        public UnitEstimatedPrice GetPriceByPostCode(string postcode, IUnitOfWork unitOfWork = null)
        {
            if (unitOfWork == null)
                unitOfWork = _unitOfWork;

            var estimatedPrice = GenerateDefaultEstimatedPrice(postcode, unitOfWork);

            //Calculate the estimated price per person per week based on usage
            var price = new UnitEstimatedPrice
            {
                ElectrcityUnitRate = estimatedPrice.Electricity,
                ElectricityStandingCharge = estimatedPrice.ElectricityStandingCharge,
                ElectrcityTCR = 0,
                GasStandingCharge = estimatedPrice.GasStandingCharge,
                GasUnitRate = estimatedPrice.Gas,
                GasTCR = 0,
                WaterStandingCharge = estimatedPrice.WaterStandingCharge,
                WaterUnitRate = estimatedPrice.Water
            };

            return price;
        }

        public Dictionary<ServiceType, decimal> GetMonthlyEstdPrice(HouseModel h, IUnitOfWork unitOfWork = null)
        {

            if (unitOfWork == null)
                unitOfWork = _unitOfWork;

            return GetWeeklyEstdPrice(h, unitOfWork)
                .ToDictionary(
                    kvp => kvp.Key,
                    kvp => kvp.Value * 52 / 12);
        }

        public Dictionary<ServiceType, decimal> GetWeeklyEstdPrice(HouseModel h, IUnitOfWork unitOfWork = null)
        {
            if (unitOfWork == null)
                unitOfWork = _unitOfWork;

            var z = GetEstimatedPrice(h, unitOfWork);   //per person

            var prices = new Dictionary<ServiceType, decimal>
            {
                { ServiceType.Broadband, (h.BroadbandType == BroadbandType.ADSL20) ? z.BroadbandADSL : z.BroadbandFibre40 },
                { ServiceType.Electricity, z.Electricity },
                { ServiceType.Gas, z.Gas },
                { ServiceType.LandlinePhone, (h.LandlineType == LandlineType.Basic) ? z.LandlinePhoneBasic : z.LandlinePhoneMedium },
                { ServiceType.LineRental, z.LineRental },
                { ServiceType.NetflixTV, z.NetflixTV },
                { ServiceType.SkyTV, z.SkyTV },
                { ServiceType.TVLicense, z.TVLicense },
                { ServiceType.Water, z.Water },
            };

            foreach (var k in prices.Keys.ToArray())
                prices[k] *= h.Housemates;

            return prices;
        }


        /// <summary>
        /// doesnt use house estimates price instead uses postcode estimated price
        /// </summary>
        /// <param name="id"></param>
        /// <param name="houseId"></param>
        /// <param name="unitOfWork"></param>
        /// <returns></returns>
        public EstimatedPriceViewModel GetEstimatedPrice(CouponSearchViewModel id, int? houseId = null, IUnitOfWork unitOfWork = null)
        {

            if (unitOfWork == null)
                unitOfWork = _unitOfWork;

            var estimatedPrice = GenerateDefaultEstimatedPrice(id.postcode, unitOfWork);

            //house estimatedPrice not used wonder why, 
            //var houseEstimatedPrice = GetHouseEstimatedPrice(houseId);
            
            //Calculate the estimated price per person per week based on usage
            var price = CreateEstimatedPriceViewModel(id, estimatedPrice);

            if (id.water == true)
                price.TotalBill += price.Water;
            if (id.gas == true)
                price.TotalBill += price.Gas;
            if (id.electricity == true)
                price.TotalBill += price.Electricity;
            if (id.broadband == BroadbandType.Fibre40)
                price.TotalBill += price.BroadbandFibre40;
            if (id.broadband == BroadbandType.ADSL20)
                price.TotalBill += price.BroadbandADSL;
            if ((id.broadband == BroadbandType.ADSL20) || (id.broadband == BroadbandType.Fibre40)
                || (id.phoneline == LandlineType.Basic) || (id.phoneline == LandlineType.Medium))
                price.TotalBill += price.LineRental;
            if (id.skytv == true)
                price.TotalBill += price.SkyTV;
            if (id.netflixtv == true)
                price.TotalBill += price.NetflixTV;
            if (id.tvlicense == true)
                price.TotalBill += price.TVLicense;
            if (id.phoneline == LandlineType.Basic)
                price.TotalBill += price.LandlinePhoneBasic;
            if (id.phoneline == LandlineType.Medium)
                price.TotalBill += price.LandlinePhoneMedium;
            //price.ServiceCharge = 0.99m;

            //Coupon selectedCoupon = _repository.CouponRepository.Get(c => c.Code == id.coupon).SingleOrDefault();

            //price.ServiceCharge = (selectedCoupon != null) ? 0.924868m : 0.99m;

            return price;
        }

        //private IEstimatedPrice GetHouseEstimatedPrice(int? houseId,IUnitOfWork unitOfWork = null)
        //{
        //    if (unitOfWork == null)
        //        unitOfWork = _unitOfWork;

        //    if (!houseId.HasValue)
        //        return null;
        //    //TODO: Change to estimated usage.
        //    //Get estimated prices for house
        //    IEstimatedPrice houseEstimatedPrice = unitOfWork.HouseEstimatedPriceRepository.GetByID(houseId.Value);
        //    return houseEstimatedPrice;
        //    //Check if estimated usage for house exists
        //    //if (houseEstimatedPrice != null)
        //    //{
        //    //    //estimatedPrice.ElectricityUsage;
        //    //}
        //}

        private EstimatedPriceViewModel CreateEstimatedPriceViewModel(CouponSearchViewModel id, IEstimatedPrice estimatedPrice)
        {
            var price = new EstimatedPriceViewModel
            {
                Water = _calculateEstimatesPricesService.GetEstimatedWaterPrices(id),
                Electricity = _calculateEstimatesPricesService.GetEstimatedElectricityPrices(id, estimatedPrice),
                Gas = _calculateEstimatesPricesService.GetEstimatedGasPrices(id, estimatedPrice),
                BroadbandADSL = _calculateEstimatesPricesService.GetEstimatedBroadbandAdslPrices(id, estimatedPrice),
                BroadbandFibre40 = _calculateEstimatesPricesService.GetEstimatedBroadbandFibre40Prices(id, estimatedPrice),
                LineRental = _calculateEstimatesPricesService.GetEstimatedLineRental(id, estimatedPrice),
                NetflixTV = _calculateEstimatesPricesService.GetEstimatedNetflixTvPrices(id, estimatedPrice),
                SkyTV = _calculateEstimatesPricesService.GetEstimatedSkyTvPrices(id, estimatedPrice),
                TVLicense = _calculateEstimatesPricesService.GetEstimatedTvLicensePrices(id, estimatedPrice),
                LandlinePhoneBasic = _calculateEstimatesPricesService.GetEstimatedLandlinePhoneBasicPrices(id, estimatedPrice),
                LandlinePhoneMedium = _calculateEstimatesPricesService.GetEstimatedLandlinePhoneMediumPrices(id, estimatedPrice),
            };
            return price;
        }


        private IEstimatedPrice GenerateDefaultEstimatedPrice(string postcode, IUnitOfWork unitOfWork = null)
        {
            if (unitOfWork == null)
                unitOfWork = _unitOfWork;
            IEstimatedPrice estimatedPrice = unitOfWork.PostcodeEstimatedPriceRepository.GetByID(postcode);

            //check if invalid address or no prices
            if(estimatedPrice == null || estimatedPrice.Electricity <= 0 || estimatedPrice.Gas <= 0)
                estimatedPrice = new DefaultEstimatedPrice();

            //estimatedPrice.ElectricityStandingCharge = 0.26m;
            //estimatedPrice.GasStandingCharge = 0.26m;

            estimatedPrice.BroadbandADSL = 17m;
            estimatedPrice.BroadbandADSLMargin = 0.667m;
            estimatedPrice.BroadbandADSLRouter = 8m;
            estimatedPrice.BroadbandFible38 = 24m;
            estimatedPrice.BroadbandFible38Router = 8m;
            estimatedPrice.BroadbandFible38Margin = 0.667m;

            estimatedPrice.LineRental = 13.1m;
            estimatedPrice.LineRentalMargin = 0.125m;
            //estimatedPrice.LineRentalInstallation = 110m;

            estimatedPrice.SkyTV = 40m;
            estimatedPrice.SkyTVMargin = 2m;
            estimatedPrice.SkyTVSTB = 55m;
            estimatedPrice.SkyTVSatellite = 30m;
            estimatedPrice.SkyTVInstallation = 65m;

            estimatedPrice.NetflixTV = 8.99m;
            estimatedPrice.TVLicense = 145.00m;

            estimatedPrice.LandlinePhoneBasic = 1.5m;
            estimatedPrice.LandlinePhoneBasicMargin = 0.285m;
            estimatedPrice.LandlinePhoneMedium = 2.5m;
            estimatedPrice.LandlinePhoneMediumMargin = 1.66m;

            return estimatedPrice;
        }

    }
}