using System;

namespace DividaBill.Services.NewBillingSystem.Impl
{
    public class ManualMandateExportOutputModel
    {
        public string mandate_id { get; set; }
        public decimal amount { get; set; }
        public string description { get; set; }
        public DateTime charge_date { get; set; }
        public string UserId { get; set; }
        public int HouseId { get; set; }
    }
}