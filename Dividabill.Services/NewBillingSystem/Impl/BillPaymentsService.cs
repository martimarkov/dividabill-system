using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DividaBill.DAL.Interfaces;
using DividaBill.Models;

namespace DividaBill.Services.NewBillingSystem.Impl
{
    public class BillPaymentsService : IBillPaymentsService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IBillingQueueTransactionLogService _billingQueueTransactionLogService;

        public BillPaymentsService(IUnitOfWork unitOfWork, IBillingQueueTransactionLogService billingQueueTransactionLogService)
        {
            _unitOfWork = unitOfWork;
            _billingQueueTransactionLogService = billingQueueTransactionLogService;
        }

        public IEnumerable<BillPayment> GenerateBillPayments(UtilityBill utilityBill, int billingQueueId, IUnitOfWork unitOfWork = null)
        {
            if (unitOfWork == null)
                unitOfWork = _unitOfWork;

            if (utilityBill.Payments?.Count > 0)
                throw new Exception("Already have payments here!");

            //see if custom allocations exist
            var alloc = utilityBill.Contract.CurrentAllocation;
            if (alloc != null)
            {
                if (new HashSet<User>(utilityBill.House.Tenants).SetEquals(alloc.UserPercentages.Select(p => p.User)))
                    return utilityBill.House.Tenants.Select(u => new BillPayment(u, utilityBill, alloc));

                const string message = "The latest allocation agreed upon by everyone does not actually include everyone!";
                _billingQueueTransactionLogService.TrackChanges(billingQueueId, message, BillingQueueStage.GenerateBills, TransactionLogStatus.Error, unitOfWork);
                throw new Exception(message);
            }

            //otherwise return simple allocation based on the number of tenants
            var nTenants = utilityBill.House.Tenants.Count;
            return utilityBill.House.Tenants.Select(u => new BillPayment(u, utilityBill, nTenants));
        }

        //not sure this should be marking bills as paid based on this criteria
        //public void UpdatePaidBillsInDatabase()
        //{
        //    foreach (var ubills in _unitOfWork.UtilityBillRepository.Get().Where(x => x.Ammount == 0).ToArray())
        //    {
        //        foreach (var payment in ubills.Payments)
        //        {
        //            payment.MarkAsPaid();
        //            _unitOfWork.BillPaymentsRepository.Update(payment);
        //        }
        //    }
        //    _unitOfWork.Save();
        //}
    }
}