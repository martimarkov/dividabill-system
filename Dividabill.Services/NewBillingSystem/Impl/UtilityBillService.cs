using System;
using System.Collections.Generic;
using System.Linq;
using DividaBill.Models;
using DividaBill.Models.Bills;

namespace DividaBill.Services.NewBillingSystem.Impl
{
    public class UtilityBillService:IUtilityBillService
    {
        /// <summary>
        /// Gets the amount this tenant has to pay. 
        /// Currently returns 1/nth of the whole bill if there are n tenants. 
        /// TODO: use <see cref="BillPayment"/>
        /// </summary>
        public decimal GetTenantAmount(UtilityBill bill, User tenant)
        {
            var userPayment = bill.Payments.SingleOrDefault(b => b.PayingUser == tenant);

            if (userPayment == null)
                return 0.00m;///throw new Exception("User {0} is not expected to pay anything for bill {1}".F(tenant, this));

            return Math.Truncate(bill.Ammount / bill.House.Tenants.Count * 100) / 100;

        }

        public decimal GetTenantUtilityBillAmount(HouseholdBill bill, User tenant, int serviceTypeId)
        {
            var utilityBill = bill.UtilityBills.FirstOrDefault(x => x.Service.Id == serviceTypeId);

            if (utilityBill == null)
                return 0.00m;

            var tenantAmount = GetTenantAmount(utilityBill, tenant);
            return tenantAmount;
        }

        /// <summary>
        /// Used to provide a breakdown/summary of the tenant's amount owed per service.
        /// </summary>
        /// <param name="tenant"></param>
        /// <param name="bill"></param>
        /// <returns></returns>
        public Dictionary<int, decimal> GetTenantUtilityBillAmountsForAllServiceTypes(User tenant, HouseholdBill bill)
        {
            var tenantServiceAmounts = new Dictionary<int, decimal>();

            //calculate what the tenant owes for each service
            foreach (var serviceTypeId in ServiceType.All)
            {
                //doesn't currently work off house percentage splits
                var serviceTypeTenantAmount = GetTenantUtilityBillAmount(bill, tenant,
                    serviceTypeId);
                tenantServiceAmounts[serviceTypeId] = serviceTypeTenantAmount;
            }
            return tenantServiceAmounts;
        }
    }
}