using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using DividaBill.DAL.Interfaces;
using DividaBill.Models;

namespace DividaBill.Services.NewBillingSystem.Impl
{

    public class BillingRunTransactionLogService
    {
        private readonly IUnitOfWork _unitOfWork;

        public BillingRunTransactionLogService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public bool TrackException(int billingRunId, Exception ex, BillingQueueStage stage)
        { 
            //var billing
            return false;
        }
    }

    public class BillingQueueTransactionLogService : IBillingQueueTransactionLogService
    {
        private readonly IUnitOfWork _unitOfWork;

        public BillingQueueTransactionLogService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public bool TrackChanges(int billingQueueId, string changes, BillingQueueStage stage, TransactionLogStatus status, IUnitOfWork unitOfWork = null)
        {
            if (unitOfWork == null)
                unitOfWork = _unitOfWork;

            var transactionLog = InsertRecord(billingQueueId, changes, stage, status);
            unitOfWork.Save();
            return transactionLog.ID != 0;
        }

        public bool TrackException(int billingQueueId, Exception ex, BillingQueueStage stage, IUnitOfWork unitOfWork = null)
        {
            if (unitOfWork == null)
                unitOfWork = _unitOfWork;

            var changes = $" Exception: message: {ex.Message} {Environment.NewLine} source: {ex.Source} {Environment.NewLine}  stacktrace: {ex.StackTrace} {Environment.NewLine}";
            var transactionLog = InsertRecord(billingQueueId, changes, stage, TransactionLogStatus.Error, unitOfWork);
            unitOfWork.Save();
            return transactionLog.ID != 0;
        }

        public bool TrackException(int billingRunId, int houseId, Exception ex, BillingQueueStage stage, IUnitOfWork unitOfWork = null)
        {
            if (unitOfWork == null)
                unitOfWork = _unitOfWork;

            var billingQueueId = unitOfWork.BillingQueueRepository.Where(w => w.BillingRunId == billingRunId && w.HouseId == houseId).Select(s => s.ID).SingleOrDefault();

            if (billingQueueId == 0)
                return false;

            return TrackException(billingQueueId, ex, stage,  unitOfWork);
        }

        public async Task<bool> TrackChangesAsync(int billingQueueId, BillingQueueStage stage, string changes, IUnitOfWork unitOfWork = null)
        {
            if (unitOfWork == null)
                unitOfWork = _unitOfWork;
            return await TrackChangesAsync(billingQueueId, changes, stage, TransactionLogStatus.Information, unitOfWork);
        }

        public async Task<bool> TrackChangesAsync(int billingQueueId, string changes, BillingQueueStage stage, TransactionLogStatus status, IUnitOfWork unitOfWork = null)
        {
            if (unitOfWork == null)
                unitOfWork = _unitOfWork;
            var transactionLog = InsertRecord(billingQueueId, changes, stage, status, unitOfWork);
            await unitOfWork.SaveAsync();
            return transactionLog.ID != 0;
        }

        private BillingQueueTransactionLog InsertRecord(int billingQueueId, string changes, BillingQueueStage stage, TransactionLogStatus status, IUnitOfWork unitOfWork = null)
        {
            if (unitOfWork == null)
                unitOfWork = _unitOfWork;
            var transactionLog = new BillingQueueTransactionLog
            {
                BillingQueueId = billingQueueId,
                CreatedAt = DateTime.UtcNow,
                Changes = changes,
                BillingQueueStage = stage,
                TransactionLogStatus = status
            };
            unitOfWork.BillingQueueTransactionLogRepository.Insert(transactionLog);
            return transactionLog;
        }

        public async Task<bool> TrackChangesAsync(int billingRunId, int houseId, string changes, BillingQueueStage stage, TransactionLogStatus status, IUnitOfWork unitOfWork = null)
        {
            if (unitOfWork == null)
                unitOfWork = _unitOfWork;

            var billingQueueId = await unitOfWork.BillingQueueRepository.Where(w => w.BillingRunId == billingRunId && w.HouseId == houseId).Select(s=> s.ID).SingleOrDefaultAsync();

            if (billingQueueId == 0)
                return false;

            return await TrackChangesAsync(billingQueueId, changes, stage, status, unitOfWork);
        }
    }
}