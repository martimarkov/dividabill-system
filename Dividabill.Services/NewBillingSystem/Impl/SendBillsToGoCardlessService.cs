﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DividaBill.DAL.Interfaces;
using DividaBill.DAL.Unity;
using DividaBill.Library;
using DividaBill.Models;
using DividaBill.Models.Bills;
using DividaBill.Models.GoCardless;
using DividaBill.Services.Impl;
using DividaBill.Services.Interfaces;
using GoCardless_API;
using GoCardless_API.api;

namespace DividaBill.Services.NewBillingSystem.Impl
{
    public class SendBillsToGoCardlessService : ISendBillsToGoCardlessService
    {
        private readonly IBillingQueueTransactionLogService _billingQueueTransactionLogService;
        private readonly IGoCardlessApiClient _goCardlessApiClient;
        private readonly IGoCardlessBankMandateService _goCardlessBankMandateService;
        private readonly IGoCardlessService _goCardlessService;
        private readonly IHouseholdBillService _householdBillService;
        private readonly IHousesToBeBilledService _housesToBeBilledService;
        private readonly IMultiThreadingService _multiThreadingService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IBankMandateService _bankMandateService;

        public SendBillsToGoCardlessService(IUnitOfWork unitOfWork, IHousesToBeBilledService housesToBeBilledService, IGoCardlessApiClient goCardlessApiClient,
            IHouseholdBillService householdBillService, IBillingQueueTransactionLogService billingQueueTransactionLogService,
            IGoCardlessService goCardlessService, IMultiThreadingService multiThreadingService,
            IGoCardlessBankMandateService goCardlessBankMandateService, IBankMandateService bankMandateService)
        {
            _bankMandateService = bankMandateService;
            _goCardlessBankMandateService = goCardlessBankMandateService;
            _unitOfWork = unitOfWork;
            _housesToBeBilledService = housesToBeBilledService;
            _goCardlessApiClient = goCardlessApiClient;
            _householdBillService = householdBillService;
            _billingQueueTransactionLogService = billingQueueTransactionLogService;
            _goCardlessService = goCardlessService;
            _multiThreadingService = multiThreadingService;
        }

        public async Task<BulkOperationResult<HouseModel>> SendBillsToGoCardlessAsync(int billingRunId, GoCardless.Environments goCardlessEnvironment)
        {
            var houses = _housesToBeBilledService.GetHousesThatHaveBeenBilledAnAmount(billingRunId).ToList();

            var now = DateTime.UtcNow;

            return await _multiThreadingService.RunTaskInParallelAsync<HouseModel, GoCardlessApiClient>(5, houses,
                async (house, unitOfWork, goCardlessApi, bulkOperationResult) =>
                {
                    try
                    {
                        await ProcessIndividualHouse(house, billingRunId, now, unitOfWork, goCardlessApi);
                        bulkOperationResult.Successful.Add(house);
                    }
                    catch (Exception ex)
                    {
                        bulkOperationResult.Failed.Add(house, ex);
                    }
                });
        }

        private async Task ProcessIndividualHouse(HouseModel h, int billingRunId, DateTime now, IUnitOfWork unitOfWork = null, IGoCardlessApiClient goCardlessApi = null)
        {
            unitOfWork = AssignThreadSafeUnitOfWork(unitOfWork);

            goCardlessApi = AssignThreadSafeGoCardlessApi(goCardlessApi);

            //var h = houses[i];
            var billingQueues = unitOfWork.BillingQueueRepository.Where(w => w.BillingRunId == billingRunId && w.HouseId == h.ID);

            var billsForHouse = billingQueues.Where(w => w.HouseholdBill.UtilityBills.All(
                b => b.Payments.Any(k =>
                    //we haven't requested payment yet
                    !k.PaymentRequestedDate.HasValue ||
                    //we have requested payment but it is past the charge date and we haven't receieved payment yet.
                    (k.IsPaid == false && k.PaymentRequestedDate.HasValue && k.ChargeDate.HasValue && k.ChargeDate <= now))))
                .Select(s => s.HouseholdBill).ToArray();


            var billingQueueId = billingQueues.Select(s => s.ID).FirstOrDefault();

            _billingQueueTransactionLogService.TrackChanges(billingQueueId, $"Submitting house {h.ID}", BillingQueueStage.SendBillsToGoCardless, TransactionLogStatus.Information, unitOfWork);

            var sendBillsToGoCardlessResult = new SendBillsToGoCardlessResult();
            await ProcessHouseholdBill(sendBillsToGoCardlessResult, billsForHouse, h, billingQueueId, unitOfWork, goCardlessApi);
        }

        private async Task ProcessHouseholdBill(SendBillsToGoCardlessResult sendBillsToGoCardlessResult, HouseholdBill[] billsForHouse, HouseModel h, int billingQueueId, IUnitOfWork unitOfWork = null, IGoCardlessApiClient goCardlessApi = null)
        {
            foreach (var bill in billsForHouse)
            {
                foreach (var tenant in h.Tenants)
                {
                    await ProcessBillForTenant(tenant, bill, billingQueueId, unitOfWork, goCardlessApi);
                }
            }
        }

        private IGoCardlessApiClient AssignThreadSafeGoCardlessApi(IGoCardlessApiClient goCardlessApi)
        {
            return goCardlessApi ?? _goCardlessApiClient;
        }

        private IUnitOfWork AssignThreadSafeUnitOfWork(IUnitOfWork unitOfWork)
        {
            return unitOfWork ?? _unitOfWork;
        }

        private async Task ProcessBillForTenant(User tenant, HouseholdBill bill, int billingQueueId, IUnitOfWork unitOfWork = null, IGoCardlessApiClient goCardlessApi = null)
        {
            unitOfWork = AssignThreadSafeUnitOfWork(unitOfWork);

            goCardlessApi = AssignThreadSafeGoCardlessApi(goCardlessApi);

            await AddTenantToGoCardlessAsync(tenant, unitOfWork, goCardlessApi);

            var amountLeft = _householdBillService.GetTenantOutstandingAmountNotYetSentToBeDebited(tenant, bill);
            var amountTotal = _householdBillService.GetTenantTotalAmount(tenant, bill);

            if (amountLeft != amountTotal)
                _billingQueueTransactionLogService.TrackChanges(billingQueueId, "Tenant {0} has already paid {1:0.00} of the {2:0.00} total".F(tenant.FullName, amountTotal - amountLeft, amountTotal), BillingQueueStage.SendBillsToGoCardless, TransactionLogStatus.Information);

            if (amountLeft == 0)
                return;

            var chargeDate = await GetPaymentChargeDate(tenant, unitOfWork, goCardlessApi);

            var paym = await CreateAndSendPaymentToGoCardless(tenant, bill, amountLeft, chargeDate, goCardlessApi);

            var payRef = CreateBankPayment(tenant, paym, unitOfWork);

            var createdAtDate = DateTime.UtcNow;

            UpdateUtilityBillPaymentsForUser(tenant, bill, payRef, createdAtDate, chargeDate, unitOfWork);
        }

        private async Task<DateTime> GetPaymentChargeDate(User tenant, IUnitOfWork unitOfWork, IGoCardlessApiClient goCardlessApi)
        {
            var asyncExecutionParam = new AsyncExecutionParam(unitOfWork);
            goCardlessApi = AssignThreadSafeGoCardlessApi(goCardlessApi);

            var mand = await goCardlessApi.GetMandateAsync(tenant.MandateID);
            var bankMandate = _bankMandateService.UpdateBankMandateFromGcMandate(tenant.MandateID, mand, asyncExecutionParam);

            var mandateNextChargeDate = Convert.ToDateTime(bankMandate.next_possible_charge_date);

            var paymentDate = DateTime.Today.AddBusinessDays(4);
            var chargeDate = mandateNextChargeDate > paymentDate
                ? mandateNextChargeDate
                : paymentDate;
            return chargeDate;
        }

        private string CreateBankPayment(User tenant, GCPayment paym, IUnitOfWork unitOfWork = null)
        {
            unitOfWork = AssignThreadSafeUnitOfWork(unitOfWork);

            var domainBankPayment = new BankPayment
            {
                amount = paym.amount,
                amount_refunded = paym.amount_refunded,
                charge_date = paym.charge_date,
                created_at = paym.created_at,
                currency = paym.currency,
                description = paym.description,
                id = paym.id,
                Mandate_ID = tenant.MandateID,
                reference = paym.reference,
                status = paym.status
            };
            var payRef = paym.id;
            unitOfWork.BankPaymentRepository.Insert(domainBankPayment);
            unitOfWork.Save();
            return payRef;
        }


        private async Task<GCPayment> CreateAndSendPaymentToGoCardless(User tenant, HouseholdBill bill, decimal amountLeft, DateTime chargeDate, IGoCardlessApiClient goCardlessApi = null)
        {
            goCardlessApi = AssignThreadSafeGoCardlessApi(goCardlessApi);

            var payment = new NewPayment
            {
                amount = Convert.ToInt32(amountLeft*100),
                charge_date = chargeDate.ToString("yyyy-MM-dd"),
                currency = "GBP",
                description = $"{bill.GetMonth()} bill run. Payment for " + bill.Month + " " + bill.Year,
                reference = ""
            };
            payment.links.Add("mandate", tenant.MandateID);

            var paym = await goCardlessApi.CreatePaymentAsync(payment);
            return paym;
        }

        private static void ExportCsv(Dictionary<string, string> problemTenants, Dictionary<string, string> problemOnes, Dictionary<string, string> mandProblemOnes)
        {
            var s = string.Join(Environment.NewLine, problemOnes.Select(x => x.Key + "," + x.Value));
            var s2 = string.Join(Environment.NewLine, mandProblemOnes.Select(x => x.Key + "," + x.Value));
            var s3 = string.Join(Environment.NewLine, problemTenants.Select(x => x.Key + "," + x.Value));


            File.WriteAllText("problemOneGoCardless-" + DateTime.Now.Ticks + ".csv", s);
            File.WriteAllText("problemMandateGoCardless-" + DateTime.Now.Ticks + ".csv", s2);
            File.WriteAllText("problemTenantGoCardless-" + DateTime.Now.Ticks + ".csv", s3);
        }

        private void UpdateUtilityBillPaymentsForUser(User u, HouseholdBill bill, string payRef, DateTime paymentRequestedDate, DateTime chargeDate, IUnitOfWork unitOfWork = null)
        {
            unitOfWork = AssignThreadSafeUnitOfWork(unitOfWork);

            foreach (var ub in bill.UtilityBills)
                foreach (var bp in ub.Payments.Where(w => w.User_Id == u.Id))
                {
                    bp.PaymentRef = payRef;
                    bp.PaymentRequestedDate = paymentRequestedDate;
                    bp.ChargeDate = chargeDate;
                    unitOfWork.BillPaymentsRepository.Update(bp);
                }
            unitOfWork.Save();
        }

        private async Task AddTenantToGoCardlessAsync(User tenant, IUnitOfWork unitOfWork = null, IGoCardlessApiClient goCardlessApi = null)
        {
            unitOfWork = AssignThreadSafeUnitOfWork(unitOfWork);
            goCardlessApi = AssignThreadSafeGoCardlessApi(goCardlessApi);

            var validationError = await _goCardlessService.AddToGoCardlessAsync(tenant, unitOfWork, goCardlessApi);
            if (validationError != null)
            {
                throw new Exception($"Key=[{validationError[0].Key}];ErrorMessage=[{validationError[0].ErrorMessage}]");
            }
        }
    }
}