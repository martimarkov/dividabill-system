using System;
using System.Collections.Generic;
using System.Linq;
using DividaBill.DAL.Interfaces;
using DividaBill.Models.Business;
using DividaBill.Services.Factories;

namespace DividaBill.Services.NewBillingSystem.Impl
{
    public class ReportingService:IReportingService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IStoredProcFactory _storedProcFactory;

        public ReportingService(IUnitOfWork unitOfWork, IStoredProcFactory storedProcFactory)
        {
            _unitOfWork = unitOfWork;
            _storedProcFactory = storedProcFactory;
        }

        public List<MonthlyBillingReport> GetTotalBilledForPeriod(DateTime start, DateTime end)
        {
            var result = _unitOfWork.sp_Monthly_Billing_Report(start, end);

            return result?.Select(sp => _storedProcFactory.Create(sp)).ToList();
        }
    }
}