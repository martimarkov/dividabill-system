﻿using System;
using DividaBill.Library;
using DividaBill.Models;
using DividaBill.Models.Bills;
using DividaBill.Models.Notifications;

namespace DividaBill.Services.NewBillingSystem.Impl
{
    public class NotificationFactory : INotificationFactory
    {
        private readonly IHouseholdBillService _householdBillService;

        public NotificationFactory(IHouseholdBillService householdBillService)
        {
            _householdBillService = householdBillService;
        }

        #region Constructors. 
        public Notification BillPayment(HouseholdBill bill, User user)
        {
            return new Notification
            {
                Type = NotificationType.BillPayment,

                NotifiedUser = user,
                TriggerUser = user,

                Message = "A payment of £{0} for the period from {1} to {2}. "
                    .F(_householdBillService.GetTenantTotalAmount(user,bill), bill.StartDate, bill.EndDate),
            };
        }

        public  Notification AllocationChange(UserPercentage perc)
        {
            var hp = perc.HousePercentage;
            return new Notification
            {
                Type = NotificationType.PriceAllocation,

                NotifiedUser = perc.User,
                TriggerUser = hp.TriggerUser,

                Message = "A proposal to change the {0} allocation so you pay {1}% of the bill. "
                    .F(hp.Service.Name, perc.PercentageOfBill * 100),
            };
        }

        public static Notification TenantJoined(User triggerUser, User notifiedUser)
        {
            return new Notification
            {
                Type = NotificationType.TenantJoined,

                NotifiedUser = notifiedUser,
                TriggerUser = triggerUser,

                Message = "{0} has joined your household. "
                    .F(triggerUser.FullName),
            };
        }

        public static Notification BalanceTransfer(User triggerUser, User notifiedUser)
        {
            throw new NotImplementedException();
        }

        public static Notification AddContract(User triggerUser, User notifiedUser)
        {
            throw new NotImplementedException();
        }

        public static Notification CancelContract(User triggerUser, User notifiedUser)
        {
            throw new NotImplementedException();
        }

        public static Notification ModifyContract(User triggerUser, User notifiedUser)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}