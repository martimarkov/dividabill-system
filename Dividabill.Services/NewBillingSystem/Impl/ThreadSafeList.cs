using System.Collections.Generic;
using System.Threading;

namespace DividaBill.Services.NewBillingSystem.Impl
{
    public class ThreadSafeList<T>: IThreadSafeList<T>
    {
        private readonly ReaderWriterLockSlim _listReaderWriterLockSlim = new ReaderWriterLockSlim();
        private List<T> _list = new List<T>();
        public List<T> List
        {
            get
            {
                _listReaderWriterLockSlim.EnterReadLock();
                var value = _list;
                _listReaderWriterLockSlim.ExitReadLock();
                return value;
            }
            set
            {
                _listReaderWriterLockSlim.EnterWriteLock();
                _list = value;
                _listReaderWriterLockSlim.ExitWriteLock();
            }
        }
    }
}