﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using API.Library.Helpers;
using DividaBill.AppConstants;
using DividaBill.DAL.Interfaces;
using DividaBill_Library;
using GoCardless_API;
using GoCardless_API.api;

namespace DividaBill.Services.NewBillingSystem.Impl
{
    public interface ICancelPendingPaymentsGocardlessService
    {
        Task CancelPaymentsAsync(int billingRunId, GoCardless.Environments goCardlessEnvironment);
    }

    public class CancelPendingPaymentsGocardlessService : ICancelPendingPaymentsGocardlessService
    {
        private readonly IGoCardlessApiClient _goCardlessApiClient;
        private readonly IUnitOfWork _unitOfWork;

        public CancelPendingPaymentsGocardlessService(IUnitOfWork unitOfWork, IGoCardlessApiClient goCardlessApiClient)
        {
            _unitOfWork = unitOfWork;
            _goCardlessApiClient = goCardlessApiClient;
        }

        public async Task CancelPaymentsAsync(int billingRunId, GoCardless.Environments goCardlessEnvironment)
        {
            var problemOnes = new Dictionary<string, string>();
            GoCardless.Environment = goCardlessEnvironment;

            var payments = _unitOfWork.BankPaymentRepository.Get(w => w.status == GoCardlessMandateStatus.PendingSubmission).ToList();
            var pCount = payments.Count;
            var i = 0;
            foreach (var pay in payments)
            {
                Console.WriteLine("Payment {0}/{1}", ++i, pCount);
                try
                {
                    var pay1 = await Retry.DoAsync(async () => await _goCardlessApiClient.GetPaymentAsync(pay.id), TimeSpan.FromSeconds(5), 1);
                    var payment = _unitOfWork.BankPaymentRepository.GetByID(pay.id);
                    payment.status = pay1.status;
                    _unitOfWork.Save();
                    if (payment.status == GoCardlessMandateStatus.PendingSubmission)
                    {
                        var pay2 = await Retry.DoAsync(async () => await _goCardlessApiClient.CancelPaymentAsync(pay.id), TimeSpan.FromSeconds(5), 1);
                    }
                }
                catch (AggregateException ae)
                {
                    ae.Handle(x =>
                    {
                        if (x is ApiException) // This we know how to handle.
                        {
                            if (!problemOnes.ContainsKey(pay.id))
                            {
                                problemOnes.Add(pay.id, ((ApiException) x).RawContent);
                            }
                        }
                        return true; //if you do something like this all exceptions are marked as handled  
                    });
                }
            }

            ExportCsv(problemOnes);
            Console.WriteLine("Submited all payments to GoCardless. Check error log files for any problems.");
        }

        private void ExportCsv(Dictionary<string, string> problemOnes)
        {
            var s = string.Join(Environment.NewLine, problemOnes.Select(x => x.Key + "," + x.Value));


            File.WriteAllText("problemCanceledPaymentsGoCardless-" + DateTime.Now.Ticks + ".csv", s);
        }
    }
}