using DividaBill.Models;

namespace DividaBill.Services.Abstraction
{
    public interface IBankAccountEncryptionService
    {
        void EncryptBankAccount(BankAccount accountHolder);
        void DecryptBankAccount(BankAccount accountHolder);
    }
}