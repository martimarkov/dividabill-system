using System.Collections.Generic;
using DividaBill.Models;

namespace DividaBill.Services.Abstraction
{
    public interface ITenantEncryptionService
    {
        void EncryptUser(User user);
        User DecryptUser(User user);
        User DecryptUserName(User user);
        IEnumerable<User> DecryptUsersList(IEnumerable<User> usersList);
    }
}