using DividaBill.ViewModels.Abstractions;

namespace DividaBill.Services.Abstraction
{
    public interface IViewModelEncryptionService<T> where T : IViewModel
    {
        T DecryptViewModel(T tenant);
    }
}