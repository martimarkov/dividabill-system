using DividaBill.DAL.Unity;

namespace DividaBill.Services.Abstraction
{
    public interface ISaveService
    {
        void Save(AsyncExecutionParam asyncExecutionParam = null);
    }
}