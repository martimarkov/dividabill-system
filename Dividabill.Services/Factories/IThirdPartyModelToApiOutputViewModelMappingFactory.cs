using DividaBill.ViewModels.Abstractions;

namespace DividaBill.Services.Factories
{
    public interface IThirdPartyModelToApiOutputViewModelMappingFactory<in TEm, TVm> : IMappingFactory<TEm, TVm> where TEm : IThirdPartyModel where TVm : IApiOutputViewModel
    {
    }
}