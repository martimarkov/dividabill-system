using System;
using System.Security.Principal;
using DividaBill.Models;
using DividaBill.Models.Emails;
using DividaBill.ViewModels.SysOp.InputModels;
using Microsoft.AspNet.Identity;

namespace DividaBill.Services.Factories.Impl
{
    public class BillingRunCreateInputModelToBillingRunMappingFactory : IBillingRunCreateInputModelToBillingRunMappingFactory
    {
        public BillingRun Map(BillingRunCreateInputModel source)
        {
            return new BillingRun
            {
                Period = source.Period,
                EmailSendFrom = source.SendFromEmail,
                EmailSendFromDisplayName = source.SendFromEmailDisplayName,
                EmailSubject = source.EmailSubject,
                Name = source.Name,
                Description = source.Description
            };
        }

        public BillingRun Map(BillingRunCreateInputModel source, BillingRun destination)
        {
            throw new NotImplementedException();
        }

        public BillingRun Map(BillingRunCreateInputModel source, IPrincipal user, EmailTemplate emailTemplate)
        {
            var billingRun = Map(source);

            billingRun.InitiatedByUserName = user.Identity.Name;
            billingRun.InitiatedByUserId = user.Identity.GetUserId();
            billingRun.EmailTemplate = emailTemplate;
            billingRun.EmailTemplateId = emailTemplate.ID;

            return billingRun;
        }
    }
}