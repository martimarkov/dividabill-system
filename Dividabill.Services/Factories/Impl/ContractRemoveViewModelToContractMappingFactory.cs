using System;
using DividaBill.Models;
using DividaBill.ViewModels;

namespace DividaBill.Services.Factories.Impl
{
    public class ContractRemoveViewModelToContractMappingFactory : IViewModelToEntityMappingFactory<ContractRemoveViewModel, Contract>
    {
        public Contract Map(ContractRemoveViewModel source)
        {
            throw new NotImplementedException();
        }

        public Contract Map(ContractRemoveViewModel source, Contract destination)
        {
            destination.RequestedStartDate = source.RequestedStartDate;
            destination.EndDate = source.EndDate;
            return destination;
        }
    }
}