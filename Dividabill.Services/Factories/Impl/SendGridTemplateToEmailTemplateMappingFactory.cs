using System;
using System.Linq;
using DividaBill.Models.Emails;
using SendGrid.API.Models;

namespace DividaBill.Services.Factories.Impl
{
    public class SendGridTemplateToEmailTemplateMappingFactory : IMappingFactory<SendGridTemplate, EmailTemplate>
    {
        public EmailTemplate Map(SendGridTemplate source)
        {
            var versionFromThirdParty = source?.GetActiveVersion();
            if (versionFromThirdParty == null)
                return null;

            return new EmailTemplate
            {
                TemplateId = source.id,
                VersionId = versionFromThirdParty.id,
                Subject = versionFromThirdParty.subject,
                HtmlContent = versionFromThirdParty.html_content,
                Name = source.name,
                PlainContent = versionFromThirdParty.plain_content
            };
        }

        public EmailTemplate Map(SendGridTemplate source, EmailTemplate destination)
        {
            throw new NotImplementedException();
        }
    }
}