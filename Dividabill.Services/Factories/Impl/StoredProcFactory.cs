using DividaBill.DAL.StoredProcs;
using DividaBill.Models.Business;

namespace DividaBill.Services.Factories.Impl
{
    public class StoredProcFactory : IStoredProcFactory
    {
        public MonthlyBillingReport Create(sp_Monthly_Billing_Report_Result sp)
        {
            var monthlyReport = new MonthlyBillingReport
            {
                Year = sp.Year,
                Month = sp.Month,
                BilledHouseCount = sp.BilledHouseCount,
                Housemates = sp.Housemates,
                NbOfHousesWithBroadbandContract = sp.Nb_of_Houses_with_Broadband_contract,
                NbOfHousesWithElectricityContract = sp.Nb_of_Houses_with_Electricity_contract,
                NbOfHousesWithGasContract = sp.Nb_of_Houses_with_Gas_contract,
                NbOfHousesWithLandlinePhoneContract = sp.Nb_of_Houses_with_LandlinePhone_contract,
                NbOfHousesWithLineRentalContract = sp.Nb_of_Houses_with_LineRental_contract,
                NbOfHousesWithNetflixTvContract = sp.Nb_of_Houses_with_NetflixTV_contract,
                NbOfHousesWithSkyTvContract = sp.Nb_of_Houses_with_SkyTV_contract,
                NbOfHousesWithTvLicenseContract = sp.Nb_of_Houses_with_TVLicense_contract,
                NbOfHousesWithWaterContract = sp.Nb_of_Houses_with_Water_contract,
                TotalAmountBilled = sp.TotalAmountBilled,
                TotalAmountBilledForBroadband = sp.Total_Amount_billed_for_Broadband,
                TotalAmountBilledForElectricity = sp.Total_Amount_billed_for_Electricity,
                TotalAmountBilledForGas = sp.Total_Amount_billed_for_Gas,
                TotalAmountBilledForLandlinePhone = sp.Total_Amount_billed_for_LandlinePhone,
                TotalAmountBilledForLineRental = sp.Total_Amount_billed_for_LineRental,
                TotalAmountBilledForNetflixTv = sp.Total_Amount_billed_for_NetflixTV,
                TotalAmountBilledForSkyTv = sp.Total_Amount_billed_for_SkyTV,
                TotalAmountBilledForTvLicense = sp.Total_Amount_billed_for_TVLicense,
                TotalAmountBilledForWater = sp.Total_Amount_billed_for_Water
            };
            return monthlyReport;
        }
    }
}