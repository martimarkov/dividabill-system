using AutoMapper;

namespace DividaBill.Services.Factories.Impl
{
    public class AutoMapperFactory<TSource, TDestination> : IMappingFactory<TSource, TDestination>
    {
        private readonly IMappingEngine _mappingEngine;

        public AutoMapperFactory(IMappingEngine mappingEngine)
        {
            _mappingEngine = mappingEngine;
        }

        public TDestination Map(TSource source)
        {
            return _mappingEngine.Map<TSource, TDestination>(source);
        }

        public TDestination Map(TSource source, TDestination destination)
        {
            return _mappingEngine.Map(source, destination);
        }
    }
}