using System;
using DividaBill.Models;
using DividaBill.ViewModels;

namespace DividaBill.Services.Factories.Impl
{
    public class ContractToContractRemoveOutputViewModelMappingFactory : IEntityToViewModelMappingFactory<Contract, ContractRemoveViewModel>
    {
        public ContractRemoveViewModel Map(Contract source)
        {
            return new ContractRemoveViewModel
            {
                RequestedStartDate = source.RequestedStartDate,
                EndDate = source.EndDate,
                ID = source.ID,
                StartDate = source.StartDate
            };
        }

        public ContractRemoveViewModel Map(Contract source, ContractRemoveViewModel destination)
        {
            throw new NotImplementedException();
        }
    }
}