using System;
using DividaBill.Models;
using DividaBill.ViewModels;

namespace DividaBill.Services.Factories.Impl
{
    public class TenancyToTenancyViewModelMappingFactory : IEntityToViewModelMappingFactory<Tenancy, TenancyViewModel>
    {
        public TenancyViewModel Map(Tenancy source)
        {
            return new TenancyViewModel
            {
                TenancyId = source.Id,
                UserId = source.UserId,
                User = source.User,
                HouseModel = source.House,
                HouseId = source.HouseId,
                JoiningDate = source.JoiningDate,
                LeavingDate = source.LeavingDate
            };
        }

        public TenancyViewModel Map(Tenancy source, TenancyViewModel destination)
        {
            throw new NotImplementedException();
        }
    }
}