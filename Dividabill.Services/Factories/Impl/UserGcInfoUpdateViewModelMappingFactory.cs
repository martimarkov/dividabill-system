using DividaBill.Models;
using DividaBill.ViewModels.DataTables;

namespace DividaBill.Services.Factories.Impl
{
    public class UserGcInfoUpdateViewModelMappingFactory : IUserGcInfoUpdateViewModelMappingFactory
    {
        public UserGcInfoUpdateViewModel Map(User tenant)
        {
            return new UserGcInfoUpdateViewModel
            {
                UserId = tenant.Id,
                GoCardlessCustomerId = tenant.GoCardlessCustomerID,
                MandateId = tenant.MandateID,
                BankAccountId = tenant.BankAccountID,
                BankAccountHolder = tenant.AccountHolder,
                BankAccountNumber = tenant.AccountNumber,
                BankAccountSortCode = tenant.AccountSortCode,
                UserRefTitle = tenant.RefTitle,
                UserFirstName = tenant.FirstName,
                UserLastName = tenant.LastName
            };
        }
    }
}