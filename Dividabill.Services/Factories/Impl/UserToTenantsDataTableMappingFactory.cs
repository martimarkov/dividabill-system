using System;
using DividaBill.Models;
using DividaBill.ViewModels.DataTables;

namespace DividaBill.Services.Factories.Impl
{
    public class UserToTenantsDataTableMappingFactory : IEntityToViewModelMappingFactory<User, TenantsDataTableViewModel>
    {
        public TenantsDataTableViewModel Map(User source)
        {
            return new TenantsDataTableViewModel
            {
                Id = source.Id,
                FirstName = source.FirstName,
                LastName = source.LastName,
                PhoneNumber = source.PhoneNumber,
                Email = source.Email,
                DOB = source.DOB,
                HouseId = source.House_ID,
                AccountNumber = source.AccountNumber,
                AccountSortCode = source.AccountSortCode,
                SignupDate = source.SignupDate
            };
        }

        public TenantsDataTableViewModel Map(User source, TenantsDataTableViewModel destination)
        {
            throw new NotImplementedException();
        }
    }
}