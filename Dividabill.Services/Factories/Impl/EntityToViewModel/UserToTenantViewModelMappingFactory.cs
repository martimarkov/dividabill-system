using AutoMapper;
using DividaBill.Models;
using DividaBill.Models.Common;
using DividaBill.ViewModels;

namespace DividaBill.Services.Factories.Impl.EntityToViewModel
{
    public class UserToTenantViewModelMappingFactory : IEntityToViewModelMappingFactory<User, TenantViewModel>
    {
        private readonly IMappingEngine _mappingEngine;

        public UserToTenantViewModelMappingFactory(IMappingEngine mappingEngine)
        {
            _mappingEngine = mappingEngine;
        }

        public TenantViewModel Map(User source)
        {
            return Map(source, new TenantViewModel());
        }

        public TenantViewModel Map(User source, TenantViewModel destination)
        {
            destination.Id = source.Id;
            if (source.House_ID != null)
                destination.House_ID = source.House_ID.Value;
            destination.FirstName = source.FirstName;
            destination.AccountSortCode = source.AccountSortCode;
            destination.Email = source.Email;
            destination.AccountNumber = source.AccountNumber;
            destination.AccountHolder = source.AccountHolder;
            destination.GoCardlessCustomerID = source.GoCardlessCustomerID;
            destination.LastName = source.LastName;
            destination.RefTitle = source.RefTitle;
            destination.SignupDate = source.SignupDate;
            destination.AccountSoleHolder = source.AccountSoleHolder;
            destination.DDRef = source.DDRef;
            destination.DOB = source.DOB;
            destination.HomeAddress = _mappingEngine.Map<SimpleAddress>(source.HomeAddress);
            destination.LockoutEnabled = source.LockoutEnabled;
            destination.NewsletterSubscription = source.NewsletterSubscription;
            destination.Note = source.Note;
            destination.PhoneNumber = source.PhoneNumber;
            return destination;
        }
    }
}