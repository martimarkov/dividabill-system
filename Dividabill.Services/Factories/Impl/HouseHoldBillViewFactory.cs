using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DividaBill.Models;
using DividaBill.Models.Bills;
using DividaBill.ViewModels.DataTables;

namespace DividaBill.Services.Factories.Impl
{
    public class HouseHoldBillViewFactory : IHouseHoldBillViewFactory
    {
        public List<HouseHoldBillsViewModel> Create(IQueryable<HouseholdBill> bills)
        {
            var b = bills.Include("House").Include("UtilityBills").ToList();
                
            var vms = b.Select(source =>
                new HouseHoldBillsViewModel
                {
                    ID = source.ID.ToString(),
                    TotalBill = source.TotalBill,
                    PaymentDate = source.PaymentDate,
                    HouseID = source.House_ID,
                    Address = source.House.Address,
                    StartDate = source.StartDate,
                    EndDate = source.EndDate,
                    CreatedDate = source.CreatedDate,
                    //Status = source.
                }).ToList();
            //foreach (var houseHoldBillsViewModel in vms)
            //{
            //    houseHoldBillsViewModel.PaymentDate = houseHoldBillsViewModel.StartDate.AddBusinessDays(3);
            //}
            return vms;
        }
    }
}