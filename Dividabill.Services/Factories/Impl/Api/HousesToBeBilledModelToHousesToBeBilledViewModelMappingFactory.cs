using System;
using DividaBill.Services.NewBillingSystem.Model;
using DividaBill.ViewModels.DataTables;

namespace DividaBill.Services.Factories.Impl.Api
{
    public class HousesToBeBilledModelToHousesToBeBilledViewModelMappingFactory : IMappingFactory<HousesToBeBilledModel, HousesToBeBilledViewModel>
    {
        public HousesToBeBilledViewModel Map(HousesToBeBilledModel source)
        {
            return new HousesToBeBilledViewModel
            {
                ID = source.ID,
                Address = source.Address.Line1 + ", " + source.Address.Postcode,
                RegisteredTenants = source.Tenants.Count,
                Housemates = source.Housemates,
                IsAlreadyInQueue = source.IsAlreadyInQueue
            };
        }

        public HousesToBeBilledViewModel Map(HousesToBeBilledModel source, HousesToBeBilledViewModel destination)
        {
            throw new NotImplementedException();
        }
    }
}