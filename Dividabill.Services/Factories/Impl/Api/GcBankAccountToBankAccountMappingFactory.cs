using DividaBill.Models;
using DividaBill.Models.GoCardless;

namespace DividaBill.Services.Factories.Impl.Api
{
    public class GcBankAccountToBankAccountMappingFactory : IGcBankAccountToBankAccountMappingFactory
    {
        public BankAccount Map(User tenant, GCBankAccount bankAcc)
        {
            var destination = new BankAccount();
            destination = Map(tenant, destination);
            destination = Map(bankAcc, destination);
            return destination;
        }

        public BankAccount Map(GCBankAccount bankAcc, BankAccount destination)
        {
            destination.created_at = bankAcc.created_at;
            destination.Id = bankAcc.id;
            destination.sort_code = bankAcc.branch_code;
            destination.account_holder_name = bankAcc.account_holder_name;
            destination.account_number = bankAcc.account_number;
            destination.country_code = bankAcc.country_code;
            destination.created_at = bankAcc.created_at;
            destination.currency = bankAcc.currency;
            destination.enabled = bankAcc.enabled;
            return destination;
        }

        private BankAccount Map(User tenant, BankAccount destination)
        {
            destination.Owner_ID = tenant.Id;
            destination.GoCardlessCustomerID = tenant.GoCardlessCustomerID;
            return destination;
        }
    }
}