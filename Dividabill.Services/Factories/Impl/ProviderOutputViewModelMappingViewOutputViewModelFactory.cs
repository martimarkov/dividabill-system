﻿using System;
using System.Collections.Generic;
using System.Linq;
using DividaBill.Models;
using DividaBill.ViewModels.API;

namespace DividaBill.Services.Factories.Impl
{
    public class ProviderOutputViewModelMappingViewOutputViewModelFactory : IEntityToApiOutputViewModelMappingFactory<Provider, ProviderApiOutputViewModel>
    {
        public ProviderApiOutputViewModel Map(Provider source)
        {
            return new ProviderApiOutputViewModel
            {
                ID = source.ID,
                Name = source.Name,
                AverageInstallationDelay = source.AverageInstallationDelay,
                ContactPerson = source.ContactPerson,
                PhoneNumber = source.PhoneNumber
            };
        }

        public ProviderApiOutputViewModel Map(Provider source, ProviderApiOutputViewModel destination)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ProviderApiOutputViewModel> Map(IEnumerable<Provider> entities)
        {
            return entities.Select(Map);
        }
    }
}