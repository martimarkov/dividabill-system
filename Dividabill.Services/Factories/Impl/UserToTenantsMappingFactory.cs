using System;
using DividaBill.Models;
using DividaBill.ViewModels.DataTables;

namespace DividaBill.Services.Factories.Impl
{
    public class UserToTenantsMappingFactory : IEntityToViewModelMappingFactory<User, TenantsViewModel>
    {
        public TenantsViewModel Map(User source)
        {
            return new TenantsViewModel
            {
                Id = source.Id,
                FirstName = source.FirstName,
                LastName = source.LastName,
                DOB = source.DOB,
                Locked = source.LockoutEnabled,
                Options = null
            };
        }

        public TenantsViewModel Map(User source, TenantsViewModel destination)
        {
            throw new NotImplementedException();
        }
    }
}