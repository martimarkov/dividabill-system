using DividaBill.Models;
using DividaBill.ViewModels.DataTables;

namespace DividaBill.Services.Factories
{
    public interface IUserGcInfoUpdateViewModelMappingFactory
    {
        UserGcInfoUpdateViewModel Map(User tenant);
    }
}