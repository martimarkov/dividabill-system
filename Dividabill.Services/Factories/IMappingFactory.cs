namespace DividaBill.Services.Factories
{
    public interface IMappingFactory<in TSource, TDestination>
    {
        TDestination Map(TSource source);

        TDestination Map(TSource source, TDestination destination);
    }
}