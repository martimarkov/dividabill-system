﻿using DividaBill.DAL.StoredProcs;
using DividaBill.Models.Business;

namespace DividaBill.Services.Factories
{
    public interface IStoredProcFactory
    {
        MonthlyBillingReport Create(sp_Monthly_Billing_Report_Result sp);
    }
}