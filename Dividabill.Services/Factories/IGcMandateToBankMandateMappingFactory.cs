using DividaBill.Models;
using DividaBill.Models.GoCardless;

namespace DividaBill.Services.Factories
{
    public interface IGcMandateToBankMandateMappingFactory
    {
        BankMandate Map(User tenant, GCMandate mand, BankMandate destination);
        BankMandate Map(GCMandate mand, BankMandate destination);
        BankMandate Map(User tenant, GCMandate mand);
    }
}