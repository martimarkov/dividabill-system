using DividaBill.Models;
using DividaBill.Models.GoCardless;

namespace DividaBill.Services.Factories
{
    public interface IGcBankAccountToBankAccountMappingFactory
    {
        BankAccount Map(User tenant, GCBankAccount mand);
        BankAccount Map(GCBankAccount bankAcc, BankAccount destination);
    }
}