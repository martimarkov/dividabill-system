﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using DividaBill.DAL.Unity;
using DividaBill.Models;
using DividaBill.Models.Errors;
using DividaBill.Services.Emails.Models;

namespace DividaBill.Services.Emails
{
    public interface IEmailService
    {
        /// <summary>
        ///     Sends an e-mail to the given user upon successful registration.
        /// </summary>
        /// <param name="user">The user to send an e-mail to. </param>
        /// <param name="isNewHouse">Whether the user just created a new household. </param>
        Task SendUserRegisteredEmail(User user, bool isNewHouse);

        Task SendSignUpConfirmationEmail(User user, string callback = null);

        Task SendBillMessage(EmailConfiguration emailConfiguration, BillMessageSubtitution subtitution, string testEmailAddress = null, AsyncExecutionParam asyncExecutionParam = null);

        void SendEmailForException(ExceptionInfo exceptionInfo);
    }
}