﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DividaBill.AppConstants;
using DividaBill.DAL.Unity;
using DividaBill.Library;
using DividaBill.Library.AppConfiguration;
using DividaBill.Models;
using DividaBill.Models.Emails;
using DividaBill.Models.Errors;
using DividaBill.Services.AppConfiguration;
using DividaBill.Services.Emails.Models;
using DividaBill.Services.Exceptions;
using DividaBill.Services.Interfaces;
using DividaBill.Services.NewBillingSystem;
using DividaBill.Services.ThirdPartyServices.Interfaces;

namespace DividaBill.Services.Emails.Impl
{
    public class EmailService : IEmailService
    {
        private readonly IAppConfigurationReader _appConfigurationReader;
        private readonly IEmailConfigurationReader _emailConfigurationReader;
        private readonly IEmailThirdPartyService _emailThirdPartyService;
        private readonly IHouseholdBillService _householdBillService;
        private readonly IHouseService _houseService;
        private readonly ITenantService _tenantService;

        public EmailService(IEmailConfigurationReader emailConfigurationReader,
            ITenantService tenantService, IHouseService houseService, IHouseholdBillService householdBillService, IEmailThirdPartyService emailThirdPartyService,
            IAppConfigurationReader appConfigurationReader)
        {
            _emailThirdPartyService = emailThirdPartyService;
            _appConfigurationReader = appConfigurationReader;
            _householdBillService = householdBillService;
            _houseService = houseService;
            _tenantService = tenantService;
            _emailConfigurationReader = emailConfigurationReader;
        }

        public async Task SendSignUpConfirmationEmail(User user, string callback = null)
        {
            var email = GetEmailMessageSignUpConfirmation(user.Id, callback);
            await _emailThirdPartyService.SaveAndSendEmail(user.Id, email);
        }

        /// <summary>
        ///     Sends an e-mail to the given user after the registration is complete.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="isNewHouse"></param>
        public async Task SendUserRegisteredEmail(User user, bool isNewHouse)
        {
            var emailToSend = GetEmailMessageUserRegisteredToHouse(user.Id, isNewHouse);
            await _emailThirdPartyService.SaveAndSendEmail(user.Id, emailToSend);
        }

        public async Task SendBillMessage(EmailConfiguration emailConfiguration, BillMessageSubtitution subtitution, string testEmailAddress = null, AsyncExecutionParam asyncExecutionParam = null)
        {
            var emailToSend = GetBillMessage(emailConfiguration, subtitution, testEmailAddress);
            await _emailThirdPartyService.SaveAndSendEmail(subtitution.Tenant.Id, emailToSend, asyncExecutionParam);
        }

        public void SendEmailForException(ExceptionInfo exceptionInfo)
        {
            var emailToSend = GetEmailForException(exceptionInfo);
            Task.Run(async () => await _emailThirdPartyService.SendMessage(emailToSend)).Wait();
        }

        private EmailMessageToSend GetEmailMessageSignUpConfirmation(string id, string callback = null)
        {
            var client = _tenantService.GetTenantDecrypted(id);
            if (!client.House_ID.HasValue)
            {
                throw new HouseNotFoundException($"Unknown HouseId for User [{client.Id}]");
            }
            var house = _houseService.GetHouse(client.House_ID.Value);

            var emailConfiguration = _emailConfigurationReader.GetEmailConfiguration(EmailEnum.EmailVerification);

            var emailDict = new Dictionary<string, string>
            {
                {"[First Name]", client.FirstName},
                {"{{first_name}}", client.FirstName},
                {"[ActivationDate]", house.StartDate.ToStringDateFormat()},
                {"{{CallBackUrl}}", callback ?? string.Empty}
            };

            return new EmailMessageToSend(emailConfiguration, EmailType.SignUpMessages, client, emailDict);
        }

        private EmailMessageToSend GetEmailMessageUserRegisteredToHouse(string userId, bool isNewHouse)
        {
            var user = _tenantService.GetTenantDecrypted(userId);
            if (!user.House_ID.HasValue)
            {
                throw new HouseNotFoundException($"Unknown HouseId for User [{user.Id}]");
            }
            var house = _houseService.GetHouse(user.House_ID.Value);

            var emailConfigurationType = EmailEnum.UserRegisteredToNewHouse;
            if (!isNewHouse)
                emailConfigurationType = EmailEnum.UserRegisteredToExistingHouse;

            var emailConfiguration = _emailConfigurationReader.GetEmailConfiguration(emailConfigurationType);

            var emailDict = new Dictionary<string, string>
            {
                {"[First Name]", user.FirstName},
                {"[House ID]", house.ID.ToString()}
            };

            return new EmailMessageToSend(emailConfiguration, EmailType.SignUpMessages, user, emailDict);
        }

        private EmailMessageToSend GetBillMessage(EmailConfiguration emailConfiguration, BillMessageSubtitution subtitution, string testEmailAddress = null)
        {
            var emailDict = CreateSubstitutions(subtitution);

            if (testEmailAddress != null)
            {
                var testingEmailMessageToSend = new EmailMessageToSend(emailConfiguration, EmailType.BillRunMessages, testEmailAddress, emailDict);
                testingEmailMessageToSend.SetTestSending(true);
                return testingEmailMessageToSend;
            }
            return new EmailMessageToSend(emailConfiguration, EmailType.BillRunMessages, subtitution.Tenant, emailDict);
        }

        private EmailMessageToSend GetEmailForException(ExceptionInfo exceptionInfo)
        {
            var emailConfiguration = _emailConfigurationReader.GetEmailConfiguration(EmailEnum.EmailForException);

            var exception = exceptionInfo.Exception;
            const string nullString = "null";

            var exceptionData = nullString;
            if (exception.Data != null)
            {
                exceptionData = exception.Data.Keys.Cast<object>().Aggregate(exceptionData, (current, key) => current + exception.Data[key]);
            }

            var headers = string.Empty;
            if (exceptionInfo.Headers != null)
            {
                for (var i = 0; i < exceptionInfo.Headers.Count; i++)
                {
                    headers += $"{{{exceptionInfo.Headers.AllKeys[i]}: {exceptionInfo.Headers[i]}}}<br/>";
                }
            }

            var source = exception.Source ?? nullString;
            var assemblyVersionInfo = Functions.GetAssemblyVersionInfo(exceptionInfo.Assembly);
            var emailDict = new Dictionary<string, string>
            {
                {"{Exception.Message}", exception.Message},
                {"{Exception.Type}", exception.GetType().FullName},
                {"{Exception.StackTrace}", exception.StackTrace ?? nullString},
                {"{Exception.Source}", source},
                {"{Exception.Data}", exceptionData},
                {"{Url}", exceptionInfo.Url ?? nullString},
                {"{HttpMethod}", exceptionInfo.HttpMethod ?? nullString},
                {"{UserAgent}", exceptionInfo.UserAgent ?? nullString},
                {"{UserHostAddress}", exceptionInfo.UserHostAddress ?? nullString},
                {"{UserHostName}", exceptionInfo.UserHostName ?? nullString},
                {"{Headers}", string.IsNullOrEmpty(headers) ? nullString : headers},
                {"{Form}", exceptionInfo.Form ?? nullString},
                {"{AssemblyVersionInfo}", assemblyVersionInfo}
            };
            var sendToEmail = _appConfigurationReader.GetFromAppConfig(AppSettingsEnum.ExceptionsSendToEmail);

            emailConfiguration.Subject += $" '{source} - {assemblyVersionInfo}'";

            var emailMessageToSend = new EmailMessageToSend(emailConfiguration, sendToEmail, emailDict);
            emailMessageToSend.SetTestSending(true);
            return emailMessageToSend;
        }

        private Dictionary<string, string> CreateSubstitutions(BillMessageSubtitution subtitution)
        {
            var elecTenantAmount = subtitution.TenantServiceAmounts[ServiceType.Electricity.Id];
            var gasTenantAmount = subtitution.TenantServiceAmounts[ServiceType.Gas.Id];
            var waterTenantAmount = subtitution.TenantServiceAmounts[ServiceType.Water.Id];
            var bbTenantAmount = subtitution.TenantServiceAmounts[ServiceType.Broadband.Id];
            var skyTenantAmount = subtitution.TenantServiceAmounts[ServiceType.SkyTV.Id];
            var tvlicenseTenantAmount = subtitution.TenantServiceAmounts[ServiceType.TVLicense.Id];
            var phoneTenantAmount = subtitution.TenantServiceAmounts[ServiceType.LandlinePhone.Id];
            var lineRentalTenantAmount = subtitution.TenantServiceAmounts[ServiceType.LineRental.Id];

            var substitutions = new Dictionary<string, string>
            {
                {"{{first_name}}", subtitution.Tenant.FirstName},
                {"{{ind_payment}}",_householdBillService.GetTenantTotalAmount(subtitution.Tenant, subtitution.Bill).ToString("F")},
                {"{{house_payment}}", subtitution.Bill.TotalAmount.ToString("F")},
                {"{{house_reference_number}}", subtitution.Tenant.House_ID.ToString()},
                {"{{bill_start_date}}", subtitution.Bill.StartDate.ToShortDateString()},
                {"{{bill_end_date}}", subtitution.Bill.EndDate.ToShortDateString()},
                {"{{payment_collection_date}}", " 3-5 working days"},
                {"{{tenant_names}}", subtitution.Tenants},
                {"{{ind_total_electricity}}", elecTenantAmount.ToString("F")},
                {"{{ind_total_gas}}", gasTenantAmount.ToString("F")},
                {"{{ind_total_water}}", waterTenantAmount.ToString("F")},
                {"{{ind_total_internet}}", bbTenantAmount.ToString("F")},
                {"{{ind_total_sky}}", skyTenantAmount.ToString("F")},
                {"{{ind_total_tvlicense}}", tvlicenseTenantAmount.ToString("F")},
                {"{{ind_total_landline}}", phoneTenantAmount.ToString("F")},
                {"{{ind_total_line_rental}}", lineRentalTenantAmount.ToString("F")},
                {"{{registered_tenants}}", subtitution.House.Tenants.Count.ToString()},
                {"{{total_tenants}}", subtitution.House.Housemates.ToString()},
                {"{{House_address_1}}", subtitution.House.Address.Line1},
                {"{{City}}", subtitution.House.Address.City},
                {"{{County}}", string.IsNullOrEmpty(subtitution.House.Address.County) ? "  ": subtitution.House.Address.County},
                {"{{Postcode}}", subtitution.House.Address.Postcode}
            };
            return substitutions;
        }
    }
}