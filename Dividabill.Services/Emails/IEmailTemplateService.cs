﻿using DividaBill.DAL.Unity;
using DividaBill.Models.Emails;
using DividaBill.Services.Abstraction;

namespace DividaBill.Services.Emails
{
    public interface IEmailTemplateService : IGetIdentityService<EmailTemplate>, IInsertService<EmailTemplate>, IUpdateService<EmailTemplate>, ISaveService
    {
        EmailTemplate GetByTemplateId(string templateId, AsyncExecutionParam asyncExecutionParam = null);
        EmailTemplate Renew(int emailTemplateId, EmailTemplate newTemplate, AsyncExecutionParam asyncExecutionParam = null);
    }
}