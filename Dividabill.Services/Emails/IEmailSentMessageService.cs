﻿using DividaBill.Models.Emails;
using DividaBill.Services.Abstraction;

namespace DividaBill.Services.Emails
{
    public interface IEmailSentMessageService : IInsertService<EmailSentMessage>, IUpdateService<EmailSentMessage>, IGetService<EmailSentMessage>, ISaveService
    {
    }
}