using System;
using DividaBill.Models.Errors;
using Microsoft.Practices.Unity;

namespace DividaBill.Services.Emails.Exceptions
{
    public static class ExceptionEmail
    {
        public static void SendEmail(IUnityContainer container, ExceptionInfo exceptionInfo)
        {
            var emailService = container.Resolve<IEmailService>();

            try
            {
                emailService.SendEmailForException(exceptionInfo);
            }
            catch (Exception)
            {
                // ignored
            }
        }
    }
}