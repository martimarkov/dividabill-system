using DividaBill.AppConstants;
using DividaBill.Library.AppConfiguration;
using DividaBill.Services.Emails.Models;

namespace DividaBill.Services.AppConfiguration.Impl
{
    public class EmailConfigurationReader : IEmailConfigurationReader
    {
        private readonly IAppConfigurationReader _appConfigurationReader;

        public EmailConfigurationReader(IAppConfigurationReader appConfigurationReader)
        {
            _appConfigurationReader = appConfigurationReader;
        }

        public EmailConfiguration GetEmailConfiguration(EmailEnum emailEnum)
        {
            var keyPrefix = $"{emailEnum}:";

            return new EmailConfiguration
            {
                TemplateId = _appConfigurationReader.GetFromAppConfig($"{keyPrefix}TemplateId"),
                SendFromEmailDisplayName = _appConfigurationReader.GetFromAppConfig($"{keyPrefix}SentFromEmailDisplayName"),
                SendFromEmail = _appConfigurationReader.GetFromAppConfig($"{keyPrefix}SentFromEmail"),
                Subject = _appConfigurationReader.GetFromAppConfig($"{keyPrefix}Subject")
            };
        }
    }
}