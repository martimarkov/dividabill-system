﻿using System.Threading.Tasks;
using DividaBill.Models;
using DividaBill.Services.Interfaces;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;

namespace DividaBill.Services.Auth
{
    public class AuthService : IAuthService
    {
        private readonly IAuthenticationManager authManager;
        private readonly UserManager<User> userManager;

        public AuthService(IAuthenticationManager authManager, UserManager<User> userManager)
        {
            this.authManager = authManager;
            this.userManager = userManager;
        }

        public async Task SignInAsync(User user, bool isPersistent)
        {
            authManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);

            var claimsId = await user.GenerateUserIdentityAsync(userManager);
            authManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, claimsId);
        }
    }
}
