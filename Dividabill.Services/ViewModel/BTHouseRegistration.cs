﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DividaBill.Services.ViewModel
{
    class BTHouseRegistration
    {
        [DisplayName("Property Ref No")]
        public string PropertyRefNo { get; set; }
        public string StartDate { get; set; }
        public string FullAddress { get; set; }
        public string PostCode { get; set; }
        
        public string SiteContact { get; set; }
        public string SiteTelNo { get; set; }
        public string SiteMobile { get; set; }

        public string SiteContact2 { get; set; }
        public string SiteTelNo2 { get; set; }
        public string SiteMobile2 { get; set; }

        public string OrderContact { get; set; }
        public string OrderTelNo { get; set; }
        public string OrderEmail { get; set; }
        public string LineRequired { get; set; }
        public string BBTerm { get; set; }
        public string ExistingLineTelNo { get; set; }
        public string BBPackage { get; set; }
        public string IP { get; set; }
        public string IPTerm { get; set; }
        public string Router { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string ChallengeQuestion { get; set; }
        public string Answer { get; set; }
        public string OneBillRef { get; set; }
        public string OrderRef { get; set; }
        public string JobNo { get; set; }
        public string BBJobNo { get; set; }
        public string BTConfirmedStartDate { get; set; }
    }
}
