﻿using DividaBill.Models;
using DividaBill.Services.Sheets;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DividaBill.Library;

namespace DividaBill.Services.ViewModel
{
    public class SparkOnboarding : ICsvRow
    {
        #region Fields
        
        [DisplayName("Agreement Date")]
        public string AgreementDate { get { return ""; } }

        [DisplayName("Move-in Date")]
        public string MoveInDate { get; set; }

        public string Salutation { get; set; }

        [DisplayName("First Name")]
        public string FirstName { get; set; }

        [DisplayName("Last Name")]
        public string LastName { get; set; }

        [DisplayName("Mailing House Name")]
        public string MailingHouseName { get { return "DividaBill"; } }

        [DisplayName("Mailing House Number")]
        public string MailingHouseNumber { get { return "35"; } }

        [DisplayName("Mailing Address 1")]
        public string MailingAddress1 { get { return "Kingsland Road"; } }

        [DisplayName("Mailing Address 2")]
        public string MailingAddress2 { get { return ""; } }

        [DisplayName("Mailing Address 3")]
        public string MailingAddress3 { get { return ""; } }

        [DisplayName("Mailing Address 4")]
        public string MailingAddress4 { get { return "London"; } }

        [DisplayName("Mailing Postcode")]
        public string MailingPostcode { get { return "E2 8AA"; } }

        [DisplayName("Supply House Name")]
        public string SupplyHouseName { get; set; }

        [DisplayName("Supply House Number")]
        public string SupplyHouseNumber { get; set; }

        [DisplayName("Supply Address 1")]
        public string SupplyAddress1 { get; set; }

        [DisplayName("Supply Address 2")]
        public string SupplyAddress2 { get; set; }

        [DisplayName("Supply Address 3")]
        public string SupplyAddress3 { get; set; }

        [DisplayName("Supply Address 4")]
        public string SupplyAddress4 { get; set; }

        [DisplayName("Supply Postcode")]
        public string SupplyPostcode { get; set; }

        [DisplayName("Home Phone")]
        public string HomePhone { get; set; }

        [DisplayName("Mobile Phone")]
        public string MobilePhone { get; set; }

        [DisplayName("Email Address")]
        public string EmailAddress { get; set; }

        public string Bank { get { return ""; } }

        [DisplayName("Bank Sort Code")]
        public string BankSortCode { get { return ""; } }

        [DisplayName("Bank Account No.")]
        public string BankAccountNo { get { return ""; } }

        [DisplayName("Bank Account Name")]
        public string BankAccountName { get { return ""; } }

        [DisplayName("Type of House")]
        public string TypeOfHouse { get { return ""; } }

        [DisplayName("No Of Bedrooms")]
        public string NoOfBedrooms { get { return ""; } }

        public string Broadband { get { return ""; } }

        public string Electricity { get; set; }

        public string Gas { get; set; }

        [DisplayName("MPRN MSN")]
        public string MprnMsn { get { return ""; } }

        [DisplayName("MPAN MSN")]
        public string MpanMsn { get { return ""; } }

        [DisplayName("MPAN 2 MSN")]
        public string Mpan2Msn { get { return ""; } }

        public string MPRN { get; set; }

        public string MPAN { get; set; }

        [DisplayName("MPAN 2")]
        public string Mpan2 { get { return ""; } }

        public string AQ { get { return ""; } }

        [DisplayName("MPAN EAC")]
        public string MpanEac { get { return ""; } }

        public string Partner { get { return ""; } }

        [DisplayName("Partner Property Ref")]
        public string PartnerPropertyRef { get { return ""; } }

        [DisplayName("Partner Portfolio Ref")]
        public string PartnerPortfolioRef { get { return ""; } }

        [DisplayName("Partner Branch Ref")]
        public string PartnerBranchRef { get { return ""; } }

        [DisplayName("House ID")]
        public int HouseId { get; set; }

        [DisplayName("Supply Elec ID")]
        public string SupplyElecId { get { return ""; } }

        [DisplayName("Supply Gas ID")]
        public string SupplyGasId { get { return ""; } }

        [DisplayName("Account ID")]
        public string AccountId { get { return ""; } }

        [DisplayName("DD Monthly Amount")]
        public string DdMonthlyAmount { get { return ""; } }

        [DisplayName("Preferred DD Date")]
        public string PreferredDdDate { get { return ""; } }

        [DisplayName("Special Needs (Electricity)")]
        public string SpecialNeedsElectricity { get { return ""; } }

        public string Password { get { return ""; } }

        [DisplayName("Priority Services")]
        public string PriorityServices { get { return ""; } }

        [DisplayName("Access Instruction")]
        public string AccessInstruction { get { return ""; } }

        [DisplayName("Special Needs (Gas)")]
        public string SpecialNeedsGas { get { return ""; } }

        [DisplayName("Preferred Method of Contact")]
        public string PreferredMethodOfContact { get { return ""; } }

        [DisplayName("Title (Previous Tenant)")]
        public string TitlePreviousTenant { get { return ""; } }

        [DisplayName("First Name (Previous Tenant)")]
        public string FirstNamePreviousTenant { get { return ""; } }

        [DisplayName("Last Name (Previous Tenant)")]
        public string LastNamePreviousTenant { get { return ""; } }

        [DisplayName("Email (Previous Tenant)")]
        public string EmailPreviousTenant { get { return ""; } }

        [DisplayName("Forwarding Address")]
        public string ForwardingAddress { get { return ""; } }

        [DisplayName("Salutation Secondary Person")]
        public string SalutationSecondaryPerson { get; set; }

        [DisplayName("First Name Secondary Person")]
        public string FirstNameSecondaryPerson { get; set; }

        [DisplayName("Last Name Secondary Person")]
        public string LastNameSecondaryPerson { get; set; }

        [DisplayName("Email Address Secondary Person")]
        public string EmailAddressSecondaryPerson { get; set; }

        [DisplayName("Mobile Phone Secondary Person")]
        public string MobilePhoneSecondaryPerson { get; set; }

        [DisplayName("Total Tenants")]
        public string TotalTenants { get; set; }

        #endregion

        public static SparkOnboarding Create(ElectricityContract elecContract, GasContract gasContract)
        {
            var spark = new SparkOnboarding();
            spark.LoadFromDb(new Contract[] { elecContract, gasContract });
            return spark;
        }

        public bool LoadFromDb(IEnumerable<Contract> contracts)
        {
            contracts = contracts.Where(c => c != null);

            var elecContract = contracts.SingleOrDefault(c => c.ServiceNew == ServiceType.Electricity);
            var gasContract = contracts.SingleOrDefault(c => c.ServiceNew == ServiceType.Gas);

            if (elecContract == null && gasContract == null)
                return false;

            //house
            var house = (elecContract?.House) ?? (gasContract?.House);
            if (house == null) throw new Exception("No house provided...");

            //tenant
            var mainTenant = house.Tenants.FirstOrDefault();
            if (mainTenant == null) throw new Exception("House has no tenants!");

            //other
            var reqStartDate = new[] { elecContract?.RequestedStartDate, gasContract?.RequestedStartDate }
                .Where(d => d.HasValue) //earlier of the two contracts
                .Min().Value;
            var address = house.Address;

            //building
            var building = house.Building;
            if (building == null)
                throw new Exception("House {0} has no building (UDPRN = '{1}') !".F(house.ID, house.Address.UDPRN));
            

            //set fields
            MoveInDate = reqStartDate.Date.ToShortDateString();
            HouseId = house.ID;
            TotalTenants = house.Housemates.ToString();

            Salutation = mainTenant.RefTitle;
            FirstName = mainTenant.FirstName;
            LastName = mainTenant.LastName;

            SupplyHouseName = building.BuildingName;
            SupplyHouseNumber = building.BuildingNumber;
            SupplyAddress1 = address.Line1;
            SupplyAddress2 = (address.Line2 + ", " + address.Line3 + ", " + address.Line4).Trim(new[] { ',', ' ' });
            SupplyAddress3 = address.City;
            SupplyAddress4 = address.County;
            SupplyPostcode = address.Postcode;

            HomePhone = "";
            MobilePhone = mainTenant.PhoneNumber;
            EmailAddress = mainTenant.Email;

            Electricity = (elecContract != null).ToString();
            Gas = (gasContract != null).ToString();

            MPAN = "";          //TODO: fill these up from somewhere unknown
            MPRN = "";

            if (house.Tenants.Count > 1)
            {
                var tenant = house.Tenants[1];

                SalutationSecondaryPerson = tenant.RefTitle;
                FirstNameSecondaryPerson = tenant.FirstName;
                LastNameSecondaryPerson = tenant.LastName;
                EmailAddressSecondaryPerson = tenant.Email;
                MobilePhoneSecondaryPerson = tenant.PhoneNumber;
            }

            return true;
        }

        public void SaveToDb(HouseModel h)
        {
            //throw new NotImplementedException();
        }
    }
}
