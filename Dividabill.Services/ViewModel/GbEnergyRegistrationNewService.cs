using System;
using System.Linq;
using DividaBill.DAL;
using DividaBill.Models;
using DividaBill.Services.Impl;
using DividaBill.Services.NewBillingSystem.Impl;

namespace DividaBill.Services.ViewModel
{
    public class GbEnergyRegistrationNewService : IGbEnergyRegistrationNewService
    {
        public GBEnergyRegistrationNew Create(ElectricityContract elecContract, GasContract gasContract, DateTime sendDate)
        {
            var hardCodedEstimatedUtilityPricesService = new HardCodedEstimatedUtilityPricesService();
            var priceService = new PriceService(new UnitOfWork(), new CalculateEstimatesPricesService(hardCodedEstimatedUtilityPricesService), hardCodedEstimatedUtilityPricesService);
            if (!elecContract.House.Tenants.Any())
                throw new Exception("No tenants in this house!");

            var house = elecContract.House;
            if (gasContract != null && gasContract.House != house)
                throw new Exception("Contracts have a different house!");

            var address = house.Address;
            var tenant = house.Tenants.First();
            var dual = (gasContract != null) ? "Y" : "N";

            return new GBEnergyRegistrationNew
            {
                ProviderUniqueReferenceNumber = house.ID.ToString(),
                ApplicationDate = sendDate.Date.ToShortDateString(),

                Title = tenant.RefTitle,
                ForenameInitial = tenant.FirstName,
                Surname = tenant.LastName,

                SupplyHouseNoName = "",
                SupplyThoroughfare = address.Line1,
                SupplyPostTown = address.Line2,
                SupplyCounty = address.Line3,
                SupplyPostcode = address.Postcode,

                ElecPersonalProjection = priceService.EstimateElectricityUsage(house).ToString(),
                GasPersonalProjection = priceService.EstimateGasUsage(house).ToString(),

                DualFuel = dual,

                Udprn = address.UDPRN,
            };
        }
    }
}