﻿using DividaBill.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DividaBill.Services.ViewModel
{
    class NetflixOnboarding
    {
        [DisplayName("Netflix Package")]
        public String NetflixPackage { get; set; }

        [DisplayName("Email Address")]
        public String EmailAddress { get; set; }

        [DisplayName("Password")]
        public String Password { get; set; }

        [DisplayName("Payment Type")]
        public String PaymentType { get; set; }

        [DisplayName("Paypal Login")]
        public String PaypalLogin { get; set; }

        [DisplayName("Paypal password")]
        public String PaypalPassword { get; set; }

        [DisplayName("Select Plan")]
        public String Plan { get; set; }

        [DisplayName("Select All Devices")]
        public String AllDevices { get; set; }

        [DisplayName("User Name 1")]
        public String UserName1 { get; set; }

        [DisplayName("User Name 2")]
        public String UserName2 { get; set; }

        [DisplayName("User Name 3")]
        public String UserName3 { get; set; }

        [DisplayName("User Name 4")]
        public String UserName4 { get; set; }

        [DisplayName("TV programmes for you")]
        public String TVProgrammes { get; set; }
    }
}
