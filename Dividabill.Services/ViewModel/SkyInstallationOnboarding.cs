﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DividaBill.Models;

namespace DividaBill.Services.ViewModel
{
    class SkyInstallationOnboarding
    {
        public int HouseID { get; set; }

        public string AccountStartDate { get; set; }

        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string AddressLine4 { get; set; }
        public string AddressCity { get; set; }
        public string AddressCounty { get; set; }
        public string AddressPostcode { get; set; }


        public string LeadTenantName { get; set; }
        public string LeadTenantContactNumber { get; set; }
        public string LeadTenantEmail { get; set; }

        public string SecondaryTenantName { get; set; }
        public string SecondaryTenantContactNumber { get; set; }
        public string SecondaryTenantEmail { get; set; }


        public string OrderIssuedBy { get; private set; }
        public string OrderIssuedContactNumber { get; private set; }
        public string OrderIssuedEmail { get; private set; }


        public SkyInstallationOnboarding()
        {
            OrderIssuedBy = @"DividaBill";
            OrderIssuedContactNumber = @"2380223596";
            OrderIssuedEmail = @"admin@dividabill.co.uk";
        }

        internal static SkyInstallationOnboarding Create(Contract c)
        {
            var house = c.House;
            if (!house.Tenants.Any() || c.ServiceNew != ServiceType.SkyTV)
                return null;

            var leadTenant = house.Tenants[0];

            var record =  new SkyInstallationOnboarding
            {
                HouseID = house.ID,
                AccountStartDate = house.StartDate.ToShortDateString(),

                AddressLine1 = house.Address.Line1,
                AddressLine2 = house.Address.Line2,
                AddressLine3 = house.Address.Line3,
                AddressLine4 = house.Address.Line4,
                AddressCity = house.Address.City,
                AddressCounty = house.Address.County,
                AddressPostcode = house.Address.Postcode,

                LeadTenantName = leadTenant.FullName,
                LeadTenantContactNumber = leadTenant.PhoneNumber,
                LeadTenantEmail = leadTenant.Email,
            };

            if(house.Tenants.Count > 1)
            {
                var tenant = house.Tenants[1];
                record.SecondaryTenantName = tenant.FullName;
                record.SecondaryTenantContactNumber = tenant.PhoneNumber;
                record.SecondaryTenantEmail = tenant.Email;
            }

            return record;
        }
    }
}
