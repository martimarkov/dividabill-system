﻿using DividaBill.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DividaBill.Services.ViewModel
{
    class WaterServicesCotForm
    {

        public string CorrespondenceName { get { return "Soleus Limited trading as DividaBill "; } }
        public string CorrespondenceEmail { get { return "water@dividaBill.co.uk"; } }
        public string CorrespondenceAddress { get { return "35 Kingsland Road, DividaBill, London, E2 8AA"; } }
        public string CorrespondenceNumber { get { return "02380223596"; } }

        public string SendBillsTo { get { return "Please send bills to correspondence email address as primary, and our postal address as secondary"; } }
        public string LOA { get { return "Digital Confirmation as part of our terms and conditions. Please see our website at www.dividabill.co.uk"; } }
        public string PaymentMethod { get { return "MonthlyBACS"; } }


        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string AddressLine4 { get; set; }

        public string Town { get; set; }
        public string County { get; set; }
        public string PostCode { get; set; }


        public string MeterNumberIfKnown { get; set; }
        public string MeterReadingIfKnown { get; set; }


        public string Tenant1Name { get; set; }
        public string Tenant1ContactNumber { get; set; }
        public string Tenant1Email { get; set; }

        public string Tenant2Name { get; set; }
        public string Tenant2ContactNumber { get; set; }
        public string Tenant2Email { get; set; }

        public string Tenant3Name { get; set; }
        public string Tenant3ContactNumber { get; set; }
        public string Tenant3Email { get; set; }

        public string Tenant4Name { get; set; }
        public string Tenant4ContactNumber { get; set; }
        public string Tenant4Email { get; set; }

        public string TotalTenantsInHousehold { get; set; }


        public string DividaBillCorrespondenceStartDate { get; set; }


        public static WaterServicesCotForm Create(Contract c)
        {
            if (c == null) throw new ArgumentNullException(nameof(c));

            var house = c.House;
            if (house.Tenants?.Count == 0)
                return null;

            var tenants = house.Tenants;
            var actualTenantsCount = tenants.Count;

            var form = new WaterServicesCotForm
            {
                AddressLine1 = house.Address.Line1,
                AddressLine2 = house.Address.Line2,
                AddressLine3 = house.Address.Line3,
                AddressLine4 = house.Address.Line4,

                Town = house.Address.City,
                County = house.Address.County,
                PostCode = house.Address.Postcode,


                Tenant1Name = tenants[0].FullName,
                Tenant1ContactNumber = tenants[0].PhoneNumber,
                Tenant1Email = tenants[0].Email,

                TotalTenantsInHousehold = house.Housemates.ToString(),
                DividaBillCorrespondenceStartDate = c.RequestedStartDate.Date.ToShortDateString(),
            };

            //fill in additional tenants
            if (actualTenantsCount > 1)
            {
                var t = tenants[1];
                form.Tenant2Name = t.FullName;
                form.Tenant2ContactNumber = t.PhoneNumber;
                form.Tenant2Email = t.Email;
            }

            if (actualTenantsCount > 2)
            {
                var t = tenants[2];
                form.Tenant3Name = t.FullName;
                form.Tenant3ContactNumber = t.PhoneNumber;
                form.Tenant3Email = t.Email;
            }

            if (actualTenantsCount > 3)
            {
                var t = tenants[3];
                form.Tenant4Name = t.FullName;
                form.Tenant4ContactNumber = t.PhoneNumber;
                form.Tenant4Email = t.Email;
            }

            return form;
        }
    }
}
