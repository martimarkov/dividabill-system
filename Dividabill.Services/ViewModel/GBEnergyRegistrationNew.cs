﻿using DividaBill.Models;
using System;
using System.ComponentModel;

namespace DividaBill.Services.ViewModel
{
    public interface IGbEnergyRegistrationNewService
    {
        GBEnergyRegistrationNew Create(ElectricityContract elecContract, GasContract gasContract, DateTime sendDate);
    }

    public class GBEnergyRegistrationNew
    {
        private const string SortCode = "20-79-29";
        private const string AccountNumber = "60496154";
        private const string AccountName = "Soleus Limited";

        [DisplayName("Provider Unique reference number")]
        public string ProviderUniqueReferenceNumber { get; set; }

        [DisplayName("Application Date")]
        public string ApplicationDate { get; set; }

        public string Title { get; set; }

        [DisplayName("Forename / Initial")]
        public string ForenameInitial { get; set; }

        public string Surname { get; set; }

        [DisplayName("Supply House no/name")]
        public string SupplyHouseNoName { get; set; }

        [DisplayName("Supply Thoroughfare")]
        public string SupplyThoroughfare { get; set; }

        [DisplayName("Supply District")]
        public string SupplyDistrict { get { return ""; } }

        [DisplayName("Supply Post Town")]
        public string SupplyPostTown { get; set; }

        [DisplayName("Supply County")]
        public string SupplyCounty { get; set; }

        [DisplayName("Supply Postcode")]
        public string SupplyPostcode { get; set; }

        [DisplayName("Supply Residential Time")]
        public string SupplyResidentialTime { get { return ""; } }

        [DisplayName("Billing House no/name")]
        public string BillingHouseNoName { get { return "35"; } }

        [DisplayName("Billing Thoroughfare")]
        public string BillingThoroughfare { get { return "Kingsland Road"; } }

        [DisplayName("Billing District")]
        public string BillingDistrict { get { return "DividaBill"; } }

        [DisplayName("Billing Post Town")]
        public string BillingPostTown { get { return "Hackney"; } }

        [DisplayName("Billing County")]
        public string BillingCounty { get { return "London"; } }

        [DisplayName("Billing Postcode")]
        public string BillingPostcode { get { return "E2 8AA"; } }

        [DisplayName("Billing Residential Time")]
        public string BillingResidentialTime { get { return ""; } }

        [DisplayName("Previous 1 House no/name")]
        public string Previous1HouseNoName { get { return ""; } }

        [DisplayName("Previous 1 Thoroughfare")]
        public string Previous1Thoroughfare { get { return ""; } }

        [DisplayName("Previous 1 District")]
        public string Previous1District { get { return ""; } }

        [DisplayName("Previous 1 Post Town")]
        public string Previous1PostTown { get { return ""; } }

        [DisplayName("Previous 1 County")]
        public string Previous1County { get { return ""; } }

        [DisplayName("Previous 1 Postcode")]
        public string Previous1Postcode { get { return ""; } }

        [DisplayName("Previous 1 Residential Time")]
        public string Previous1ResidentialTime { get { return ""; } }

        [DisplayName("Previous 2 House no/name")]
        public string Previous2HouseNoName { get { return ""; } }

        [DisplayName("Previous 2 Thoroughfare")]
        public string Previous2Thoroughfare { get { return ""; } }

        [DisplayName("Previous 2 District")]
        public string Previous2District { get { return ""; } }

        [DisplayName("Previous 2 Post Town")]
        public string Previous2PostTown { get { return ""; } }

        [DisplayName("Previous 2 County")]
        public string Previous2County { get { return ""; } }

        [DisplayName("Previous 2 Postcode")]
        public string Previous2Postcode { get { return ""; } }

        [DisplayName("Previous 2 Residential Time")]
        public string Previous2ResidentialTime { get { return ""; } }

        [DisplayName("Daytime Landline")]
        public string DaytimeLandline { get { return "2380223596"; } }

        [DisplayName("Evening Landline")]
        public string EveningLandline { get { return ""; } }

        [DisplayName("Phone number Mobile")]
        public string PhoneNumberMobile { get { return "7769203585"; } }

        [DisplayName("Email Address")]
        public string EmailAddress { get { return "energy@dividabill.co.uk"; } }

        [DisplayName("Date of Birth")]
        public string DateOfBirth { get { return "01/01/1970"; } }

        [DisplayName("Residential Status")]
        public string ResidentialStatus { get { return ""; } }

        [DisplayName("Employment Status")]
        public string EmploymentStatus { get { return "Full time"; } }

        public string Password { get { return ""; } }

        [DisplayName("Priority Register Flag")]
        public string PriorityRegisterFlag { get { return "N"; } }

        [DisplayName("Special Needs detail")]
        public string SpecialNeedsDetail { get { return ""; } }

        [DisplayName("Dual Fuel")]
        public string DualFuel { get; set; }

        [DisplayName("Manage Account Online flag")]
        public string ManageAccountOnlineFlag { get { return "Y"; } }

        [DisplayName("application type")]
        public string ApplicationType { get { return "acquisition"; } }

        [DisplayName("Marketing Preference Flag")]
        public string MarketingPreferenceFlag { get { return "N"; } }

        [DisplayName("Green Deal flag")]
        public string GreenDealFlag { get { return "N"; } }

        [DisplayName("Direct debit day of month")]
        public string DirectDebitDayOfMonth { get { return "28"; } }

        [DisplayName("Bank Sort Code")]
        public string BankSortCode { get { return SortCode; } }

        [DisplayName("Bank Account Number")]
        public string BankAccountNumber { get { return AccountNumber; } }

        [DisplayName("Bank Account Name")]
        public string BankAccountName { get { return AccountName; } }

        [DisplayName("MPAN")]
        public string Mpan { get { return ""; } }

        [DisplayName("Elec Previous annual usage KWH")]
        public string ElecPreviousAnnualUsageKwh { get { return ""; } }

        [DisplayName("Elec Type of previous annual usage")]
        public string ElecTypeOfPreviousAnnualUsage { get { return ""; } }

        [DisplayName("Elec Meter Type Flag")]
        public string ElecMeterTypeFlag { get { return "Non Smart Standard Credit"; } }

        [DisplayName("Elec Smart accepts flag")]
        public string ElecSmartAcceptsFlag { get { return "N"; } }

        [DisplayName("Elec Smart PPM accepts flag")]
        public string ElecSmartPpmAcceptsFlag { get { return "N"; } }

        [DisplayName("Elec Current Pay Method")]
        public string ElecCurrentPayMethod { get { return ""; } }

        [DisplayName("Elec Current Supplier")]
        public string ElecCurrentSupplier { get { return ""; } }

        [DisplayName("Elec Current Tariff")]
        public string ElecCurrentTariff { get { return ""; } }

        [DisplayName("Elec Current Annual Bill")]
        public string ElecCurrentAnnualBill { get { return ""; } }

        [DisplayName("Elec Proposed Pay Method")]
        public string ElecProposedPayMethod { get { return "Invoice"; } }

        [DisplayName("Elec Proposed Tariff name")]
        public string ElecProposedTariffName { get { return "Premium Energy Saver"; } }

        [DisplayName("Elec Personal Projection")]
        public string ElecPersonalProjection { get; set; }

        [DisplayName("Elec Proposed Saving")]
        public string ElecProposedSaving { get { return ""; } }

        [DisplayName("MPRN")]
        public string Mprn { get { return ""; } }

        [DisplayName("Gas Previous annual usage KWH")]
        public string GasPreviousAnnualUsageKwh { get { return ""; } }

        [DisplayName("Gas Type of previous annual usage")]
        public string GasTypeOfPreviousAnnualUsage { get { return ""; } }

        [DisplayName("Gas Meter Type Flag")]
        public string GasMeterTypeFlag { get { return ""; } }

        [DisplayName("Gas Smart accepts flag")]
        public string GasSmartAcceptsFlag { get { return ""; } }

        [DisplayName("Gas Smart PPM accepts flag")]
        public string GasSmartPpmAcceptsFlag { get { return ""; } }

        [DisplayName("Gas Current Pay Method")]
        public string GasCurrentPayMethod { get { return ""; } }

        [DisplayName("Gas Current Supplier")]
        public string GasCurrentSupplier { get { return ""; } }

        [DisplayName("Gas Current Tariff")]
        public string GasCurrentTariff { get { return ""; } }

        [DisplayName("Gas Current Annual Bill")]
        public string GasCurrentAnnualBill { get { return ""; } }

        [DisplayName("Gas Proposed Pay Method")]
        public string GasProposedPayMethod { get { return "Invoice"; } }

        [DisplayName("Gas Proposed Tariff name")]
        public string GasProposedTariffName { get { return "Premium Energy Saver"; } }

        [DisplayName("Gas Personal Projection")]
        public string GasPersonalProjection { get; set; }

        [DisplayName("Gas Proposed Saving")]
        public string GasProposedSaving { get { return ""; } }

        [DisplayName("opt in daytime landline")]
        public string OptInDaytimeLandline { get { return "N"; } }

        [DisplayName("opt in evening landline")]
        public string OptInEveningLandline { get { return "N"; } }

        [DisplayName("opt in mobile")]
        public string OptInMobile { get { return "N"; } }

        [DisplayName("opt in text")]
        public string OptInText { get { return "N"; } }

        [DisplayName("opt in mail")]
        public string OptInMail { get { return "N"; } }

        [DisplayName("opt in email")]
        public string OptInEmail { get { return "N"; } }

        public string Source { get { return ""; } }

        public string Channel { get { return "outbound"; } }

        public string Campaign { get { return ""; } }

        [DisplayName("Brand")]
        public string Brand { get { return "GB Energy Supply"; } }

        [DisplayName("UDPRN")]
        public string Udprn { get; set; }
    }
}
