using DividaBill.Services.ThirdPartyServices.Interfaces;

namespace DividaBill.Services.ThirdPartyServices.Configurations
{
    public class IdealPostCodesConfig : IThirdPartyConfig, IThirdPartyMethodsSortOrderConfig
    {
        public string Version { get; set; }
        public string ApiKey { get; set; }
        public string BaseUrl { get; set; }

        public ThirdPartyExecutionSortOrder SortOrder { get; set; }
    }
}