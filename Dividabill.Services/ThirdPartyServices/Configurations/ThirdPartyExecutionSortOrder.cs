namespace DividaBill.Services.ThirdPartyServices.Configurations
{
    public class ThirdPartyExecutionSortOrder
    {
        public int PremisesByPostCode { get; set; }
        public int BuildingByPremise { get; set; }
        public int ValidateBankAccountDetails { get; set; }
    }
}