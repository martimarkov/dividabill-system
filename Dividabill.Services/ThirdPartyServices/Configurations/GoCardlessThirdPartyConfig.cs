using DividaBill.Services.ThirdPartyServices.Interfaces;

namespace DividaBill.Services.ThirdPartyServices.Configurations
{
    public class GoCardlessThirdPartyConfig : IThirdPartyMethodsSortOrderConfig
    {
        public ThirdPartyExecutionSortOrder SortOrder { get; set; }
    }
}