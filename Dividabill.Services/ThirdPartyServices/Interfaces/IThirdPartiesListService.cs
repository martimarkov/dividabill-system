﻿using System.Collections.Generic;

namespace DividaBill.Services.ThirdPartyServices.Interfaces
{
    public interface IThirdPartiesListService
    {
        List<IThirdPartyService> Services { get; }
    }
}