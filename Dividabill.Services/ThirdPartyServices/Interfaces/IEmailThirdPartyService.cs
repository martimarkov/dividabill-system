using System.Threading.Tasks;
using DividaBill.DAL.Unity;
using DividaBill.Models.Emails;
using DividaBill.Services.Emails.Models;
using SendGrid.API.Models;

namespace DividaBill.Services.ThirdPartyServices.Interfaces
{
    public interface IEmailThirdPartyService
    {
        Task<SendGridTemplate> GetTemplateFromThirdParty(string templateId);
        EmailTemplate CheckEmailTemplate(SendGridTemplate templateFromThirdParty, AsyncExecutionParam asyncExecutionParam = null);

        Task SaveAndSendEmail(string userId, EmailMessageToSend emailMessageToSend, AsyncExecutionParam asyncExecutionParam = null);

        Task<bool> SendMessage(EmailMessageToSend emailMessageToSend);
        Task SendAndUpdate(EmailMessageToSend emailMessageToSend, EmailSentMessage sentMessage, AsyncExecutionParam asyncExecutionParam = null);
    }
}