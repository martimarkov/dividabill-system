using DividaBill.Services.ThirdPartyServices.Models.DbIp;

namespace DividaBill.Services.ThirdPartyServices.Interfaces
{
    public interface IDbIpService
    {
        IpAddressInfo GetIpAddressInfo(string ip);
    }
}