using DividaBill.Services.ThirdPartyServices.Configurations;

namespace DividaBill.Services.ThirdPartyServices.Interfaces
{
    public interface IThirdPartyMethodsSortOrderConfig
    {
        ThirdPartyExecutionSortOrder SortOrder { get; set; }
    }
}