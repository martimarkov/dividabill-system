namespace DividaBill.Services.ThirdPartyServices.Interfaces
{
    public interface IThirdPartyConfig
    {
        string ApiKey { get; set; }
        string BaseUrl { get; set; }
    }
}