﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DividaBill.Models;
using DividaBill.Services.ThirdPartyServices.Configurations;
using DividaBill.ViewModels;

namespace DividaBill.Services.ThirdPartyServices.Interfaces
{
    public interface IThirdPartyService
    {
        ThirdPartyExecutionSortOrder SortOrder { get; }
        List<Premise> GetPremisesByPostcode(string postCode);
        List<Building> GetBuildingsByPremise(double premise);
        Task<List<UIError>> ValidateBankAccountDetails(BankAccountDetails bankAccountDetails);
    }
}