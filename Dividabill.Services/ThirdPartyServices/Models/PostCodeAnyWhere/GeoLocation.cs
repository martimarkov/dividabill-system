using DividaBill.ViewModels.Abstractions;

namespace DividaBill.Services.ThirdPartyServices.Models.PostCodeAnyWhere
{
    public class GeoLocation : IThirdPartyModel
    {
        public string Postcode { get; set; }
        public int Easting { get; set; }
        public int Northing { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public string OsGrid { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string CountyCode { get; set; }
        public string CountyName { get; set; }
        public string DistrictCode { get; set; }
        public string DistrictName { get; set; }
        public string WardCode { get; set; }
        public string WardName { get; set; }
        public string NhsShaCode { get; set; }
        public string NhsShaName { get; set; }
        public string NhsPctCode { get; set; }
        public string NhsPctName { get; set; }
        public string LeaCode { get; set; }
        public string LeaName { get; set; }
        public string GovernmentOfficeCode { get; set; }
        public string GovernmentOfficeName { get; set; }
        public string WestminsterConstituencyCode { get; set; }
        public string WestminsterConstituencyName { get; set; }
        public string WestminsterMP { get; set; }
        public string WestminsterParty { get; set; }
        public int Distance { get; set; }
        public string WestminsterConstituencyCode2010 { get; set; }
        public string WestminsterConstituencyName2010 { get; set; }
    }
}