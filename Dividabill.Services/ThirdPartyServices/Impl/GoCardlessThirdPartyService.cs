using System.Collections.Generic;
using System.Threading.Tasks;
using DividaBill.Models;
using DividaBill.Services.Impl;
using DividaBill.Services.Interfaces;
using DividaBill.Services.ThirdPartyServices.Configurations;
using DividaBill.Services.ThirdPartyServices.Interfaces;
using DividaBill.ViewModels;

namespace DividaBill.Services.ThirdPartyServices.Impl
{
    public class GoCardlessThirdPartyService : IThirdPartyService
    {
        private readonly GoCardlessThirdPartyConfig _config;
        private readonly IGoCardlessBankAccountDetailsService _goCardlessBankAccountDetailsService;

        public GoCardlessThirdPartyService(GoCardlessThirdPartyConfig config, IGoCardlessBankAccountDetailsService goCardlessBankAccountDetailsService)
        {
            _config = config;
            _goCardlessBankAccountDetailsService = goCardlessBankAccountDetailsService;
        }

        public List<Premise> GetPremisesByPostcode(string postCode)
        {
            return null;
        }

        public List<Building> GetBuildingsByPremise(double premise)
        {
            return null;
        }

        public async Task<List<UIError>> ValidateBankAccountDetails(BankAccountDetails bankAccountDetails)
        {
            return await _goCardlessBankAccountDetailsService.ValidateBankAccountDetails(bankAccountDetails);
        }

        public ThirdPartyExecutionSortOrder SortOrder => _config.SortOrder ?? new ThirdPartyExecutionSortOrder();
    }
}