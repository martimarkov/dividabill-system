using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using DividaBill.Models;
using DividaBill.Services.ThirdPartyServices.Configurations;
using DividaBill.Services.ThirdPartyServices.Interfaces;
using DividaBill.ViewModels;

namespace DividaBill.Services.ThirdPartyServices.Impl
{
    public class IdealPostCodesService : IThirdPartyService
    {
        private readonly IdealPostCodesConfig _config;
        private readonly IThirdPartyReader _thirdPartyReader;

        public IdealPostCodesService(IdealPostCodesConfig config, IThirdPartyReader thirdPartyReader)
        {
            _thirdPartyReader = thirdPartyReader;
            _config = config;
        }

        public List<Premise> GetPremisesByPostcode(string postCode)
        {
            var url = GetUrl($"postcodes/{postCode}");

            var data = _thirdPartyReader.ReadAndParseSafety(url);
            if (data == null || (int) data["code"] != (int) Statuses.Success)
            {
                return null;
            }

            return data["result"].Select(item => new Premise
            {
                PAId = (double) item["udprn"],
                StreetAddress = $"{(string) item["line_1"]} " +
                                $"{(string) item["line_2"]} " +
                                $"{(string) item["line_3"]}",
                Place = $"{(string) item["post_town"]} " +
                        $"{(string) item["postcode_outward"]}",
                Postcode = (string) item["postcode"]
            }).ToList();
        }

        public List<Building> GetBuildingsByPremise(double premise)
        {
            var url = GetUrl($"addresses/{HttpUtility.UrlEncode(premise.ToString())}");

            var data = _thirdPartyReader.ReadAndParseSafety(url);
            if (data == null || (int) data["code"] != (int) Statuses.Success)
            {
                return null;
            }

            var item = data["result"];
            return new List<Building>
            {
                new Building
                {
                    PostCode = (string) item["postcode"],
                    Udprn = (double) item["udprn"],
                    County = (string) item["county"],
                    PostTown = (string) item["post_town"],
                    Line1 = (string) item["line_1"],
                    Line2 = (string) item["line_2"],
                    Line3 = (string) item["line_3"],
                    //Line4 = (string) item[""],
                    Company = (string) item["organisation_name"],
                    Department = (string) item["department_name"],
                    Line5 = (string) item["dependant_locality"],
                    //Mailsort = (int)item[""],
                    //Barcode = (string)item[""],
                    Type = (string) item["postcode_type"],
                    DeliveryPointSuffix = (string) item["delivery_point_suffix"],
                    SubBuilding = (string) item["sub_building_name"],
                    BuildingName = (string) item["building_name"],
                    BuildingNumber = (string) item["building_number"],
                    PrimaryStreet = (string) item["thoroughfare"],
                    SecondaryStreet = (string) item["dependant_thoroughfare"],
                    DoubleDependentLocality = (string) item["double_dependant_locality"],
                    DependentLocality = (string) item["dependant_locality"],
                    PoBox = (string) item["po_box"],
                    PrimaryStreetName = (string) item["thoroughfare"],
                    //PrimaryStreetType = (string)item[""],
                    SecondaryStreetName = (string) item["dependant_thoroughfare"],
                    //SecondaryStreetType = (string)item[""],
                    CountryName = (string) item["country"]
                    //CountryISO2 = (string)item[""],
                    //CountryISO3 = (string)item[""],
                }
            };
        }

        public Task<List<UIError>> ValidateBankAccountDetails(BankAccountDetails bankAccountDetails)
        {
            throw new NotImplementedException();
        }

        public ThirdPartyExecutionSortOrder SortOrder => _config.SortOrder ?? new ThirdPartyExecutionSortOrder();

        private string GetUrl(string subUrl)
        {
            return $"{_config.BaseUrl}/{_config.Version}/{subUrl}?api_key={_config.ApiKey}";
        }

        private enum Statuses
        {
            Success = 2000,
            PostcodeNotFound = 4040,
            NoLookupsRemaining = 4020,
            DailyOrIndividualLookupLimitBreached = 4021
        }
    }
}