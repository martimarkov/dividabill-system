﻿using System.Linq;
using DividaBill.DAL.Unity;
using DividaBill.Models;
using DividaBill.Models.GoCardless;
using DividaBill.Services.Abstraction;

namespace DividaBill.Services.Interfaces
{
    public interface IBankMandateService : IGetIdentityService2<BankMandate>, IUpdateService<BankMandate>, IInsertService<BankMandate>
    {
        BankMandate InsertAndSaveBankMandateFromGcMandate(User tenant, GCMandate mand, AsyncExecutionParam asyncExecutionParam = null);
        BankMandate UpdateBankMandateFromGcMandate(string mandateId, GCMandate mand, AsyncExecutionParam asyncExecutionParam = null);
        IQueryable<BankMandate> GetAllMandatesIDsForUser(User tenant, AsyncExecutionParam asyncExecutionParam = null);
        User LinkOrUnlinkBankMandateToUser(User tenant, BankMandate bankMandate, AsyncExecutionParam asyncExecutionParam = null);
    }
}