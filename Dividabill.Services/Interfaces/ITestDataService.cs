﻿using DividaBill.Models;
using DividaBill.ViewModels;

namespace DividaBill.Services.Interfaces
{
    public interface ITestDataService
    {
        bool ValidateBankAccountDetailsForTestData(BankAccountDetails bankAccountDetails);
        bool ValidateBankAccountDetailsForTestData(User tenant);
        bool ValidateBankAccountDetailsForTestData(string accountNumber, string accountSortCode);
        bool IsTestBankAccountNumber(string accountNumber);
        bool IsTestBankAccountSortCode(string accountSortCode);
    }
}