﻿using DividaBill.Models;
using System;
using System.Linq;
using DividaBill.Models.Bills;

namespace DividaBill.Services.Interfaces
{
    public interface IBillService
    {
        IQueryable<HouseholdBill> GetHouseholdBills();

        IQueryable<HouseholdBill> GetHouseholdBillsForMonth(DateTime month);
        HouseholdBill GetHouseholdBill(int id);
        IQueryable<int> GetHouseholdBillsAllYearsList();
    }
}
