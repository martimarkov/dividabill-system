﻿using System;
using System.Collections.Generic;
using System.Linq;
using DividaBill.DAL.Interfaces;
using DividaBill.Models;
using DividaBill.Models.Bills;
using DividaBill.ViewModels;
using ExcelLibrary.SpreadSheet;

namespace DividaBill.Services.Interfaces
{
    public interface IHouseService
    {
        HouseModel AddHouseIfNotExists(HouseDetailsViewModel wizard);

        void AddTenantToHouse(HouseModel house, User tenant);
        void ArchiveHouse(HouseModel house);

        Workbook GenerateBritishGasRegistrationForm(int houseId);
        string GenerateGBEnergyRegistrationsNew(IEnumerable<HouseModel> houses);
        string GenerateBTRegistrationForm(int houseId);
        string GenerateBTRegistrationFormForHouses(List<HouseModel> houses);
        IQueryable<HouseModel> GetYesterdayHouses();
        HouseDetailsViewModel GetHouseDetails(HouseSearchViewModel houseSearchParameters);
        HouseDetailsViewModel GetHouseDetails(int houseSearchParameters);
        List<User> GetTenantsForHouse(int id);

        void MergeHouses(int[] houses);

        IEnumerable<Contract> GetContractsForHouse(int p);

        List<HouseholdBill> GetHouseholdBills(HouseModel house);

        IQueryable<HouseModel> GetTodayHouses();
        IQueryable<HouseModel> GetLastWeekHouses();
        IQueryable<HouseModel> GetActiveHouses(IUnitOfWork unitOfWork = null);
        IQueryable<HouseModel> GetAllHouses();
        HouseModel GetHouse(int id);
        HouseModel GetHouseIncluding(int id, string includeProperties);
        void UpdateHouse(HouseModel house);
        void ArchiveHouse(int house_ID);
        void UnarchiveHouse(HouseModel house);
        void UnarchiveHouse(int house_ID);
        void RemoveTenantFromHouse(string userId, int house_ID);
        void MarkHouseActive(HouseModel house);
        IQueryable<HouseModel> FilterActiveHouses(IQueryable<HouseModel> houses, DateTime when);
    }
}