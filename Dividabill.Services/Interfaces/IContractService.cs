﻿using DividaBill.Models;
using DividaBill.ViewModels;

namespace DividaBill.Services.Interfaces
{
    public interface IContractService
    {
        Contract GetContract(int id);
        Provider GetDefaultProvider(HouseModel house, ServiceType ty);
        void CreateSignUpContracts(SignUpViewModel viewModel);
        Contract Create(ContractRequest cr);
        void AddContract(ContractRequest contractRequest);
        void EditContract(Contract contract);
    }
}