﻿using System.Collections.Generic;
using DividaBill.Models;

namespace DividaBill.Services.Interfaces
{
    public interface ITenancyService
    {
        List<Tenancy> GetTenanciesbyUserId(string userId);
        List<Tenancy> GetTenanciesbyHouseId(int houseId);
        Tenancy GetTenancy(int tenancyId);
        bool TenancyExists(int tenancyId);
        Tenancy CreateTenancy(Tenancy tenancy);
        Tenancy UpdateTenancy(Tenancy tenancy);
    }
}