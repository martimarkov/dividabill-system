﻿using System.Collections.Generic;
using System.Linq;
using DividaBill.Models;

namespace DividaBill.Services.Interfaces
{
    public interface IPremiseService
    {
        Building GetBuildingsForPremise(double premise);
        List<Building> GetBuildingsForPremiseFromThirdParty(double premise);

        IQueryable<Premise> GetPremisesForPostcode(string postcode);
        List<Premise> GetPremisesForPostcodeFromThirdParty(string postcode);
    }
}