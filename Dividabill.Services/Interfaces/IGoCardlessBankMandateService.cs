﻿using System.Threading.Tasks;
using DividaBill.DAL.Interfaces;
using DividaBill.DAL.Unity;
using DividaBill.Models;
using DividaBill.Models.GoCardless;
using GoCardless_API.api;

namespace DividaBill.Services.Interfaces
{
    public interface IGoCardlessBankMandateService
    {
        Task<BankMandate> CreateAndInsertBankMandateFromGc(User tenant, IGoCardlessApiClient goCardlessApi = null, AsyncExecutionParam asyncExecutionParam = null);
        Task<GCMandate> GetExistingOrCreateGcMandate(User tenant, IGoCardlessApiClient goCardlessApi = null);
        BankMandate UpdateOrInsertBankMandateFromGcMandate(User tenant, GCMandate mandate, AsyncExecutionParam asyncExecutionParam = null);
        Task<GCMandate> CreateGcMandate(string bankAccountId, IGoCardlessApiClient goCardlessApi = null);
        Task UpdateAllExistingBankMandatesFromGc(User tenant, AsyncExecutionParam asyncExecutionParam = null, IGoCardlessApiClient goCardlessApi = null);
        Task<BankMandate> SyncBankMandateAsync(User tenant, IUnitOfWork unitOfWork = null, IGoCardlessApiClient goCardlessApi = null);
    }
}