using System.Collections.Generic;
using System.Threading.Tasks;
using DividaBill.Models;
using DividaBill.Services.Responses;
using DividaBill.ViewModels;

namespace DividaBill.Services.Interfaces
{
    public interface IBankAccountRegistrationService
    {
        Task<RegisterResult> RegisterAccount(IUserManager userManager, SignUpViewModel signUpViewModel);
        Task<List<UIError>> ValidateBankAccountDetails(BankAccountDetails bankAccountDetails);
    }
}