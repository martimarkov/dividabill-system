using System.Threading.Tasks;
using DividaBill.ViewModels;

namespace DividaBill.Services.Interfaces
{
    public interface IUserRegistrationService
    {
        Task<SignUpViewModel> ProcessUserRegistration(SignUpViewModel userViewModel);
    }
}