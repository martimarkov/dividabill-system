﻿using DividaBill.DAL.Unity;
using DividaBill.Models;
using DividaBill.Models.GoCardless;
using DividaBill.Services.Abstraction;

namespace DividaBill.Services.Interfaces
{
    public interface IAccountService : IGetIdentityService2<BankAccount>, IInsertService<BankAccount>, IUpdateService<BankAccount>
    {
        User LinkOrUnlinkBankAccountToUser(User tenant, BankAccount bankAccount, AsyncExecutionParam asyncExecutionParam = null);
        BankAccount InsertBankAccountFromGcAccount(User tenant, GCBankAccount gcBankAccount, AsyncExecutionParam asyncExecutionParam = null);
        BankAccount UpdateBankAccountFromGcAccount(string bankAccountId, GCBankAccount gcBankAccount, AsyncExecutionParam asyncExecutionParam = null);
    }
}