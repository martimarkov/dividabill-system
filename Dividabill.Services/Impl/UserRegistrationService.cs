﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DividaBill.DAL.Interfaces;
using DividaBill.Services.Auth;
using DividaBill.Services.Interfaces;
using DividaBill.ViewModels;

namespace DividaBill.Services.Impl
{
    internal class UserRegistrationService : IUserRegistrationService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ApplicationUserManager _userManager;
        private readonly IBankAccountRegistrationService _bankAccountRegistrationService;

        public UserRegistrationService(IUnitOfWork unitOfWork, ApplicationUserManager userManager,
            IBankAccountRegistrationService bankAccountRegistrationService)
        {
            _bankAccountRegistrationService = bankAccountRegistrationService;
            _unitOfWork = unitOfWork;
            _userManager = userManager;
        }

        public async Task<SignUpViewModel> ProcessUserRegistration(SignUpViewModel userViewModel)
        {
            //register the account
            var registerResult = await _bankAccountRegistrationService.RegisterAccount(_userManager, userViewModel);

            if (registerResult.Errors.Any())
            {
                userViewModel.ValidationErrors = new Dictionary<string, string>();
                foreach (var uiError in registerResult.Errors)
                {
                    if (!userViewModel.ValidationErrors.ContainsKey(uiError.Key))
                    {
                        userViewModel.ValidationErrors.Add(uiError.Key, uiError.ErrorMessage);
                    }
                }
            }
            return userViewModel;
        }

        public bool AccountHouseExists(SignUpViewModel wizard)
        {
            var existingHouse = _unitOfWork.ActiveHouses
                .Get(h => h.Address.UDPRN == wizard.Udprn, null, "Tenants")
                .SingleOrDefault();

            return existingHouse != null;
        }
    }
}