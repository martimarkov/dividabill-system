﻿using System.Linq;
using DividaBill.DAL.Interfaces;
using DividaBill.Models;
using DividaBill.Services.Exceptions;
using DividaBill.Services.Interfaces;

namespace DividaBill.Services.Impl
{
    public class CouponService : ICouponService
    {
        private readonly IUnitOfWork _unitOfWork;

        public CouponService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Coupon Get(string code)
        {
            var coupon = _unitOfWork.CouponRepository.Get().AsEnumerable().FirstOrDefault(x => CleanStringForCompare(x.Code) == CleanStringForCompare(code));
            if (coupon == null)
                throw new CouponNotFound(code);
            return coupon;
        }

        private string CleanStringForCompare(string value)
        {
            if (string.IsNullOrEmpty(value)) return value;
            value = value.Trim();
            value = value.ToLower();
            return value;
        }
    }
}