﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DividaBill.DAL.Interfaces;
using DividaBill.Models;
using DividaBill.Services.Abstraction;
using DividaBill.Services.Exceptions;
using DividaBill.Services.Interfaces;
using DividaBill.ViewModels;

namespace DividaBill.Services.Impl
{
    public class ContractService : IContractService
    {
        // give a servicetype
        // get a Request -> Contract function
        private static readonly Dictionary<ServiceType, Func<ContractRequest, Contract>> ContractPrototypes = new Dictionary<ServiceType, Func<ContractRequest, Contract>>
        {
            {ServiceType.Broadband, req => new BroadbandContract(req)},
            {ServiceType.Electricity, req => new ElectricityContract(req)},
            {ServiceType.Gas, req => new GasContract(req)},
            {ServiceType.LandlinePhone, req => new LandlinePhoneContract(req)},
            {ServiceType.LineRental, req => new LineRentalContract(req)},
            {ServiceType.NetflixTV, req => new NetflixTVContract(req)},
            {ServiceType.SkyTV, req => new SkyTVContract(req)},
            {ServiceType.TVLicense, req => new TVLicenseContract(req)},
            {ServiceType.Water, req => new WaterContract(req)}
        };

        private static readonly Dictionary<ServiceType, Func<SignUpViewModel, bool>> ContractServiceGetter = new Dictionary<ServiceType, Func<SignUpViewModel, bool>>
        {
            {ServiceType.Broadband, m => m.Broadband},
            {ServiceType.Electricity, m => m.Electricity},
            {ServiceType.Gas, m => m.Gas},
            {ServiceType.LandlinePhone, m => m.LandlinePhone},
            {ServiceType.LineRental, m => m.LandlinePhone || m.Broadband},
            {ServiceType.NetflixTV, m => m.NetflixTV},
            {ServiceType.SkyTV, m => m.SkyTV},
            {ServiceType.TVLicense, m => m.TVLicense},
            {ServiceType.Water, m => m.Water}
        };

        private static readonly Dictionary<ServiceType, Func<SignUpViewModel, int>> ContractPackageGetter = new Dictionary<ServiceType, Func<SignUpViewModel, int>>
        {
            {ServiceType.Broadband, m => (int) m.BroadbandType},
            {ServiceType.Electricity, m => 0},
            {ServiceType.Gas, m => 0},
            {ServiceType.LandlinePhone, m => (int) m.LandlinePhoneType},
            {ServiceType.LineRental, m => 0},
            {ServiceType.NetflixTV, m => 0},
            {ServiceType.SkyTV, m => 0},
            {ServiceType.TVLicense, m => 0},
            {ServiceType.Water, m => 0}
        };

        /// <summary>
        ///     Allows insertion of contracts in their respective repository.
        /// </summary>
        private static readonly Dictionary<ServiceType, Action<IUnitOfWork, Contract>> ContractInsertGuy = new Dictionary<ServiceType, Action<IUnitOfWork, Contract>>
        {
            {ServiceType.Broadband, (u, c) => u.BroadbandContractRepository.Insert((BroadbandContract) c)},
            {ServiceType.Electricity, (u, c) => u.ElectricityContractRepository.Insert((ElectricityContract) c)},
            {ServiceType.Gas, (u, c) => u.GasContractRepository.Insert((GasContract) c)},
            {ServiceType.LandlinePhone, (u, c) => u.LandlinePhoneContractRepository.Insert((LandlinePhoneContract) c)},
            {ServiceType.LineRental, (u, c) => u.LineRentalContractRepository.Insert((LineRentalContract) c)},
            {ServiceType.NetflixTV, (u, c) => u.NetflixTVContractRepository.Insert((NetflixTVContract) c)},
            {ServiceType.SkyTV, (u, c) => u.SkyTVContractRepository.Insert((SkyTVContract) c)},
            {ServiceType.TVLicense, (u, c) => u.TVLicenseContractRepository.Insert((TVLicenseContract) c)},
            {ServiceType.Water, (u, c) => u.WaterContractRepository.Insert((WaterContract) c)}
        };

        private readonly IProviderService _providerService;

        private readonly IUnitOfWork _unitOfWork;

        public ContractService(IUnitOfWork unitOfWork, IProviderService providerService)
        {
            _providerService = providerService;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        ///     Creates all service contracts as requested by the provided sign-up view model
        ///     and then saves them to the database.
        /// </summary>
        public void CreateSignUpContracts(SignUpViewModel viewModel)
        {
            foreach (var ty in ServiceType.All)
            {
                var req = CreateContractRequestFromSignUp(viewModel, ty);

                //was that actually requested
                if (req == null)
                    continue;

                var c = Create(req);
            }
            _unitOfWork.Save();
        }

        /// <summary>
        ///     Creates a <see cref="Contract" /> as specified by the given <see cref="ContractRequest" />
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public Contract Create(ContractRequest req)
        {
            if (req.Provider == null)
                req.Provider = _providerService.GetProvidersByServiceTypeId(req.Service.Id).First();

            var c = ContractPrototypes[req.Service](req);
            ContractInsertGuy[req.Service](_unitOfWork, c);
            return c;
        }

        /// <summary>
        ///     Gets the default provider for the given house and service.
        ///     TODO: Currently returns the first provider for the given service type.
        /// </summary>
        public Provider GetDefaultProvider(HouseModel house, ServiceType ty)
        {
            return _unitOfWork.ProviderRepository.Get(p => p.Type.Any(t => t.Id == ty.Id)).FirstOrDefault();
        }


        public Contract GetContract(int id)
        {
            var contract = _unitOfWork.ContractRepository.Get().Include("ServiceNew").Single(c => c.ID == id);
            if (contract == null)
            {
                throw new ContractNotFound();
            }
            return contract;
        }

        public void EditContract(Contract contract)
        {
            GetContract(contract.ID);

            _unitOfWork.ContractRepository.Update(contract);

            _unitOfWork.Save();
        }

        public void AddContract(ContractRequest contractRequest)
        {
            Create(contractRequest);

            _unitOfWork.Save();
        }

        /// <summary>
        ///     Creates a service request from the given sign-up view model.
        ///     Returns null if the view model does not include this service.
        /// </summary>
        private ContractRequest CreateContractRequestFromSignUp(SignUpViewModel viewModel, ServiceType ty)
        {
            //was this service checked?
            if (!ContractServiceGetter[ty](viewModel))
                return null;

            var user = _unitOfWork.UserRepository.GetByEmail(viewModel.Email);
            var provider = GetDefaultProvider(user.House, ty);

            var req = new ContractRequest
            {
                Duration = (ContractLength) viewModel.Period,
                Provider = provider,
                TriggeringTenant = user,
                RequestedStartDate = viewModel.StartTime,
                Service = ty,
                PackageRaw = ContractPackageGetter[ty](viewModel)
            };
            return req;
        }
    }
}