using DividaBill.AppConstants;
using DividaBill.Library.AppConfiguration;
using DividaBill.Models;
using DividaBill.Services.Interfaces;
using DividaBill.ViewModels;

namespace DividaBill.Services.Impl
{
    public class TestDataService : ITestDataService
    {
        private readonly IAppConfigurationReader _appConfigurationReader;

        public TestDataService(IAppConfigurationReader appConfigurationReader)
        {
            _appConfigurationReader = appConfigurationReader;
        }

        public bool ValidateBankAccountDetailsForTestData(BankAccountDetails bankAccountDetails)
        {
            return ValidateBankAccountDetailsForTestData(bankAccountDetails.AccountNumber, bankAccountDetails.AccountSortCode);
        }

        public bool ValidateBankAccountDetailsForTestData(User tenant)
        {
            return ValidateBankAccountDetailsForTestData(tenant.AccountNumber, tenant.AccountSortCode);
        }

        public bool ValidateBankAccountDetailsForTestData(string accountNumber, string accountSortCode)
        {
            var replace = accountSortCode.Replace("-", string.Empty);
            return accountNumber == GetTestBankAccountNumber()
                   && replace == GetTestBankAccountSortCode();
        }

        public bool IsTestBankAccountNumber(string accountNumber)
        {
            return accountNumber == GetTestBankAccountNumber()
                   || accountNumber == "00000000";
        }

        public bool IsTestBankAccountSortCode(string accountSortCode)
        {
            var replace = accountSortCode.Replace("-", string.Empty);
            return replace == GetTestBankAccountSortCode()
                   || replace == "000000";
        }

        private string GetTestBankAccountNumber()
        {
            return _appConfigurationReader.GetFromAppConfig(AppSettingsEnum.TestBankAccountNumber);
        }

        private string GetTestBankAccountSortCode()
        {
            return _appConfigurationReader.GetFromAppConfig(AppSettingsEnum.TestBankAccountSortCode);
        }
    }
}