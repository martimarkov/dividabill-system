﻿using System.Collections.Generic;
using System.Linq;
using DividaBill.DAL.Interfaces;
using DividaBill.DAL.Unity;
using DividaBill.Models;
using DividaBill.Services.Abstraction;
using DividaBill.Services.Exceptions;
using DividaBill.Services.Interfaces;

namespace DividaBill.Services.Impl
{
    public class TenantService : ITenantService
    {
        private readonly ITenantEncryptionService _tenantEncryptionService;
        private readonly IUnitOfWork _unitOfWork;

        public TenantService(IUnitOfWork unitOfWork, ITenantEncryptionService tenantEncryptionService)
        {
            _tenantEncryptionService = tenantEncryptionService;
            _unitOfWork = unitOfWork;
        }

        public IQueryable<User> GetAllTenants()
        {
            return _unitOfWork.UserRepository.Get();
        }

        public List<User> GetAllTenantsDecrypted()
        {
            var users = _unitOfWork.UserRepository.Get();
            _tenantEncryptionService.DecryptUsersList(users);
            return users.ToList();
        }

        public IEnumerable<User> GetAllTenantsNameDecrypted()
        {
            return _unitOfWork.UserRepository.Get().AsEnumerable().Select(u => _tenantEncryptionService.DecryptUserName(u));
        }

        //public IQueryable<TenantsViewModel> GetAllTenantsForDataTable()
        //{
        //    return GetAllTenants().AsEnumerable().Select(u => _userViewFactory.Create(u)).AsQueryable();
        //}

        //public IEnumerable<TenantsViewModel> GetAllTenantsForDataTableDecrypted()
        //{
        //    return GetAllTenantsNameDecrypted().AsEnumerable().Select(u => _userViewFactory.Create(u));
        //}

        //public IQueryable<TenantsViewModel> GetActiveTenantsForDataTableDecrypted()
        //{
        //    var users = GetActiveTenantNameDecrypted().AsQueryable();
        //    var data = users.ProjectTo<TenantsViewModel>();
        //    return data;
        //}

        public IQueryable<User> GetActiveTenants()
        {
            return _unitOfWork.UserRepository.Get();
        }


        public IEnumerable<User> GetActiveTenantsDecrypted()
        {
            var users = _unitOfWork.UserRepository.Get();
            return _tenantEncryptionService.DecryptUsersList(users);
        }

        public User GetTenant(string id)
        {
            var user = _unitOfWork.UserRepository.Get().SingleOrDefault(u => u.Id == id);
            if (user == null)
            {
                throw new UserNotFound();
            }
            return user;
        }

        public User GetTenantDecrypted(string id)
        {
            var user = _unitOfWork.UserRepository.Get().SingleOrDefault(u => u.Id == id);
            if (user == null)
            {
                throw new UserNotFound();
            }
            _tenantEncryptionService.DecryptUser(user);
            return user;
        }

        public User UpdateAndSave(User entityToUpdate, AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);
            var updatedEntity = Update(entityToUpdate, asyncExecutionParam);
            asyncExecutionParam.UnitOfWork.UserRepository.SaveChanges();
            return updatedEntity;
        }

        public User Update(User entityToUpdate, AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);

            _tenantEncryptionService.EncryptUser(entityToUpdate);
            return asyncExecutionParam.UnitOfWork.UserRepository.Update(entityToUpdate);
        }
    }
}