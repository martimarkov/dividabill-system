using DividaBill.Models;
using DividaBill.Security.Abstractions;
using DividaBill.Services.Abstraction;

namespace DividaBill.Services.Impl
{
    public class AddressEncryptionService : IAddressEncryptionService
    {
        private readonly IEncryptionService _encryptionService;

        public AddressEncryptionService(IEncryptionService encryptionService)
        {
            _encryptionService = encryptionService;
        }

        public void EncryptAddress(Address address)
        {
            address.Line1 = _encryptionService.Encrypt(address.Line1);
            address.Line2 = _encryptionService.Encrypt(address.Line2);
            address.Line3 = _encryptionService.Encrypt(address.Line3);
            address.Line4 = _encryptionService.Encrypt(address.Line4);
            address.Postcode = _encryptionService.Encrypt(address.Postcode);
            address.City = _encryptionService.Encrypt(address.City);
            address.County = _encryptionService.Encrypt(address.County);
        }

        public void DecryptAddress(Address address)
        {
            address.Line1 = _encryptionService.Decrypt(address.Line1);
            address.Line2 = _encryptionService.Decrypt(address.Line2);
            address.Line3 = _encryptionService.Decrypt(address.Line3);
            address.Line4 = _encryptionService.Decrypt(address.Line4);
            address.Postcode = _encryptionService.Decrypt(address.Postcode);
            address.City = _encryptionService.Decrypt(address.City);
            address.County = _encryptionService.Decrypt(address.County);
        }
    }
}
