using System.Threading.Tasks;
using DividaBill.DAL.Interfaces;
using DividaBill.DAL.Unity;
using DividaBill.Models;
using DividaBill.Models.GoCardless;
using DividaBill.Services.Interfaces;
using GoCardless_API.api;

namespace DividaBill.Services.Impl
{
    public class GoCardlessCustomerService : IGoCardlessCustomerService
    {
        private readonly ITenantService _tenantService;
        private readonly IGoCardlessApiClient _goCardlessApiClient;

        public GoCardlessCustomerService(IGoCardlessApiClient goCardlessApiClient, ITenantService tenantService)
        {
            _goCardlessApiClient = goCardlessApiClient;
            _tenantService = tenantService;
        }

        public async Task AddUserAsGoCardlessCustomerAsync(User tenant, AsyncExecutionParam asyncExecutionParam = null, IGoCardlessApiClient goCardlessApi = null)
        {
            goCardlessApi = AssignThreadSafeGoCardlessApi(goCardlessApi);
            if (string.IsNullOrEmpty(tenant.GoCardlessCustomerID))
            {
                
                var customer = new NewCustomerInfo(tenant);
                var createCustomer = await goCardlessApi.CreateCustomerAsync(customer);

                tenant.GoCardlessCustomerID = createCustomer.id;
                _tenantService.Update(tenant, asyncExecutionParam);
            }
        }

        private IGoCardlessApiClient AssignThreadSafeGoCardlessApi(IGoCardlessApiClient goCardlessApi)
        {
            return goCardlessApi ?? _goCardlessApiClient;
        }
    }
}