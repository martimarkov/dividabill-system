﻿using System.Collections.Generic;
using System.Linq;
using DividaBill.DAL.Interfaces;
using DividaBill.Models;
using DividaBill.Services.Interfaces;
using DividaBill.Services.ThirdPartyServices.Interfaces;

namespace DividaBill.Services.Impl
{
    public class PremiseService : IPremiseService
    {
        private readonly IUnitOfWork _repository;
        private readonly IThirdPartiesListService _thirdPartyServices;

        public PremiseService(IUnitOfWork unitofwork, IThirdPartiesListService thirdPartyServices)
        {
            _thirdPartyServices = thirdPartyServices;
            _repository = unitofwork;
        }

        public IQueryable<Premise> GetPremisesForPostcode(string postcode)
        {
            var premises = _repository.PremiseRepository.Get().Where(p => p.Postcode == postcode);
            if (!premises.Any())
            {
                var resultFromThirParty = GetPremisesForPostcodeFromThirdParty(postcode) ?? new List<Premise>();
                if (resultFromThirParty.Count > 0)
                {
                    foreach (var item in resultFromThirParty)
                    {
                        _repository.PremiseRepository.Insert(item);
                    }
                    _repository.Save();
                }
                premises = resultFromThirParty.AsQueryable();
            }
            return premises.OrderBy(p => p.Id);
        }

        public Building GetBuildingsForPremise(double premise)
        {
            var buildings = _repository.BuildingRepository.Get().Where(p => p.Udprn == premise);
            if (!buildings.Any())
            {
                var resultFromThirParty = GetBuildingsForPremiseFromThirdParty(premise) ?? new List<Building>();
                if (resultFromThirParty.Count > 0)
                {
                    foreach (var item in resultFromThirParty)
                    {
                        _repository.BuildingRepository.Insert(item);
                    }
                    _repository.Save();
                }
                buildings = resultFromThirParty.AsQueryable();
            }
            return buildings.SingleOrDefault();
        }

        public List<Premise> GetPremisesForPostcodeFromThirdParty(string postcode)
        {
            return _thirdPartyServices.Services.OrderBy(service => service.SortOrder.PremisesByPostCode).Select(thirdPartyService => thirdPartyService.GetPremisesByPostcode(postcode)).FirstOrDefault(premisesByPostcode => premisesByPostcode != null);
        }

        public List<Building> GetBuildingsForPremiseFromThirdParty(double premise)
        {
            return _thirdPartyServices.Services.OrderBy(service => service.SortOrder.BuildingByPremise).Select(thirdPartyService => thirdPartyService.GetBuildingsByPremise(premise)).FirstOrDefault(buildingsForPremise => buildingsForPremise != null);
        }
    }
}