using System.Collections.Generic;
using System.Threading.Tasks;
using DividaBill.DAL.Interfaces;
using DividaBill.Models;
using DividaBill.Services.NewBillingSystem.Impl;
using GoCardless_API;
using GoCardless_API.api;

namespace DividaBill.Services.Impl
{
    public interface IGoCardlessService
    {
        Task<BulkOperationResult<string>> AddBillingRunTenantsToGoCardlessAsync(int billingRunId, GoCardless.Environments environment);
        Task<List<UIError>> AddToGoCardlessAsync(User tenant, IUnitOfWork unitOfWork = null, IGoCardlessApiClient goCardlessApi = null);
    }
}