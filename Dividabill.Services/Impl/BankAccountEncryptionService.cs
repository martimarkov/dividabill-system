using DividaBill.Models;
using DividaBill.Security.Abstractions;
using DividaBill.Services.Abstraction;

namespace DividaBill.Services.Impl
{
    public class BankAccountEncryptionService : IBankAccountEncryptionService
    {
        private readonly IEncryptionService _encryptionService;

        public BankAccountEncryptionService(IEncryptionService encryptionService)
        {
            _encryptionService = encryptionService;
        }

        public void EncryptBankAccount(BankAccount accountHolder)
        {
            accountHolder.account_holder_name = _encryptionService.Encrypt(accountHolder.account_holder_name);
        }

        public void DecryptBankAccount(BankAccount accountHolder)
        {
            accountHolder.account_holder_name = _encryptionService.Decrypt(accountHolder.account_holder_name);
        }
    }
}