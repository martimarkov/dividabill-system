using System;
using System.Linq;
using DividaBill.DAL.Interfaces;
using DividaBill.DAL.Unity;
using DividaBill.Models;
using DividaBill.Models.GoCardless;
using DividaBill.Services.Exceptions;
using DividaBill.Services.Factories;
using DividaBill.Services.Interfaces;

namespace DividaBill.Services.Impl
{
    public class BankMandateService : IBankMandateService
    {
        private readonly IGcMandateToBankMandateMappingFactory _gcMandateToBankMandateMappingFactory;
        private readonly ITenantService _tenantService;
        private readonly IUnitOfWork _unitOfWork;

        public BankMandateService(IUnitOfWork unitOfWork, IGcMandateToBankMandateMappingFactory gcMandateToBankMandateMappingFactory,
            ITenantService tenantService)
        {
            _tenantService = tenantService;
            _gcMandateToBankMandateMappingFactory = gcMandateToBankMandateMappingFactory;
            _unitOfWork = unitOfWork;
        }

        public BankMandate GetById(string id, AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);
            var bankMandate = GetActive(asyncExecutionParam).FirstOrDefault(x => x.Id == id);
            if (bankMandate == null)
            {
                throw new BankMandateNotFound(id);
            }
            return bankMandate;
        }

        public IQueryable<BankMandate> GetActive(AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);
            return asyncExecutionParam.UnitOfWork.BankMandateRepository.Get();
        }

        public BankMandate UpdateAndSave(BankMandate entityToUpdate, AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);
            var updatedEntity = Update(entityToUpdate, asyncExecutionParam);
            asyncExecutionParam.UnitOfWork.BankMandateRepository.SaveChanges();
            return updatedEntity;
        }

        public BankMandate Update(BankMandate entityToUpdate, AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);
            entityToUpdate.LastModified = DateTime.UtcNow;
            return asyncExecutionParam.UnitOfWork.BankMandateRepository.Update(entityToUpdate);
        }

        public BankMandate InsertAndSave(BankMandate entityToInsert, AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);
            var newEntity = Insert(entityToInsert, asyncExecutionParam);
            asyncExecutionParam.UnitOfWork.BankMandateRepository.SaveChanges();
            return newEntity;
        }

        public BankMandate Insert(BankMandate entityToInsert, AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);
            entityToInsert.LastModified = DateTime.UtcNow;
            entityToInsert.CreatedAt = DateTime.UtcNow;
            return asyncExecutionParam.UnitOfWork.BankMandateRepository.Insert(entityToInsert);
        }

        public IQueryable<BankMandate> GetAllMandatesIDsForUser(User tenant, AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);

            return GetByUserId(tenant.Id, asyncExecutionParam).Union(
                GetActive(asyncExecutionParam).Where(x => x.BankAccount.Owner_ID == tenant.Id).Union(
                    GetByBankAccountId(tenant.BankAccountID, asyncExecutionParam)));
        }

        public BankMandate InsertAndSaveBankMandateFromGcMandate(User tenant, GCMandate mand, AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);

            var domainBankMandate = _gcMandateToBankMandateMappingFactory.Map(tenant, mand);
            domainBankMandate = InsertAndSave(domainBankMandate, asyncExecutionParam);
            return domainBankMandate;
        }

        public BankMandate UpdateBankMandateFromGcMandate(string mandateId, GCMandate mand, AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);

            var domainBankMandate = GetById(mandateId, asyncExecutionParam);
            domainBankMandate = _gcMandateToBankMandateMappingFactory.Map(mand, domainBankMandate);
            domainBankMandate = Update(domainBankMandate, asyncExecutionParam);
            return domainBankMandate;
        }

        public User LinkOrUnlinkBankMandateToUser(User tenant, BankMandate bankMandate, AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);

            tenant.MandateID = bankMandate?.Id;
            return _tenantService.Update(tenant, asyncExecutionParam);
        }

        private IQueryable<BankMandate> GetByUserId(string userId, AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);
            return GetActive(asyncExecutionParam).Where(x => x.User_ID == userId);
        }

        private IQueryable<BankMandate> GetByBankAccountId(string bankAccountId, AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);
            return GetActive(asyncExecutionParam).Where(x => x.BankAccount_ID == bankAccountId);
        }
    }
}