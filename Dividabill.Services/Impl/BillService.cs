﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DividaBill.DAL.Interfaces;
using DividaBill.Models;
using DividaBill.Models.Bills;
using DividaBill.Services.Exceptions;
using DividaBill.Services.Interfaces;

namespace DividaBill.Services.Impl
{
        [Obsolete]
    public class BillService : IBillService
    {
        private readonly IUnitOfWork _unitOfWork;

        private Dictionary<ServiceType, Func<UtilityBill>> billPrototypes = new Dictionary
            <ServiceType, Func<UtilityBill>>
        {
            {ServiceType.Gas, () => new GasBill()},
            {ServiceType.Electricity, () => new ElectricityBill()},
            {ServiceType.Broadband, () => new BroadbandBill()},
            {ServiceType.LandlinePhone, () => new LandlinePhoneBill()},
            {ServiceType.LineRental, () => new LineRentalBill()},
            {ServiceType.NetflixTV, () => new NetflixTVBill()},
            {ServiceType.SkyTV, () => new SkyTVBill()},
            {ServiceType.TVLicense, () => new TVLicenseBill()},
            {ServiceType.Water, () => new WaterBill()}
        };


        [Obsolete]
        public BillService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [Obsolete]
        public HouseholdBill GetHouseholdBill(int id)
        {
            try
            {
                var bill =
                    _unitOfWork.HouseholdBillRepository.Get()
                        .Where(b => b.ID == id)
                        .Include("UtilityBills")
                        .SingleOrDefault();
                if (bill != null)
                {
                    return bill;
                }
                    throw new Exception();
                }
            catch (Exception)
            {
                throw new BillNotFound {type = BillType.HouseholdBill};
            }
        }

        public IQueryable<HouseholdBill> GetHouseholdBillsForMonth(DateTime dateInMonth)
        {
            return GetHouseholdBills().Where(b => b.Month == dateInMonth.Month && b.Year == dateInMonth.Year);
        }

        public IQueryable<HouseholdBill> GetHouseholdBills()
        {
            return _unitOfWork.HouseholdBillRepository.Get();
        }
            
        public IQueryable<int> GetHouseholdBillsAllYearsList()
        {
            return _unitOfWork.HouseholdBillRepository.GroupBy(x => x.Year).Select(x => x.Key);
        }
    }
}
