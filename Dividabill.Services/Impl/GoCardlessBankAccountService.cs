using System.Linq;
using System.Threading.Tasks;
using DividaBill.DAL.Interfaces;
using DividaBill.DAL.Unity;
using DividaBill.Models;
using DividaBill.Models.GoCardless;
using DividaBill.Services.Interfaces;
using GoCardless_API.api;

namespace DividaBill.Services.Impl
{
    public class GoCardlessBankAccountService : IGoCardlessBankAccountService
    {
        private readonly IAccountService _bankAccountService;
        private readonly IGoCardlessApiClient _goCardlessApiClient;
        private readonly ITestDataService _testDataService;
        private readonly IUnitOfWork _unitOfWork;

        public GoCardlessBankAccountService(IUnitOfWork unitOfWork, IGoCardlessApiClient goCardlessApiClient, IAccountService bankAccountService,
            ITestDataService testDataService)
        {
            _testDataService = testDataService;
            _goCardlessApiClient = goCardlessApiClient;
            _unitOfWork = unitOfWork;
            _bankAccountService = bankAccountService;
        }

        private BankAccount UpdateOrInsertBankAccountFromGcBankAccount(User tenant, GCBankAccount bankAcc, AsyncExecutionParam asyncExecutionParam = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);

            var bankAccount = _bankAccountService.GetById(bankAcc.id, asyncExecutionParam);
            bankAccount = bankAccount == null
                ? _bankAccountService.InsertBankAccountFromGcAccount(tenant, bankAcc, asyncExecutionParam)
                : _bankAccountService.UpdateBankAccountFromGcAccount(bankAccount.Id, bankAcc, asyncExecutionParam);

            return bankAccount;
        }

        private async Task<GCBankAccount> GetExistingOrCreateGcBankAccount(User tenant, IGoCardlessApiClient goCardlessApi = null)
        {
            goCardlessApi = AssignThreadSafeGoCardlessApi(goCardlessApi);

            if (await ValidateForTestDataAndDisableBankAccount(tenant, goCardlessApi))
                return null;

            GCBankAccount gcBankAccount = null;
            if (string.IsNullOrEmpty(tenant.BankAccountID))
            {
                gcBankAccount = await GetActiveOrCreateGcBankAccount(tenant, goCardlessApi);
            }
            else
            {
                gcBankAccount = await goCardlessApi.GetBankAccountAsync(tenant.BankAccountID);

                var existingBankAccount = await GetActiveOrCreateGcBankAccount(tenant, goCardlessApi);
                if (existingBankAccount.id != tenant.BankAccountID)
                {
                    gcBankAccount = await goCardlessApi.GetBankAccountAsync(existingBankAccount.id);
                }
            }
            return gcBankAccount;
        }

        private async Task<bool> ValidateForTestDataAndDisableBankAccount(User tenant, IGoCardlessApiClient goCardlessApi = null)
        {
            goCardlessApi = AssignThreadSafeGoCardlessApi(goCardlessApi);
            if (!_testDataService.ValidateBankAccountDetailsForTestData(tenant))
                return false;
            if (!string.IsNullOrEmpty(tenant.BankAccountID))
            {
                await goCardlessApi.DisableBankAccountAsync(tenant.BankAccountID);
            }
            return true;
        }

        private async Task<GCBankAccount> GetActiveOrCreateGcBankAccount(User tenant, IGoCardlessApiClient goCardlessApi)
        {
            var existingBankAccount = await GetActiveBankAccount(tenant, goCardlessApi);

            if (existingBankAccount == null)
            {
                return await CreateGcBankAccount(tenant, goCardlessApi);
            }
            return Map(tenant, existingBankAccount);
        }

        private async Task<GCExistingBankAccount> GetActiveBankAccount(User tenant, IGoCardlessApiClient goCardlessApi = null)
        {
            goCardlessApi = AssignThreadSafeGoCardlessApi(goCardlessApi);
            var customerBankAccounts = await goCardlessApi.GetBankAccountsAsync(tenant.GoCardlessCustomerID);

            GCExistingBankAccount existingBankAccount = null;
            if (!string.IsNullOrEmpty(tenant.AccountNumber))
            {
                existingBankAccount = customerBankAccounts.FirstOrDefault(
                    w =>
                        w.account_number_ending ==
                        tenant.AccountNumber.Substring(tenant.AccountNumber.Length - 2, 2) && w.enabled);
            }
            else
            {
                existingBankAccount = customerBankAccounts.FirstOrDefault(w => w.enabled);
            }
            return existingBankAccount;
        }

        private async Task<GCBankAccount> CreateGcBankAccount(User tenant, IGoCardlessApiClient goCardlessApi = null)
        {
            goCardlessApi = AssignThreadSafeGoCardlessApi(goCardlessApi);

            var bankAccount = new NewBankAccount
            {
                account_holder_name = tenant.AccountHolder.Substring(0, tenant.AccountHolder.Length > 17 ? 17 : tenant.AccountHolder.Length),
                account_number = tenant.AccountNumber,
                country_code = "GB",
                currency = "GBP",
                branch_code = tenant.AccountSortCode.Replace("-", "").Replace(" ", "")
            };
            bankAccount.links.Add("customer", tenant.GoCardlessCustomerID);
            return await goCardlessApi.CreateBankAccountAsync(bankAccount);
        }

        private GCBankAccount Map(User tenant, GCExistingBankAccount existingBankAccount)
        {
            var bankAcc = new GCBankAccount
            {
                id = existingBankAccount.id,
                links = existingBankAccount.links,
                created_at = existingBankAccount.created_at,
                enabled = existingBankAccount.enabled,
                account_number = tenant.AccountNumber,
                currency = existingBankAccount.currency,
                account_holder_name = existingBankAccount.account_holder_name,
                branch_code = tenant.AccountSortCode,
                country_code = existingBankAccount.country_code,
                metadata = existingBankAccount.metadata
            };
            return bankAcc;
        }

        private IGoCardlessApiClient AssignThreadSafeGoCardlessApi(IGoCardlessApiClient goCardlessApi)
        {
            return goCardlessApi ?? _goCardlessApiClient;
        }

        public async Task<BankAccount> SyncUserBankAccountAsync(User tenant, AsyncExecutionParam asyncExecutionParam = null, IGoCardlessApiClient goCardlessApi = null)
        {
            asyncExecutionParam = asyncExecutionParam.AssignThreadSafeUnitOfWork(_unitOfWork);
            goCardlessApi = AssignThreadSafeGoCardlessApi(goCardlessApi);

            var bankAcc = await GetExistingOrCreateGcBankAccount(tenant, goCardlessApi);
            if (bankAcc != null)
            {
                return UpdateOrInsertBankAccountFromGcBankAccount(tenant, bankAcc, asyncExecutionParam);
            }
            return null;
        }
    }
}