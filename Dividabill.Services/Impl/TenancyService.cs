using System.Collections.Generic;
using System.Linq;
using DividaBill.DAL.Interfaces;
using DividaBill.Models;
using DividaBill.Services.Exceptions;
using DividaBill.Services.Interfaces;

namespace DividaBill.Services.Impl
{
    public class TenancyService : ITenancyService
    {
        private readonly IUnitOfWork _unitOfWork;

        public TenancyService(IUnitOfWork unitOfWork
            )
        {
            _unitOfWork = unitOfWork;
        }

        public List<Tenancy> GetTenanciesbyUserId(string userId)
        {
            var tenancies = _unitOfWork.TenancyRepository.Get(t => t.UserId == userId).ToList();
            return tenancies;
        }

        public List<Tenancy> GetTenanciesbyHouseId(int houseId)
        {
            var tenancies = _unitOfWork.TenancyRepository.Get(t => t.HouseId == houseId).ToList();
            return tenancies;
        }

        public Tenancy GetTenancy(int tenancyId)
        {
            var tenancy = _unitOfWork.TenancyRepository.Get(t => t.Id == tenancyId);

            if (tenancy == null)
                throw new TenancyNotFoundException();

            return tenancy.Single();
        }

        public bool TenancyExists(int tenancyId)
        {
            var tenancy = GetTenancy(tenancyId);
            return tenancy != null;
        }

        public Tenancy CreateTenancy(Tenancy tenancy)
        {
            _unitOfWork.TenancyRepository.Insert(tenancy);
            _unitOfWork.TenancyRepository.SaveChanges();

            return tenancy;
        }

        public Tenancy UpdateTenancy(Tenancy tenancy)
        {
            if (tenancy == null)
                throw new TenancyNotFoundException();

            _unitOfWork.TenancyRepository.Update(tenancy);
            _unitOfWork.TenancyRepository.SaveChanges();

            return tenancy;
        }
    }
}