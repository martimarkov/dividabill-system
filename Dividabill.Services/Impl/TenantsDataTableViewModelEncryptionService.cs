using DividaBill.Security.Abstractions;
using DividaBill.Services.Abstraction;
using DividaBill.ViewModels.DataTables;

namespace DividaBill.Services.Impl
{
    public class TenantsDataTableViewModelEncryptionService : IViewModelEncryptionService<TenantsDataTableViewModel>
    {
        private readonly IEncryptionService _encryptionService;

        public TenantsDataTableViewModelEncryptionService(IEncryptionService encryptionService)
        {
            _encryptionService = encryptionService;
        }

        public TenantsDataTableViewModel DecryptViewModel(TenantsDataTableViewModel tenantsDataTable)
        {
            tenantsDataTable.FirstName = _encryptionService.Decrypt(tenantsDataTable.FirstName);
            tenantsDataTable.LastName = _encryptionService.Decrypt(tenantsDataTable.LastName);
            tenantsDataTable.PhoneNumber = _encryptionService.Decrypt(tenantsDataTable.PhoneNumber);
            tenantsDataTable.AccountNumber = _encryptionService.Decrypt(tenantsDataTable.AccountNumber);
            tenantsDataTable.AccountSortCode = _encryptionService.Decrypt(tenantsDataTable.AccountSortCode);

            return tenantsDataTable;
        }
    }
}