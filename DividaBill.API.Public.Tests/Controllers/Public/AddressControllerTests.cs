﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Results;
using AutoMapper;
using Dividabill.Tests.Core;
using DividaBill.API.Controllers.Public;
using DividaBill.Models;
using DividaBill.Services.Interfaces;
using DividaBill.ViewModels;
using Moq;
using NUnit.Framework;

namespace DividaBill.API.Tests.Controllers.Public
{
    [TestFixture]
    public class AddressControllerTests : BaseAutoMock<AddressController>
    {
        [Test]
        public void GivenAnyPostcodeAndRandomPremisesCount_WhenGetInvoked_ThenOkResultResponseAndGetPremisesForPostcodeInvokedOnceAndMappingInvokedExactlyTimes()
        {
            var postCode = "test postcode just for unit test";
            var premisesCount = new Random().Next(999);

            var listOfPremises = new List<Premise>();
            listOfPremises.AddRange(Enumerable.Repeat(new Premise(), premisesCount));
            var premiseService = GetMock<IPremiseService>();
            premiseService.Setup(x => x.GetPremisesForPostcode(It.IsAny<string>())).Returns(listOfPremises.AsQueryable());
            var mappingEngine = GetMock<IMappingEngine>();
            mappingEngine.Setup(x => x.Map<Premise, PremiseViewModel>(It.IsAny<Premise>())).Returns(new PremiseViewModel());

            var result = ClassUnderTest.Get(postCode);

            Assert.That(result, Is.TypeOf<OkNegotiatedContentResult<List<PremiseViewModel>>>());
            mappingEngine.Verify(x => x.Map<Premise, PremiseViewModel>(It.IsAny<Premise>()), Times.Exactly(premisesCount));
            premiseService.Verify(x => x.GetPremisesForPostcode(It.IsAny<string>()), Times.Once);
        }

        [Test]
        public void GivenInvalidModelState_WhenGetInvoked_ThenBadRequestResponse()
        {
            var postCode = "test postcode just for unit test";
            ClassUnderTest.ModelState.AddModelError("test error UnitTest", "Just some custom error for Unit test");

            var result = ClassUnderTest.Get(postCode);

            Assert.That(result, Is.TypeOf<InvalidModelStateResult>());
        }
    }
}