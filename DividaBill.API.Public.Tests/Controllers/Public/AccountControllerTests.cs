﻿using DividaBill.API.Controllers.Public;
using DividaBill.Models;
using DividaBill.Services.Interfaces;
using DividaBill.Services.Responses;
using DividaBill.ViewModels;
using Moq;
using NUnit.Framework;

namespace DividaBill.API.Tests.Controllers.Public
{
    [TestFixture]
    public class AccountControllerTests
    {
        [SetUp]
        public void SetUp()
        {
            _accountService = new Mock<IBankAccountRegistrationService>();
            _authService = new Mock<IAuthService>();
            _userManager = new Mock<IUserManager>();
            _classUnderTest = new AccountController(_accountService.Object, _userManager.Object, _authService.Object);
        }

        private Mock<IBankAccountRegistrationService> _accountService;
        private Mock<IAuthService> _authService;
        private Mock<IUserManager> _userManager;
        private AccountController _classUnderTest;

        [Test]
        public void GivenController_WhenCreateInvoked_ThenUserManagerGenerateEmailConfirmationTokenAsyncInvoked()
        {
            // arrange
            var signUpViewModel = new SignUpViewModel();
            var registerResult = new RegisterResult {User = new User {Id = "something"}};
            _accountService.Setup(x => x.RegisterAccount(It.IsAny<IUserManager>(), signUpViewModel)).ReturnsAsync(registerResult);

            // act
            var task = _classUnderTest.Create(signUpViewModel);
            task.Wait();
            var result = task.Result;

            // assert
            _userManager.Verify(x => x.GenerateEmailConfirmationTokenAsync(It.IsAny<string>()), Times.Once);
        }
    }
}