﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DividaBill;
using System.ComponentModel.DataAnnotations.Schema;

namespace DividaBill.Models.Sheets
{
    /// <summary>
    /// Represents a supplier's file in our Google Drive account. 
    /// </summary>
    [Table("Suppliers.Files")]
    public class File
    {
        /// <summary>
        /// The name of the file as seen in GDrive and GSheets. 
        /// </summary>
        [Key]
        public string FileName { get; private set; }


        /// <summary>
        /// Gets the provider this file was made for. 
        /// </summary>
        public virtual Provider Provider { get; private set; }

        /// <summary>
        /// Gets the column which defines a row's key. 
        /// </summary>
        public int KeyColumnId { get; private set; }

        public virtual ICollection<FileRevision> Revisions { get; private set; } = new List<FileRevision>();


        protected File() { }

        public File(Provider provider, string fileName, int keyColumn)
        {
            Provider = provider;
            FileName = fileName;
            KeyColumnId = keyColumn;
        }
    }
}
