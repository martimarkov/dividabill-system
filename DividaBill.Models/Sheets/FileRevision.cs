﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DividaBill.Models.Sheets
{
    [Table("Suppliers.FileRevisions")]
    public class FileRevision
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; private set; }

        /// <summary>
        /// Gets the date and time this revision was fetched. 
        /// </summary>
        public DateTime CreationDate { get; private set; }

        /// <summary>
        /// Gets whether the changes were done by ourselves. 
        /// </summary>
        public bool WasDoneByUs { get; private set; }

        /// <summary>
        /// Gets the <see cref="Sheets.File"/> object that was changed. 
        /// </summary>
        public Sheets.File ChangedFile { get; private set; }


        /// <summary>
        /// The CSV data we read from Google Drive. 
        /// </summary>
        public byte[] FileData { get; private set; }


        public virtual ICollection<RowChange> ChangedRows { get; private set; } = new List<RowChange>();

        private FileRevision() { }

        public FileRevision(File file, byte[] data, bool wasDoneByUs)
        {
            CreationDate = DateTime.Now;
            WasDoneByUs = wasDoneByUs;
            ChangedFile = file;
        }
    }
}
