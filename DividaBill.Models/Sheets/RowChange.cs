﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DividaBill.Models.Sheets
{
    [Table("Suppliers.FileRowChanges")]
    public class RowChange
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; private set; }

        /// <summary>
        /// Gets the file revision this change is part of. 
        /// </summary>
        public FileRevision Revision { get; private set; }

        /// <summary>
        /// Gets the Id of the object at this row. 
        /// </summary>
        public int RowId { get; private set; }

        /// <summary>
        /// Gets or sets the type of change this row went thru. 
        /// </summary>
        public RowChangeEvent ChangeType { get; private set; }


        public File ChangedFile
        {
            get { return Revision.ChangedFile; }
        }

        RowChange() { }

        public RowChange(FileRevision revision, int rowId, RowChangeEvent evType)
        {
            this.Revision = revision;
            this.RowId = rowId;
            this.ChangeType = evType;
        }
    }

    public enum RowChangeEvent : byte
    {
        Added, Removed, Edited
    }
}
