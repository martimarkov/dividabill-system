using System;
using System.Collections.Specialized;
using System.Reflection;

namespace DividaBill.Models.Errors
{
    public class ExceptionInfo
    {
        public ExceptionInfo(Exception exception, Assembly assembly)
        {
            Exception = exception;
            Assembly = assembly;
        }

        public Exception Exception { get; }
        public string Url { get; set; }
        public string HttpMethod { get; set; }
        public NameValueCollection Headers { get; set; }
        public string Form { get; set; }
        public string UserAgent { get; set; }
        public string UserHostAddress { get; set; }
        public string UserHostName { get; set; }

        public Assembly Assembly { get; set; }
    }
}