namespace DividaBill.Models
{
    public enum TransactionLogStatus
    {
        Information = 0,
        Warning = 1,
        Error = 2
    }
}