﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DividaBill.Models.Abstractions;

namespace DividaBill.Models
{
    public class Provider : IEntityModel
    {
        [Key]
        public int ID { get; set; }

        [Index("IX_ProviderName", 1, IsUnique = true)]
        [MaxLength(100)]
        public string Name { get; set; }

        public string PhoneNumber { get; set; }
        public string ContactPerson { get; set; }

        /// <summary>
        ///     Gets the average delay before a service is requested, and its actual starting date.
        /// </summary>
        public int AverageInstallationDelay { get; set; }

        public virtual ICollection<ServiceType> Type { get; set; }

        public virtual ICollection<Contract> Contracts { get; set; }
    }
}