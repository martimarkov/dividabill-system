using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using DividaBill.Models.Common;

namespace DividaBill.Models
{
    [ComplexType]
    public class Address : SimpleAddress
    {
        public DbGeography Position { get; set; }
    }
}