﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using DividaBill.Models.Bills;

namespace DividaBill.Models
{
    [Table("dbo.BillBases")]
    public class UtilityBill : IUtilityBill
    {
        #region IDs
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        //[Index("IX_UtilityBill_ContractHousehold", IsUnique = true, Order = 1)]
        public int? Contract_Id { get; set; }

        //[Index("IX_UtilityBill_ContractHousehold", IsUnique = true, Order = 2)]
        public int? HouseholdBill_Id { get; set; }
        #endregion

        #region TO REMOVE
        public int House_ID { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime StartDate { get; set; } = new DateTime(2000, 1, 1);

        #endregion

        [ForeignKey(nameof(Contract_Id))]
        public virtual Contract Contract { get; private set; }

        [ForeignKey(nameof(HouseholdBill_Id))]
        public virtual HouseholdBill HouseholdBill { get; private set; }

        public virtual ICollection<BillPayment> Payments { get; private set; } = new List<BillPayment>();

        [DataType(DataType.Currency)]
        public decimal Ammount { get; set; }

        public string Note { get; set; }

        /// <summary>
        /// Gets the date at which this bill is going to be debited. 
        /// </summary>
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime PaymentDate { get; private set; }


        public ServiceType Service
        {
            get { return Contract.ServiceNew; }
        }

        public HouseModel House
        {
            get { return HouseholdBill.House; }
        }
            
        /// <summary>
        /// Gets whether this bill is fully paid by all tenants. 
        /// </summary>
        public bool IsPaid
        {
            get { return Payments.All(p => p.IsPaid); }
        }

        /// <summary>
        /// Gets the date at which this bill starts ticking. 
        /// </summary>
        public DateTimeOffset FromDate
        {
            get
            {
                return new[] { HouseholdBill.StartDate, Contract.StartDate.Value }.Max();
            }
        }

        /// <summary>
        /// Gets the date this bill spans to. 
        /// </summary>
        public DateTimeOffset ToDate
        {
            get
            {
                return new[] { HouseholdBill.EndDate, Contract.EndDate }.Min();
            }
        }

        protected UtilityBill() { }

        /// <summary>
        /// Creates a new utility bill. 
        /// </summary>
        /// <param name="contract">The contract this utility bill was generated for. </param>
        /// <param name="houseBill">The monthly household bill </param>
        public UtilityBill(Contract contract, HouseholdBill houseBill, Dictionary<ServiceType, decimal> pricePerWeek)
        {
            if (contract == null) throw new ArgumentNullException(nameof(contract));
            if (houseBill == null) throw new ArgumentNullException(nameof(houseBill));


            Contract = contract;
            HouseholdBill = houseBill;
            PaymentDate = HouseholdBill.PaymentDate;

            //calculate the bill amount
            //var estdModel = House.GetServiceEstdModel(contract.ServiceNew);
            //if (estdModel != null)
            //    Ammount = decimal.Round(estdModel.EstimateMoneyz(FromDate, ToDate), 2, MidpointRounding.AwayFromZero);
            //else
            //{
                Ammount = decimal.Round(pricePerWeek[contract.ServiceNew] * DurationDays / 7, 2, MidpointRounding.AwayFromZero);
            //}
        }
            

        public override string ToString()
        {
            return string.Format("{0} Bill #{1}", " ", Id);
        }

        public int DurationDays
        {
            get
            {
                if (!Contract.StartDate.HasValue)
                {
                    return 0;
                }
                return (int)Math.Round((ToDate - FromDate).TotalDays);
            }
        }
    }

}