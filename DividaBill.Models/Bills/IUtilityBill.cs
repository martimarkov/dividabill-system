﻿using DividaBill.Models;
using System;
namespace DividaBill.Models
{
    public interface IUtilityBill
    {
        decimal Ammount { get; }
        //DateTime EndDate { get; set; }
        //HouseModel House { get; set; }
        //int House_ID { get; set; }
        int Id { get; set; }
        string Note { get; set; }
        DateTime PaymentDate { get; }

        Contract Contract { get; }

        HouseModel House { get; }

        DateTime StartDate { get; }

        bool IsPaid { get; }
        //BillStatus Status { get; set; }
        //BillType Type { get; set; }
    }
}
