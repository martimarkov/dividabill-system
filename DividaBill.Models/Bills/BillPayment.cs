﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using DividaBill.Library;

namespace DividaBill.Models
{
    //[TrackChanges]
    public class BillPayment
    {
        #region ID fields
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Index("IX_Payment_UserBill", IsUnique =true, Order = 1)]
        public int Bill_Id { get; set; }

        [Index("IX_Payment_UserBill", IsUnique =true, Order = 2)]
        public string User_Id { get; set; }
        #endregion

        [ForeignKey(nameof(Bill_Id))]
        public virtual UtilityBill Bill { get; private set; }

        [ForeignKey(nameof(User_Id))]
        public virtual User PayingUser { get; private set; }


        /// <summary>
        /// Gets the percentage of the bill this payment is responsible for as a value between 0 and 100. 
        /// </summary>
        //[Range(0, 100), DataType(DataType.Currency)]
        public decimal PercentageOfBill { get; private set; }

        public decimal Amount { get; set; }

        public bool IsPaid { get; private set; } = false;
        public string PaymentRef { get; set; }

        public DateTime? CreatedAt { get; set; }
        public DateTime? ChargeDate { get; set; }

        public DateTime? PaymentRequestedDate { get; set; }

        public BillPayment(User tenant, UtilityBill bill, HouseholdPercentage housePerc)
        {
            housePerc.ConsistencyCheck();

            var usrPerc = housePerc.UserPercentages
                .SingleOrDefault(up => up.User == tenant);
            var totalAmount = bill.Ammount;

            if (usrPerc == null)
                throw new Exception("Creating a BillPayment for user {0} who is not in the latest HouseholdPercentage!".F(tenant));

            Bill = bill;
            PayingUser = tenant;

            var perc = usrPerc.PercentageOfBill;
            Amount = decimal.Round(totalAmount * perc, 2, MidpointRounding.AwayFromZero);
            PercentageOfBill = 100 * perc;
            CreatedAt = DateTime.UtcNow;
        }

        public BillPayment(User tenant, UtilityBill bill, int nTenants)
        {
            var totalAmount = bill.Ammount;

            Bill = bill;
            PayingUser = tenant;
            var perc = 1m / nTenants;
            Amount = decimal.Round(totalAmount * perc, 2, MidpointRounding.AwayFromZero);
            PercentageOfBill = 100 * perc;
        }

        protected BillPayment() { }


        public void MarkAsPaid()
        {
            //if (IsPaid)
                //throw new Exception("BillPayment is already paid");
            IsPaid = true;
        }
    }
}
