using System;

namespace DividaBill.Models.Abstractions
{
    public interface IChangeableEntity
    {
        DateTime? CreatedAt { get; set; }
        DateTime? LastModified { get; set; }
    }
}