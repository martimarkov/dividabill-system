﻿namespace DividaBill.Models.Abstractions
{
    public interface IEstimatedPrice
    {
        decimal BroadbandADSL { get; set; }
        decimal BroadbandADSLMargin { get; set; }
        decimal BroadbandADSLRouter { get; set; }
        decimal BroadbandADSLRouterMargin { get; set; }
        decimal BroadbandFible38 { get; set; }
        decimal BroadbandFible38Margin { get; set; }
        decimal BroadbandFible38Router { get; set; }
        decimal BroadbandFible76 { get; set; }
        decimal BroadbandFibre40RouterMargin { get; set; }
        decimal Electricity { get; set; }
        decimal ElectricityStandingCharge { get; set; }
        decimal Gas { get; set; }
        decimal GasStandingCharge { get; set; }
        decimal LandlinePhoneBasic { get; set; }
        decimal LandlinePhoneBasicMargin { get; set; }
        decimal LandlinePhoneMedium { get; set; }
        decimal LandlinePhoneMediumMargin { get; set; }
        decimal LineRental { get; set; }
        decimal LineRentalInstallation { get; set; }
        decimal LineRentalMargin { get; set; }
        decimal NetflixTV { get; set; }
        decimal SkyTV { get; set; }
        decimal SkyTVInstallation { get; set; }
        decimal SkyTVInstallationMargin { get; set; }
        decimal SkyTVMargin { get; set; }
        decimal SkyTVSatellite { get; set; }
        decimal SkyTVSatelliteMargin { get; set; }
        decimal SkyTVSTB { get; set; }
        decimal SkyTVSTBMargin { get; set; }
        decimal TVLicense { get; set; }
        decimal Water { get; set; }
        decimal WaterStandingCharge { get; set; }
    }
}
