﻿using System.ComponentModel.DataAnnotations;

namespace DividaBill.Models.Abstractions
{
    public interface INamedEntity
    {
        [MaxLength(128)]
        string Name { get; set; }

        [MaxLength(256)]
        string Description { get; set; }
    }
}