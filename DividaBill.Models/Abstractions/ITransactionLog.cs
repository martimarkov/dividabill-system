namespace DividaBill.Models.Abstractions
{
    public interface ITransactionLog
    {
        string Changes { get; set; }
    }
}