﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DividaBill.Models.Abstractions
{
    public interface IIdentityEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        int ID { get; set; }
    }

    public interface IIdentityEntity2
    {
        string Id { get; set; }
    }
}