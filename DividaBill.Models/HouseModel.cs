﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;
using DelegateDecompiler;
using DividaBill.Library;
using DividaBill.Models.Abstractions;
using DividaBill.Models.Bills;

namespace DividaBill.Models
{
    public class HouseModel : IIdentityEntity, IEntityModel
    {
        public HouseModel()
        {
        }

        public int? Coupon_Id { get; set; }

        public int Housemates { get; set; }

        public Address Address { get; set; }

        [DataType(DataType.MultilineText)]
        public string Note { get; set; }

        [ForeignKey("Coupon_Id")]
        public virtual Coupon Coupon { get; set; }

        public virtual List<User> Tenants { get; set; }

        public virtual IList<HouseholdBill> HouseholdBills { get; set; } = new List<HouseholdBill>();

        public virtual IList<Contract> Contracts { get; set; } = new List<Contract>();

        /// <summary>
        ///     Gets the requested end date for this house.
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        ///     Gets all payment allocations for all services this house went through.
        /// </summary>
        public virtual List<HouseholdPercentage> PaymentAllocations { get; set; }

        /// <summary>
        ///     Gets all estimation models for all services.
        /// </summary>
        public virtual List<HouseholdServiceEstimate> ServiceEstimates { get; set; }

        /// <summary>
        ///     The starting date that was requested by the tenants.
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        ///     The date this house was registered at.
        /// </summary>
        public DateTime RegisteredDate { get; set; }

        /// <summary>
        ///     Gets the date on which the first contract of this household actually starts.
        /// </summary>
        public DateTimeOffset? ActualStartDate
        {
            get
            {
                return Contracts
                    .Where(c => c.StartDate.HasValue)
                    .OrderBy(c => c.StartDate)
                    .Select(c => c.StartDate)
                    .FirstOrDefault();
            }
        }

        public string houseDisplay
        {
            get
            {
                return "HouseID: " + ID + " - Address: " + Address.Line1 + " - Town: " + Address.City + " - Postcode: " +
                       Address.Postcode;
            }
        }

        public virtual ICollection<Tenancy> Tenancies { get; set; }

        public int TenancyId { get; set; }

        [NotMapped]
        public Building Building { get; set; }

        [Key]
        public int ID { get; set; }

        /// <summary>
        ///     Gets whether this household was active (i.e. has at least one active service) at the specified date.
        /// </summary>
        /// <param name="now">The date to check for. </param>
        /// <returns></returns>
        public bool IsActive(DateTime now)
        {
            return StartDate <= now && now < EndDate;
        }

        /// <summary>
        ///     Gets the currently active contract of this household for the given service.
        ///     Returns null if there is no such contract.
        /// </summary>
        public Contract GetActiveContract(ServiceType ty)
        {
            return GetActiveContract(ty, DateTime.Now);
        }

        /// <summary>
        ///     Gets the currently active or pending contract of this household for the given service.
        ///     Returns null if there is no such contract.
        /// </summary>
        public Contract GetRequestedContract(ServiceType ty)
        {
            return GetRequestedContract(ty, DateTime.Now);
        }

        /// <summary>
        ///     Gets the currently active contract of this household for the given service on the specified date.
        ///     Returns null if there is no such contract.
        /// </summary>
        public Contract GetActiveContract(ServiceType ty, DateTime dt)
        {
            return Contracts
                .Where(c => c.IsActive(dt))
                .Where(c => c.ServiceNew == ty)
                .SingleOrDefault();
        }

        /// <summary>
        ///     Gets the currently requested contract of this household for the given service on the specified date.
        ///     Returns null if there is no such contract.
        /// </summary>
        public Contract GetRequestedContract(ServiceType ty, DateTime dt)
        {
            return Contracts?
                .Where(c => c.IsRequested(dt))
                .Where(c => c.ServiceNew.Id == ty.Id)
                .SingleOrDefault();
        }

        public IEnumerable<Contract> GetRequestedContracts()
        {
            return Contracts
                .Where(c => c.IsRequested(DateTime.Today));
        }

        /// <summary>
        ///     Gets the contract for the given service which starts between the specified dates.
        ///     Returns null if there is no such contract.
        /// </summary>
        public Contract GetContractStartingBetween(ServiceType service, DateTime from, DateTime to)
        {
            return Contracts
                .Where(c => c.RequestedStartDate >= from && c.RequestedStartDate < to)
                .Where(c => c.ServiceNew_ID == service.Id)
                .FirstOrDefault();
        }

        /// <summary>
        ///     Gets the current price estimation model for the specified service type.
        /// </summary>
        /// <param name="ty">The service type to get the price model for. </param>
        /// <returns></returns>
        public HouseholdServiceEstimate GetServiceEstdModel(ServiceType ty)
        {
            return ServiceEstimates.SingleOrDefault(e => e.Service == ty);
        }

        /// <summary>
        ///     Gets the currently active contract of this household for the given service.
        ///     Returns null if there is no such contract.
        /// </summary>
        public T GetActiveContract<T>()
            where T : Contract
        {
            return GetActiveContract<T>(DateTime.Now);
        }

        /// <summary>
        ///     Gets the currently active contract of this household for the given service at the specified date.
        ///     Returns null if there is no such contract.
        /// </summary>
        public T GetActiveContract<T>(DateTime dt)
            where T : Contract
        {
            return (T) Contracts
                .Where(c => c.IsActive(dt))
                .Where(c => c is T)
                .SingleOrDefault();
        }

        public T GetRequestedContract<T>()
            where T : Contract
        {
            return Contracts
                .Where(c => c.IsRequested(DateTime.Now))
                .OfType<T>()
                .ArgMax(c => c.RequestedStartDate.DateTime);
        }

        #region TODO: DELETE

        [Obsolete("Electricity property is deprecated, please use Electricity Contract status instead.")]
        public bool Electricity { get; set; }

        [Obsolete("Gas property is deprecated, please use Gas Contract status instead.")]
        public bool Gas { get; set; }

        [Obsolete("Water property is deprecated, please use Water Contract status instead.")]
        public bool Water { get; set; }

        [Obsolete("Media property is being removed.")]
        public bool Media { get; set; }

        [Obsolete("Broadband property is deprecated, please use Broadband Contract status instead.")]
        public bool Broadband { get; set; }

        [Obsolete("LandlinePhone property is deprecated, please use LandlinePhone Contract status instead.")]
        public bool LandlinePhone { get; set; }

        [Obsolete("SkyTV property is deprecated, please use SkyTV Contract status instead.")]
        public bool SkyTV { get; set; }

        [Obsolete("TVLicense property is deprecated, please use TVLicense Contract status instead.")]
        public bool TVLicense { get; set; }

        [Obsolete("NetflixTV property is deprecated, please use NetflixTV Contract status instead.")]
        public bool NetflixTV { get; set; }

        [Obsolete("BroadbandType property is deprecated, please use BroadbandType Contract type instead.")]
        public BroadbandType BroadbandType { get; set; }

        [Obsolete("LandlineType property is deprecated, please use LandlineType Contract type instead.")]
        public LandlineType LandlineType { get; set; }

        #endregion

        #region Services Getters

        /// <summary>
        ///     Gets whether the specified service is currently active.
        /// </summary>
        public bool HasService(ServiceType ty)
        {
            return GetActiveContract(ty) != null;
        }

        /// <summary>
        ///     Gets whether the specified service is requested.
        /// </summary>
        public bool WantsService(ServiceType ty)
        {
            return GetRequestedContract(ty) != null;
        }

        //[NotMapped]
        public bool WantsElectricity
        {
            get { return WantsService(ServiceType.Electricity); }
        }

        [NotMapped]
        public bool WantsGas
        {
            get { return WantsService(ServiceType.Gas); }
        }

        [NotMapped]
        public bool WantsWater
        {
            get { return WantsService(ServiceType.Water); }
        }

        [NotMapped]
        public bool WantsBroadband
        {
            get { return WantsService(ServiceType.Broadband); }
        }

        [NotMapped]
        public bool WantsLandlinePhone
        {
            get { return WantsService(ServiceType.LandlinePhone); }
        }

        [NotMapped]
        public bool WantsSkyTV
        {
            get { return WantsService(ServiceType.SkyTV); }
        }

        [NotMapped]
        public bool WantsTVLicense
        {
            get { return WantsService(ServiceType.TVLicense); }
        }

        [NotMapped]
        public bool WantsNetflixTV
        {
            get { return WantsService(ServiceType.NetflixTV); }
        }

        [Computed]
        public bool HasElectricity
        {
            get { return HasService(ServiceType.Electricity); }
        }

        public Expression<Func<HouseModel, bool>> HasContract(ServiceType ty)
        {
            return p => p.IsActive(DateTime.Now) && p.Contracts.Any(x => x.ServiceNew_ID == ty.Id);
        }

        public bool HasGas
        {
            get { return HasService(ServiceType.Gas); }
        }

        public bool HasWater
        {
            get { return HasService(ServiceType.Water); }
        }

        public bool HasBroadband
        {
            get { return HasService(ServiceType.Broadband); }
        }
        #endregion
    }
}