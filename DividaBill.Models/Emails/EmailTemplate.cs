using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DividaBill.Models.Abstractions;

namespace DividaBill.Models.Emails
{
    public class EmailTemplate : IIdentityEntity, IChangeableEntity, IUsedForPeriodEntity, IEntityModel
    {
        [MaxLength(36)]
        public string TemplateId { get; set; }

        [MaxLength(36)]
        public string VersionId { get; set; }

        [MaxLength(100)]
        public string Name { get; set; }

        [Column(TypeName = "text")]
        public string HtmlContent { get; set; }

        [Column(TypeName = "text")]
        public string PlainContent { get; set; }

        [MaxLength(500)]
        public string Subject { get; set; }

        public int EmailProviderId { get; set; }

        [ForeignKey(nameof(EmailProviderId))]
        public virtual EmailProvider EmailProvider { get; set; }

        public bool Enabled { get; set; }

        public DateTime? CreatedAt { get; set; }
        public DateTime? LastModified { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public DateTime UsedFrom { get; set; }
        public DateTime? UsedTo { get; set; }
    }
}