using System;
using System.ComponentModel.DataAnnotations;
using DividaBill.Models.Abstractions;

namespace DividaBill.Models.Emails
{
    public class EmailType : IIdentityEntity, INamedEntity, IEquatable<EmailType>
    {
        public static readonly EmailType EmptyType = new EmailType {ID = 0, Name = "EmptyType", Description = "This type is not in DB, so Messages can not have this type on Saving in DB"};
        public static readonly EmailType SignUpMessages = new EmailType {ID = 1, Name = "SignUpMessages", Description = "Email messages in SignUp process"};
        public static readonly EmailType BillRunMessages = new EmailType {ID = 2, Name = "BillRunMessages", Description = "Email messages in Bill running process"};

        public static EmailType[] All =
        {
            SignUpMessages,
            BillRunMessages
        };

        public bool Equals(EmailType other)
        {
            if (other == null)
                return false;

            return ID == other.ID;
        }

        public int ID { get; set; }

        [MaxLength(128)]
        public string Name { get; set; }

        [MaxLength(256)]
        public string Description { get; set; }

        public override string ToString()
        {
            return Name;
        }

        public static implicit operator int(EmailType ty)
        {
            return ty.ID;
        }

        public static implicit operator EmailType(int id)
        {
            if (id <= 0 || id > All.Length)
                throw new ArgumentOutOfRangeException(nameof(id));
            return All[id - 1];
        }

        public override bool Equals(object obj)
        {
            if (!(obj is EmailType))
                return false;
            return ((EmailType) obj).ID == ID;
        }

        public override int GetHashCode()
        {
            return ID;
        }

        public static bool operator ==(EmailType a, EmailType b)
        {
            if ((object) a == null)
                return (object) b == null;
            return a.Equals(b);
        }

        public static bool operator !=(EmailType a, EmailType b)
        {
            return !(a == b);
        }
    }
}