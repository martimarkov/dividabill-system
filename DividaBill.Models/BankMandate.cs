﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DividaBill.Models.Abstractions;

namespace DividaBill.Models
{
    public class BankMandate : IEntityModel, IIdentityEntity2, IChangeableEntity
    {
        public string created_at { get; set; }
        public string status { get; set; }
        public string next_possible_charge_date { get; set; }
        public string reference { get; set; }

        [Required]
        public string scheme { get; set; }

        [ForeignKey("BankAccount")]
        public string BankAccount_ID { get; set; }

        public virtual BankAccount BankAccount { get; set; }

        [ForeignKey("User")]
        public string User_ID { get; set; }

        public virtual User User { get; set; }

        public DateTime? CreatedAt { get; set; }
        public DateTime? LastModified { get; set; }

        [Key]
        public string Id { get; set; }
    }
}