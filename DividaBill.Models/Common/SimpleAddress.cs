﻿namespace DividaBill.Models.Common
{
    public class SimpleAddress
    {
        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public string Line3 { get; set; }
        public string Line4 { get; set; }
        public string City { get; set; }
        public string Postcode { get; set; }
        public string County { get; set; }

        public string UDPRN { get; set; }

        public override string ToString()
        {
            return Line1 + ", "
                   + (Line2 == "" ? Line2 + ", " : "")
                   + (Line3 == "" ? Line3 + ", " : "")
                   + (Line4 == "" ? Line4 + ", " : "")
                   + City + ", " + County + ", " + Postcode;
        }
    }
}