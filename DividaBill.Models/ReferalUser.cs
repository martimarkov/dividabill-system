﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace DividaBill.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ReferalUser : IdentityUser
    {
        public DateTime LastActivityDate
        {

            get
            {
                return this.lastActivityDate.HasValue
                   ? this.lastActivityDate.Value
                   : DateTime.Now;
            }

            set { this.lastActivityDate = value; }
        }

        private DateTime? lastActivityDate = null;

        public DateTime SignupDate { get; set; }

        [DataType(DataType.MultilineText)]
        public String Note { get; set; }


        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ReferalUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
}