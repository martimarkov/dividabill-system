﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DividaBill.Models.GoCardless
{
    public class GCMandate : NewMandate
    {
        public string id { get; set; }
        public string created_at { get; set; }
        public string status { get; set; }
        public string next_possible_charge_date { get; set; }
    }
    public class NewMandate
    {
        public NewMandate()
        {
            metadata = new Dictionary<string, string>();
            links = new Dictionary<string, string>();
        }
        public string reference { get; set; }
        [Required]
        public string scheme { get; set; }
        public Dictionary<string, string> metadata { get; set; }
        public Dictionary<string, string> links { get; set; }

    }
}
