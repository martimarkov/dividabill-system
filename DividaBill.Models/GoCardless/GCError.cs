namespace DividaBill.Models.GoCardless
{
    public class GCError
    {
        public string field { get; set; }
        public string message { get; set; }
        public string request_pointer { get; set; }
    }
}