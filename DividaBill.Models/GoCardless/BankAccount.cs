﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DividaBill.Models.GoCardless
{
    public class GCExistingBankAccount
    {
        public GCExistingBankAccount()
        {
            links = new Dictionary<string, string>();
            metadata = new Dictionary<string, string>();
        }

        public string id { get; set; }
        public string created_at { get; set; }
        public string account_number_ending { get; set; }
        public string account_holder_name { get; set; }
        public string bank_name { get; set; }
        public string currency { get; set; }
        public string country_code { get; set; }
        public bool enabled { get; set; }
        public virtual Dictionary<string, string> metadata { get; set; }
        public virtual Dictionary<string, string> links { get; set; }
    }

    public class GCBankAccount : NewBankAccount
    {
        public string id { get; set; }
        public string created_at { get; set; }
        public bool enabled { get; set; }
    }

    public class NewBankAccount
    {
        public NewBankAccount()
        {
            links = new Dictionary<string, string>();
            metadata = new Dictionary<string, string>();
        }

        [Required]
        public string account_number { get; set; }

        [Required]
        public string branch_code { get; set; }

        [Required]
        public string country_code { get; set; }

        [Required]
        public string currency { get; set; }

        [Required]
        public string account_holder_name { get; set; }

        public virtual Dictionary<string, string> metadata { get; set; }
        public virtual Dictionary<string, string> links { get; set; }
    }
}