using System.Collections.Generic;

namespace DividaBill.Models.GoCardless
{
    public class GCErrors
    {
        public string message { get; set; }
        public List<GCError> errors { get; set; }
        public string documentation_url { get; set; }
        public string type { get; set; }
        public string request_id { get; set; }
        public int code { get; set; }
    }
}