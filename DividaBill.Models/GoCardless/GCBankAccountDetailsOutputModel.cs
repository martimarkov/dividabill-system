﻿using System.Collections.Generic;

namespace DividaBill.Models.GoCardless
{
    public class BankAccountDetailsLookupsOutputModel
    {
        public List<string> available_debit_schemes { get; set; }
        public string bank_name { get; set; }
        public string bic { get; set; }
    }

    public class GCBankAccountDetailsOutputModel
    {
        public BankAccountDetailsLookupsOutputModel bank_details_lookups { get; set; }
    }
}