namespace DividaBill.Models.GoCardless
{
    public class BankAccountDetailsInputModel
    {
        public string account_number { get; set; }
        public string branch_code { get; set; }
        public string country_code { get; set; }
    }

    public class GCBankAccountDetailsInputModel
    {
        public BankAccountDetailsInputModel bank_details_lookups { get; set; }
    }
}