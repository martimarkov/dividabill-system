﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using DividaBill.Library;
using DividaBill.Models.Abstractions;

namespace DividaBill.Models
{
    public enum ServiceTypeOld
    {
        elec = 1, gas = 2,
    }

    public abstract class Contract : IEntityModel
    {
        protected Contract() { }

        public Contract(ContractRequest req)
        {
            if (!req.Provider.Type.Any(t => req.Service == t))
                throw new InvalidOperationException("Unable to assign provider {0} for service {1}!".F(req.Provider.Name, req.Service));

            RequestedBy = req.TriggeringTenant;
            House = req.TriggeringTenant.House;
            ServiceNew_ID = req.Service.Id;
            Provider = req.Provider;
            RequestedStartDate = req.RequestedStartDate;
            EndDate = RequestedStartDate.AddContractLength(req.Duration);
            PackageRaw = req.PackageRaw;

            StartDate = req.StartDate;
            SubmissionStatus = ProviderSubmissionStatus.New;
            Length = req.Duration;
        }
        #region IDs
        [Key]
        public int ID { get; set; }

        //[Index("ID_HouseContractType", IsUnique = true, Order = 1)]
        public int House_ID { get; set; }

        //[Index("ID_HouseContractType", IsUnique = true, Order = 2)]
        public int ServiceNew_ID { get; set; }

        public string RequestedBy_ID { get; set; }
        public int Provider_ID { get; set; }
        #endregion

        #region TO REMOVE
        public ServiceTypeOld Service { get; set; }

        /// <summary>
        /// Gets or sets the tariff of the contract as determined by its duration. 
        /// </summary>
        public ContractLength Length { get; set; }
        #endregion

        [ForeignKey(nameof(ServiceNew_ID))]
        public virtual ServiceType ServiceNew { get; private set; }

        [ForeignKey(nameof(House_ID))]
        public virtual HouseModel House { get; private set; }

        [ForeignKey(nameof(RequestedBy_ID))]
        public User RequestedBy { get; private set; }

        [ForeignKey(nameof(Provider_ID))]
        public Provider Provider { get; private set; }

        public int PackageRaw { get; set; }

        public virtual IList<IUtilityBill> Bills { get; private set; } = new List<IUtilityBill>();

        public ProviderSubmissionStatus SubmissionStatus { get; private set; } = ProviderSubmissionStatus.New;
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        /// <summary>
        /// The requested start date. 
        /// </summary>
        public DateTimeOffset RequestedStartDate { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        /// <summary>
        /// The confirmed date this contract is starting. 
        /// </summary>
        public DateTimeOffset? StartDate { get; protected set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTimeOffset EndDate { get; set; }

        /// <summary>
        /// Gets the latest payment allocation for this contract
        /// that was agreed upon by all housemates, if any. 
        /// </summary>
        public HouseholdPercentage CurrentAllocation
        {
            get
            {
                return House.PaymentAllocations
                 .Where(pa => pa.Service == ServiceNew && pa.IsApproved)
                 .OrderByDescending(pa => pa.ApprovedDate)
                 .FirstOrDefault();
            }
        }

        public bool IsSubmitted
        {
            get { return SubmissionStatus == ProviderSubmissionStatus.Submitted; }
        }

        /// <summary>
        /// Gets whether this contract has a CONFIRMED, FINAL start date. 
        /// </summary>
        public bool IsConfirmed
        {
            get { return StartDate.HasValue; }
        }

        /// <summary>
        /// Gets whether this contract was active at the specified date and time.
        /// </summary>
        /// <param name="dt">The date to check for. </param>
        public bool IsActive(DateTime dt)
        {
            return StartDate <= dt && dt < EndDate && IsConfirmed && IsSubmitted;
        }
        
        /// <summary>
        /// Gets whether this contract was wanted at the specified date and time. 
        /// </summary>
        /// <param name="dt">The date to check for. </param>
        /// <returns></returns>
        public bool IsRequested(DateTime dt)
        {
            return dt < EndDate;
        }

        public void MarkAsSubmitted()
        {
            if (IsSubmitted)
                throw new Exception("This contract was already submitted!");

            SubmissionStatus = ProviderSubmissionStatus.Submitted;
        }

        public void SetAsActive(DateTimeOffset? dt)
        {
            //if (StartDate.HasValue)
                //throw new Exception("StartDate has already been set for this contract");
           // else
           // {
                //if (dt.HasValue)
                    StartDate = dt;
           // }
                
        }
    }

}
