﻿using System;

namespace DividaBill.Models
{
    public class ContractRequest
    {
        public User TriggeringTenant { get; set; }

        public ServiceType Service { get; set; }

        public DateTimeOffset? StartDate { get; set; }
        public DateTimeOffset RequestedStartDate { get; set; }

        public Provider Provider { get; set; }

        public ContractLength Duration { get; set; }

        public int PackageRaw { get; set; }
    }
}