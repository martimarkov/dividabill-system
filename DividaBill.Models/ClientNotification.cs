﻿using DividaBill.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DividaBill.Models
{
    public enum ClientNotificationPriority
    {
        High = 3,
        Medium = 2,
        Low = 1,
        None = 0
    }
    public enum ClientNotificationType
    {
        Approval = 1,
        Bill = 2,
        SystemNotification = 3,
    }
    public class ClientNotification
    {
        [Key]
        public int ID { get; set; }
        public String User_ID { get; set; }
        public virtual User User { get; set; }
        public DateTimeOffset Created { get; set; }
        public ClientNotificationPriority Priority { get; set; }
        public ClientNotificationType Type { get; set; }
        public bool Seen { get; set; }
        public bool Responded { get; set; }
        public String Content { get; set; }
        public String Extra { get; set; }
        public bool Actionalable { get; set; }
        public String Sender_ID { get; set; }
        public virtual User Sender { get; set; }
    }
}
