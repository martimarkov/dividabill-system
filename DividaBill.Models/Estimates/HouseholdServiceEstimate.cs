﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DividaBill.Models
{
    /// <summary>
    /// Contains all data needed to estimate the prices for a given household's service. 
    /// 
    /// Can be overridden on behalf of specific services to create service-specific models. 
    /// </summary>
    public class HouseholdServiceEstimate
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        //[Index("IX_Estimates_HouseService", IsUnique = true, Order = 1)]
        public int House_Id { get; set; }

        //[Index("IX_Estimates_HouseService", IsUnique = true, Order = 2)]
        public int Service_Id { get; set; }


        [ForeignKey(nameof(House_Id))]
        public virtual HouseModel House { get; set; }

        [ForeignKey(nameof(Service_Id))]
        public virtual ServiceType Service { get; set; }


        public double Mean { get; set; }

        public double Variance { get; set; }


        public decimal EstimateMoneyz(DateTimeOffset from, DateTimeOffset to)
        {
            throw new NotImplementedException();
        }

    }
}
