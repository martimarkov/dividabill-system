﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DividaBill.Library;

namespace DividaBill.Models
{
    //[TrackChanges]
    public class HouseholdPercentage
    {
        #region IDs
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        #endregion

        public HouseModel House { get; private set; }

        /// <summary>
        /// Gets the user who created this percentage allocation. 
        /// </summary>
        public User TriggerUser
        {
            get
            {
                return UserPercentages
                     .Where(pc => pc.DecisionDate.HasValue)
                     .ArgMin(pc => pc.DecisionDate.Value.DateTime)
                     .User;
            }
        }

        public ServiceType Service { get; private set; }

        public virtual IList<UserPercentage> UserPercentages { get; private set; }

        /// <summary>
        /// Gets the date this household percentage was created (and thus proposed). 
        /// </summary>
        public DateTimeOffset CreatedDate
        {
            get
            {
                return UserPercentages
                 .Where(pc => pc.DecisionDate.HasValue)
                 .Min(pc => pc.DecisionDate.Value);
            }
        }

        /// <summary>
        /// Gets the date this allocation was approved by all of the household tenants. 
        /// </summary>
        public DateTimeOffset? ApprovedDate
        {
            get
            {
                if (!IsApproved)
                    return null;

                return UserPercentages
                 .Max(pc => pc.DecisionDate.Value);
            }
        }

        /// <summary>
        /// Checks if all 
        /// </summary>
        internal void ConsistencyCheck()
        {
            var totalPerc = UserPercentages.Sum(p => p.PercentageOfBill);
            if (totalPerc != 1)
                throw new Exception("Household allocation {0} for household #{1} has an invalid percentage allocation! (sum={2})".F(Id, House.ID, totalPerc));
        }

        /// <summary>
        /// Gets whether this non-pending HouseholdPercentage is approved by all tenants. 
        /// Returns false if some tenants are still yet to decide. 
        /// </summary>
        public bool IsApproved
        {
            get { return !IsPending && UserPercentages.All(pc => pc.IsApproved); }
        }

        /// <summary>
        /// Gets whether all tenants have taken a decision about this payment allocation. 
        /// </summary>
        public bool IsPending
        {
            get { return UserPercentages.Any(pc => !pc.HasMadeDecision); }
        }

        private HouseholdPercentage() { }

        /// <summary>
        /// Creates a new household allocation as suggested by the specified user 
        /// with the specified amounts for each user in the house. 
        /// allocations. 
        /// </summary>
        public HouseholdPercentage(User triggerUser, ServiceType service, Dictionary<User, decimal> newPrices)
        {
            this.House = triggerUser.House;
            this.Service = service;

            //check if all users from the household have a percentage
            doHouseTenantCheck(newPrices);

            //check if all percentages sum to 1 !!
            doPercentageCheck(newPrices);

            throw new NotImplementedException();
            //TODO: create user percentages
            //TODO: send out an e-mail
        }

        void doHouseTenantCheck(Dictionary<User, decimal> prices)
        {
            // House must have all its members registered
            if (House.Housemates != House.Tenants.Count)
                throw new Exception("House wants to have {0} people, but there are {1} people in it!".F(House.Housemates, House.Tenants.Count));

            //users should be the same count
            var areEqual = new HashSet<User>(prices.Keys).SetEquals(House.Tenants);
            if (!areEqual)
                throw new InvalidOperationException("A household percentage's new prices must be defined for all of the household tenants!");
        }

        void doPercentageCheck(Dictionary<User, decimal> prices)
        {
            if(prices.Values.Sum() != 1.0m)
                throw new InvalidOperationException("A household percentage's new prices must sum to 1.0!");
        }
    }
}
