﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DividaBill.Models
{
    public class PostCode
    {
        [Key]
        [Index(IsUnique = true)]
        [MaxLength(15)]
        public String Postcode { get; set; }
        [Index(IsClustered = true)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusteredID { get; set; }
        public String Latitude { get; set; }
        public String Longitude { get; set; }
        public String Easting { get; set; }
        public String Northing { get; set; }
        public String GridRef { get; set; }
        public String County { get; set; }
        public String District { get; set; }
        public String Ward { get; set; }
        public String DistrictCode { get; set; }
        public String WardCode { get; set; }
        public String Country { get; set; }
        public String CountyCode { get; set; }
        public String Constituency { get; set; }
        public String Introduced { get; set; }
        public String Terminated { get; set; }
        public String Parish { get; set; }
        public String NationalPark { get; set; }
        public String Population { get; set; }
        public String Households { get; set; }
        public String Built_up_area { get; set; }
        public String Built_up_sub_division { get; set; }
        public String Lower_layer_super_output_area { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTimeOffset? Added { get; set; }

    }
}
