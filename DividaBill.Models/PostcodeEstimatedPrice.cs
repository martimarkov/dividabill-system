﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DividaBill.Models
{
    public class PostcodeEstimatedPrice : EstimatedPrice
    {
        [Key]
        [Index(IsUnique = true)]
        public String PostCode_ID { get; set; }

        [ForeignKey("PostCode_ID")]
        public virtual PostCode Postcode { get; set; }
    }

}