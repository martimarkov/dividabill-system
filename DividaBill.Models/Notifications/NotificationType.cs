﻿using System;

namespace DividaBill.Models.Notifications
{
    public enum NotificationType
    {
        BillPayment,
        PriceAllocation,
        ServiceChange,
        TenantJoined,
        OtherTransfer,
    }
}