﻿using System;

namespace DividaBill.Models.Notifications
{
    public enum NotificationStatus
    {
        Open,
        Complete,
        Failed,
    }
}