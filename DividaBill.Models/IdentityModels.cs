﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using System.Threading.Tasks;
using DividaBill.Models.Abstractions;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DividaBill.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class User : IdentityUser, IEntityModel
    {
        private DateTime? lastActivityDate;

        public DateTime SignupDate { get; set; }

        public int? House_ID { get; set; }

        [ForeignKey("House_ID")]
        public virtual HouseModel House { get; set; }

        public string RefTitle { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        [MinLength(8), MaxLength(100)]
        public string AccountNumber { get; set; }

        [MinLength(6), MaxLength(100)]
        public string AccountSortCode { get; set; }

        public string AccountHolder { get; set; }
        public bool AccountSoleHolder { set; get; }


        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DOB { get; set; }

        public Address HomeAddress { get; set; }

        [DataType(DataType.MultilineText)]
        public string Note { get; set; }

        public bool NewsletterSubscription { get; set; }

        public string DDRef { get; set; }
        public string BankAccountID { get; set; }

        public string GoCardlessCustomerID { get; set; }

        public string MandateID { get; set; }

        public string FullName
        {
            get { return RefTitle + " " + FirstName + " " + LastName; }
        }

        public DateTime LastActivityDate
        {
            get
            {
                return lastActivityDate.HasValue
                    ? lastActivityDate.Value
                    : DateTime.Now;
            }

            set { lastActivityDate = value; }
        }

        //TODO: make the following two properties user-specific
        public DateTime StartDate
        {
            get { return House.StartDate; }
        }

        public DateTime EndDate
        {
            get { return House.EndDate; }
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
}